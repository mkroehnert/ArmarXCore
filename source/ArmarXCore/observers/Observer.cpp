/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke@kit.edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "Observer.h"
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/observers/exceptions/local/InvalidChannelException.h>
#include <ArmarXCore/observers/exceptions/local/InvalidDataFieldException.h>
#include <ArmarXCore/observers/exceptions/local/InvalidCheckException.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/observers/exceptions/user/InvalidDatafieldException.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

using namespace armarx;
using namespace armarx::exceptions::local;

const std::string LAST_REFRESH_DELTA_CHANNEL = "_LastRefreshDelta";


// *******************************************************
// implementation of ObserverInterface
// *******************************************************
CheckIdentifier Observer::installCheck(const CheckConfiguration& configuration, const Ice::Current& c)
{
    // create condition check
    ConditionCheckPtr check;
    {
        boost::mutex::scoped_lock lock_checks(checksMutex);
        boost::recursive_mutex::scoped_lock lock_channels(channelsMutex);
        check = createCheck(configuration);
    }
    ARMARX_CHECK_EXPRESSION(check);
    // insert into registry
    CheckIdentifier identifier;
    {
        boost::recursive_mutex::scoped_lock lock_conditions(channelsMutex);
        identifier = registerCheck(check);
    }

    ARMARX_DEBUG << "Installed check " << identifier.uniqueId << flush;

    // perform initial check
    {
        boost::recursive_mutex::scoped_lock lock_channels(channelsMutex);
        ARMARX_CHECK_EXPRESSION(configuration.dataFieldIdentifier);
        // retrieve channel
        ChannelRegistryEntry channel = channelRegistry[configuration.dataFieldIdentifier->channelName];

        // evaluate
        evaluateCheck(check, channel);
    }

    return identifier;
}

void Observer::removeCheck(const CheckIdentifier& id, const Ice::Current& c)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    // find channel
    ChannelRegistry::iterator iter = channelRegistry.find(id.channelName);

    if (iter == channelRegistry.end())
    {
        return;
    }

    // find condition
    ConditionCheckRegistry::iterator iterCheck = iter->second.conditionChecks.find(id.uniqueId);

    // remove condition
    if (iterCheck != iter->second.conditionChecks.end())
    {
        iter->second.conditionChecks.erase(iterCheck);
    }

    ARMARX_DEBUG << "Removed check " << id.uniqueId << flush;
}

TimedVariantBasePtr Observer::getDataField(const DataFieldIdentifierBasePtr& identifier, const Ice::Current& c) const
{
    return getDatafieldByName(identifier->channelName, identifier->datafieldName);
}

TimedVariantBasePtr Observer::getDatafieldByName(const std::string& channelName, const std::string& datafieldName, const Ice::Current& c) const
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    auto it = channelRegistry.find(channelName);
    if (it == channelRegistry.end())
    {
        throw exceptions::user::InvalidChannelException(channelName);
    }

    auto itDF = it->second.dataFields.find(datafieldName);
    if (itDF == it->second.dataFields.end())
    {
        throw exceptions::user::InvalidDataFieldException(channelName, datafieldName);
    }

    //    VariantPtr var = VariantPtr::dynamicCast(channelRegistry[identifier->channelName].dataFields[identifier->datafieldName].value);
    TimedVariantPtr tv = TimedVariantPtr::dynamicCast(itDF->second.value);
    if (!tv)
    {
        ARMARX_IMPORTANT << "could not cast timed variant: " <<  itDF->second.value->ice_id();
    }
    return tv;
}


TimedVariantBaseList Observer::getDataFields(const DataFieldIdentifierBaseList& identifiers, const Ice::Current& c)
{
    TimedVariantBaseList result;

    DataFieldIdentifierBaseList::const_iterator iter = identifiers.begin();

    while (iter != identifiers.end())
    {
        result.push_back(getDataField(*iter, c));
        iter++;
    }

    return result;
}

ChannelRegistry Observer::getAvailableChannels(bool includeMetaChannels, const Ice::Current&)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    if (!includeMetaChannels)
    {
        ChannelRegistry result;
        result = channelRegistry;
        result.erase(LAST_REFRESH_DELTA_CHANNEL);
        return result;
    }
    else
    {
        return channelRegistry;
    }
}

StringConditionCheckMap Observer::getAvailableChecks(const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(checksMutex);

    return availableChecks;
}

// *******************************************************
// offering of channels, datafield, and checks
// *******************************************************
void Observer::offerChannel(std::string channelName, std::string description)
{
    if (getState() < eManagedIceObjectInitialized)
    {
        throw LocalException() << "offerChannel() must not be called before the Observer is initalized (i.e. not in onInitObserver(), use onConnectObserver()";
    }

    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    if (channelRegistry.find(channelName) != channelRegistry.end())
    {
        throw exceptions::user::InvalidChannelException(channelName);
    }

    ChannelRegistryEntry channel;
    channel.name = channelName;
    channel.description = description;
    channel.initialized = false;

    std::pair<std::string, ChannelRegistryEntry> entry;
    entry.first = channelName;
    entry.second = channel;

    channelRegistry.insert(entry);
}

void Observer::offerDataFieldWithDefault(std::string channelName, std::string datafieldName, const Variant& defaultValue, std::string description)
{
    if (getState() < eManagedIceObjectInitialized)
    {
        throw LocalException() << "offerDataFieldWithDefault() must not be called before the Observer is initalized (i.e. not in onInitObserver(), use onConnectObserver()";
    }

    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    auto channelIt = channelRegistry.find(channelName);

    if (channelIt == channelRegistry.end())
    {
        throw exceptions::user::InvalidChannelException(channelName);
    }

    if (channelIt->second.dataFields.find(datafieldName) != channelIt->second.dataFields.end())
    {
        throw exceptions::user::DatafieldExistsAlreadyException(channelName, datafieldName);
    }

    DataFieldRegistryEntry dataField;
    dataField.identifier = new DatafieldRef(this, channelName, datafieldName, false);
    dataField.description = description;
    dataField.typeName = defaultValue.getTypeName();
    dataField.value = new TimedVariant();
    *dataField.value = defaultValue;

    std::pair<std::string, DataFieldRegistryEntry> entry;
    entry.first = datafieldName;
    entry.second = dataField;

    channelIt->second.dataFields.insert(entry);
}

void Observer::offerDataField(std::string channelName, std::string datafieldName, VariantTypeId type, std::string description)
{
    if (getState() < eManagedIceObjectInitialized)
    {
        throw LocalException() << "offerDataField() must not be called before the Observer is initalized (i.e. not in onInitObserver(), use onConnectObserver()";
    }

    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    auto channelIt = channelRegistry.find(channelName);

    if (channelIt == channelRegistry.end())
    {
        throw exceptions::user::InvalidChannelException(channelName);
    }

    if (channelIt->second.dataFields.find(datafieldName) != channelIt->second.dataFields.end())
    {
        throw exceptions::user::InvalidDataFieldException(channelName, datafieldName);
    }

    DataFieldRegistryEntry dataField;
    dataField.identifier = new DatafieldRef(this, channelName, datafieldName, false);
    dataField.description = description;
    dataField.typeName = Variant::typeToString(type);
    dataField.value = new TimedVariant();
    dataField.value->setType(type);

    std::pair<std::string, DataFieldRegistryEntry> entry;
    entry.first = datafieldName;
    entry.second = dataField;

    channelRegistry[channelName].dataFields.insert(entry);
}

bool Observer::offerOrUpdateDataField(std::string channelName, std::string datafieldName, const Variant& value, const std::string& description)
{
    if (!existsDataField(channelName, datafieldName))
    {
        offerDataFieldWithDefault(channelName, datafieldName, value, description);
        return true;
    }
    else
    {
        setDataField(channelName, datafieldName, value);
        return false;
    }
}

void Observer::offerConditionCheck(std::string checkName, ConditionCheck* conditionCheck)
{
    boost::mutex::scoped_lock lock(checksMutex);

    if (availableChecks.find(checkName) != availableChecks.end())
    {
        throw InvalidCheckException(checkName);
    }

    std::pair<std::string, ConditionCheckPtr> entry;
    entry.first = checkName;
    entry.second = conditionCheck;

    availableChecks.insert(entry);
}

void Observer::removeChannel(std::string channelName)
{
    {
        boost::recursive_mutex::scoped_lock lock(channelsMutex);

        ChannelRegistry::iterator iter = channelRegistry.find(channelName);

        if (iter == channelRegistry.end())
        {
            return;
        }

        DataFieldIdentifierPtr id = new DataFieldIdentifier(getName(), LAST_REFRESH_DELTA_CHANNEL, channelName);
        removeDatafield(id);
        channelRegistry.erase(iter);
        ARMARX_INFO << "Removed channel " << channelName;
    }

    {
        ScopedRecursiveLock lock(historyMutex);
        channelHistory.erase(channelName);
    }

}

void Observer::removeDatafield(DataFieldIdentifierBasePtr id)
{
    {
        ScopedRecursiveLock lock(filterMutex);
        // Remove filter
        DataFieldIdentifierPtr idptr = DataFieldIdentifierPtr::dynamicCast(id);
        std::string idStr = idptr->getIdentifierStr();
        auto itFilter = filteredToOriginal.find(idStr);

        if (itFilter != filteredToOriginal.end())
        {
            DatafieldRefPtr refPtr = DatafieldRefPtr::dynamicCast(itFilter->second->original);
            auto range = orignalToFiltered.equal_range(refPtr->getDataFieldIdentifier()->getIdentifierStr());

            for (auto it = range.first; it != range.second; it++)
                if (it->second->filtered->getDataFieldIdentifier()->getIdentifierStr()
                    == idStr)
                {
                    orignalToFiltered.erase(it);
                    break;
                }

            filteredToOriginal.erase(itFilter);
        }
    }

    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    ChannelRegistry::iterator itChannel = channelRegistry.find(id->channelName);

    if (itChannel == channelRegistry.end())
    {
        return;
    }

    itChannel->second.dataFields.erase(id->datafieldName);
}

bool Observer::existsChannel(const std::string& channelName, const Ice::Current& c) const
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);
    return channelRegistry.find(channelName) != channelRegistry.end();
}

bool Observer::existsDataField(const std::string& channelName, const std::string& datafieldName, const Ice::Current& c) const
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);
    ChannelRegistry::const_iterator itChannel = channelRegistry.find(channelName);

    if (itChannel == channelRegistry.end())
    {
        return false;
    }

    return itChannel->second.dataFields.find(datafieldName) != itChannel->second.dataFields.end();
}

// *******************************************************
// utility methods for sensordatalistener
// *******************************************************
std::set<std::string> Observer::updateDatafieldFilter(const std::string& channelName, const std::string& datafieldName, const VariantBasePtr& value)
{
    std::vector<FilterQueueData> filterData;
    std::set<std::string> foundFilterFields;
    {
        ScopedRecursiveLock lock(filterMutex);
        const std::string id = getName() + "." + channelName + "." + datafieldName;
        //    DatafieldRefPtr ref = new DatafieldRef(this, channelName, datafieldName);
        auto range = orignalToFiltered.equal_range(id);

        //IceUtil::Time start = IceUtil::Time::now();
        //bool found = false;
        TimedVariantPtr origTV = TimedVariantPtr::dynamicCast(value);
        auto t = origTV ? origTV->getTime() : TimeUtil::GetTime();
        long tLong = t.toMicroSeconds();

        for (auto it = range.first; it != range.second; it++)
        {
            it->second->filter->update(tLong, value);

            foundFilterFields.insert(it->second->filtered->datafieldName);

            TimedVariantPtr tv = new TimedVariant(VariantPtr::dynamicCast(it->second->filter->getValue()), t);
            filterData.emplace_back(FilterQueueData {tv, it->second->filtered->channelRef->channelName, it->second->filtered->datafieldName});
            //found = true;
        }
    }

    for (auto& elem : filterData)
    {
        setDataFieldFlatCopy(elem.channelName, elem.datafieldName, VariantPtr::dynamicCast(elem.value));
    }

    /*if(found)
    {
        IceUtil::Time end = IceUtil::Time::now();
        IceUtil::Time duration = end - start;
        ARMARX_IMPORTANT << deactivateSpam(0.1f) << channelName << ":" << datafieldName << ": all filters calc microseconds: " << duration.toMicroSeconds();
    }*/

    return foundFilterFields;
}

void Observer::scheduleDatafieldFilterUpdate(const std::string& channelName, const std::string& datafieldName, const VariantBasePtr& value)
{
    if (orignalToFiltered.size() == 0)
    {
        return; // no filters installed anyway ...nothing todo
    }
    const std::string id = getName() + "." + channelName + "." + datafieldName;
    {
        ScopedRecursiveLock lock(filterMutex);
        if (orignalToFiltered.count(id) == 0)
        {
            return;    // no filter for this datafield installed ...nothing todo
        }
    }
    Mutex::scoped_lock lock(filterQueueMutex);
    filterQueue[id] = {value, channelName, datafieldName};
    idleCondition.notify_all();
}

void Observer::updateFilters()
{
    while (!filterUpdateTask->isStopped())
    {
        FilterUpdateQueue queue;
        {
            Mutex::scoped_lock lock(filterQueueMutex);
            queue.swap(filterQueue);
        }
        boost::unordered_map<std::string, std::set<std::string> > channels;
        for (const FilterUpdateQueue::value_type& elem : queue)
        {
            auto foundFilterFields = updateDatafieldFilter(elem.second.channelName,
                                     elem.second.datafieldName,
                                     elem.second.value);
            channels[elem.second.channelName].insert(foundFilterFields.begin(), foundFilterFields.end());
        }
        for (const auto& channel : channels)
        {
            if (channel.second.size() > 0)
            {
                updateChannel(channel.first, channel.second);
            }
        }
        Mutex::scoped_lock lock(filterQueueMutex);
        if (filterQueue.size() == 0)
        {
            idleCondition.wait(lock);
        }
    }
}

void Observer::setDataFieldFlatCopy(const DataFieldRegistry::iterator& dataFieldIter, const VariantPtr& value)
{
    ARMARX_CHECK_EXPRESSION_W_HINT(value, "Datafieldvariant is NULL!");
    TimedVariantPtr tval = TimedVariantPtr::dynamicCast(value);
    if (tval)
    {
        dataFieldIter->second.value = tval;
    }
    else
    {
        dataFieldIter->second.value = new TimedVariant(value, TimeUtil::GetTime());
    }
}

void Observer::setDataField(const std::string& channelName, const std::string& datafieldName, const Variant& value, bool triggerFilterUpdate)
{
    VariantBasePtr valuePtr;
    {
        boost::recursive_mutex::scoped_lock lock(channelsMutex);
        ChannelRegistry::iterator itChannel = channelRegistry.find(channelName);

        if (itChannel == channelRegistry.end())
        {
            throw exceptions::user::InvalidChannelException(channelName);
        }

        DataFieldRegistry::iterator itDF = itChannel->second.dataFields.find(datafieldName);

        if (itDF == itChannel->second.dataFields.end())
        {
            throw exceptions::user::InvalidDataFieldException(channelName, datafieldName);
        }

        if (dynamic_cast<const TimedVariant*>(&value))
        {
            itDF->second.value = value.clone();
        }
        else
        {
            itDF->second.value = new TimedVariant(value, TimeUtil::GetTime());
        }
        valuePtr = itDF->second.value;
    }
    if (triggerFilterUpdate)
    {
        scheduleDatafieldFilterUpdate(channelName, datafieldName, valuePtr);
    }
    //*dataFieldValue = value;
}

void Observer::setDataFieldFlatCopy(const std::string& channelName, const std::string& datafieldName, const VariantPtr& value, bool triggerFilterUpdate)
{
    {
        boost::recursive_mutex::scoped_lock lock(channelsMutex);

        ChannelRegistry::iterator itChannel = channelRegistry.find(channelName);

        if (itChannel == channelRegistry.end())
        {
            throw exceptions::user::InvalidChannelException(channelName);
        }

        DataFieldRegistry::iterator itDF = itChannel->second.dataFields.find(datafieldName);

        if (itDF == itChannel->second.dataFields.end())
        {
            throw exceptions::user::InvalidDataFieldException(channelName, datafieldName);
        }

        setDataFieldFlatCopy(itDF, value);
    }

    if (triggerFilterUpdate)
    {
        scheduleDatafieldFilterUpdate(channelName, datafieldName, value);
    }
}

void Observer::updateDatafieldTimestamps(const std::string& channelName, const::boost::unordered_map<std::string, Ice::Long>& datafieldValues)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    ChannelRegistry::iterator itChannel = channelRegistry.find(channelName);

    if (itChannel == channelRegistry.end())
    {
        throw exceptions::user::InvalidChannelException(channelName);
    }

    for (const auto& elem : datafieldValues)
    {
        DataFieldRegistry::iterator itDF = itChannel->second.dataFields.find(elem.first);

        if (itDF == itChannel->second.dataFields.end())
        {
            throw exceptions::user::InvalidDataFieldException(channelName, elem.first);
        }


        TimedVariantPtr oldValue = TimedVariantPtr::dynamicCast(itDF->second.value);
        if (!oldValue || (elem.second > oldValue->getTimestamp() && oldValue->getInitialized()))
        {
            itDF->second.value = new TimedVariant(VariantPtr::dynamicCast(itDF->second.value), IceUtil::Time::microSeconds(elem.second));
        }
    }
}

void Observer::updateDatafieldTimestamps(const std::string& channelName, Ice::Long timestamp)
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);

    ChannelRegistry::iterator itChannel = channelRegistry.find(channelName);

    if (itChannel == channelRegistry.end())
    {
        throw exceptions::user::InvalidChannelException(channelName);
    }

    for (auto& elem : itChannel->second.dataFields)
    {

        TimedVariantPtr oldValue = TimedVariantPtr::dynamicCast(elem.second.value);
        if (!oldValue || (timestamp > oldValue->getTimestamp() && oldValue->getInitialized()))
        {
            elem.second.value = new TimedVariant(VariantPtr::dynamicCast(elem.second.value), IceUtil::Time::microSeconds(timestamp));
        }
    }
}


void Observer::setDataFieldsFlatCopy(const std::string& channelName, const ::boost::unordered_map< ::std::string, ::armarx::VariantBasePtr>& datafieldValues, bool triggerFilterUpdate)
{
    {
        boost::recursive_mutex::scoped_lock lock(channelsMutex);

        ChannelRegistry::iterator itChannel = channelRegistry.find(channelName);

        if (itChannel == channelRegistry.end())
        {
            throw exceptions::user::InvalidChannelException(channelName);
        }

        for (const auto& elem : datafieldValues)
        {
            DataFieldRegistry::iterator itDF = itChannel->second.dataFields.find(elem.first);

            if (itDF == itChannel->second.dataFields.end())
            {
                throw exceptions::user::InvalidDataFieldException(channelName, elem.first);
            }

            setDataFieldFlatCopy(itDF, VariantPtr::dynamicCast(elem.second));
        }
    }
    if (triggerFilterUpdate)
    {
        for (const auto& elem : datafieldValues)
        {
            scheduleDatafieldFilterUpdate(channelName, elem.first, elem.second);
        }
    }
}

void Observer::setDataFieldsFlatCopy(const std::string& channelName, const StringVariantBaseMap& datafieldValues, bool triggerFilterUpdate)
{
    {
        boost::recursive_mutex::scoped_lock lock(channelsMutex);

        ChannelRegistry::iterator itChannel = channelRegistry.find(channelName);

        if (itChannel == channelRegistry.end())
        {
            throw exceptions::user::InvalidChannelException(channelName);
        }

        for (const auto& elem : datafieldValues)
        {
            DataFieldRegistry::iterator itDF = itChannel->second.dataFields.find(elem.first);

            if (itDF == itChannel->second.dataFields.end())
            {
                throw exceptions::user::InvalidDataFieldException(channelName, elem.first);
            }
            ARMARX_CHECK_EXPRESSION_W_HINT(elem.second, "Datafieldname: " << elem.first);
            setDataFieldFlatCopy(itDF, VariantPtr::dynamicCast(elem.second));
        }
    }

    if (triggerFilterUpdate)
    {
        for (const auto& elem : datafieldValues)
        {
            scheduleDatafieldFilterUpdate(channelName, elem.first, elem.second);
        }
    }
}

void Observer::updateRefreshRateChannel(const std::string& channelName)
{
    auto& oldUpdateTime = channelUpdateTimestamps[channelName];
    auto now = IceUtil::Time::now();

    try
    {
        setDataFieldFlatCopy(LAST_REFRESH_DELTA_CHANNEL, channelName, new Variant((now - oldUpdateTime).toMilliSecondsDouble()), false);
    }
    catch (exceptions::user::InvalidDataFieldException& e)
    {
        offerDataFieldWithDefault(LAST_REFRESH_DELTA_CHANNEL, channelName, 0.0, "Update delta of channel '" + channelName + "'");
    }
    oldUpdateTime = now;
}

void Observer::updateChannel(const std::string& channelName, const std::set<std::string>& updatedDatafields)
{
    if (!existsChannel(channelName))
    {
        throw exceptions::user::InvalidChannelException(channelName);
    }
    ScopedLock lock(channelQueueMutex);
    std::set<std::string>& dfs = channelQueue[channelName];
    dfs.insert(updatedDatafields.begin(), updatedDatafields.end());
    idleChannelCondition.notify_all();
}

void Observer::channelUpdateFunction()
{

    while (!channelUpdateTask->isStopped())
    {
        std::map<std::string, std::set<std::string> > queue;
        {
            Mutex::scoped_lock lock(channelQueueMutex);
            queue.swap(channelQueue);
        }
        for (const auto& elem : queue)
        {
            doChannelUpdate(elem.first, elem.second);
        }
        Mutex::scoped_lock lock(channelQueueMutex);
        if (channelQueue.size() == 0)
        {
            idleChannelCondition.wait(lock);
        }
    }
}
void Observer::addToChannelHistory(const std::pair<IceUtil::Time, ChannelRegistryEntry>& historyEntry, const std::string& channelName)
{
    if (maxHistorySize == 0)
    {
        return;
    }
    boost::recursive_mutex::scoped_lock lockHistory(historyMutex);
    auto historyIt = channelHistory.find(channelName);
    if (historyIt == channelHistory.end())
    {
        channelHistory[channelName].set_capacity(maxHistorySize);
        historyIt = channelHistory.find(channelName);
    }
    auto now = TimeUtil::GetTime();
    if (historyIt->second.empty() || now > historyIt->second.rbegin()->first + IceUtil::Time::secondsDouble(1.0 / maxHistoryRecordFrequency))
    {
        historyIt->second.push_back(historyEntry);
    }
}

void Observer::doChannelUpdate(const std::string& channelName, const std::set<std::string>& updatedDatafields)
{


    try
    {
        ChannelRegistryEntry entry;
        std::pair<IceUtil::Time, ChannelRegistryEntry> historyEntry;
        {
            boost::recursive_mutex::scoped_lock lock_channels(channelsMutex);
            // check if channels exists
            ChannelRegistry::iterator iterChannel =  channelRegistry.find(channelName);

            if (iterChannel == channelRegistry.end())
            {
                throw exceptions::user::InvalidChannelException(channelName);
            }



            if (logChannelUpdateRate)
            {
                updateRefreshRateChannel(channelName);
            }

            // update initialized state
            const DataFieldRegistry& dataFields = iterChannel->second.dataFields;
            DataFieldRegistry::const_iterator iterDataFields = dataFields.begin();
            bool channelInitialized = true;

            while (iterDataFields != dataFields.end())
            {
                channelInitialized &= iterDataFields->second.value->getInitialized();
                iterDataFields++;
            }

            iterChannel->second.initialized = channelInitialized;
            if (iterChannel->second.conditionChecks.size() > 0)
            {
                entry = iterChannel->second;
            }
            historyEntry = std::make_pair(TimeUtil::GetTime(), iterChannel->second);

        }
        addToChannelHistory(historyEntry, channelName);
        // evaluate checks
        ConditionCheckRegistry::iterator iterChecks = entry.conditionChecks.begin();

        while (iterChecks != entry.conditionChecks.end())
        {
            bool found = false;
            if (updatedDatafields.size() > 0)
            {
                // check if this check belongs to an updated datafield.
                for (const auto& elem : entry.dataFields)
                {
                    const DataFieldRegistryEntry& datafieldEntry = elem.second;
                    if (updatedDatafields.count(datafieldEntry.identifier->datafieldName))
                    {
                        found = true;
                        break;
                    }
                }
            }
            else
            {
                found = true;
            }

            try
            {
                if (found)
                {
                    evaluateCheck(ConditionCheckPtr::dynamicCast(iterChecks->second), entry);
                }
            }
            catch (...)
            {
                ARMARX_ERROR << "Evaluating condition for channel " << channelName << " failed!";
                handleExceptions();
            }

            iterChecks++;
        }
    }
    catch (...)
    {
        //        ARMARX_ERROR << "Upating channel " << channelName << " failed!";
        //        handleExceptions();
    }
}

// *******************************************************
// Component hooks
// *******************************************************
void Observer::onInitComponent()
{
    maxHistorySize = getProperty<int>("MaxHistorySize");
    maxHistoryRecordFrequency = getProperty<float>("MaxHistoryRecordFrequency");

    {
        boost::mutex::scoped_lock lock(idMutex);
        currentId = 0;
    }

    onInitObserver();
    filterUpdateTask = new RunningTask<Observer>(this, &Observer::updateFilters, "Filter update task");
    filterUpdateTask->start();
    channelUpdateTask = new RunningTask<Observer>(this, &Observer::channelUpdateFunction, "Channel update task");
    channelUpdateTask->start();
}

void Observer::onConnectComponent()
{
    logChannelUpdateRate = getProperty<bool>("CreateUpdateFrequenciesChannel").getValue();
    if (logChannelUpdateRate && !existsChannel(LAST_REFRESH_DELTA_CHANNEL))
    {
        offerChannel(LAST_REFRESH_DELTA_CHANNEL, "Metachannel with the last channel update deltas of all channels in milliseconds");
    }
    channelHistory.clear();
    // subclass init
    onConnectObserver();
    //    offerDataFieldWithDefault(channelName, LAST_REFRESH_DELTA_CHANNEL, 0.0, );
    //    channelUpdateTimestamps[channelName] = IceUtil::Time::now();

    boost::recursive_mutex::scoped_lock lock(channelsMutex);
    auto proxy = ObserverInterfacePrx::checkedCast(getProxy());
    // update the proxy in all existing datafield refs
    for (auto& channel : channelRegistry)
    {
        ChannelRegistryEntry& c = channel.second;
        for (auto& df : c.dataFields)
        {
            DataFieldRegistryEntry& d = df.second;
            d.identifier->channelRef->observerProxy = proxy;
        }
    }
    metaTask = new PeriodicTask<Observer>(this, &Observer::metaUpdateTask, 50);
    if (logChannelUpdateRate)
    {
        metaTask->start();
    }
}

void Observer::onExitComponent()
{
    onExitObserver();
    if (metaTask)
    {
        metaTask->stop();
    }
    if (filterUpdateTask)
    {
        filterUpdateTask->stop(false);
    }
    idleCondition.notify_all();
    if (filterUpdateTask)
    {
        filterUpdateTask->stop(true);
    }
    if (channelUpdateTask)
    {
        channelUpdateTask->stop(false);
    }
    idleChannelCondition.notify_all();
    if (channelUpdateTask)
    {
        channelUpdateTask->stop(true);
    }
    channelHistory = ChannelRegistryHistory();

}

PropertyDefinitionsPtr Observer::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ObserverPropertyDefinitions(
                                      getConfigIdentifier()));
}

void Observer::metaUpdateTask()
{
    updateChannel(LAST_REFRESH_DELTA_CHANNEL);
}



// *******************************************************
// private methods
// *******************************************************
int Observer::generateId()
{
    boost::mutex::scoped_lock lock(idMutex);

    return currentId++;
}


ConditionCheckPtr Observer::createCheck(const CheckConfiguration& configuration) const
{
    std::string checkName = configuration.checkName;

    // create check from elementary condition
    StringConditionCheckMap::const_iterator iterChecks = availableChecks.find(checkName);

    if (iterChecks == availableChecks.end())
    {
        std::string reason = "Invalid condition check \"" + checkName + "\" for observer \"" + getName() + "\".";
        throw InvalidConditionException(reason.c_str());
    }

    ARMARX_CHECK_EXPRESSION(iterChecks->second);
    ConditionCheckPtr check = ConditionCheckPtr::dynamicCast(iterChecks->second)->createInstance(configuration, channelRegistry);

    return check;
}

CheckIdentifier Observer::registerCheck(const ConditionCheckPtr& check)
{
    ARMARX_CHECK_EXPRESSION(check);
    ARMARX_CHECK_EXPRESSION(check->configuration.dataFieldIdentifier);
    // create identifier
    int id = generateId();
    CheckIdentifier identifier;
    identifier.uniqueId = id;
    identifier.channelName = check->configuration.dataFieldIdentifier->channelName;
    identifier.observerName = getName();

    // add to conditions list
    std::pair<int, ConditionCheckPtr> entry;
    entry.first = id;
    entry.second = check;
    channelRegistry[check->configuration.dataFieldIdentifier->channelName].conditionChecks.insert(entry);

    return identifier;
}

void Observer::evaluateCheck(const ConditionCheckPtr& check, const ChannelRegistryEntry& channel) const
{
    check->evaluateCondition(channel.dataFields);
}


DatafieldRefBasePtr Observer::createFilteredDatafield(const DatafieldFilterBasePtr& filter, const DatafieldRefBasePtr& datafieldRef, const Ice::Current& c)
{

    //    if( auto it = orignalToFiltered.find(datafieldRef) != orignalToFiltered.end())
    //    {
    //        return it->second.filtered;
    //    }
    ARMARX_CHECK_EXPRESSION(datafieldRef);


    std::string filteredName = datafieldRef->datafieldName + "_" + filter->ice_id();



    int i = 1;

    while (existsDataField(datafieldRef->channelRef->channelName, filteredName))
    {
        //        ARMARX_IMPORTANT << "Checking if datafield " << filteredName << " exists";
        filteredName = datafieldRef->datafieldName + "_" + filter->ice_id() + "_" + ValueToString(i);
        i++;
    }

    return createNamedFilteredDatafield(filteredName, filter, datafieldRef);
}

DatafieldRefBasePtr Observer::createNamedFilteredDatafield(const std::string& filterDatafieldName, const DatafieldFilterBasePtr& filter, const DatafieldRefBasePtr& datafieldRef, const Ice::Current& c)
{

    DatafieldRefPtr ref = DatafieldRefPtr::dynamicCast(datafieldRef);
    ARMARX_CHECK_EXPRESSION(ref);
    ARMARX_CHECK_EXPRESSION(filter);
    if (!filter->checkTypeSupport(ref->getDataField()->getType()))
    {
        auto types = filter->getSupportedTypes();
        std::string suppTypes = "supported types";

        for (auto t : types)
        {
            suppTypes += Variant::typeToString(t) + ", ";
        }

        ARMARX_WARNING << suppTypes;
        throw exceptions::user::UnsupportedTypeException(ref->getDataField()->getType());
    }

    // if filter with that name exists -> remove it
    if (existsDataField(datafieldRef->channelRef->channelName, filterDatafieldName))
    {
        DatafieldRefPtr filteredRef = new DatafieldRef(ref->getChannelRef(), filterDatafieldName);
        removeFilteredDatafield(filteredRef);

    }

    // calculate initial value
    auto var = ref->getDataField();
    filter->update(var->getTime().toMicroSeconds(), var);

    // create datafield for new filter
    offerDataFieldWithDefault(datafieldRef->channelRef->channelName, filterDatafieldName, *VariantPtr::dynamicCast(filter->getValue()), "Filtered value of " + ref->getDataFieldIdentifier()->getIdentifierStr());

    //create and store the refs for the filters
    ChannelRefPtr channel = ChannelRefPtr::dynamicCast(ref->channelRef);
    channel->refetchChannel();
    DatafieldRefPtr filteredRef = new DatafieldRef(ref->getChannelRef(), filterDatafieldName);
    FilterDataPtr data = new FilterData;
    data->filter = filter;
    data->original = ref;
    data->filtered = filteredRef;
    ScopedRecursiveLock lock(filterMutex);
    orignalToFiltered.insert(std::make_pair(ref->getDataFieldIdentifier()->getIdentifierStr(), data));
    filteredToOriginal[filteredRef->getDataFieldIdentifier()->getIdentifierStr()] = data;
    return filteredRef;

}




void Observer::removeFilteredDatafield(const DatafieldRefBasePtr& datafieldRef, const Ice::Current&)
{
    bool remove = true;
    DatafieldRefPtr ref;
    {
        ScopedRecursiveLock lock(filterMutex);
        ref = DatafieldRefPtr::dynamicCast(datafieldRef);
        const std::string idStr = ref->getDataFieldIdentifier()->getIdentifierStr();
        auto it = filteredToOriginal.find(idStr);
        remove = (it != filteredToOriginal.end());
    }

    if (remove && ref)
    {
        removeDatafield(ref->getDataFieldIdentifier());
    }
}

ChannelHistory Observer::getChannelHistory(const std::string& channelName, Ice::Float timestepMs, const Ice::Current& c) const
{
    return getPartialChannelHistory(channelName, 0, std::numeric_limits<long>::max(), timestepMs,  c);
}

ChannelHistory Observer::getPartialChannelHistory(const std::string& channelName, Ice::Long startTimestamp, Ice::Long endTimestamp, Ice::Float timestepMs, const Ice::Current&) const
{
    ARMARX_IMPORTANT << "Waiting for mutex";
    boost::recursive_mutex::scoped_lock lock_channels(historyMutex);
    ARMARX_IMPORTANT << "GOT mutex";
    ChannelHistory result;
    auto historyIt = channelHistory.find(channelName);
    if (historyIt == channelHistory.end())
    {
        return result;
    }
    ARMARX_IMPORTANT << "found channel";

    auto lastInsertIt = result.begin();
    Ice::Long lastUsedTimestep = 0;
    float timestepUs =  timestepMs * 1000;
    for (auto& entry : historyIt->second)
    {
        long timestamp = entry.first.toMicroSeconds();
        if (timestamp > endTimestamp)
        {
            break;
        }

        if (timestamp > startTimestamp && timestamp - lastUsedTimestep >  timestepUs)
        {
            lastInsertIt = result.emplace_hint(result.end(), std::make_pair(timestamp, entry.second.dataFields));
            lastUsedTimestep = timestamp;
        }
    }
    return result;
}

TimedVariantBaseList Observer::getDatafieldHistory(const std::string& channelName, const std::string& datafieldName, Ice::Float timestepMs, const Ice::Current& c) const
{
    return getPartialDatafieldHistory(channelName, datafieldName, 0, std::numeric_limits<long>::max(), timestepMs,  c);
}

TimedVariantBaseList Observer::getPartialDatafieldHistory(const std::string& channelName, const std::string& datafieldName, Ice::Long startTimestamp, Ice::Long endTimestamp, Ice::Float timestepMs, const Ice::Current&) const
{
    boost::recursive_mutex::scoped_lock lock_channels(historyMutex);
    TimedVariantBaseList result;
    auto historyIt = channelHistory.find(channelName);
    if (historyIt == channelHistory.end())
    {
        return result;
    }
    if (!existsDataField(channelName, datafieldName))
    {
        return result;
    }

    Ice::Long lastUsedTimestep = 0;
    float timestepUs =  timestepMs * 1000;
    for (auto& entry : historyIt->second)
    {
        long timestamp = entry.first.toMicroSeconds();
        if (timestamp > endTimestamp)
        {
            break;
        }

        if (timestamp > startTimestamp && timestamp - lastUsedTimestep >  timestepUs)
        {
            auto datafieldIt = entry.second.dataFields.find(datafieldName);
            if (datafieldIt == entry.second.dataFields.end())
            {
                continue;
            }
            else
            {
                TimedVariantPtr tvar = TimedVariantPtr::dynamicCast(datafieldIt->second.value);
                if (tvar)
                {
                    result.emplace_back(tvar);
                    lastUsedTimestep = timestamp;
                }
            }
        }
    }
    return result;
}


std::string Observer::getObserverName(const Ice::Current&) const
{
    return getName();
}

DatafieldRefBasePtr Observer::getDataFieldRef(const DataFieldIdentifierBasePtr& identifier, const Ice::Current&) const
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);
    auto it = channelRegistry.find(identifier->channelName);
    if (it == channelRegistry.end())
    {
        throw exceptions::user::InvalidChannelException(identifier->channelName);
    }

    auto itDF = it->second.dataFields.find(identifier->datafieldName);
    if (itDF == it->second.dataFields.end())
    {
        throw exceptions::user::InvalidDataFieldException(identifier->channelName, identifier->datafieldName);
    }
    //    itDF->second.identifier->channelRef->observerProxy = ObserverInterfacePrx::uncheckedCast(getProxy());
    return DatafieldRefPtr::dynamicCast(itDF->second.identifier);
}

DatafieldRefBasePtr Observer::getDatafieldRefByName(const std::string& channelName, const std::string& datafieldName, const Ice::Current&) const
{
    return getDataFieldRef(new DataFieldIdentifier(getName(), channelName, datafieldName));
}


StringTimedVariantBaseMap Observer::getDatafieldsOfChannel(const std::string& channelName, const Ice::Current&) const
{
    StringTimedVariantBaseMap result;
    boost::recursive_mutex::scoped_lock lock(channelsMutex);
    auto it = channelRegistry.find(channelName);
    if (it == channelRegistry.end())
    {
        throw exceptions::user::InvalidChannelException(channelName);
    }
    DataFieldRegistry fields = it->second.dataFields;
    for (auto& field : fields)
    {
        result[field.first] = TimedVariantBasePtr::dynamicCast(field.second.value);
    }
    return result;
}

ChannelRegistryEntry Observer::getChannel(const std::string& channelName, const Ice::Current& c) const
{
    boost::recursive_mutex::scoped_lock lock(channelsMutex);
    auto it = channelRegistry.find(channelName);
    if (it == channelRegistry.end())
    {
        throw exceptions::user::InvalidChannelException(channelName);
    }
    return it->second;
}
