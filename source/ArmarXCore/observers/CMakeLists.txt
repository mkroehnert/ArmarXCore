armarx_set_target("Core Library: ArmarXCoreObservers")
find_package(Eigen3 QUIET)

armarx_build_if(Eigen3_FOUND "Eigen3 not available")

if(Eigen3_FOUND)
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
endif()

set(LIB_NAME       ArmarXCoreObservers)



set(LIBS ArmarXCoreInterfaces ArmarXCore)

set(LIB_FILES ConditionCheck.cpp
              Observer.cpp
              Event.cpp
              ConditionHandler.cpp
              SystemObserver.cpp
              DebugObserver.cpp
              ProfilerObserver.cpp
              AbstractObjectSerializer.cpp

              ObserverObjectFactories.cpp
              condition/Term.cpp
              condition/TermImpl.cpp
              condition/LiteralImpl.cpp
              condition/Operations.cpp
              condition/ConditionRoot.cpp
              parameter/Parameter.cpp
              parameter/VariantParameter.cpp
              parameter/VariantListParameter.cpp
              variant/Variant.cpp
              variant/TimedVariant.cpp
              variant/VariantInfo.cpp
              variant/VariantContainer.cpp
              #variant/VariantContainerLoader.cpp
              variant/SingleTypeVariantList.cpp
              variant/StringValueMap.cpp
              variant/DataFieldIdentifier.cpp
              variant/ChannelRef.cpp
              variant/DatafieldRef.cpp
              variant/TimestampVariant.cpp
              filters/DatafieldFilter.cpp
              filters/MinMaxFilter.cpp
              filters/DatafieldFilter.cpp
              filters/MedianFilter.cpp
              filters/AverageFilter.cpp
              filters/GaussianFilter.cpp
              filters/ButterworthFilter.cpp
              filters/DerivationFilter.cpp
              test/ExampleUnit.cpp
              test/ExampleUnitObserver.cpp
              test/LoggingExampleComponent.cpp
              )

set(LIB_HEADERS ConditionCheck.h
                Event.h
                ObserverObjectFactories.h
                Observer.h
                ConditionHandler.h
                SystemObserver.h
                DebugObserver.h
                ProfilerObserver.h
                AbstractObjectSerializer.h
                condition/Term.h
                condition/TermImpl.h
                condition/LiteralImpl.h
                condition/Operations.h
                condition/ConditionRoot.h
                parameter/Parameter.h
                parameter/VariantParameter.h
                parameter/VariantListParameter.h
                variant/Variant.h
                variant/TimedVariant.h
                variant/VariantInfo.h
                variant/VariantContainer.h
                #variant/VariantContainerLoader.h
                variant/SingleTypeVariantList.h
                variant/StringValueMap.h
                variant/DataFieldIdentifier.h
                variant/ChannelRef.h
                variant/DatafieldRef.h
                variant/TimestampVariant.h
                filters/MinMaxFilter.h
                filters/DatafieldFilter.h
                filters/MedianFilter.h
                filters/AverageFilter.h
                filters/GaussianFilter.h
                filters/ButterworthFilter.h
                filters/DerivationFilter.h

                exceptions/local/InvalidChannelException.h
                exceptions/local/InvalidCheckException.h
                exceptions/local/InvalidDataFieldException.h
                exceptions/user/InvalidTypeException.h
                exceptions/user/InvalidDatafieldException.h
                exceptions/user/UnknownTypeException.h
                exceptions/user/NotInitializedException.h
                checks/ConditionCheckEquals.h
                checks/ConditionCheckEqualsWithTolerance.h
                checks/ConditionCheckInRange.h
                checks/ConditionCheckLarger.h
                checks/ConditionCheckSmaller.h
                checks/ConditionCheckStringContains.h
                checks/ConditionCheckUpdated.h
                checks/ConditionCheckValid.h
                checks/ConditionCheckChanged.h
                test/ExampleUnit.h
                test/ExampleUnitObserver.h
                test/LoggingExampleComponent.h
                )

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

add_subdirectory(test)
