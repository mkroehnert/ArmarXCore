/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke@kit.edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>

#include <IceUtil/Handle.h>

#include <map>



namespace armarx
{
    class StringValueMap;
    typedef IceInternal::Handle<StringValueMap> StringValueMapPtr;

    /**
     * \class StringValueMap
     * \ingroup VariantsGrp
     * \brief The StringValueMap class is a subclass of VariantContainer and is comparable to a std::map<std::string, T> containing values of type T.
     */
    class ARMARXCORE_IMPORT_EXPORT StringValueMap :
        virtual public VariantContainer,
        virtual public StringValueMapBase
    {
    public:
        StringValueMap(bool forceSingleTypeMap = true);
        StringValueMap(const StringValueMap& source);
        explicit StringValueMap(const ContainerType& subType);
        explicit StringValueMap(VariantTypeId subType);
        StringValueMap& operator=(const StringValueMap& source);
        VariantContainerBasePtr cloneContainer(const Ice::Current& c = ::Ice::Current()) const override;
        Ice::ObjectPtr ice_clone() const override;


        // element manipulation
        void addElement(const std::string& key, const VariantContainerBasePtr& variantContainer, const Ice::Current& c = ::Ice::Current()) override;
        void addVariant(const std::string& key, const Variant& variant);
        void setElement(const std::string& key, const VariantContainerBasePtr& variantContainer, const Ice::Current& c = ::Ice::Current()) override;

        /**
         * @brief setElements adds all pairs from @p map to the current instance using StringValueMap::setElement().
         \code
         armarx::StringValueMapPtr container = ...;
         std::map<std::string, std::string> intMap;
         intMap["p1"] = 1;
         intMap["p2"] = 2;
         container->setElements<int>(intMap);
         \endcode
         */
        template <typename ValueType>
        void setElements(const std::map<std::string, ValueType>& map);

        /**
         * @brief setElements adds elements with @p keyVec as keys and @p values as associated values using StringValueMap::setElement(). If the vectors sizes do not match, only the minimal amount of pairs is added.
         */
        template <typename ValueType>
        void setElements(const std::vector<std::string>& keyVec, const std::vector<ValueType>& values);
        /**
         * @brief clear calls ::clear() on the internal StringValueMap::elements container
         * @param c
         */
        void clear(const Ice::Current& c = ::Ice::Current()) override;

        // getters
        Ice::Int getType(const Ice::Current& c = ::Ice::Current()) const override;

        static VariantTypeId getStaticType(const Ice::Current& c = ::Ice::Current());
        //        VariantTypeId getSubType(const Ice::Current& c = ::Ice::Current()) const;
        int getSize(const Ice::Current& c = ::Ice::Current()) const override;
        bool validateElements(const Ice::Current& c = ::Ice::Current()) override;


        /**
         * @brief getElementBase is the slice-interface implementation for
         * getting an Element and only returns a basepointer, so a manual upcast
         * is usually necessary.
         *
         * This function exists only for completeness and compatibility. Usually
         * you should use the getElement()-function.
         * @param index is the index of the Element in the list
         * @param c Not needed, leave blank.
         * @throw IndexOutOfBoundsException
         * @return a base variant pointer
         */
        VariantContainerBasePtr getElementBase(const std::string& key, const Ice::Current& c = ::Ice::Current()) const override;

        /**
         * @brief getElement is the getter-function to retrieve variants from
         * the list.
         *

         * @param index is the index of the Element in the list
         * @throw IndexOutOfBoundsException
         * @return a variant pointer
         */
        template <typename ContainerType>
        IceInternal::Handle<ContainerType> getElement(const std::string& key) const
        {
            IceInternal::Handle<ContainerType> ptr = IceInternal::Handle<ContainerType>::dynamicCast(getElementBase(key));

            if (!ptr)
            {
                throw InvalidTypeException();
            }

            return ptr;
        }


        /**
         * @brief getVariant returns a pointer to a Variant object associated with \p key
         * @param key
         * @return
         */
        VariantPtr getVariant(const std::string& key) const;




        // Convenience functions
        /**
         * @brief toStdMap creates a std::map<std::string, ValueType> from this container. ValueType must be of a type that can be stored in a Variant.
         */
        template <class ValueType>
        std::map<std::string, ValueType> toStdMap()
        {
            std::map<std::string, ValueType> map;
            StringVariantContainerBaseMap::const_iterator it = elements.begin();

            for (; it != elements.end(); it++)
            {
                const std::string& key = it->first;
                map[key] = getVariant(key)->get<ValueType>();
            }

            return map;
        }

        /**
         * @brief toContainerStdMap creates a std::map<std::string, Type> from this container. Type must be a subclass of VariantContainer. Use this for nested VariantContainers.
         */
        template <typename Type>
        std::map<std::string, Type> toContainerStdMap() const
        {
            std::map<std::string, Type> map;
            StringVariantContainerBaseMap::const_iterator it = elements.begin();

            for (; it != elements.end(); it++)
            {
                const std::string& key = it->first;
                map[key] = getElement<typename Type::element_type>(key);
            }

            return map;
        }

        /**
         * @brief FromStdMap creates a StringValueMap from a std::map<std::string, Type>.
         *
         \code
         std::map<std::string, std::string> intMap;
         intMap["p1"] = 1;
         intMap["p2"] = 2;
         armarx::StringValueMapPtr container = armarx::StringValueMap::FromStdMap<int>(intMap);
         \endcode
         */
        template <typename Type>
        static StringValueMapPtr FromStdMap(const std::map<std::string, Type>& map)
        {
            StringValueMapPtr result = new StringValueMap();

            for (typename std::map<std::string, Type>::const_iterator it = map.begin(); it != map.end(); it++)
            {
                result->addVariant(it->first, it->second);
            }

            return result;
        }

        /**
         * @brief FromStdMap creates a StringValueMap from a std::map<std::string, Type>.
         *
         \code
         std::map<std::string, VariantContainerPtr> containerMap;
         cotnainerMap["c1"] = new VariantContainer();
         containerMap["c2"] = new VariantContainer();
         armarx::StringValueMapPtr container = armarx::StringValueMap::FromContainerStdMap<int>(intMap);
         \endcode
         */
        template <typename Type>
        static StringValueMapPtr FromContainerStdMap(const std::map<std::string, Type>& map)
        {
            StringValueMapPtr result = new StringValueMap();

            for (typename std::map<std::string, Type>::const_iterator it = map.begin(); it != map.end(); it++)
            {
                result->addElement(it->first, it->second);
            }

            return result;
        }


        static std::string getTypePrefix();
    public: // serialization
        void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override;
        void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override;

        // VariantContainerBase interface

        std::string toString(const Ice::Current& c) const override;
    protected:
        bool forceSingleTypeMap;
    };


    namespace VariantType
    {
        const VariantContainerType Map = VariantContainerType(StringValueMap::getTypePrefix());

        const VariantTypeId StringValueMap = Variant::addTypeName(StringValueMap::getTypePrefix());
        inline void suppressWarningUnusedVariableForStringValueMap()
        {
            ARMARX_DEBUG_S << VAROUT(StringValueMap);
        }
    }





    /////////////////////////////////////////////////////////////////////////
    // Implementations
    /////////////////////////////////////////////////////////////////////////
    template <typename ValueType>
    void StringValueMap::setElements(const std::map<std::string, ValueType>& map)
    {
        typename std::map<std::string, ValueType>::const_iterator it = map.begin();

        for (; it != map.end(); it++)
        {
            setElement(it->first, it->second);
        }
    }
    template <typename ValueType>
    void StringValueMap::setElements(const std::vector<std::string>& keyVec, const std::vector<ValueType>& values)
    {

        int size = std::min(keyVec.size(), values.size());

        if (keyVec.size() != values.size())
        {
            ARMARX_WARNING_S << "Size of keys vector does not match values vector size:" << keyVec.size() << " vs. " << values.size();
        }

        for (int i = 0; i < size; i++)
        {
            setElement(keyVec[i], values[i]);
        }
    }

}

