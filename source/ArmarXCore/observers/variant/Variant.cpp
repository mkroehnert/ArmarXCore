/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>
#include <ArmarXCore/observers/exceptions/user/UnknownTypeException.h>
#include <ArmarXCore/observers/exceptions/local/InvalidDataFieldException.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <unordered_map>

template class ::IceInternal::Handle<::armarx::Variant>;

namespace armarx
{
    // template specialization

    template <> Variant::Variant(const int& var, void* t)
    {
        data = new IntVariantData(var);
        typeId = VariantType::Int;
        initialized = true;
    }
    template <> Variant::Variant(const float& var, void* t)
    {
        data = new FloatVariantData(var);
        typeId = VariantType::Float;
        initialized = true;
    }
    template <> Variant::Variant(const double& var, void* t)
    {
        data = new DoubleVariantData(var);
        typeId = VariantType::Double;
        initialized = true;
    }
    template <> Variant::Variant(const bool& var, void* t)
    {
        data = new BoolVariantData(var);
        typeId = VariantType::Bool;
        initialized = true;
    }
    template <> Variant::Variant(const std::string& var, void* t)
    {
        data = new StringVariantData(var);
        typeId = VariantType::String;
        initialized = true;
    }


    template<> bool Variant::get<bool>() const
    {
        return getBool();
    }
    template<> int Variant::get<int>()  const
    {
        return getInt();
    }
    template<> unsigned int Variant::get<unsigned int>()  const
    {
        int i = getInt();
        ARMARX_CHECK_GREATER_EQUAL(i, 0);
        return static_cast<unsigned int>(i);
    }
    template<> float Variant::get<float>() const
    {
        return getFloat();
    }
    template<> double Variant::get<double>() const
    {
        return getDouble();
    }
    template<> std::string Variant::get<std::string>() const
    {
        return getString();
    }

    template<> void Variant::set<bool>(const bool& value)
    {
        setBool(value);
    }
    template<> void Variant::set<int>(const int& value)
    {
        setInt(value);
    }
    template<> void Variant::set<int16_t>(const std::int16_t& value)
    {
        setInt(value);
    }
    template<> void Variant::set<float>(const float& value)
    {
        setFloat(value);
    }
    template<> void Variant::set<double>(const double& value)
    {
        setDouble(value);
    }
    template<> void Variant::set<std::string>(const std::string& value)
    {
        setString(value);
    }

    Variant::Variant()
        : IceUtil::Shared(),
          VariantBase()
    {
        invalidate();
    }

    Variant::Variant(const Variant& source):
        IceUtil::Shared(source),
        Ice::Object(source),
        VariantBase(source),
        SafeShared<Variant>(source)
    {
        BOOST_ASSERT(source.data);

        data = VariantDataPtr::dynamicCast(source.data->ice_clone());
        setType(source.typeId);
        initialized = source.initialized;

    }

    VariantPtr Variant::clone() const
    {
        return new Variant(*this);
    }

    // *******************************************************
    // setter
    // *******************************************************

    void Variant::setType(VariantTypeId typeId, const Ice::Current& c)
    {
        if (getType() != VariantType::Invalid && (getType() != typeId))
            throw LocalException("A Variant's type cannot be changed after it was set! Current Variant type: ")
                    <<  Variant::typeToString(getType()) << " Requested Type: " <<  Variant::typeToString(typeId);

        this->typeId = typeId;
    }


    void Variant::setInt(int n, const Ice::Current& c)
    {
        if (getType() != VariantType::Invalid && (getType() != VariantType::Int))
        {
            throw exceptions::user::InvalidTypeException(getType(), VariantType::Int, __FUNCTION__);
        }

        if (!getInitialized())
        {
            data = new IntVariantData();
        }

        setType(VariantType::Int);
        IntVariantDataPtr::dynamicCast(this->data)->n = n;
    }

    void Variant::setFloat(float f, const Ice::Current& c)
    {
        if (getType() != VariantType::Invalid && (getType() != VariantType::Float))
        {
            throw exceptions::user::InvalidTypeException(getType(), VariantType::Float, __FUNCTION__);
        }

        if (!getInitialized())
        {
            data = new FloatVariantData();
        }

        setType(VariantType::Float);

        FloatVariantDataPtr::dynamicCast(this->data)->f = f;
    }

    void Variant::setDouble(double d, const Ice::Current& c)
    {
        if (getType() != VariantType::Invalid && (getType() != VariantType::Double))
        {
            throw exceptions::user::InvalidTypeException(getType(), VariantType::Double, __FUNCTION__);
        }

        if (!getInitialized())
        {
            data = new DoubleVariantData();
        }

        setType(VariantType::Double);

        DoubleVariantDataPtr::dynamicCast(this->data)->d = d;
    }

    void Variant::setString(const std::string& s, const Ice::Current& c)
    {
        if (getType() != VariantType::Invalid && (getType() != VariantType::String))
        {
            throw exceptions::user::InvalidTypeException(getType(), VariantType::String, __FUNCTION__);
        }

        if (!getInitialized())
        {
            data = new StringVariantData();
        }

        setType(VariantType::String);

        StringVariantDataPtr::dynamicCast(this->data)->s = s;
    }

    void Variant::setBool(bool b, const Ice::Current& c)
    {
        if (getType() != VariantType::Invalid && (getType() != VariantType::Bool))
        {
            throw exceptions::user::InvalidTypeException(getType(), VariantType::Bool, __FUNCTION__);
        }

        if (!getInitialized())
        {
            data = new BoolVariantData();
        }

        setType(VariantType::Bool);

        BoolVariantDataPtr::dynamicCast(this->data)->b = b;
    }

    void Variant::setClass(const VariantDataClassPtr& variantDataClass)
    {
        if (getType() != VariantType::Invalid && (getType() != variantDataClass->getType()))
        {
            throw exceptions::user::InvalidTypeException(getType(), variantDataClass->getType(), __FUNCTION__);
        }

        data = variantDataClass/*->clone()*/;
        setType(variantDataClass->getType());
    }

    void Variant::setClass(const VariantDataClass& variantDataClass)
    {
        if (getType() != VariantType::Invalid && (getType() != variantDataClass.getType()))
        {
            throw exceptions::user::InvalidTypeException(getType(), variantDataClass.getType(), __FUNCTION__);
        }

        data = variantDataClass.clone();
        setType(variantDataClass.getType());
    }


    // *******************************************************
    // getter
    // *******************************************************
    int Variant::getInt(const Ice::Current& c) const
    {
        if (getType() != VariantType::Int)
        {
            throw exceptions::user::InvalidTypeException(getType(), VariantType::Int, __FUNCTION__);
        }

        if (!getInitialized())
        {
            throw exceptions::user::NotInitializedException();
        }

        IntVariantData* dataPtr = static_cast<IntVariantData*>(this->data.get());

        return dataPtr->n;
    }

    float Variant::getFloat(const Ice::Current& c) const
    {
        if (getType() != VariantType::Float)
        {
            throw exceptions::user::InvalidTypeException(getType(), VariantType::Float, __FUNCTION__);
        }

        if (!getInitialized())
        {
            throw exceptions::user::NotInitializedException();
        }

        FloatVariantData* dataPtr = static_cast<FloatVariantData*>(this->data.get());

        return dataPtr->f;
    }


    double Variant::getDouble(const Ice::Current& c) const
    {
        if (getType() != VariantType::Double)
        {
            throw exceptions::user::InvalidTypeException(getType(), VariantType::Double, __FUNCTION__);
        }

        if (!getInitialized())
        {
            throw exceptions::user::NotInitializedException();
        }

        DoubleVariantData* dataPtr = static_cast<DoubleVariantData*>(this->data.get());

        return dataPtr->d;
    }


    std::string Variant::getString(const Ice::Current& c) const
    {
        if (getType() != VariantType::String)
        {
            throw exceptions::user::InvalidTypeException(getType(), VariantType::String, __FUNCTION__);
        }

        if (!getInitialized())
        {
            throw exceptions::user::NotInitializedException();
        }

        StringVariantData* dataPtr = static_cast<StringVariantData*>(this->data.get());

        return dataPtr->s;
    }

    bool Variant::getBool(const Ice::Current& c) const
    {
        if (getType() != VariantType::Bool)
        {
            throw exceptions::user::InvalidTypeException(getType(), VariantType::Bool, __FUNCTION__);
        }

        if (!getInitialized())
        {
            throw exceptions::user::NotInitializedException();
        }

        BoolVariantData* dataPtr = static_cast<BoolVariantData*>(this->data.get());

        return dataPtr->b;
    }

    std::string Variant::getOutputValueOnly() const
    {
        VariantTypeId type = getType();
        std::stringstream stream;
        stream.precision(std::numeric_limits<double>::digits10);

        if (!getInitialized())
        {
            stream << "Not Initialized";
        }
        else if (type == VariantType::Invalid)
        {
            stream << "n/a";
        }
        else if (type == VariantType::Int)
        {
            stream << getInt();
        }
        else if (type == VariantType::Float)
        {
            stream << getFloat();
        }
        else if (type == VariantType::Double)
        {
            stream << getDouble();
        }
        else if (type == VariantType::String)
        {
            stream << getString();
        }
        else if (type == VariantType::Bool)
        {
            if (getBool() == true)
            {
                stream << "True";
            }
            else
            {
                stream << "False";
            }
        }
        else
        {

            VariantDataClassPtr ptr = VariantDataClassPtr::dynamicCast(this->data);

            if (!data)
            {
                stream << "NULLDATA (" << typeToString(type) << ")";
            }
            else if (ptr)
            {
                stream << ptr->output();
            }
            else
            {
                stream << data.get() << "(NULLCAST for" << typeToString(type) << ")";
            }
        }

        return stream.str();
    }

    // *******************************************************
    // properties
    // *******************************************************
    VariantTypeId Variant::getType(const Ice::Current& c) const
    {
        return typeId;
    }

    std::string Variant::getTypeName(const Ice::Current& c) const
    {
        try
        {
            return typeToString(typeId);
        }
        catch (exceptions::user::UnknownTypeException& e)
        {
            return "UnknownType";
        }
    }

    bool Variant::getInitialized(const Ice::Current& c) const
    {
        if (!initialized)
        {
            initialized = data && (hashTypeName(data->ice_id()) != VariantType::Invalid);
        }
        return initialized;
    }


    bool Variant::validate(const Ice::Current& c) const
    {
        if (!getInitialized())
        {
            return false;
        }

        if (data->ice_id() == VariantData::ice_staticId())
        {
            throw exceptions::local::IncompleteTypeException(getType(), data->ice_id());
        }

        VariantDataClassPtr classPtr = VariantDataClassPtr::dynamicCast(data);

        if (classPtr)
        {
            return classPtr->validate();
        }

        return true; // basic variant types are always valid
    }


    // *******************************************************
    // operator
    // *******************************************************
    Variant& Variant::operator =(const VariantDataClass& prototype)
    {
        if (getType() != VariantType::Invalid && (getType() != prototype.getType()))
        {
            throw exceptions::user::InvalidTypeException(getType(), prototype.getType(), __FUNCTION__);
        }

        data = VariantDataPtr::dynamicCast(prototype.ice_clone());
        setType(hashTypeName(prototype.ice_id()));

        return *this;
    }

    Variant& Variant::operator =(const VariantDataClassPtr& prototype)
    {
        if (getType() != VariantType::Invalid && (getType() != prototype->getType()))
        {
            throw exceptions::user::InvalidTypeException(getType(), prototype->getType(), __FUNCTION__);
        }

        data = VariantDataPtr::dynamicCast(prototype->ice_clone());
        setType(hashTypeName(prototype->ice_id()));

        return *this;
    }

    Variant& Variant::operator =(const Variant& prototype)
    {
        if (getType() != VariantType::Invalid && (getType() != prototype.getType()))
        {
            throw exceptions::user::InvalidTypeException(getType(), prototype.getType(), __FUNCTION__);
        }

        data = VariantDataPtr::dynamicCast(prototype.data->ice_clone());
        setType(prototype.typeId);

        return *this;
    }

    // *******************************************************
    // private methods
    // *******************************************************
    void Variant::output(std::ostream& stream) const
    {
        VariantTypeId type = getType();

        try
        {
            stream << "(" << Variant::typeToString(type) << ") ";
        }
        catch (exceptions::user::UnknownTypeException&)
        {
            stream << "(UNKNOWNTYPE) ";
        }

        if (!getInitialized())
        {
            stream << "not Initialized " ;
            return;
        }

        stream << getOutputValueOnly();

    }

    void Variant::invalidate()
    {
        data = new InvalidVariantData();
        typeId = VariantType::Invalid;
        initialized = false;
    }

    int Variant::hashTypeName(const std::string& typeName)
    {
        static std::unordered_map<std::string, VariantTypeId> idMap;
        auto it = idMap.find(typeName);

        if (it != idMap.end())
        {
            return it->second;
        }

        int hash = 0;
        int index = 0;
        int ch;

        while ((ch = typeName[index++]))
        {
            /* hash = hash * 33 ^ c */
            hash = ((hash << 5) + hash) ^ ch;
        }

        idMap[typeName] = hash;
        return hash;
    }

    std::map<VariantTypeId, std::string>& Variant::types()
    {
        static std::map<VariantTypeId, std::string> types = std::map<VariantTypeId, std::string>();
        return types;
    }

    // *******************************************************
    // static methods for type handling
    // *******************************************************
    std::string Variant::typeToString(VariantTypeId type)
    {
        auto& typeMap = types();
        std::map<VariantTypeId, std::string>::iterator it = typeMap.find(type);


        if (it == types().end())
        {
            throw exceptions::user::UnknownTypeException();
        }

        return it->second;
    }

    const std::map<VariantTypeId, std::string>& Variant::getTypes()
    {
        return types();
    }

    VariantTypeId Variant::addTypeName(const std::string& typeName)
    {
        int hash = hashTypeName(typeName);
        std::pair<unsigned int, std::string> entry;
        entry.first = hash;
        entry.second = typeName;
        //            std::cout << typeName << ": " << hash << std::endl;
        //            if(types().find(hash) !=  types().end())
        //                throw LocalException("A Type with this string ('" +typeName +  "') is already added. It has to be renamed.");
        std::map<VariantTypeId, std::string>::iterator it = types().find(hash);

        if (it !=  types().end() && typeName != it->second)
        {
            throw LocalException("A Type with the same hash value is already added. It has to be renamed. TypeName: " + typeName);
        }

        Variant::types().insert(entry);

        return hash;
    }

    bool VariantType::IsBasicType(VariantTypeId id)
    {
        if (id == VariantType::Bool
            || id == VariantType::Float
            || id == VariantType::Double
            || id == VariantType::Int
            || id == VariantType::String)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}


std::ostream& std::operator<<(std::ostream& stream, const armarx::VariantDataClass& variant)
{
    stream << variant.output();
    return stream;
}
