/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <chrono>

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/interface/observers/Timestamp.h>

#include <IceUtil/Time.h>

namespace armarx
{
    namespace VariantType
    {
        // Variant types
        const VariantTypeId Timestamp = Variant::addTypeName("::armarx::TimestampBase");
    }

    class TimestampVariant;
    typedef IceInternal::Handle<TimestampVariant> TimestampVariantPtr;

    /**
     * @class TimestampVariant
     * @ingroup VariantsGrp
     * Implements a Variant type for timestamps. The timestamp value is interpreted as microseconds since the Unix Epoch.
     * Internally the class bases on the IceUtil::Time functionality of Ice.
     *
     * For information on how to use this type, refer to Variant.
     */
    class ARMARXCORE_IMPORT_EXPORT TimestampVariant : virtual public TimestampBase
    {
    public:
        TimestampVariant();

        /**
         * Construct a timestamp Variant from an initialization value.
         *
         * @param timestamp         Initialization timestamp in microseconds since the Unix Epoch
         */
        TimestampVariant(long timestamp);
        TimestampVariant(IceUtil::Time time);
        template<class...Ts>
        TimestampVariant(std::chrono::duration<Ts...> duration) :
            TimestampVariant(std::chrono::duration_cast<std::chrono::microseconds>(duration).count())
        {}
        template<class...Ts>
        TimestampVariant(std::chrono::time_point<Ts...> timepoint) :
            TimestampVariant(timepoint.time_since_epoch())
        {}

        /**
         * @brief return timestamp in microseconds since the Unix Epoch.
         */
        long getTimestamp();


        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const override
        {
            return this->clone();
        }
        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const override
        {
            return new TimestampVariant(*this);
        }
        std::string output(const Ice::Current& c = ::Ice::Current()) const override
        {
            std::stringstream s;
            s << IceUtil::Time::microSeconds(timestamp).toDateTime();
            return s.str();
        }
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const override
        {
            return VariantType::Timestamp;
        }
        bool validate(const Ice::Current& c = ::Ice::Current()) override
        {
            return true;
        }

        friend std::ostream& operator<<(std::ostream& stream, const TimestampVariant& rhs)
        {
            stream << "TimestampVariant: " << std::endl << rhs.output() << std::endl;
            return stream;
        }

        static TimestampVariantPtr nowPtr()
        {
            return new TimestampVariant(TimeUtil::GetTime());
        }

        IceUtil::Time toTime()
        {
            return IceUtil::Time::microSeconds(timestamp);
        }

    public:
        // serialization
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override;
    };
}

