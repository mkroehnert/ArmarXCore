/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::FactoryLinkTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/ArmarXManager.h>


#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <Ice/Initialize.h>
using namespace armarx;




BOOST_AUTO_TEST_CASE(CheckLinkingOfObjectFactories)
{
    auto c = Ice::initialize();
    BOOST_CHECK_EQUAL(c->findObjectFactory("::armarx::ChannelRefBase"), ::Ice::ObjectFactoryPtr());
    ArmarXManager::RegisterKnownObjectFactoriesWithIce(c);
    BOOST_CHECK_NE(c->findObjectFactory("::armarx::ChannelRefBase"), ::Ice::ObjectFactoryPtr());

}

