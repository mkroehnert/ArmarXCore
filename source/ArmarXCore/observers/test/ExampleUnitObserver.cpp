/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExampleUnitObserver.h"

#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>

armarx::ExampleUnitObserver::ExampleUnitObserver()
{
}

//! [ObserversDocumentation ObserverComponent4]
void armarx::ExampleUnitObserver::onInitObserver()
{
    usingTopic("PeriodicExampleValue");


    offerConditionCheck("larger", new ConditionCheckLarger());
    offerConditionCheck("smaller", new ConditionCheckSmaller());
}

void armarx::ExampleUnitObserver::onConnectObserver()
{
    offerChannel("exampleChannel", "A constant value periodically published by ExampleUnit");
    offerDataField("exampleChannel", "exampleDataFieldInt", VariantType::Int, "The value published by ExampleUnit (if it is in integer)");
    offerDataField("exampleChannel", "exampleDataFieldFloat", VariantType::Float, "Another field");
    offerDataField("exampleChannel", "exampleDataFieldType", VariantType::String, "The type of the value published by ExampleUnit");
    offerDataField("exampleChannel", "dataReportDelay", VariantType::Double, "The delay of the reporting from the unit to the observer");
}

void armarx::ExampleUnitObserver::reportPeriodicValue(const armarx::VariantBasePtr& value, Ice::Double timestamp, const Ice::Current&)
{
    double delay = IceUtil::Time::now().toMicroSecondsDouble() - timestamp;
    ARMARX_INFO_S << "Delay: " << delay << deactivateSpam(1);
    VariantPtr v = VariantPtr::dynamicCast(value);

    if (v->getType() == VariantType::Int)
    {
        setDataFieldFlatCopy("exampleChannel", "exampleDataFieldInt", v);
    }
    else if (v->getType() == VariantType::Float)
    {
        setDataFieldFlatCopy("exampleChannel", "exampleDataFieldFloat", v);
    }

    setDataField("exampleChannel", "exampleDataFieldType", v->getTypeName());
    setDataField("exampleChannel", "dataReportDelay", delay);
    updateChannel("exampleChannel");
}
//! [ObserversDocumentation ObserverComponent4]

armarx::PropertyDefinitionsPtr armarx::ExampleUnitObserver::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new ExampleUnitObserverPropertyDefinitions(getConfigIdentifier()));
}
