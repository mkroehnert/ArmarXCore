/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::test
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <ArmarXCore/interface/observers/ExampleUnitInterface.h>

namespace armarx
{
    //! [ObserversDocumentation UnitComponent1]
    class ExampleUnitPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        ExampleUnitPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<int>("ReportPeriod", 1, "The unit's reporting period in ms");
        }
    };
    //! [ObserversDocumentation UnitComponent1]

    //! [ObserversDocumentation UnitComponent2]
    class ExampleUnit :
        virtual public ExampleUnitInterface,
        virtual public Component
    {
    public:
        std::string getDefaultName() const override
        {
            return "ExampleUnit";
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        void setPeriodicValue(const VariantBasePtr& value, const Ice::Current& c = ::Ice::Current()) override;

        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        void reportValue();

        ExampleUnitListenerPrx listenerPrx;

        PeriodicTask<ExampleUnit>::pointer_type periodicTask;

        int period;
        VariantPtr periodicValue;
    };
    typedef IceInternal::Handle<ExampleUnit> ExampleUnitPtr;
    //! [ObserversDocumentation UnitComponent2]
}


