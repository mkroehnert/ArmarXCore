/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Mirko Waechter (waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::ObserverTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/ArmarXManager.h>


#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/observers/variant/Variant.h>


#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/observers/filters/AverageFilter.h>
#include <ArmarXCore/observers/filters/GaussianFilter.h>
#include <ArmarXCore/observers/filters/MedianFilter.h>
#include <ArmarXCore/observers/filters/MinMaxFilter.h>


#include <ArmarXCore/core/test/IceTestHelper.h>
#include "ExampleUnit.h"
#include "ExampleUnitObserver.h"

using namespace armarx;




BOOST_AUTO_TEST_CASE(ObserverTest)
{

    Ice::PropertiesPtr properties = Ice::createProperties();

    int registryPort = 11220;
    IceTestHelperPtr iceTestHelper = new IceTestHelper(registryPort, registryPort + 1);
    iceTestHelper->startEnvironment();
    TestArmarXManagerPtr manager = new TestArmarXManager("ObserverMedianFilterTest", iceTestHelper->getCommunicator(), properties);
    //    ExampleUnitObserverPrx obs = manager->createComponentAndRun<ExampleUnitObserver, ExampleUnitObserverInterfacePrx>("ArmarX", "ExampleUnitObserver");
    ExampleUnitObserverPtr observer = Component::create<ExampleUnitObserver>();
    manager->addObject(observer);


    if (!observer->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted, 5000))
    {
        throw LocalException("ExampleUnitObserver couldnot be started");
    }

    observer->reportPeriodicValue(new Variant(42), IceUtil::Time::now().toMicroSecondsDouble());
    usleep(30000); // observer update is async -> wait
    DatafieldRefPtr datafield = new DatafieldRef(observer.get(), "exampleChannel", "exampleDataFieldInt");
    DatafieldRefPtr strDatafield = new DatafieldRef(observer.get(), "exampleChannel", "exampleDataFieldType");
    DatafieldRefPtr delayId = new DatafieldRef(observer.get(), "exampleChannel", "exampleDataFieldType");


    BOOST_CHECK(observer->existsDataField(datafield->channelRef->channelName, datafield->datafieldName));
    BOOST_CHECK(observer->getDataField(datafield->getDataFieldIdentifier())->getInitialized());
    BOOST_CHECK_EQUAL(observer->getDataField(datafield->getDataFieldIdentifier())->getInt(), 42); // 91 = avg(14*14, 13*13, ... 4*4)

    manager->shutdown();
}


BOOST_AUTO_TEST_CASE(ObserverPerformanceTest)
{

    auto propValues = Ice::StringSeq {"--ArmarX.ExampleUnit.ReportPeriod=1"};
    Ice::PropertiesPtr properties = Ice::createProperties(propValues);
    int registryPort = 11220;
    IceTestHelperPtr iceTestHelper = new IceTestHelper(registryPort, registryPort + 1);
    iceTestHelper->startEnvironment();
    TestArmarXManagerPtr manager = new TestArmarXManager("ObserverPerformanceTestSender", iceTestHelper->getCommunicator(), properties);
    auto args = iceTestHelper->getArgs();
    TestArmarXManagerPtr manager2 = new TestArmarXManager("ObserverPerformanceTestReceiver", Ice::initialize(args), properties);
    //    ExampleUnitObserverPrx obs = manager->createComponentAndRun<ExampleUnitObserver, ExampleUnitObserverInterfacePrx>("ArmarX", "ExampleUnitObserver");
    ExampleUnitObserverPtr observer = Component::create<ExampleUnitObserver>();
    manager->addObject(observer);

    ExampleUnitPtr unit = Component::create<ExampleUnit>();
    manager2->addObject(unit);

    if (!observer->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted, 5000))
    {
        throw LocalException("ExampleUnitObserver could not be started");
    }


    if (!unit->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted, 5000))
    {
        throw LocalException("ExampleUnit could not be started");
    }
    usleep(300000);
    DatafieldRefPtr delayId = new DatafieldRef(observer.get(), "exampleChannel", "dataReportDelay");
    DatafieldRefPtr filtered = DatafieldRefPtr::dynamicCast(observer->createFilteredDatafield(
                                   DatafieldFilterBasePtr(new filters::MedianFilter()),
                                   delayId));
    DatafieldFilterBasePtr maxFilter = DatafieldFilterBasePtr(new filters::MaxFilter());
    maxFilter->windowFilterSize = 200;
    DatafieldRefPtr maxValue = DatafieldRefPtr::dynamicCast(observer->createFilteredDatafield(
                                   maxFilter,
                                   delayId));

    auto delay = filtered->getDataField()->getDouble();
    auto maxDelay = maxValue->getDataField()->getDouble();
    ARMARX_INFO_S << VAROUT(delay);
    ARMARX_INFO_S << VAROUT(maxDelay);
    BOOST_WARN(delay < 3000);


    manager2->shutdown();
    manager->shutdown();
    if (unit->getObjectScheduler())
    {
        unit->getObjectScheduler()->waitForTermination();
    }
}




