/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/condition/TermImpl.h>

namespace armarx
{
    class Operation;

    /**
     * Typedef of OperationPtr as IceInternal::Handle<Operation> for convenience.
     */
    typedef IceInternal::Handle<Operation> OperationPtr;

    /**
     * @class Operation
     * @ingroup Conditions
     *
     * Operation is the superclass for all logical operators within an expression tree.
     * This superclass provides a getter for the type of operation and implements
     * the slice OperationBase interface. Further it inherits the TermImpl superclass
     * in order to be usable in an expression tree.
     */
    class ARMARXCORE_IMPORT_EXPORT Operation :
        virtual public TermImpl,
        virtual public OperationBase
    {
    public:
        /**
        * Retrieve the type of the operation. Either eOperationAnd, eOperationOr, eOperationNot, or eUndefinedOperation.
        *
        * @return type of the operation
        */
        OperationType getOperationType(const Ice::Current& c = ::Ice::Current()) const override;
    protected:
        // required for Ice::ObjectFactory
        Operation();


    };

    /**
     * @class OperationAnd
     * @ingroup Conditions
     *
     * OperationAnd implements a logical AND operation within the expression tree.
     */
    class ARMARXCORE_IMPORT_EXPORT OperationAnd :
        virtual public Operation,
        virtual public OperationAndBase
    {
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;
    public:
        /**
         * Constructs a logical AND operation on the two terms a and b
         *
         * @param a first term
         * @param b second term
         */
        OperationAnd(TermImplPtr a, TermImplPtr b);

        /**
         * Implementation for updating this term from its children in the expression tree. Inherited from TermImpl.
         */
        void update(const Ice::Current& c = ::Ice::Current()) override;
        void updateWithData(const Ice::Current&) override;

        /**
         * output operation to stream
         *
         * @param out output stream
         */
        void output(std::ostream& out) const override;

        /**
         * Retrieve string identifying the operation.
         *
         * @return string identifier
         */
        std::string getOperationString(const Ice::Current& c = ::Ice::Current()) override;

        /**
         * Reimplementation of the ice_clone method.
         *
         * @return clone of the object
         */
        Ice::ObjectPtr ice_clone() const override;

    protected:
        OperationAnd();
    };

    /**
     * @class OperationOr
     * @ingroup Conditions
     *
     * OperationOr implements a logical OR operation within the expression tree.
     */
    class ARMARXCORE_IMPORT_EXPORT OperationOr :
        virtual public Operation,
        virtual public OperationOrBase
    {
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;
    public:
        /**
         * Constructs a logical OR operation on the two terms a and b
         *
         * @param a first term
         * @param b second term
         */
        OperationOr(TermImplPtr a, TermImplPtr b);

        /**
         * Implementation for updating this term from its children in the expression tree. Inherited from TermImpl.
         */
        void update(const Ice::Current& c = ::Ice::Current()) override;
        void updateWithData(const Ice::Current&) override;

        /**
         * output operation to stream
         *
         * @param out output stream
         */
        void output(std::ostream& out) const override;

        /**
         * Retrieve string identifying the operation.
         *
         * @return string identifier
         */
        std::string getOperationString(const Ice::Current& c = ::Ice::Current()) override;

        /**
         * Reimplementation of the ice_clone method.
         *
         * @return clone of the object
         */
        Ice::ObjectPtr ice_clone() const override;

    protected:
        OperationOr();
    };

    /**
     * @class OperationNot
     * @ingroup Conditions
     *
     * OperationNot implements a logical NOT operation within the expression tree.
     */
    class ARMARXCORE_IMPORT_EXPORT OperationNot :
        virtual public Operation,
        virtual public OperationNotBase
    {
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;
    public:
        /**
         * Constructs a logical NOT operation from the term
         *
         * @param a term
         */
        OperationNot(TermImplPtr a);

        /**
         * Implementation for updating this term from its children in the expression tree. Inherited from TermImpl.
         */
        void update(const Ice::Current& c = ::Ice::Current()) override;
        void updateWithData(const Ice::Current&) override;

        /**
         * output operation to stream
         *
         * @param out output stream
         */
        void output(std::ostream& out) const override;

        /**
         * Retrieve string identifying the operation.
         *
         * @return string identifier
         */
        std::string getOperationString(const Ice::Current& c = ::Ice::Current()) override;

        /**
         * Reimplementation of the ice_clone method.
         *
         * @return clone of the object
         */
        Ice::ObjectPtr ice_clone() const override;

    protected:
        OperationNot();
    };
}

