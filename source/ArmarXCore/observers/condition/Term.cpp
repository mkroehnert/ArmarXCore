/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/condition/Term.h>
#include <ArmarXCore/observers/condition/Operations.h>

using namespace armarx;

Term::Term()
{
    termImpl = nullptr;
}

Term::Term(const Term& other)
{
    if (other.termImpl)
    {
        this->termImpl = TermImplPtr::dynamicCast(other.termImpl->ice_clone());
    }
    else
    {
        this->termImpl = nullptr;
    }
}

Term::Term(armarx::TermImplPtr impl)
{
    this->termImpl = impl;
}

Term Term::operator&&(const Term& right) const
{
    if (!this->getImpl())
    {
        return right;
    }

    Term opAnd;
    opAnd.termImpl = new OperationAnd(this->getImpl(), right.getImpl());

    return opAnd;
}

Term Term::operator||(const Term& right) const
{
    if (!this->getImpl())
    {
        return right;
    }

    Term opOr;
    opOr.termImpl = new OperationOr(this->getImpl(), right.getImpl());
    return opOr;
}

Term Term::operator!(void) const
{
    if (!this->getImpl())
    {
        return *this;
    }

    Term opNot;
    opNot.termImpl = new OperationNot(this->getImpl());
    return opNot;
}

Term& Term::operator=(const Term& right)
{
    if (right.getImpl())
    {
        this->termImpl = TermImplPtr::dynamicCast(right.getImpl()->ice_clone());
    }
    else
    {
        this->termImpl = nullptr;
    }

    return *this;
}

TermImplPtr Term::getImpl() const
{
    return termImpl;
}


Literal::Literal(const std::string& dataFieldIdentifierStr, const std::string& checkName, const VarList& checkParameters)
{
    termImpl = new LiteralImpl(dataFieldIdentifierStr, checkName, toParamList(checkParameters));
}

Literal::Literal(const DataFieldIdentifier& dataFieldIdentifier, const std::string& checkName, const VarList& checkParameters)
{
    termImpl = new LiteralImpl(dataFieldIdentifier, checkName, toParamList(checkParameters));
}

Literal::Literal(const DataFieldIdentifierPtr& dataFieldIdentifier, const std::string& checkName, const VarList& checkParameters)
{
    termImpl = new LiteralImpl(dataFieldIdentifier, checkName, toParamList(checkParameters));
}

Literal::Literal(const DatafieldRefBasePtr& datafieldRef, const std::string& checkName, const VarList& checkParameters)
{
    termImpl = new LiteralImpl(datafieldRef, checkName, toParamList(checkParameters));
}

VarList Literal::createParameterList()
{
    return VarList();
}

VarList Literal::createParameterList(const Variant& param1)
{
    VarList list;
    list.push_back((param1));

    return list;
}

VarList Literal::createParameterList(const Variant& param1, const Variant& param2)
{
    VarList list;
    list.push_back((param1));
    list.push_back((param2));

    return list;
}

VarList Literal::createParameterList(const Variant& param1, const Variant& param2, const Variant& param3)
{
    VarList list;
    list.push_back((param1));
    list.push_back((param2));
    list.push_back((param3));

    return list;
}

ParameterList Literal::toParamList(const VarList& varList) const
{
    ParameterList result;

    for (const auto& var : varList)
    {
        result.push_back(new Variant(var));
    }

    return result;
}
