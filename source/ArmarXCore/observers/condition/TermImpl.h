/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// ArmarX
#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/variant/Variant.h>

// interface
#include <ArmarXCore/interface/observers/TermImplBase.h>

namespace armarx
{
    class TermImpl;

    /**
     * Typedef of TermImplPtr as IceInternal::Handle<TermImpl> for convenience.
     */
    typedef IceInternal::Handle<TermImpl> TermImplPtr;

    /**
    * @class TermImpl
    * @ingroup Conditions
    * TermImpl is the superclass for all implementations of terms in the expression tree, such as LiteralImpl, Operation and ConditionRoot.
    * The TermImpl is the backend to the API frontend Term. It inherits from Ice::Object in order to allow serialization via
    * Ice and provides all methods to build an expression tree.
    *
    * Each term has a boolean value. Values are updated from the childs in the expression tree by calling update(). Further,
    * the update method calls the parents state update(). Thus, expression trees consisting of TermImpl are updated from bottom to top.
    */
    class ARMARXCORE_IMPORT_EXPORT TermImpl :
        virtual public TermImplBase
    {
    public:
        /**
        * Destructor of TermImpl. Assures handling of cyclic IceInternal::Handle dependencies
        */
        ~TermImpl() override;

        /**
        * Add child to term. This is used to generate an expression tree.
        *
        * @param child shared pointer to child
        */
        void addChild(const TermImplBasePtr& child, const Ice::Current& c = ::Ice::Current()) override;

        /**
        * retrieve childs of this term in the expression tree.
        *
        * @return list of child terms
        */
        TermImplSequence getChilds(const Ice::Current& c = ::Ice::Current()) override;

        /**
        * retrieve parent of this term in the expression tree. For the root node this is NULL.
        *
        * @return shared pointer to parent
        */
        TermImplBasePtr getParent(const Ice::Current& c = ::Ice::Current()) override;

        /**
        * retrieve current value of term. Does not evaluate the child nodes.
        *
        * @return current value of the term
        */
        bool getValue(const Ice::Current& c = ::Ice::Current()) const override;

        StringVariantBaseMap getDatafields(const Ice::Current&) const override;

        /**
        * Retrieve type of term
        *
        * @return type of the term
        */
        TermType getType(const Ice::Current& c = ::Ice::Current()) const override;

        /**
        * Updates the parent in the expression tree. call update after each change to this->value.
        */
        void update(const Ice::Current& c = ::Ice::Current()) override;

        void updateWithData(const Ice::Current& c = ::Ice::Current()) override;

        /**
        * output to stream. pure virtual.
        *
        * @param stream
        */
        virtual void output(std::ostream& out) const = 0;

        /**
        * Streaming operator for this class
        *
        * @param stream stream to output
        * @param rhs right handside
        */
        friend std::ostream& operator<<(std::ostream& stream, const TermImplPtr& rhs)
        {
            rhs->output(stream);

            return stream;
        }


        /**
        * Streaming operator for this class
        *
        * @param stream stream to output
        * @param rhs right handside
        */
        friend std::ostream& operator<<(std::ostream& stream, const TermImpl* rhs)
        {
            rhs->output(stream);

            return stream;
        }

        void removeChildren();

    protected:
        /**
        * Sets the parent for this term in the expression tree.
        *
        * @param shared pointer to parent
        */
        void setParent(const TermImplBasePtr& parent, const Ice::Current& c = ::Ice::Current()) override;

        /**
        * Reset the parent of this term.
        */
        void resetParent(const Ice::Current& c = ::Ice::Current()) override;

        /**
         * atomicDecAndTestValue - decrement and test
         * @v: pointer of type AtomicCounter
         * @value: value to test for
         *
         * Atomically decrements @v by 1 and returns true if the result is value,
         * or false for all other cases. Note that the guaranteed useful
         * range of an AtomicCounter is only 24 bits.
         *
         * Inlined because this operation is performance critical.
         */
        static inline int atomicDecAndTestValue(volatile int* counter, int value);

        /**
         * Overwritten version of reference counting from GCShared.
         * Reference counter increment without garbage collection.
         * Uses Shared::__incRef instead of GCShared::__incRef()
         *
         * @see Shared::__incRef()
         * @see GCShared::__incRef()
         */
        void __incRef() override;

        /**
         * Overwritten version of reference counting from GCShared.
         * Resolves cyclic dependencies in the tree without makin
         * use of the garbage collector.
         *
         * @see Shared::__decRef()
         * @see GCShared::__decRef()
         */
        void __decRef() override;

    };
}
extern template class ::IceInternal::Handle<::armarx::TermImpl>;
