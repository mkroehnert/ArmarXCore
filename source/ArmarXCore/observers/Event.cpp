/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "Event.h"

#include <ArmarXCore/observers/parameter/Parameter.h>
#include <ArmarXCore/observers/parameter/VariantParameter.h>
#include <ArmarXCore/observers/parameter/VariantListParameter.h>
//#include <ArmarXCore/observers/variant/Variant.h>

template class ::IceInternal::Handle<::armarx::Event>;

armarx::Event::Event(std::string eventReceiverName, std::string eventName)
{
    this->eventReceiverName = eventReceiverName;
    this->eventName = eventName;
}

armarx::EventPtr armarx::Event::add(const std::string key, const armarx::Variant& value)
{
    properties[key] = new SingleVariant(value);
    return this;
}

armarx::EventPtr armarx::Event::add(const std::string key, const armarx::VariantContainerBasePtr& valueContainer)
{
    properties[key] = valueContainer->cloneContainer();
    return this;
}

Ice::ObjectPtr armarx::Event::ice_clone() const
{
    EventPtr newEvent = new Event(*this);
    return newEvent;
}

armarx::EventPtr armarx::Event::clone() const
{
    EventPtr newEvent = new Event(*this);
    newEvent->properties.clear();

    for (StringVariantContainerBaseMap::const_iterator it = properties.begin(); it != properties.end(); it++)
    {
        newEvent->properties[it->first] = it->second->cloneContainer();
    }

    return newEvent;
}


armarx::Event::Event()
{
    eventReceiverName = "";
    eventName = "";
}
