/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "DebugObserver.h"

#include <ArmarXCore/observers/exceptions/user/InvalidDatafieldException.h>

namespace armarx
{

    DebugObserver::DebugObserver()
    {
    }

    void DebugObserver::onInitObserver()
    {
        usingTopic(getProperty<std::string>("DebugObserverTopicName"));
    }

    void DebugObserver::onConnectObserver()
    {
    }

    void DebugObserver::setDebugDatafield(const std::string& channelName, const std::string& datafieldName, const VariantBasePtr& value, const Ice::Current& c)
    {
        if (!existsChannel(channelName))
        {
            offerChannel(channelName, "");
        }

        if (!existsDataField(channelName, datafieldName))
        {
            offerDataFieldWithDefault(channelName, datafieldName, *VariantPtr::dynamicCast(value), "");
        }
        else
        {
            setDataFieldFlatCopy(channelName, datafieldName, VariantPtr::dynamicCast(value));
        }

        updateChannel(channelName);
    }

    void DebugObserver::setDebugChannel(const std::string& channelName, const StringVariantBaseMap& valueMap, const Ice::Current& c)
    {
        if (!existsChannel(channelName))
        {
            offerChannel(channelName, "");
        }
        try
        {
            // fastest way is to just try to set the datafields
            setDataFieldsFlatCopy(channelName, valueMap, true);
        }
        catch (exceptions::user::InvalidDataFieldException& e)
        {
            // on failure: do it the slow way
            for (const auto& value : valueMap)
            {
                const std::string& datafieldName = value.first;

                if (!existsDataField(channelName, datafieldName))
                {
                    try
                    {
                        offerDataFieldWithDefault(channelName, datafieldName, *VariantPtr::dynamicCast(value.second), "");
                    }
                    catch (exceptions::user::DatafieldExistsAlreadyException& e)
                    {
                        setDataFieldFlatCopy(channelName, datafieldName, VariantPtr::dynamicCast(value.second));
                    }
                }
                else
                {
                    setDataFieldFlatCopy(channelName, datafieldName, VariantPtr::dynamicCast(value.second));
                }
            }
        }


        updateChannel(channelName);
    }

    void DebugObserver::removeDebugDatafield(const std::string& channelName, const std::string& datafieldName, const Ice::Current&)
    {
        removeDatafield(new DataFieldIdentifier(getName(), channelName, datafieldName));
    }

    void DebugObserver::removeDebugChannel(const std::string& channelName, const Ice::Current&)
    {
        removeChannel(channelName);
    }

    void DebugObserver::removeAllChannels(const Ice::Current&)
    {
        ChannelRegistry channels = getAvailableChannels(false);
        ChannelRegistry::iterator it = channels.end();

        for (; it != channels.end(); it++)
        {
            ChannelRegistryEntry& entry = it->second;
            removeChannel(entry.name);
        }
    }

}


armarx::PropertyDefinitionsPtr armarx::DebugObserver::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new DebugObserverPropertyDefinitions(
                                      getConfigIdentifier()));
}
