/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/ConditionCheck.h>

namespace armarx
{
    /**
     * \ingroup ConditionChecks
     *
     * Checks if strings published in the relevant data fields contain a certain substring.
     *
     * Parameters: The substring to check for
     * Supported types: Strings
     */
    class ARMARXCORE_IMPORT_EXPORT ConditionCheckStringContains :
        public ConditionCheck
    {
    public:
        ConditionCheckStringContains()
        {
            setNumberParameters(1);

            addSupportedType(VariantType::String, createParameterTypeList(1, VariantType::String));
        }

        ConditionCheck* clone()
        {
            return new ConditionCheckStringContains(*this);
        }

        bool evaluate(const StringVariantMap& dataFields)
        {
            if (dataFields.size() != 1)
            {
                printf("Size of dataFields: %d\n", dataFields.size());
                throw InvalidConditionException("Wrong number of datafields for condition smaller");
            }

            const Variant& value = dataFields.begin()->second;

            return (value.getString().find(getParameter(0).getString()) != std::string::npos);
        }
    };
}

