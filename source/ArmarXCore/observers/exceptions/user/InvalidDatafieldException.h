/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/interface/observers/ObserverInterface.h>

namespace armarx
{
    namespace exceptions
    {
        namespace user
        {
            class InvalidDataFieldException: public armarx::InvalidDatafieldException
            {
            public:


                InvalidDataFieldException(std::string channelName, std::string datafieldName) :
                    armarx::InvalidDatafieldException("The dataField " + datafieldName + " is not valid for channel " + channelName + ".",
                                                      channelName, datafieldName)
                {
                }

                ~InvalidDataFieldException() noexcept override { }
            };

            class InvalidChannelException: public armarx::InvalidChannelException
            {
            public:
                InvalidChannelException(std::string channelName) :
                    armarx::InvalidChannelException("The channel '" + channelName + "' is not valid.", channelName)

                {
                }

                ~InvalidChannelException() noexcept override { }
            };

            class DatafieldExistsAlreadyException: public armarx::DatafieldExistsAlreadyException
            {
            public:
                DatafieldExistsAlreadyException(std::string channelName, std::string datafieldName) :
                    armarx::DatafieldExistsAlreadyException("The dataField " + datafieldName + " exists already for channel " + channelName + ".",
                                                            channelName, datafieldName)
                {
                }

                ~DatafieldExistsAlreadyException() noexcept override { }
            };
        }
    }
}

