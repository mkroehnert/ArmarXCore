/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke
* @date       2011 Humanoids Group, HIS, KIT
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/observers/variant/Variant.h>

#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace user
        {
            class InvalidTypeException: public armarx::InvalidTypeException
            {
            public:

                InvalidTypeException(VariantTypeId typeId1, VariantTypeId typeId2, const std::string& infoString = "")
                {
                    std::stringstream sstream;
                    sstream << "Type1: " << Variant::typeToString(typeId1) << " Type2: " << Variant::typeToString(typeId2);
                    if (!infoString.empty())
                    {
                        sstream << "\nInfo: " << infoString;
                    }
                    reason = sstream.str();
                }
                InvalidTypeException(std::string typeId1, std::string typeId2, const std::string& infoString = "")
                {
                    std::stringstream sstream;
                    sstream << "Type1: " << typeId1 << " Type2: " << typeId2;
                    if (!infoString.empty())
                    {
                        sstream << "\nInfo: " << infoString;
                    }
                    reason = sstream.str();
                }

                ~InvalidTypeException() noexcept override { }

                std::string ice_name() const override
                {
                    return "armarx::exceptions::user::InvalidTypeException";
                }
            };
            class UnsupportedTypeException: public armarx::UnsupportedTypeException
            {
            public:
                UnsupportedTypeException(VariantTypeId typeId, const std::string& infoString = "")
                {
                    std::stringstream sstream;
                    sstream <<  "Unsupported Type: " << Variant::typeToString(typeId);
                    if (!infoString.empty())
                    {
                        sstream << "\nInfo: " << infoString;
                    }
                    reason = sstream.str();
                }
                UnsupportedTypeException(const std::string& typeId, const std::string& infoString = "")
                {
                    std::stringstream sstream;
                    sstream <<  "Unsupported Type: " << typeId;
                    if (!infoString.empty())
                    {
                        sstream << "\nInfo: " << infoString;
                    }
                    reason = sstream.str();
                }

                ~UnsupportedTypeException() noexcept override { }

                std::string ice_name() const override
                {
                    return "armarx::exceptions::user::UnsupportedTypeException";
                }
            };
        }
    }
}
