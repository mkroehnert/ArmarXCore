/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once
#include "DatafieldFilter.h"

#include <ArmarXCore/interface/observers/Filters.h>

namespace armarx
{
    namespace filters
    {


        /**
         * @class  GaussianFilter
         * @ingroup ObserverFilters
         * @brief The GaussianFilter class provides a filter implemtentation
         * with gaussian weighted values for datafields of type float, int and double.
         */
        class GaussianFilter :
            public DatafieldFilter,
            public GaussianFilterBase
        {
        public:
            /**
             * @brief GaussianFilter
             * @param filterSizeInMs Width of the gauss function
             * @param windowSize size of the filter window
             */
            GaussianFilter(int filterSizeInMs = 200, int windowSize = 20);


            // DatafieldFilterBase interface
        public:
            template <typename Type>
            Type calcGaussianFilteredValue(const TimeVariantBaseMap& map) const
            {
                const double sigma = filterSizeInMs / 2.5;
                //-log(0.05) / pow(itemsToCheck+1+centerIndex,2) * ( sqrt(1.0/h) * sqrt(2*3.14159265));

                double weightedSum = 0;
                double sumOfWeight = 0;


                const double sqrt2PI = sqrt(2 * M_PI);


                for (auto it = map.begin();
                     it != map.end();
                     it++
                    )
                {
                    double value;
                    value = VariantPtr::dynamicCast(it->second)->get<Type>();
                    double diff = 0.001 * (it->first - map.rbegin()->first);

                    double squared = diff * diff;
                    const double gaussValue = exp(-squared / (2 * sigma * sigma)) / (sigma * sqrt2PI);
                    sumOfWeight += gaussValue;
                    weightedSum += gaussValue * value;

                }

                double result;
                result = weightedSum / sumOfWeight;
                return result;
            }

            VariantBasePtr calculate(const Ice::Current& c = GlobalIceCurrent) const override;

            /**
             * @brief This filter supports: Int, Float, Double
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current& c = GlobalIceCurrent) const override;



            // DatafieldFilterBase interface
        public:
            StringFloatDictionary getProperties(const Ice::Current& c = GlobalIceCurrent) const override;
            void setProperties(const StringFloatDictionary& newValues, const Ice::Current& c = GlobalIceCurrent) override;
        };

    } // namespace filters
} // namespace armarx

