/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "DatafieldFilter.h"

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/Filters.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

namespace armarx
{
    template <class IceBaseClass, class DerivedClass>
    class GenericFactory;

    namespace filters
    {
        class ButterworthFilter :
            public ButterworthFilterBase,
            public DatafieldFilter
        {
            template <class BaseClass, class VariantClass>
            friend class ::armarx::GenericFactory;
        protected:
            ButterworthFilter();
        public:

            ButterworthFilter(double frequency, int sampleRate, PassType passType, double resonance);
            void setInitialValue(double value);
            /**
             * @brief This filter supports: Int, Float, Double
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current& = GlobalIceCurrent) const override;


        private:

            /// Array of input values, latest are in front
            std::vector<double> inputHistory = std::vector<double>(2, 0.0);

            /// Array of output values, latest are in front
            std::vector<double> outputHistory = std::vector<double>(3, 0.0);




            // DatafieldFilterBase interface
        public:
            void update(Ice::Long, const VariantBasePtr& value, const Ice::Current& = GlobalIceCurrent) override;
            void update(double newInput);
            VariantBasePtr getValue(const Ice::Current& = GlobalIceCurrent) const override;
            double getRawValue() const;
            VariantBasePtr calculate(const Ice::Current& = GlobalIceCurrent) const override;

            void reset(double frequency, int sampleRate, PassType passType, double resonance);

            // DatafieldFilterBase interface
        public:
            StringFloatDictionary getProperties(const Ice::Current&) const override;
            void setProperties(const StringFloatDictionary& values, const Ice::Current&) override;
        };

    }
}

