/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "RemoteStateOfferer.h"
#include "RemoteStateWrapper.h"

namespace armarx
{
    ////////////////////////////////////////////////////////////////////////////////
    //////////// RemoteStateWrapper
    ////////////////////////////////////////////////////////////////////////////////

    RemoteStateWrapper::RemoteStateWrapper(StatePtr realState, RemoteStateIceBasePrx  callerStatePrx):
        callerStatePrx(callerStatePrx),
        realState(realState)
    {
        stateName = "remoteStateWrapper_of_" + realState->stateName;
        activeSubstate = realState;

        if (callerStatePrx)
        {
            StateIceBasePtr remoteParentStateLayout = callerStatePrx->getParentStateLayout();

            if (remoteParentStateLayout)
            {
                this->subStateList = remoteParentStateLayout->subStateList;
                this->transitions = remoteParentStateLayout->transitions;
                this->initState = remoteParentStateLayout->initState;
            }
        }
    }
    RemoteStateWrapper::RemoteStateWrapper(const RemoteStateWrapper& source):
        Shared(),
        Ice::Object(source),
        StateIceBase(source),
        Logging(source),
        StateBase(source),
        StateController(source),
        StateUtility(source),
        State(source)

    {
        realState = StatePtr::dynamicCast(source.realState->clone());

        if (source.realState && !realState)
        {
            throw exceptions::local::eNullPointerException("Could not cast from StateBasePtr to StatePtr");
        }

        callerStatePrx = source.callerStatePrx;
    }

    RemoteStateWrapper::~RemoteStateWrapper()
    {
        subStateList.clear(); // clear this list here, so that RemoteStates are not removed in ~StateBase(). This sublist is just for information purposes
    }

    void
    RemoteStateWrapper::__processBufferedEvents()
    {
        ARMARX_DEBUG << "calling remoteprocessbufferedevets" << "" << flush;

        //call remoteProcessBufferedEvents over ice
        if (callerStatePrx._ptr)
        {
            callerStatePrx->begin_remoteProcessBufferedEvents();
        }
    }



    void RemoteStateWrapper::__processEvent(const EventPtr ev, bool buffered)
    {
        // call substatesFinished over Ice
        if (callerStatePrx)
        {
            callerStatePrx->remoteProcessEvent(ev, buffered);
        }
        else
        {
            ARMARX_INFO << "No remote state proxy was set - will not report back to remote state. In case this is a toplevel state this is not an issue (Event: " << ev->eventName << ")";
            realState->_baseOnExit();
        }
    }

    void RemoteStateWrapper::__enqueueEvent(const EventPtr event)
    {
        if (callerStatePrx._ptr)
        {
            callerStatePrx->begin_remoteEnqueueEvent(event);
        }

    }

    unsigned int
    RemoteStateWrapper::__getUnbreakableBufferSize() const
    {

        if (callerStatePrx._ptr)
        {
            return callerStatePrx->getRemoteUnbreakableBufferSize();
        }
        else
        {
            return 0;
        }
    }

    bool RemoteStateWrapper::__getUnbreakableBufferStati() const
    {
        if (callerStatePrx._ptr)
        {
            return callerStatePrx->getRemoteUnbreakableBufferStati();
        }
        else
        {
            return true;
        }
    }


    void RemoteStateWrapper::__finalize(const EventPtr event)
    {
        //realState->baseOnExit();
        ARMARX_INFO << "sending remote finalize " << realState._ptr << "";

        if (callerStatePrx._ptr)
        {
            callerStatePrx->begin_remoteFinalize(StateUtilFunctions::getSetValues(realState->getOutputParameters()), event);
        }
    }

    StateBasePtr RemoteStateWrapper::clone() const
    {
        StateBasePtr result = new RemoteStateWrapper(*this);
        return result;
    }

    StateBasePtr RemoteStateWrapper::createEmptyCopy() const
    {
        assert(0);
        return new RemoteStateWrapper(nullptr, callerStatePrx);
    }
}
