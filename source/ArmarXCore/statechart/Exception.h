/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <ArmarXCore/core/exceptions/Exception.h>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            struct eStatechartLogicError
                : LocalException
            {

                eStatechartLogicError(std::string logicError): LocalException("The statechart contains a logic error: " + logicError) {}
                ~eStatechartLogicError() noexcept override {}
                std::string name() const override
                {
                    return "armarx::exceptions::local::eLogicError";
                }

            };
            struct eNullPointerException : LocalException
            {

                eNullPointerException(): LocalException("An accessed pointer is NULL!") {}

                eNullPointerException(std::string hint): LocalException("An accessed pointer is NULL!" + (!hint.empty() ? " Hint: " + hint : "")) {}

                ~eNullPointerException() noexcept override {}
                std::string name() const override
                {
                    return "armarx::exceptions::local::eNullPointerException";
                }
            };
            struct eNotInitialized : LocalException
            {
                eNotInitialized(): LocalException() {}
                std::string name() const override
                {
                    return "armarx::exceptions::local::eNotInitialized";
                }
            };

            struct eStateAlreadyLeft : LocalException
            {
                eStateAlreadyLeft() : LocalException("The state was already left!") {}
                std::string name() const override
                {
                    return "armarx::exceptions::local::eStateAlreadyLeft";
                }

            };

            struct eUnknownParameter : LocalException
            {
                eUnknownParameter(const std::string& stateName, const std::string& paramName, const std::string hint = "") : LocalException("The state parameter is unknown at this point! State: " + stateName + " parameter: " + paramName + "\n" + hint) {}
                std::string name() const override
                {
                    return "armarx::exceptions::local::eUnknownParameter";
                }

            };
        }
    }
}
