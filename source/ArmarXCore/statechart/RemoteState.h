/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once


// Statechart Includes
#include "StateBase.h"
#include "State.h"
#include "StateController.h"
#include "StatechartContext.h"
#include <ArmarXCore/interface/statechart/RemoteStateIce.h>
#include <ArmarXCore/interface/statechart/RemoteStateOffererIce.h>


// ArmarX Includes
#include <ArmarXCore/core/ManagedIceObject.h>


namespace armarx
{

    DEFINEEVENT(EvConnectionLost)

    class RemoteState;

    typedef IceInternal::Handle<RemoteState> RemoteStatePtr;

    /**
      \class RemoteState
      \ingroup StatechartGrp
      \brief This Statetype is used to create a state instance that represents a state that is located in another application.
      It is added in a parent state by calling State::addRemoteState(statename, proxyname).<br/>
      If a RemoteState is entered for the <b>first</b> time, it creates automatically a new instance
      at the remotely located RemoteStateOfferer that contains the real state. The RemoteState receives
      upon creation of the new real-state instance an unique id of this state for further communication.<br/>
      This id is automatically used in the onBreak- and onExit-functions, to communicate with the correct state.<br/>
      Except the state creation-call (which should return immediately) all remote procedure calls are async calls.

      @see RemoteStateOfferer
      */
    class RemoteState :
        virtual public RemoteStateIceBase,
        virtual public State,
        virtual public ManagedIceObject

    {
        //! Proxy to this component that is sent to the state that this RemoteState is representing
        RemoteStateIceBasePrx myProxy;

        //! Proxy to the state that this state is communicating with.
        RemoteStateOffererIceBasePrx stateOffererPrx;
        //! not used at the moment

    protected:
        //! Overridden function to redirect this call to the real state in the other application.
        bool __hasSubstates() override;
        //! Overridden function to redirect this call to the real state in the other application.
        bool __hasActiveSubstate() override;
        //! Overridden function to redirect this call to the real state in the other application.
        bool __breakActiveSubstate(const EventPtr event) override;
        //! Overridden function to redirect this call to the real state in the other application.
        void _baseOnEnter() override;
        /*! \brief Called by processEvent()-function or parentstate. Must NOT be called by user.

            Calls OnBreak() in this hierarchylevel and all sub levels.
          */
        bool _baseOnBreak(const EventPtr evt) override;
        //! Overridden function to redirect this call to the real state in the other application.
        void _baseOnExit() override;
        //void deepCopy(const StateBasePtr sourceState, bool reset = true);
        //! Overridden function to redirect this call to the real state in the other application.
        void __notifyEventBufferedDueToUnbreakableState(bool eventBuffered) override;

        void refetchSubstates() override;

        void __updateGlobalStateIdRecursive() override;

    public:
        RemoteState();
        RemoteState(const RemoteState& source);
        RemoteState& operator=(const RemoteState& source);
        ~RemoteState() override;

        void setStateName(const std::string& stateName);
        void setProxyName(const std::string& proxyName);

        StateBasePtr clone() const override ;
        StateBasePtr createEmptyCopy() const override;
        // inherited from Component
        std::string getDefaultName() const override;
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        void setOutput(const::armarx::StringVariantContainerBaseMap& properties, const::Ice::Current& c = ::Ice::Current());
        void remoteProcessBufferedEvents(const ::Ice::Current& c = ::Ice::Current()) override;
        void remoteProcessEvent(const EventBasePtr& evt, bool buffered, const ::Ice::Current& c = ::Ice::Current()) override;
        void remoteEnqueueEvent(const EventBasePtr& evt, const ::Ice::Current& c = ::Ice::Current()) override;
        void remoteFinalize(const StringVariantContainerBaseMap& properties, const EventBasePtr& event, const ::Ice::Current& c = ::Ice::Current()) override;
        void remoteRefetchSubstates(const ::Ice::Current& c = ::Ice::Current());
        ::Ice::Int getRemoteUnbreakableBufferSize(const ::Ice::Current& = ::Ice::Current()) const override;
        bool getRemoteUnbreakableBufferStati(const ::Ice::Current& = ::Ice::Current()) const override;
        StateIceBasePtr getParentStateLayout(const Ice::Current&) const override;
        StateParameterMap getInputParameters() override;
        StateParameterMap& getOutputParameters() override;

        //! Function that retrieves a copy of the remoteState. Calling functions or setting members on this instance won't have any effect.
        StateBasePtr getRemoteStatePtr();
        friend class DynamicRemoteState;
        bool waitForInitialization(int timeoutMS) const override;
        bool isInitialized() const override;
    };

}
