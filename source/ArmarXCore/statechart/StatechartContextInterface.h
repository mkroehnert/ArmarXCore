/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <Ice/Handle.h>
#include <ArmarXCore/interface/observers/VariantBase.h>

namespace armarx
{

    class DatafieldRef;
    typedef IceInternal::Handle<DatafieldRef> DatafieldRefPtr;

    class ChannelRef;
    typedef IceInternal::Handle<ChannelRef> ChannelRefPtr;



    class DataFieldIdentifier;
    class DataFieldIdentifierBase;
    typedef IceInternal::Handle<DataFieldIdentifierBase> DataFieldIdentifierBasePtr;
    typedef ::std::vector< ::armarx::DataFieldIdentifierBasePtr> DataFieldIdentifierBaseList;


    class StatechartContextInterface
    {
    public:
        virtual TimedVariantBaseList getDataListFromObserver(std::string observerName, const DataFieldIdentifierBaseList& identifierList) = 0;
        virtual VariantBasePtr getDataFromObserver(const DataFieldIdentifierBasePtr& identifier) = 0;
        virtual ChannelRefPtr getChannelRef(const std::string& observerName, const std::string& channelName) = 0;
        virtual DatafieldRefPtr getDatafieldRef(const DataFieldIdentifier& datafieldIdentifier) = 0;
        virtual DatafieldRefPtr getDatafieldRef(ChannelRefPtr channelRef, const std::string& datafieldName) = 0;
    };

}
