/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once
#include "../State.h"

#include <ArmarXCore/observers/variant/ChannelRef.h>

namespace armarx
{
    DEFINEEVENT(EvCounterFulfilled)
    DEFINEEVENT(EvCounterNotFulfilled)


    /**
     * @class CounterStateTemplate
     *
     * Stateimplementation, that increments a given counter by one
     * on the SystemObserver and installs 2 conditions with different events,
     * which trigger on Counter-Threshold-Reached and Counter-Threshold-Not-Reached.
     *
     * You can pass your own events to this State as Template parameters.
     * The first template parameter is the Event, that should be send on
     * Counter-Threshold-Reached and the second template parameter is the Event,
     * that should be send on Counter-Threshold-Not-Reached.
     *
     * @note If you want to use the Standardevents, just use the typedef
     * @ref CounterState.
     * @see @ref CounterState
     */
    template <class EventTypeConditionNotFulfilled = EvCounterNotFulfilled, class EventTypeConditionFulfilled = EvCounterFulfilled>
    struct CounterStateTemplate :
        StateTemplate< CounterStateTemplate<EventTypeConditionNotFulfilled, EventTypeConditionFulfilled> >
    {
        void defineParameters()
        {
            this->addToInput("counterRef", VariantType::ChannelRef, false);
            this->addToInput("counterThreshold", VariantType::Int, false);

            this->addToOutput("counterValue", VariantType::Int, false);
        }

        void onEnter()
        {

            DataFieldIdentifierPtr counter = State::getInput<ChannelRef>("counterRef")->getDataFieldIdentifier("value");
            Literal notFullfilled(*counter,
                                  "smaller",
                                  Literal::createParameterList(State::getInput<int>("counterThreshold")));


            this->getContext()->systemObserverPrx->incrementCounter(State::getInput<ChannelRef>("counterRef"));
            cond1 = State::installCondition<EventTypeConditionNotFulfilled>(notFullfilled, "Counter maximum not reached");
            cond2 = State::installCondition<EventTypeConditionFulfilled>(!notFullfilled, "Counter maximum reached");

        }


        void onExit()
        {
            State::removeCondition(cond1);
            State::removeCondition(cond2);
            this->setOutput("counterValue",  *State::getInput<ChannelRef>("counterRef")->getDataField("value"));
        }
    private:
        ConditionIdentifier cond1, cond2;
    };

    /**
     * @ingroup StatechartGrp
     * @brief CounterState is a typedef for the CounterStateTemplate, that uses
     * the events EvCounterNotFulfilled and EvCounterFulfilled.
     *
     * @see CounterStateTemplate
     */
    typedef CounterStateTemplate<EvCounterNotFulfilled, EvCounterFulfilled> CounterState;
}

