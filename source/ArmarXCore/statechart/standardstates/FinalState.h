/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once
#include "../StateTemplate.h"
#include "../StateBase.h"
#include "../RemoteState.h"
#include "../StateUtilFunctions.h"

namespace armarx
{
    /**
      \class FinalStateBase
      \ingroup StatechartGrp
      Base class for all Finalstates. Finalstates automatically send in onEnter()
      an event to the grandparent state that triggers the parent state to be left.<br/>
      Before sending the event the output-dictionary of the parentstate is filled.
      The input-dictionary of the FinalState is copied from the output-dictionary
      of the parent state before StateBase::onEnter().
      To map from the output of the previous state to the output of the parent state,
      the mapping has to be specified for the transition between FinalState and
      the previous state.<br/>
      @Note onExit() <b>will not</b> be called in this state.
      @see FinalState
      */
    template <class EventType, class StateType>
    class FinalStateBase :
        virtual public StateTemplate<StateType>
    {
        //! \brief Function to signal the parent state that an end/final-state of this substatemachine is reached.
        //! This function gets called bei derived types of FinalStateBase.<br/>
        //! The FinalStateBase's output and the event parameters are mapped into the parent's output with a '*'=>'*' mapping.
        void signalFinish(const EventPtr event)
        {
            //send to parent of parent to induce transition
            if (this->__parentState)
            {

                StringVariantContainerBaseMap setOutputValues = StateUtilFunctions::getSetValues(this->getOutputParameters());
                createMapping()
                ->mapFromOutput("*")
                ->mapFromEvent("*")
                ->_addSourceDictionary(eOutput, setOutputValues)
                ->_addSourceDictionary(eEvent, event->properties)
                ->_applyMapping(this->__parentState->outputParameters);

                EventPtr ev = new Event(this->__parentState->stateName, event->eventName);
                ev->properties = event->properties;
                this->__getParentState()->__substatesFinished(ev);
            }
            else   //if no parent of parent exists, statemachine has reached its endpoint
            {

            }

        }
    protected:
        void defineParameters() override
        {
            StatePtr parent = StatePtr::dynamicCast(StateController::__getParentState());
            StateUtilFunctions::copyDictionary(parent->getOutputParameters(), this->StateIceBase::inputParameters);

            // make all input parameters optional, because the output paramters
            // of the parent state might be set in onEnter of the parent state
            StateParameterMap::iterator it = this->StateIceBase::inputParameters.begin();

            for (; it != this->StateIceBase::inputParameters.end(); it++)
            {
                it->second->optionalParam = true;
            }

        }

        void onEnter() override
        {

            StateUtilFunctions::copyDictionary(State::getInputParameters(), State::getOutputParameters());

            signalFinish(eventToSend);

        }


    public:
        FinalStateBase()
        {
            this->stateType = eFinalState;
            this->unbreakable = false;
            this->greedyInputDictionary = false;
            this->eventToSend = StateUtility::createEvent<EventType>();
        }
        //! Overridden function, which always throws an exception, to forbid the usage of this function here
        template <class derivedClass> StatePtr addState(std::string stateName)
        {
            throw exceptions::local::eStatechartLogicError("You cannot add substates to a final state!");

            return nullptr;
        }
        //! Overridden function, which always throws an exception, to forbid the usage of this function here
        RemoteStatePtr addRemoteState(std::string stateName, std::string proxyName, std::string instanceName = "") override
        {
            throw exceptions::local::eStatechartLogicError("You cannot add substates to a final state!");

            return nullptr;
        }

    protected:
        //! Event that is automatically sent when this state is entered
        EventPtr eventToSend;

        template <class Event>
        friend class FinalState;
    };

    DEFINEEVENT(EvDummy)
    /** \brief This is the standard implementation of FinalStateBase. It should
     * be used, if no additional functionality is needed. The template parameter
     * EventType specifies the event that is automatically send, when this state
     * is entered.
     *
     * @see FinalStateBase
     */
    template <class EventType = EvDummy>
    class FinalState : public FinalStateBase<EventType, FinalState<EventType> >
    {

    public:
        /**
         * @brief createState creates a finalstate instance with the specified
         * event type.
         * This function is needed if the EventType is only known at runtime.
         * @param event EventType that is automatically send in StateBase::onEnter()
         * @return new instance to the FinalState
         */
        static StatePtr createState(const EventPtr& event)
        {
            IceInternal::Handle< FinalStateBase<EventType, FinalState<EventType> > > state = new FinalState<EvDummy>();
            state->setStateName(event->eventName);
            state->eventToSend = event;
            return state;
        }
    };

    // Definition of the event that the SuccessState sends to the grandparent state
    DEFINEEVENT(Success)
    /**
    *\typedef armarx::SuccessState
    *\ingroup StatechartGrp
    *State that automatically signals "Success" to the parent state.
    * See class FinalStateBase for more information.
    */
    typedef FinalState<Success> SuccessState;

    // Definition of the event that the FailureState sends to the grandparent state
    DEFINEEVENT(Failure)

    /**
    *  \typedef armarx::FailureState
    *  \ingroup StatechartGrp
    *  State that automatically signals "Failure" to the parent state.
    *  See class FinalStateBase for more information.
      */
    typedef FinalState<Failure> FailureState;

}

