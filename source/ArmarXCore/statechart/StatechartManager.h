/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core::Statechart
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/system/Synchronization.h>


#include "StateController.h"
#include "StateBase.h"
#include "State.h"


namespace armarx
{
    class StatechartManager :
        virtual public IceUtil::Shared,
        virtual public Logging
    {
    public:
        StatechartManager();
        ~StatechartManager() override;

        /**
         * @brief setToplevelState sets the toplevel state of this manager.<br/>
         *
         * This state will be started in start().
         *
         * @see start()
         * @param newToplevelState Pointer to the toplevel state
         * @param startParameters Parameters that should be given to the toplevel
         * state on startup.
         * @return returns true if the toplevelstate was successfully set.
         * Returns false if the StatechartManager is already running.
         */
        bool setToplevelState(const armarx::StatePtr& newToplevelState, StringVariantContainerBaseMap startParameters = StringVariantContainerBaseMap());
        StatePtr getToplevelState() const;

        enum StatechartManagerState
        {
            eConstructed,
            eRunning,
            eShutDown
        };

        void start(bool enterToplevelState = true);
        void shutdown();

        bool isRunning() const;
        bool isShutdown() const;
        StatechartManagerState getManagerState() const;

        /**
         * @brief wakeUp signals the event processing thread to wake up and
         * check for new events or shutdown conditions
         */
        void wakeUp();

        bool addEvent(const EventPtr& newEvent, const StateControllerPtr& receivingState);
        template <typename EventType>
        bool addEvent(const StateControllerPtr& receivingState)
        {
            EventPtr event = new EventType(StateIceBasePtr::dynamicCast(receivingState)->stateName);
            return addEvent(event, receivingState->__getParentState());
        }

    protected:
        virtual void processEvents();
    private:
        struct EventProcessingData
        {
            EventPtr event;
            StateControllerPtr receivingState;
        };

        bool checkEvent(const EventProcessingData& eventData) const;

        RunningTask<StatechartManager>::pointer_type runningTask;



        std::list<EventProcessingData> eventQueue;

        mutable Mutex eventQueueMutex;
        boost::condition_variable idleCondition;


        StatechartManagerState managerState;
        mutable HiddenTimedMutex managerStateMutex;


        StatePtr toplevelState;
        StringVariantContainerBaseMap startParameters;
        //        HiddenTimedMutex runningTaskMutex;


    };
    typedef IceUtil::Handle<StatechartManager> StatechartManagerPtr;

}
