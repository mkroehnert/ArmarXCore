/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/DynamicLibrary.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>
//#include <ArmarXCore/statechart/StatechartContext.h>
//#include "XMLRemoteStateOfferer.h"

#define STATEOFFERERSUFFIX "RemoteStateOfferer"

namespace armarx
{

    struct XMLStateOffererFactoryBase;
    typedef  IceInternal::Handle<XMLStateOffererFactoryBase > XMLStateOffererFactoryBasePtr;

    class XMLStateComponentProperties :
        public ComponentPropertyDefinitions
    {
    public:
        XMLStateComponentProperties(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("XMLStatechartGroupDefinitionFile", "Path to statechart group definition file (*.scgxml) - relative to projects' source dir");
            defineOptionalProperty<std::string>("XMLStatechartProfile", "", "Profile to use for state execution. Should be the same for all Statechart Groups in one scenario. This profile name is also added as a prefix to the Ice identifier of the RemoteStateOfferer of this statechart group.");
            defineOptionalProperty<std::string>("XMLRemoteStateOffererName", "", "Name of the RemoteStateOfferer to start. The default name is [StatechartGroupName]RemoteStateOfferer.");
            defineOptionalProperty<std::string>("StatesToEnter", "", "List of states that are directly entered and executed. Seperated by comma. These must not need any input parameters!");
            defineOptionalProperty<std::string>("StateReportingTopic", "StateReportingTopic", "Topic on which state changes are published. Leave empty to disable reporting.");
            //defineOptionalProperty<std::string>("XMLStatechartProfileDefinitionFile", "", "Path to StatechartProfiles-<ProjectName>.xml file.");
        }
    };

    /**
     * @defgroup Component-XMLStateComponent XMLStateComponent
     * @ingroup ArmarXCore-Components StatechartGrp
     * This component is the managing component for XML statechart groups offered with XMLRemoteStateOfferer.
     * This component is used in the application *XMLRemoteStateOfferer*.
     * It loads a statechart group from a XML file (defined in property XMLStatechartGroupDefinitionFile) and
     * offers all contained public states via the XMLRemoteStateOfferer.
     * The XMLRemoteStateOfferer is the remote interface to all public states of a statechart group.
     *
     * **Configuration:**
     *
     * Two properties of the XMLStateComponent are essential: *XMLStatechartGroupDefinitionFile* and *XMLStatechartProfile*.<br/>
     * *XMLStatechartGroupDefinitionFile* is a required property and defines which statechart
     * group (*.scgxml) is to be loaded. All public states
     * will be available after start up for remote access.
     * *XMLStatechartProfile* is an optional property which defines which \ref statechartprofiles "configuration profile"
     * a statechart group should be used.
     * This is usually a robot specific profile like *Armar3Simulation*.
     * This profile contains two types of information:<br/>
     *  - information about the default parameters of each state
     *  - default configuration for the XMLRemoteStateOfferer, which consists mostly of
     * proxy names like the KinematicUnit. This configuration can be overwritten
     * with config files attached to the XMLStateComponent app.
     *    The profile name is also the prefix for the XMLRemoteStateOfferer network identifier,
     *  e.g. *Armar3SimulationGraspingGroupRemoteStateOfferer*. This way there can ba a RemoteStateOffer for each profile.
     *
     * Both types of information are stored in the statechart xml files and specified in the \ref ArmarXGui-GuiPlugins-StatechartEditorController "Statechart Editor".
     *
     * **Auto entering of states**:
     *
     * The property *StatesToEnter* specifies which *public* states are entered automatically on start up of the XMLComponent.
     * These **must not** require any input parameters.
     * It is also possible to specify multiple states by a comma-seperated list, which then will be run in parallel.
     * (Though, event processing is not parallel.)
     *
     * **Configuration of a multi robot statechart setup:**
     *
     * It is easily possible to configure a statechart setup for multiple robots. You can just start multiple XMLStateComponent apps,
     * choose a different profile for each app and a unique name for each of the XMLStateComponents (property *ArmarX.XMLStateComponent.objectName*).
     *
     * An example could look like this:
     * \verbatim
       config1.cfg:
       ArmarX.XMLStateComponent.XMLStatechartGroupDefinitionFile = RobotSkillTemplates/statecharts/BringObjectGroup/BringObjectGroup.scgxml
       ArmarX.XMLStateComponent.ObjectName = "XMLStateComponentBringObjectGroupArmar3a"
       ArmarX.XMLStateComponent.XMLStatechartProfile=Armar3a

       config2.cfg:
       ArmarX.XMLStateComponent.XMLStatechartGroupDefinitionFile = RobotSkillTemplates/statecharts/BringObjectGroup/BringObjectGroup.scgxml
       ArmarX.XMLStateComponent.ObjectName = "XMLStateComponentBringObjectGroupArmar3b"
       ArmarX.XMLStateComponent.XMLStatechartProfile=Armar3b
       \endverbatim
     * \note This does not work for usage of two robot with the same profile. Then the properties for
     * the XMLRemoteStateOfferer need to be specified in the .cfg file as well.
     *
     * @class XMLStateComponent
     * @ingroup Component-XMLStateComponent
     */
    class XMLStateComponent :
        public Component
    {
    public:
        XMLStateComponent();

        // PropertyUser interface
        PropertyDefinitionsPtr createPropertyDefinitions() override;


        // ManagedIceObject interface
        bool loadLib(std::string libPath);
    protected:
        void onInitComponent() override;
        void onConnectComponent() override;
        std::string getDefaultName() const override;
        void createAndEnterInstance(std::string stateClassName);

        std::map<std::string, DynamicLibraryPtr> libraries;
        std::string uuid;
        std::string offererName;
        XMLStateOffererFactoryBasePtr offerer;
        StatechartProfilesPtr profiles;
        StatechartProfilePtr selectedProfile;
    };

}

