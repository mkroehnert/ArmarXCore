/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "GroupXmlReader.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/DoxTable.h>
#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/DoxDoc.h>

namespace armarx
{

    class StatechartGroupDocGenerator
    {
        struct StateInfo
        {
            StateInfo(std::string package, std::string group, std::string state)
                : package(package), group(group), state(state) {}
            std::string package, group, state;
            std::string toString() const
            {
                return package + "-" + group + "-" + state;
            }
        };

    public:
        StatechartGroupDocGenerator();
        std::string generateDocString(const StatechartGroupXmlReaderPtr& reader) const;
        void generateDoxygenFile(const std::string& groupDefinitionFilePath);
        void generateDoxygenFiles(const std::vector<std::string>& groups);

        void setOutputpath(const std::string& outputPath);

        static std::vector<std::string> FindAllStatechartGroupDefinitions(const boost::filesystem::path& path);

        CMakePackageFinder getFinder(const StatechartGroupXmlReaderPtr& reader);

        std::set<std::string> getUsedProfiles(const RapidXmlReaderNode& stateNode) const;
        void buildReferenceIndex(const std::vector<std::string>& groups);
    protected:
        //std::map<std::string, StatechartGroupXmlReaderPtr> readers;
        std::map<std::string, CMakePackageFinder> finders;
        std::string outputPath;
        std::map<std::string, StateInfo> uuidToStateInfoMap;
        std::map<std::string, std::set<std::string>> usageMap;
    private:
        DoxTablePtr buildParameterTable(RapidXmlReaderNode parametersNode) const;
        void addParameterTable(const DoxDocPtr& doc, const RapidXmlReaderNode& stateNode, std::string name, const char* nodeName) const;
        std::string unescapeString(std::string str) const;
        std::string nToBr(std::string str) const;
        std::string fmt(const std::string& fmt, const std::string& arg1) const;
        std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2) const;
        std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3) const;
        std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3, const std::string& arg4) const;
        std::string getNodeAttrsFromStateType(const std::string& nodeType) const;
    };

} // namespace armarx

