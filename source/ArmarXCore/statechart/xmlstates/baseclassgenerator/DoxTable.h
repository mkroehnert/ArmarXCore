/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>
#include "DoxEntry.h"


namespace armarx
{
    class DoxTable;
    typedef boost::shared_ptr<DoxTable> DoxTablePtr;


    class DoxTable : public DoxEntry
    {
    public:
        DoxTable(const std::vector<std::string>& header);
        void addRow(const std::vector<std::string>& row);
        int rows();
        std::string getDoxString() override;
    private:
        std::vector<std::string> header;
        std::vector<std::vector<std::string>> cells;
    };
}

