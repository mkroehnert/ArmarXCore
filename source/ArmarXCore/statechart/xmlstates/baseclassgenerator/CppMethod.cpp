/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CppMethod.h"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

using namespace armarx;

CppMethod::CppMethod(const std::string& header, const std::string& doc)
{
    this->compact = false;
    this->header = header;
    this->doc = doc;
    this->block.reset(new CppBlock());
}

void CppMethod::writeCpp(CppWriterPtr writer)
{
    if (!doc.empty())
    {
        std::string delimiters = "\n";
        std::vector<std::string> doclines;
        boost::split(doclines, doc, boost::is_any_of(delimiters));
        writer->line("/**");

        for (auto& line : doclines)
        {
            writer->line(" * " + line);
        }

        writer->line(" */");
    }
    if (isPureVirtual)
    {
        if (header.find("virtual") == std::string::npos)
        {
            writer->line("virtual " + header + " = 0;");
        }
        else
        {
            writer->line(header + " = 0;");
        }
    }
    else
    {
        if (compact)
        {
            writer->line(header + " " + block->getAsSingleLine());
        }
        else
        {
            writer->line(header);
            block->writeCpp(writer);
        }
    }
}

void CppMethod::addLine(const std::string& line)
{
    block->addLine(line);
}

void CppMethod::addLine(const boost::basic_format<char>& line)
{
    block->addLine(line);
}

void CppMethod::setCompact(bool compact)
{
    this->compact = compact;
}

bool CppMethod::getIsPureVirtual() const
{
    return isPureVirtual;
}

void CppMethod::setIsPureVirtual(bool value)
{
    isPureVirtual = value;
}
