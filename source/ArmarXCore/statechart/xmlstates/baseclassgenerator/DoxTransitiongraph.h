/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>
#include "DoxEntry.h"

namespace armarx
{
    class DoxTransition;

    typedef boost::shared_ptr<DoxTransition> DoxTransitionPtr;

    class DoxTransition
    {
    public:
        DoxTransition(std::string from, std::string to, std::string eventName);
        std::string toString();
    private:
        std::string from;
        std::string to;
        std::string eventName;
    };

    class DoxTransitionGraphNode;
    typedef boost::shared_ptr<DoxTransitionGraphNode> DoxTransitionGraphNodePtr;

    class DoxTransitionGraphNode
    {
    public:
        DoxTransitionGraphNode(const std::string& name, const std::string& attrs);
        std::string toString();
    private:
        std::string name;
        std::string attrs;
    };

    class DoxTransitionGraph;

    typedef boost::shared_ptr<DoxTransitionGraph> DoxTransitionGraphPtr;

    class DoxTransitionGraph : public DoxEntry
    {
    public:
        DoxTransitionGraph();
        void addTransition(DoxTransitionPtr transition);
        std::string getDoxString() override;
        void addTransition(const std::string& from, const std::string& to, const std::string& eventName);
        void addNode(const std::string& name, const std::string& attrs);
        int transitionCount();
    private:
        std::vector<DoxTransitionPtr> transitions;
        std::vector<DoxTransitionGraphNodePtr> nodes;

    };
}



