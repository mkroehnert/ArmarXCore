/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "XmlContextBaseClassGenerator.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include "../GroupXmlReader.h"
using namespace armarx;
using namespace boost;
#include <tuple>

XmlContextBaseClassGenerator::XmlContextBaseClassGenerator()
{

}


std::tuple<std::string, std::string> XmlContextBaseClassGenerator::GenerateCpp(std::vector<std::string> namespaces, std::vector<std::string> proxies, std::string groupName, VariantInfoPtr variantInfo, const std::set<std::string>& usedVariantTypes)
{
    CppClassPtr cppClass, cppClassBase;
    std::tie(cppClass, cppClassBase) = BuildClass(namespaces, proxies, groupName, variantInfo, usedVariantTypes);
    CppWriterPtr writer(new CppWriter());
    cppClass->writeCpp(writer);
    CppWriterPtr writerBase(new CppWriter());
    cppClassBase->writeCpp(writerBase);
    return std::make_tuple(writer->getString(), writerBase->getString());
}

std::tuple<CppClassPtr, CppClassPtr> XmlContextBaseClassGenerator::BuildClass(std::vector<std::string> namespaces, std::vector<std::string> proxies, std::string groupName, VariantInfoPtr variantInfo, const std::set<std::string>& usedVariantTypes)
{
    std::string className = groupName + "StatechartContext";

    for (std::string& ns : namespaces)
    {
        ns = boost::replace_all_copy(ns, "-", "_");
    }

    CppClassPtr cppImplClass(new CppClass(namespaces, className));
    CppClassPtr cppBaseClass(new CppClass(namespaces, className + "Base"));

    std::stringstream ss;
    ss << "_ARMARX_COMPONENT_";

    for (auto ns : namespaces)
    {
        ss << boost::to_upper_copy(ns) << "_";
    }

    ss << boost::to_upper_copy(className) << "_H";
    cppImplClass->setIncludeGuard(ss.str());

    ss << "_BASE";
    cppBaseClass->setIncludeGuard(ss.str());
    cppImplClass->addInherit("virtual public StatechartContext");
    cppImplClass->addInherit("public " + cppBaseClass->getName());

    cppImplClass->addInclude("<ArmarXCore/core/Component.h>");
    cppImplClass->addInclude("<ArmarXCore/core/system/ImportExportComponent.h>");
    cppImplClass->addInclude("<ArmarXCore/statechart/StatechartContext.h>");
    cppImplClass->addInclude("\"" + className + "Base.generated.h\"");
    cppBaseClass->addInclude("<ArmarXCore/core/system/ImportExportComponent.h>");

    std::vector<VariantInfo::ProxyEntryPtr> proxyEntries;

    for (std::string proxyId : proxies)
    {
        VariantInfo::ProxyEntryPtr p = variantInfo->getProxyEntry(proxyId);

        if (p)
        {
            proxyEntries.push_back(p);
        }
    }

    CppClassPtr propertiesClass = cppImplClass->addInnerClass("PropertyDefinitions");
    propertiesClass->addInherit("public StatechartContextPropertyDefinitions");

    CppCtorPtr propertiesCtor = propertiesClass->addCtor("std::string prefix");
    propertiesCtor->addInitListEntry("StatechartContextPropertyDefinitions", "prefix");

    CppMethodPtr getDefaultName = cppImplClass->addMethod("std::string getDefaultName() const override");
    CppMethodPtr onInit = cppImplClass->addMethod("void onInitStatechartContext() override");
    CppMethodPtr onConnect = cppImplClass->addMethod("void onConnectStatechartContext() override");

    getDefaultName->addLine(fmt("return \"%s\";", className));

    for (VariantInfo::ProxyEntryPtr p : proxyEntries)
    {
        std::string description = fmt("Name of the %s that should be used", boost::to_lower_copy(magicNameSplit(p->getPropertyName())));

        if (p->getPropertyIsOptional())
        {
            propertiesCtor->addLine(fmt("defineOptionalProperty<std::string>(\"%s\", \"%s\", \"%s\");", p->getPropertyName(), p->getPropertyDefaultValue(), description));
        }
        else
        {
            propertiesCtor->addLine(fmt("defineRequiredProperty<std::string>(\"%s\", \"%s\");", p->getPropertyName(), description));
        }


        if (p->getProxyType() == VariantInfo::SingleProxy)
        {
            onInit->addLine(fmt("usingProxy(getProperty<std::string>(\"%s\").getValue());", p->getPropertyName()));
            onConnect->addLine(fmt("%s = getProxy<%s>(getProperty<std::string>(\"%s\").getValue());", p->getMemberName(), p->getTypeName(), p->getPropertyName()));
        }
        else if (p->getProxyType() == VariantInfo::Topic)
        {
            onInit->addLine(fmt("offeringTopic(getProperty<std::string>(\"%s\").getValue());", p->getPropertyName()));
            onConnect->addLine(fmt("%s = getTopic<%s>(getProperty<std::string>(\"%s\").getValue());", p->getMemberName(), p->getTypeName(), p->getPropertyName()));
        }
        else
        {
            throw LocalException("Not supported ProxyType");
        }

        cppImplClass->addPrivateField(fmt("%s %s;", p->getTypeName(), p->getMemberName()));
        std::string proxyFunc = fmt("const %s& %s() const", p->getTypeName(), p->getGetterName());
        CppMethodPtr virtualGetProxy = cppBaseClass->addMethod(proxyFunc);
        virtualGetProxy->setIsPureVirtual(true);
        CppMethodPtr getProxy = cppImplClass->addMethod(proxyFunc + " override");
        getProxy->addLine(fmt("return %s;", p->getMemberName()));
        getProxy->setCompact(true);
        cppBaseClass->addInclude(fmt("<%s>", p->getIncludePath()));

    }

    // special custom proxy code
    for (VariantInfo::ProxyEntryPtr p : proxyEntries)
    {
        for (std::string member : p->getMembers())
        {
            cppImplClass->addPrivateField(member);
        }

        for (std::string include : p->getIncludes())
        {
            cppBaseClass->addInclude(fmt("<%s>", include));
        }

        for (std::string oni : p->getOnInit())
        {
            onInit->addLine(oni);
        }

        for (std::string onc : p->getOnConnect())
        {
            onConnect->addLine(onc);
        }

        for (std::pair<std::string, std::string> method : p->getMethods())
        {
            cppImplClass->addMethod(method.first + " override")->addLine(method.second);
            cppBaseClass->addMethod(method.first)->setIsPureVirtual(true);
        }
    }

    CppMethodPtr createPropertyDefinitions = cppImplClass->addMethod("PropertyDefinitionsPtr createPropertyDefinitions() override");
    createPropertyDefinitions->addLine(fmt("return PropertyDefinitionsPtr(new %s::PropertyDefinitions(getConfigIdentifier()));", className));

    if (usedVariantTypes.size() > 0)
    {
        // Add special method that uses all non-basic types to force compiler/linker to include/load all libraries in binary (ensure GCC does not optimise it out).
        CppMethodPtr forceLibLoading(new CppMethod("void __attribute__((used)) __forceLibLoading()"));
        forceLibLoading->addLine("// Do not call this method.");
        forceLibLoading->addLine("// The sole purpose of this method is to force the compiler/linker to include all libraries.");

        int typeNr = 1;
        std::set<std::string> includes;
        for (std::string innerNonBasicVariantDataType : usedVariantTypes)
        {
            VariantInfo::LibEntryPtr lib = variantInfo->findLibByVariant(innerNonBasicVariantDataType);

            ARMARX_CHECK_EXPRESSION_W_HINT(lib, innerNonBasicVariantDataType);
            for (auto& include : lib->getFactoryIncludes())
            {
                includes.insert(include);
            }
            for (auto& include : lib->getVariantIncludes(innerNonBasicVariantDataType))
            {
                includes.insert(include);
            }
            std::string implementationType = variantInfo->getDataTypeName(innerNonBasicVariantDataType);
            forceLibLoading->addLine(fmt("armarx::GenericFactory< %s, %s> ().create(%s::ice_staticId());", implementationType, implementationType, implementationType));
            typeNr++;
        }
        for (auto& include : includes)
        {
            cppImplClass->addInclude("<" + include + ">");
        }

        cppImplClass->addMethod(forceLibLoading);
    }

    return std::make_tuple(cppImplClass, cppBaseClass);
}

std::string XmlContextBaseClassGenerator::magicNameSplit(std::string name)
{
    if (boost::algorithm::ends_with(name, "Name"))
    {
        name = name.substr(0, name.size() - 4);
    }

    boost::regex re("([A-Z][a-z])");
    name = boost::regex_replace(name, re, " \\1");
    boost::to_lower(name);
    boost::trim(name);
    return name;
}

std::string XmlContextBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1)
{
    return str(format(fmt) % arg1);
}

std::string XmlContextBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2)
{
    return str(format(fmt) % arg1 % arg2);
}

std::string XmlContextBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3)
{
    return str(format(fmt) % arg1 % arg2 % arg3);
}
