/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "DoxTable.h"
#include <sstream>

using namespace armarx;


DoxTable::DoxTable(const std::vector<std::string>& header)
{
    this->header = header;
}

void DoxTable::addRow(const std::vector<std::string>& row)
{
    cells.push_back(row);
}

int DoxTable::rows()
{
    return cells.size();
}

std::string DoxTable::getDoxString()
{
    std::stringstream ss;

    /*ss << "| a | b | c |\n"
       << "|---|---|---|\n"
       << "| 1 | 2 | 3 |\n"
       << "| 4 | 5 | 6 |\n";*/
    ss << "@htmlonly\n";
    ss << "<table  class=\"doxtable\">\n";
    ss << "  <tr>\n";

    for (std::string s : header)
    {
        ss << "    <th>" << s << "</th>\n";

    }

    ss << "  </tr>\n";

    for (std::vector<std::string> row : cells)
    {
        ss << "  <tr>\n";

        for (std::string cell : row)
        {
            ss << "    <td>" << cell << "</td>\n";
        }

        ss << "  </tr>\n";
    }

    ss << "</table>\n";

    ss << "@endhtmlonly";

    return ss.str();
    /*
    std::stringstream ss_header;
    std::stringstream ss_line;


    for (std::string s : header)
    {
        ss_header << "|" << s << "|";
        ss_line << "|-";

    }
    ss_header << "|\n";
    ss_line << "|\n";

    std::stringstream ss;
    ss << ss_header.str() << ss_line.str();


    for (std::vector<std::string> row : cells)
    {
        for (std::string cell : row)
        {
            ss << "| " << cell << " ";
        }
        ss << "|\n";
    }

    ss << "@endhtmlonly";

    return ss.str();*/

}
