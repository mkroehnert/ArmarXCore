/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include "../StateBase.h"
#include "profiles/StatechartProfiles.h"

#include <boost/filesystem/path.hpp>


namespace armarx
{
    class StatechartGroupXmlReader
    {
    public:
        enum StateVisibility
        {
            ePrivate,
            ePublic
        };

        StatechartGroupXmlReader(const StatechartProfilePtr& selectedProfile);

        void readXml(const boost::filesystem::path& groupDefinitionFile);
        //void readXml(const std::string &groupDefinitionXMLString);
        void readXml(RapidXmlReaderPtr reader);

        boost::filesystem::path getGroupDefinitionFilePath() const;
        std::string getGroupName() const
        {
            return groupName;
        }
        std::vector<std::string> getStateFilepaths() const
        {
            return allstateFilePaths;
        }
        std::string getDescription() const;
        std::string getPackageName() const;
        bool contextGenerationEnabled() const;
        StateVisibility getStateVisibility(const std::string& filepath) const;
        int getStateNestingLevel(std::string filepath) const;
        std::vector<std::string> getProxies() const;

        StatechartProfilePtr getSelectedProfile() const;

        std::string getConfigurationFileContent() const;

    private:
        void ReadChildren(RapidXmlReaderNode xmlNode, const boost::filesystem::path& path, int nesting);
        std::vector<std::string> allstateFilePaths;
        boost::filesystem::path basePath;
        boost::filesystem::path groupDefinitionFile;
        std::string groupName;
        std::string packageName;
        std::string description;
        bool generateContext;
        std::vector<std::string> proxies;
        std::map<std::string, StateVisibility> stateVisibilityMap;
        std::map<std::string, int> stateNestingMap;
        StatechartProfilePtr selectedProfile;
        std::map<std::string, std::string> profileConfigurations;

    };
    typedef boost::shared_ptr<StatechartGroupXmlReader> StatechartGroupXmlReaderPtr;
}

