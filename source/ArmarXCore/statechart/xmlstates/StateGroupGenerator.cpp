/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StateGroupGenerator.h"

#include <fstream>
#include <time.h>
#include <algorithm>

#include "baseclassgenerator/XmlStateBaseClassGenerator.h"
#include <ArmarXCore/core/exceptions/local/FileIOException.h>
#include <ArmarXCore/core/util/algorithm.h>

#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/XmlContextBaseClassGenerator.h>
#include "baseclassgenerator/XmlStateBaseClassGenerator.h"

#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/CMakeSourceListGenerator.h>

using namespace armarx;

void StatechartGroupGenerator::generateStatechartFiles(const std::string& statechartsPath)
{
    std::vector<std::string> groups;

    for (boost::filesystem::recursive_directory_iterator end, dir(statechartsPath); dir != end; ++dir)
    {
        // search for all statechart group xml files
        if (dir->path().extension() == ".scgxml" && dir->path().string().find("deprecated") == std::string::npos)
        {
            groups.push_back(dir->path().string());
            ARMARX_INFO_S << dir->path().string();
        }
    }
}

void StatechartGroupGenerator::generateStateFile(const std::string& statechartGroupXmlFilePath, const std::string& statePath, const std::string& packagePath)
{
    StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfilePtr()));
    reader->readXml(boost::filesystem::path(statechartGroupXmlFilePath));
    //ARMARX_INFO_S << reader->getPackageName();
    boost::filesystem::path buildDir = CMakePackageFinder(reader->getPackageName(), packagePath).getBuildDir();

    VariantInfoPtr variantInfo = VariantInfo::ReadInfoFilesRecursive(reader->getPackageName(), packagePath, false);

    boost::filesystem::path boostStatePath(statePath);
    generateStateFile(boostStatePath.filename().replace_extension().string(),
                      RapidXmlReader::FromFile(statePath),
                      buildDir,
                      reader->getPackageName(),
                      reader->getGroupName(),
                      reader->getProxies(),
                      reader->contextGenerationEnabled(),
                      variantInfo,
                      false);
}

bool StatechartGroupGenerator::generateStateFile(const std::string& stateName, RapidXmlReaderPtr reader, boost::filesystem::path buildDir, const std::string& packageName, const std::string& groupName, const std::vector<std::string>& proxies, bool contextGenerationEnabled, const VariantInfoPtr& variantInfo, bool forceRewrite)
{
    boost::filesystem::path outputPath = buildDir / "source" / packageName / "statecharts" / groupName / (stateName + ".generated.h");

    boost::filesystem::path dir = outputPath;
    dir.remove_filename();
    boost::filesystem::create_directories(dir);
    //ARMARX_INFO_S << "generating into " << outputPath.string();

    std::vector<std::string> namespaces({"armarx", groupName});
    std::string cpp = XmlStateBaseClassGenerator::GenerateCpp(namespaces,
                      reader,
                      proxies,
                      contextGenerationEnabled,
                      groupName,
                      variantInfo);

    time_t rawtime;
    struct tm* timeinfo;
    char buffer [80];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, 80, "%Y-%m-%d %H:%M:%S%n", timeinfo);
    writeFileContents(outputPath.string() + ".touch", std::string(buffer));

    if (forceRewrite)
    {
        writeFileContents(outputPath.string(), cpp);
        return true;
    }
    else
    {
        return writeFileContentsIfChanged(outputPath.string(), cpp);
    }

    //ARMARX_INFO_S << "written " << cpp.length() << " bytes to " << outputPath.string();
}

void StatechartGroupGenerator::generateStatechartContextFile(const std::string& statechartGroupXmlFilePath, const std::string& packagePath)
{
    StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfilePtr()));
    reader->readXml(boost::filesystem::path(statechartGroupXmlFilePath));

    if (!reader->contextGenerationEnabled())
    {
        throw LocalException("Will not generate context for ") << statechartGroupXmlFilePath << ". Context generation is not enabled for this group.";
    }

    VariantInfoPtr variantInfo = VariantInfo::ReadInfoFilesRecursive(reader->getPackageName(), packagePath, false);

    boost::filesystem::path buildDir = CMakePackageFinder(reader->getPackageName(), packagePath).getBuildDir();

    generateStatechartContextFile(buildDir,
                                  reader->getPackageName(),
                                  reader->getGroupName(),
                                  reader->getProxies(),
                                  variantInfo,
                                  getVariantTypesOfStates(reader, variantInfo),
                                  false);
}



bool StatechartGroupGenerator::generateStatechartContextFile(boost::filesystem::path buildDir, const std::string& packageName, const std::string& groupName, const std::vector<std::string>& proxies, const VariantInfoPtr& variantInfo, const std::set<std::string>& usedVariantTypes, bool forceRewrite)
{
    boost::filesystem::path outputPath = buildDir / "source" / packageName / "statecharts" / groupName;
    boost::filesystem::path baseClassPath = outputPath / (groupName + "StatechartContextBase.generated.h");
    outputPath /= (groupName + "StatechartContext.generated.h");
    boost::filesystem::path dir = outputPath;
    dir.remove_filename();
    boost::filesystem::create_directories(dir);

    //ARMARX_INFO_S << "generating into " << outputPath.string();
    std::vector<std::string> namespaces({"armarx", groupName});
    std::string cpp, cppBase;
    std::tie(cpp, cppBase) = XmlContextBaseClassGenerator::GenerateCpp(namespaces,
                             proxies,
                             groupName,
                             variantInfo,
                             usedVariantTypes);

    time_t rawtime;
    struct tm* timeinfo;
    char buffer [80];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, 80, "%Y-%m-%d %H:%M:%S%n", timeinfo);
    writeFileContents(outputPath.string() + ".touch", std::string(buffer));
    //    writeFileContents(baseClassPath.string() + ".touch", std::string(buffer));

    if (forceRewrite)
    {
        writeFileContents(outputPath.string(), cpp);
        writeFileContents(baseClassPath.string(), cppBase);
        return true;
    }
    else
    {
        bool result = writeFileContentsIfChanged(outputPath.string(), cpp);
        return writeFileContentsIfChanged(baseClassPath.string(), cppBase) || result;
    }

    //ARMARX_INFO_S << "written " << cpp.length() << " bytes to " << outputPath.string();
}

bool StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFile(const std::string& statechartGroupXmlFilePath, const std::string& packagePath, bool forceRewrite)
{
    //    ARMARX_INFO << "Groupfile: " << statechartGroupXmlFilePath;
    StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfilePtr()));
    reader->readXml(boost::filesystem::path(statechartGroupXmlFilePath));

    //    VariantInfoPtr variantInfo = VariantInfo::ReadInfoFilesRecursive(reader->getPackageName(), packagePath, false);
    auto groupName = reader->getGroupName();
    boost::filesystem::path buildDir = CMakePackageFinder(reader->getPackageName(), packagePath).getBuildDir();
    auto packageName = reader->getPackageName();
    boost::filesystem::path outputPath = buildDir / "source" / packageName / "statecharts" / groupName;
    boost::filesystem::path generatedFileName = outputPath / (groupName + "Files.generated.cmake");
    //    outputPath /= (groupName + "StatechartContext.generated.h");
    boost::filesystem::path dir = outputPath;
    dir.remove_filename();
    boost::filesystem::create_directories(dir);

    //    ARMARX_INFO_S << "generating cmake file into " << generatedFileName.string();

    Ice::StringSeq xmlFiles, headerFiles, sourceFiles;
    xmlFiles = reader->getStateFilepaths();

    VariantInfoPtr variantInfo = VariantInfo::ReadInfoFilesRecursive(reader->getPackageName(), packagePath, false);
    Ice::StringSeq libs;
    for (std::string& xmlFile : xmlFiles)
    {
        auto stateNode = RapidXmlReader::FromFile(xmlFile);
        auto types = XmlStateBaseClassGenerator::GetUsedInnerNonBasicVariantTypes(stateNode->getRoot("State"), variantInfo);
        //        ARMARX_INFO << "types: " << (Ice::StringSeq(types.begin(), types.end()));

        for (std::string& lib : variantInfo->findLibNames(Ice::StringSeq(types.begin(), types.end())))
        {
            auto libName = lib;
            if (!Contains(libs, libName))
            {
                libs.push_back(libName);
            }
        }
    }

    auto proxies = reader->getProxies();

    for (VariantInfo::LibEntryPtr lib : variantInfo->getLibs())
    {
        auto libName = lib->getName();
        for (VariantInfo::ProxyEntryPtr proxy : lib->getProxies())
        {
            std::string proxyMemberName = proxy->getMemberName();

            std::string proxyId = boost::str(boost::format("%s.%s") % libName % proxyMemberName);
            if (std::find(proxies.begin(), proxies.end(), proxyId) != proxies.end())
            {
                if (!Contains(libs, libName))
                {
                    libs.push_back(libName);
                }

            }
        }
    }

    //    ARMARX_INFO << VAROUT(libs);


    boost::filesystem::path groupFilePath(reader->getGroupDefinitionFilePath());
    boost::filesystem::path groupDir = groupFilePath.parent_path();

    for (std::string& xmlFile : xmlFiles)
    {

        auto headerFilePath = boost::filesystem::path(xmlFile).replace_extension("h");
        //        ARMARX_INFO << "Checking " << headerFilePath;
        if (boost::filesystem::exists(headerFilePath))
        {
            headerFiles.push_back("./" + ArmarXDataPath::relativeTo(groupDir.string(), headerFilePath.string()));
        }
        auto sourceFilePath = boost::filesystem::path(xmlFile).replace_extension("cpp");
        if (boost::filesystem::exists(sourceFilePath))
        {
            sourceFiles.push_back("./" + ArmarXDataPath::relativeTo(groupDir.string(), sourceFilePath.string()));
        }
        xmlFile = "./" + ArmarXDataPath::relativeTo(groupDir.string(), xmlFile);
    }







    auto cmakeFileContent = CMakeSourceListGenerator::GenerateCMakeFile(groupName, xmlFiles, sourceFiles, headerFiles, libs);

    time_t rawtime;
    struct tm* timeinfo;
    char buffer [80];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, 80, "%Y-%m-%d %H:%M:%S%n", timeinfo);
    //    writeFileContents(outputPath.string() + ".touch", std::string(buffer));
    //    writeFileContents(baseClassPath.string() + ".touch", std::string(buffer));

    if (forceRewrite)
    {
        writeFileContents(outputPath.string(), cmakeFileContent);
        return true;
    }
    else
    {
        return writeFileContentsIfChanged(generatedFileName.string(), cmakeFileContent) ;
    }

}

bool StatechartGroupGenerator::writeFileContentsIfChanged(const std::string& path, const std::string& contents)
{
    if (boost::filesystem::exists(boost::filesystem::path(path))
        && RapidXmlReader::ReadFileContents(path) == contents)
    {
        return false;
    }

    writeFileContents(path, contents);
    return true;
}

void StatechartGroupGenerator::writeFileContents(const std::string& path, const std::string& contents)
{
    std::ofstream file;
    file.open(path.c_str());

    if (!file.is_open())
    {
        throw armarx::exceptions::local::FileOpenException(path);
    }
    ARMARX_DEBUG << "Writing to " << path;
    file << contents;
    file.flush();
    file.close();
}

std::set<std::string> StatechartGroupGenerator::getVariantTypesOfStatesWithNoCpp(StatechartGroupXmlReaderPtr groupReader, const VariantInfoPtr& variantInfo)
{
    std::set<std::string> variantTypes;
    std::set<std::string> alreadyLinkedTypes;
    for (auto& statefile : groupReader->getStateFilepaths())
    {
        std::set<std::string>* currentTypeSet = &variantTypes;
        RapidXmlReaderPtr stateReader = RapidXmlReader::FromFile(statefile);
        auto headerFilePath = boost::filesystem::path(statefile);
        headerFilePath.replace_extension(boost::filesystem::path("h"));
        if (boost::filesystem::exists(headerFilePath))
        {
            currentTypeSet = &alreadyLinkedTypes;
        }
        RapidXmlReaderNode stateNode = stateReader->getRoot("State");
        std::set<std::string> types = XmlStateBaseClassGenerator::GetUsedInnerNonBasicVariantTypes(stateNode, variantInfo);
        currentTypeSet->insert(types.begin(), types.end());
    }
    std::set<std::string> results;
    std::set_difference(variantTypes.begin(), variantTypes.end(),
                        alreadyLinkedTypes.begin(), alreadyLinkedTypes.end(),
                        std::inserter(results, results.end()));
    return results;
}

std::set<std::string> StatechartGroupGenerator::getVariantTypesOfStates(StatechartGroupXmlReaderPtr groupReader, const VariantInfoPtr& variantInfo)
{
    std::set<std::string> variantTypes;

    for (auto& statefile : groupReader->getStateFilepaths())
    {
        RapidXmlReaderPtr stateReader = RapidXmlReader::FromFile(statefile);
        auto headerFilePath = boost::filesystem::path(statefile);
        headerFilePath.replace_extension(boost::filesystem::path("h"));

        RapidXmlReaderNode stateNode = stateReader->getRoot("State");
        std::set<std::string> types = XmlStateBaseClassGenerator::GetUsedInnerNonBasicVariantTypes(stateNode, variantInfo);
        variantTypes.insert(types.begin(), types.end());
    }

    return variantTypes;
}

