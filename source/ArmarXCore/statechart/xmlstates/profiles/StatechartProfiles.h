/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <vector>
#include <string>
#include <set>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace armarx
{
    class StatechartProfiles;
    typedef boost::shared_ptr<StatechartProfiles> StatechartProfilesPtr;

    class StatechartProfile;
    typedef boost::shared_ptr<StatechartProfile> StatechartProfilePtr;
    typedef boost::weak_ptr<StatechartProfile> StatechartProfileWeakPtr;

    class StatechartProfile
        : public boost::enable_shared_from_this<StatechartProfile>
    {
        friend class StatechartProfiles;

    public:
        StatechartProfile(const std::string& name, StatechartProfilePtr parent);
        void readFromXml(RapidXmlReaderNode node);
        bool isLeaf() const;
        bool isRoot() const;
        std::vector<std::string> getAllPackages() const;
        std::string getName() const;
        /**
         * @brief Returns name of profile with all parents in the form of
         * *Root::Armar3Base::Armar3Simualation* if *Armar3Simulation* is the current profile.
         */
        std::string getFullName() const;
        StatechartProfilePtr findByNameRecursive(std::string name) const;
        const std::vector<StatechartProfilePtr>& getChildren() const;
        StatechartProfilePtr getChildByName(std::string name) const;
        StatechartProfilePtr getParent() const;
        int getNesting() const;
        std::string getStatechartGroupPrefix() const;
    private:

        StatechartProfileWeakPtr parent;
        std::vector<StatechartProfilePtr> children;
        std::vector<std::string> packages;
        std::string name;
    };

    class StatechartProfiles
    {
    public:
        static std::string GetRootName()
        {
            return "Root";
        }



    public:
        StatechartProfiles();
        void readStatechartProfiles(RapidXmlReaderPtr reader);

        static StatechartProfilesPtr ReadProfileFiles(const std::vector<std::string>& packages);

        std::vector<StatechartProfilePtr> getAllLeaves() const;
        std::set<std::string> getAllLeafNames() const;
        StatechartProfilePtr getProfileByName(std::string name);
        StatechartProfilePtr getRootProfile() const;
    private:
        StatechartProfilePtr rootProfile;
        void getAllLeaves(StatechartProfilePtr currentProfile, std::vector<StatechartProfilePtr>& profiles) const;
    };
}

