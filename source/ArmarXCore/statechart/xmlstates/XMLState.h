/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once
#ifndef _ARMARX_XMLSTATE_H
#define _ARMARX_XMLSTATE_H


#include <ArmarXCore/core/system/AbstractFactoryMethod.h>
#include <ArmarXCore/statechart/State.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
//#include <fstream>
#include <boost/shared_ptr.hpp>
//#include <boost/tuple/tuple.hpp>


namespace armarx
{

    class RapidXmlReader;
    typedef boost::shared_ptr<RapidXmlReader> RapidXmlReaderPtr;
    class RapidXmlReaderNode;
    typedef boost::shared_ptr<RapidXmlReaderNode> RapidXmlReaderNodePtr;

    typedef std::map<std::string, RapidXmlReaderPtr> StringXMLNodeMap;
    typedef boost::shared_ptr<StringXMLNodeMap> StringXMLNodeMapPtr;

    class StatechartProfile;
    typedef boost::shared_ptr<StatechartProfile> StatechartProfilePtr;
    class StatechartProfiles;
    typedef boost::shared_ptr<StatechartProfiles> StatechartProfilesPtr;

    struct XMLStateConstructorParams
    {
        std::string xmlFilepath;
        RapidXmlReaderPtr reader;
        StatechartProfilePtr selectedProfile;
        StringXMLNodeMapPtr uuidToXMLMap;
        Ice::CommunicatorPtr ic;
        XMLStateConstructorParams(const std::string& xmlFilepath,
                                  RapidXmlReaderPtr reader,
                                  StatechartProfilePtr selectedProfile,
                                  StringXMLNodeMapPtr uuidToXMLMap,
                                  Ice::CommunicatorPtr ic);
    };

    //typedef boost::tuple<boost::filesystem::path, RapidXmlReaderPtr, StatechartProfilePtr, StringXMLNodeMapPtr, Ice::CommunicatorPtr> XMLStateConstructorParams;
    struct XMLStateFactoryBase;
    typedef  IceInternal::Handle<XMLStateFactoryBase > XMLStateFactoryBasePtr;
    struct XMLStateFactoryBase : virtual Ice::Object,
        virtual public AbstractFactoryMethod<XMLStateFactoryBase, XMLStateConstructorParams, XMLStateFactoryBasePtr >
    {

    };

    class StateParameterDeserialization
    {
    public:
        StateParameterDeserialization(RapidXmlReaderNode const& parameterNode, Ice::CommunicatorPtr ic, StatechartProfilePtr selectedProfile);

        std::string getName();
        bool getOptional();
        std::string getTypeStr();
        ContainerTypePtr getTypePtr();
        VariantContainerBasePtr getContainer();

    protected:
        std::string name;
        bool optional;
        std::string typeStr;
        ContainerTypePtr typePtr;
        VariantContainerBasePtr container;
    };

    VariantContainerBasePtr GetSelectedProfileValue(RapidXmlReaderNode parameterNode, StatechartProfilePtr selectedProfile, Ice::CommunicatorPtr ic, std::string defaultValueJsonString = "");


    struct PrivateXmlStateClass;
    typedef boost::shared_ptr<PrivateXmlStateClass> PrivateXmlStateClassPtr;
    class XMLState :
        virtual public State
    {
    public:

        XMLState() {}
        XMLState(const XMLStateConstructorParams& stateData);
        ~XMLState() override;

        StatechartProfilePtr getSelectedProfile();

    protected:

        StateParameterMap getParameters(RapidXmlReaderNode  parametersNode);

        StatePtr addState(StatePtr state);
        void addXMLSubstates(RapidXmlReaderNode  substatesNode, const std::string& parentStateName);
        StateBasePtr addXMLSubstate(RapidXmlReaderNode  stateNode, const std::string& parentStateName);

        void defineParameters() override;
        void defineSubstates() override;
    private:
        void setXMLStateData(const XMLStateConstructorParams& stateData);


        void addTransitions(const RapidXmlReaderNode& transitionsNode);
        void setStartState(const RapidXmlReaderNode& startNode);
        ParameterMappingPtr getMapping(const RapidXmlReaderNode& mappingNode);
        PrivateXmlStateClassPtr privateStateData;

        template <typename type>
        friend class XMLStateTemplate;
        template <typename ContextType>
        friend class XMLRemoteStateOfferer;

    };

    /**
     * Class for legacy to stay compatible with old statecharts
     */
    template <typename StateType>
    class XMLStateTemplate :
        virtual public XMLState
    {
    public:
        XMLStateTemplate(const XMLStateConstructorParams& params) : XMLState(params)
        {
            setXMLStateData(params);
        }

    };

    struct NoUserCodeState :
        virtual public XMLState,
        public virtual XMLStateFactoryBase
    {
        NoUserCodeState(XMLStateConstructorParams stateData);
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static std::string GetName();

        StateBasePtr clone() const override;
        static SubClassRegistry Registry;

    };
}

#endif
