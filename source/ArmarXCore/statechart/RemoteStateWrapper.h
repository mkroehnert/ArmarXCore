/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include "State.h"
#include "RemoteState.h"

namespace armarx
{

    class RemoteStateWrapper;
    typedef IceInternal::Handle<RemoteStateWrapper> RemoteStateWrapperPtr;

    /**
      \class RemoteStateWrapper
      \ingroup StatechartGrp
      This class functions as a pseudo parent state. Every instance of a
      remoteaccessable state has an instance of this class as parent.
      It overrides all the functions that a normal state can call on his
      parent state and forwards the call over Ice to the "real" parent
      state.

      @see RemoteState, DynamicRemoteState, RemoteStateOfferer
      */
    class RemoteStateWrapper:
        virtual public State
    {
    protected:
        RemoteStateIceBasePrx  callerStatePrx;
        StatePtr realState;

    public:

        RemoteStateWrapper(StatePtr realState, RemoteStateIceBasePrx  callerStatePrx);

        RemoteStateWrapper(const RemoteStateWrapper& source);
        ~RemoteStateWrapper() override;
        // inherited from StateBase
        void __processBufferedEvents() override;
        void __processEvent(const EventPtr event, bool buffered = false) override;
        void __enqueueEvent(const EventPtr event) override;
        unsigned int __getUnbreakableBufferSize() const override;
        bool __getUnbreakableBufferStati() const override;
        void __finalize(const EventPtr event) override;
        StateBasePtr clone() const override;
        StateBasePtr createEmptyCopy() const override;
        template <typename ContextType>
        friend class RemoteStateOfferer;
    };
}

