/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once
#include <boost/thread/mutex.hpp>

//#include <ArmarXCore/observers/variant/Variant.h>
//#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
//#include <ArmarXCore/observers/variant/ChannelRef.h>
#include <ArmarXCore/observers/Event.h>
#include <ArmarXCore/observers/condition/Term.h>
#include "StateController.h"


namespace armarx
{
    class StatechartContext;
    typedef IceInternal::Handle<StatechartContext> StatechartContextPtr;

    class ChannelRef;
    typedef IceInternal::Handle<ChannelRef> ChannelRefPtr;


    /**
      * \class StateUtility
      * \ingroup StatechartGrp
      * This class provides utility functions for statechart-implementing-users to communicate
      * with other distributed components like observers. These functions should only be used
      * in the onEnter(), onBreak() and onExit() functions.
      */
    class StateUtility :
        virtual public StateController
    {
        std::string contextString;

        bool checkEventIsHandled(const EventPtr& event) const;
    public:
        //! Struct for the return value of setCounter/TimerEvent
        struct ActionEventIdentifier
        {
            ChannelRefBasePtr actionId;
            ConditionIdentifier conditionId;
        };


        //! Utility function to create a new Event
        template <class EventClass>
        EventPtr createEvent()
        {
            EventPtr event = new EventClass(this->stateName);
            return event;
        }
        //! Utility function to create a new Event
        EventPtr createEvent(const std::string& eventName, const StringVariantContainerBaseMap& properties = StringVariantContainerBaseMap());
        /*! \brief Utility function to install a condition on the distributed conditionhandler
          @param condition Smartpointer to the condition that should be installed on the conditionhandler
          @param evt The event that the statecharts wants to receive if the condition is fulfilled
          */

    protected:
        ConditionIdentifier installCondition(const Term& condition, const EventPtr evt, const std::string& desc = "");
        /*! \brief Utility function to install a condition on the distributed conditionhandler
          @param condition Smartpointer to the condition that should be installed on the conditionhandler
          @param Eventclass The eventtype that the statecharts wants to receive if the condition is fulfilled
          */
        template <class Eventclass>
        ConditionIdentifier installCondition(const Term& condition, const std::string& desc = "")
        {
            return installCondition(condition, createEvent<Eventclass>(), desc);
        }

        /*! \brief Utility function to remove an installed condition on the distributed conditionhandler
          @param conditionId The ConditionIdentifier that was returned from installCondition
          */
        void removeCondition(ConditionIdentifier conditionId);
        /*! \brief Utility function to start a timer on the systemObserver and register an event on the conditionHandler.
          The condition is automatically removed, when the state is left.
          @param Eventclass Eventtype that the state wants to receive
          @param timeoutDurationMs Duration until the event should be send back
          @return Identifier for this timeoutevent to be able to remove/reset the condition/timer
          */
        ActionEventIdentifier setTimeoutEvent(int timeoutDurationMs, const EventPtr& evt);
        template <class Eventclass>
        ActionEventIdentifier setTimeoutEvent(int timeoutDurationMs)
        {
            return setTimeoutEvent(timeoutDurationMs, createEvent<Eventclass>());
        }
        ActionEventIdentifier setCounterEvent(int counterThreshold, const EventPtr& evt, int initialValue = 0);
        template <class Eventclass>
        ActionEventIdentifier setCounterEvent(int counterThreshold, int initialValue = 0)
        {
            return setCounterEvent(counterThreshold, createEvent<Eventclass>(), initialValue);
        }

        void removeTimeoutEvent(const ActionEventIdentifier& id);
        void removeCounterEvent(const ActionEventIdentifier& id);

        /*! \brief Function to send an event to a specific state from an onEnter()-function. Must not be called anywhere else.
          The event is processed after the onEnter()-function of this and all initial substates have returned.<br/>
          Use of this function should be avoided since this statemachine is blocked until this event and all subsequent sendEvents() are processed.
          @param event Event that is to be sent
          @param eventProcessor Pointer to the state, that should process this event. If set to NULL, the event it sent to the parent state.
                 If you want to leave the current state with this event, set this parameter to NULL.
          */
        void sendEvent(const EventPtr event, StateBasePtr eventProcessor = nullptr);
        template <class Event>
        void sendEvent(StateBasePtr eventProcessor = nullptr)
        {
            sendEvent(createEvent<Event>(), eventProcessor);
        }

        //        //! Function for identifying this state
        //        std::string getScope() const;
        //        //! same function as getScope(), only shorter
        //        std::string s() const
        //        {
        //            return getScope();
        //        }

        //void startWorkerThread();


    private:
        std::vector< ConditionIdentifier > __installedConditionIdentifiers;
        std::vector< ChannelRefBasePtr > __installedTimerChannelRefs;
        std::vector< ChannelRefBasePtr > __installedCounterChannelRefs;

        void _removeInstalledConditions() override;
    };

}

