/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Jan Issac (jan dot issac at gmail dot com)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::IceProperties
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

// ArmarXCore
#include <ArmarXCore/core/application/properties/IceProperties.h>

#include <ArmarXCore/core/exceptions/local/PropertyInheritanceCycleException.h>

// Ice
#include <Ice/Ice.h>

#include <string>
#include <iostream>
#include <limits>

using namespace std;

BOOST_AUTO_TEST_CASE(IcePropertiesCreateEmpty)
{
    Ice::PropertiesPtr iceProperties = armarx::IceProperties::create();

    iceProperties->getPropertiesForPrefix(string());

    Ice::PropertyDict propertyDict = iceProperties->getPropertiesForPrefix(string());

    BOOST_CHECK_EQUAL(propertyDict.size(), 1);
    BOOST_CHECK(propertyDict.begin()->first.compare("Ice.Config") == 0);
}

BOOST_AUTO_TEST_CASE(IcePropertiesCreateWithDefaults)
{
    Ice::PropertiesPtr defaults = Ice::createProperties();
    defaults->setProperty("MyDomain.MyObject.MyProperty", "Foo");

    Ice::PropertiesPtr iceProperties = armarx::IceProperties::create(defaults);

    // get all properties
    Ice::PropertyDict propertyDict = iceProperties->getPropertiesForPrefix(string());

    BOOST_CHECK_EQUAL(propertyDict.size(), 2);
    BOOST_CHECK(propertyDict.begin()->first.compare("Ice.Config") == 0);
    BOOST_CHECK((++(propertyDict.begin()))->first.compare("MyDomain.MyObject.MyProperty") == 0);
    BOOST_CHECK((++(propertyDict.begin()))->second.compare("Foo") == 0);
}

BOOST_AUTO_TEST_CASE(IcePropertiesCreateWithPropertySequence)
{
    Ice::StringSeq optionSeq;
    optionSeq.push_back("ProgramName");
    optionSeq.push_back("--MyDomain.MyOption = Foo");

    Ice::PropertiesPtr iceProperties = armarx::IceProperties::create(optionSeq);

    // get all properties
    Ice::PropertyDict propertyDict = iceProperties->getPropertiesForPrefix(string());
    Ice::PropertyDict::iterator dictIt = propertyDict.begin();

    BOOST_CHECK_EQUAL(propertyDict.size(), 3);
    BOOST_CHECK(dictIt->first.compare("Ice.Config") == 0);
    BOOST_CHECK((++dictIt)->first.compare("Ice.ProgramName") == 0);
    BOOST_CHECK(dictIt->second.compare("ProgramName") == 0);
    BOOST_CHECK((++dictIt)->first.compare("MyDomain.MyOption") == 0);
    BOOST_CHECK(dictIt->second.compare("Foo") == 0);
}

BOOST_AUTO_TEST_CASE(IcePropertiesCreateWithPropertySequenceAndDefaults)
{
    Ice::PropertiesPtr defaults = Ice::createProperties();
    defaults->setProperty("MyDomain.MyObject.MyProperty", "Foo");

    Ice::StringSeq optionSeq;
    optionSeq.push_back("ProgramName");
    optionSeq.push_back("--MyOtherDomain.MyOtherObject.MyOption = Boo");

    Ice::PropertiesPtr iceProperties = armarx::IceProperties::create(optionSeq, defaults);

    // get all properties
    Ice::PropertyDict propertyDict = iceProperties->getPropertiesForPrefix(string());
    Ice::PropertyDict::iterator dictIt = propertyDict.begin();

    /*
    // ----------------------------------------------------------------
    Ice::PropertyDict::iterator dictIter = propertyDict.begin();
    while (dictIter != propertyDict.end())
    {
        string propertyFullName = dictIter->first;

        std::cout << dictIter->first << "=" << dictIter->second << std::endl;

        ++dictIter;
    }
    // ----------------------------------------------------------------
    */

    BOOST_CHECK_EQUAL(propertyDict.size(), 4);
    BOOST_CHECK(dictIt->first.compare("Ice.Config") == 0);
    BOOST_CHECK((++dictIt)->first.compare("Ice.ProgramName") == 0);
    BOOST_CHECK(dictIt->second.compare("ProgramName") == 0);
    BOOST_CHECK((++dictIt)->first.compare("MyDomain.MyObject.MyProperty") == 0);
    BOOST_CHECK(dictIt->second.compare("Foo") == 0);
    BOOST_CHECK((++dictIt)->first.compare("MyOtherDomain.MyOtherObject.MyOption") == 0);
    BOOST_CHECK(dictIt->second.compare("Boo") == 0);
}

BOOST_AUTO_TEST_CASE(IcePropertiesCreateWithArgs)
{
    char* argv[] =
    {
        (char*) "ProgramName",
        (char*) "--MyDomain.MyOption = Foo"
    };
    int argn = sizeof(argv) / sizeof(*argv);
    Ice::PropertiesPtr iceProperties = armarx::IceProperties::create(argn, argv);

    // get all properties
    Ice::PropertyDict propertyDict = iceProperties->getPropertiesForPrefix(string());
    Ice::PropertyDict::iterator dictIt = propertyDict.begin();

    BOOST_CHECK_EQUAL(propertyDict.size(), 3);
    BOOST_CHECK(dictIt->first.compare("Ice.Config") == 0);
    BOOST_CHECK((++dictIt)->first.compare("Ice.ProgramName") == 0);
    BOOST_CHECK(dictIt->second.compare("ProgramName") == 0);
    BOOST_CHECK((++dictIt)->first.compare("MyDomain.MyOption") == 0);
    BOOST_CHECK(dictIt->second.compare("Foo") == 0);
}

BOOST_AUTO_TEST_CASE(IcePropertiesCreateWithArgsAndDefaults)
{
    Ice::PropertiesPtr defaults = Ice::createProperties();
    defaults->setProperty("MyDomain.MyObject.MyProperty", "Foo");

    char* argv[] =
    {
        (char*) "ProgramName",
        (char*) "--MyOtherDomain.MyOtherObject.MyOption = Boo"
    };
    int argn = sizeof(argv) / sizeof(*argv);
    Ice::PropertiesPtr iceProperties = armarx::IceProperties::create(argn, argv, defaults);

    // get all properties
    Ice::PropertyDict propertyDict = iceProperties->getPropertiesForPrefix(string());
    Ice::PropertyDict::iterator dictIt = propertyDict.begin();

    BOOST_CHECK_EQUAL(propertyDict.size(), 4);
    BOOST_CHECK(dictIt->first.compare("Ice.Config") == 0);
    BOOST_CHECK((++dictIt)->first.compare("Ice.ProgramName") == 0);
    BOOST_CHECK(dictIt->second.compare("ProgramName") == 0);
    BOOST_CHECK((++dictIt)->first.compare("MyDomain.MyObject.MyProperty") == 0);
    BOOST_CHECK(dictIt->second.compare("Foo") == 0);
    BOOST_CHECK((++dictIt)->first.compare("MyOtherDomain.MyOtherObject.MyOption") == 0);
    BOOST_CHECK(dictIt->second.compare("Boo") == 0);
}

BOOST_AUTO_TEST_CASE(IcePropertiesExtractNamespaces)
{
    /*
    Ice::StringSeq namespaceList;
    namespaceList.push_back("DomainA.ObjectName");
    namespaceList.push_back("DomainB.ObjectName");
    namespaceList.push_back("DomainC.ObjectName");
    namespaceList.push_back("DomainD.ObjectName");
    namespaceList.push_back("DomainE.ObjectName");
    */

    // add "DomainA.ObjectName" namespace via argument array
    char* argv[] =
    {
        (char*) "ProgramName",
        (char*) "--DomainA.ObjectName.PropertyName = fooA"
    };
    int argn = sizeof(argv) / sizeof(*argv);
    // add "DomainB.ObjectName" via string sequence and Ice::Properties defaults
    Ice::StringSeq defaults;
    defaults.push_back("--DomainB.ObjectName.PropertyName = fooB");
    Ice::PropertiesPtr defaultProperties = Ice::createProperties();
    defaultProperties->parseCommandLineOptions("DomainB.ObjectName", defaults);

    Ice::PropertiesPtr iceProperties = armarx::IceProperties::create(
                                           argn, argv, defaultProperties);

    // finally add several properties using the Ice::Properties setter
    iceProperties->setProperty("DomainC.ObjectName.PropertyName", "fooC ");
    iceProperties->setProperty("DomainD.ObjectName.PropertyName", "fooD");
    iceProperties->setProperty("DomainE.ObjectName.PropertyName", "fooE");

    armarx::IceProperties::InheritanceSolver inheritanceSolver;

    armarx::IceProperties::InheritanceSolver::NamespaceMap namespaces;
    inheritanceSolver.extractNamespaces(iceProperties, namespaces);

    /*
    Ice::StringSeq::iterator namespaceListIter = namespaceList.begin();
    while (namespaceListIter != namespaceList.end())
    {
        BOOST_CHECK(namespaces.find(*namespaceListIter) != namespaces.end());

        // std::cout << namespaceIter->first << std::endl;

        ++namespaceListIter;
    }*/

    BOOST_CHECK(namespaces.find("DomainA.ObjectName") != namespaces.end());
    BOOST_CHECK(namespaces.find("DomainB.ObjectName") != namespaces.end());
    BOOST_CHECK(namespaces.find("DomainC.ObjectName") != namespaces.end());
    BOOST_CHECK(namespaces.find("DomainD.ObjectName") != namespaces.end());
    BOOST_CHECK(namespaces.find("DomainE.ObjectName") != namespaces.end());
}

BOOST_AUTO_TEST_CASE(IcePropertiesHasParentTest)
{
    Ice::PropertiesPtr iceProperties = Ice::createProperties();

    // finally add several properties using the Ice::Properties setter
    iceProperties->setProperty("DomainA.ObjectName.PropertyName", "fooA");
    iceProperties->setProperty("DomainB.ObjectName.PropertyName", "fooB");
    iceProperties->setProperty("DomainC.ObjectName.PropertyName", "fooC");

    iceProperties->setProperty("DomainB.ObjectName.inheritFrom", "DomainA.ObjectName");
    iceProperties->setProperty("DomainC.ObjectName.inheritFrom", "");

    armarx::IceProperties::InheritanceSolver inheritanceSolver;

    BOOST_CHECK(inheritanceSolver.hasParent("DomainA.ObjectName", iceProperties) == false);
    BOOST_CHECK(inheritanceSolver.hasParent("DomainB.ObjectName", iceProperties) == true);
    BOOST_CHECK(inheritanceSolver.hasParent("DomainC.ObjectName", iceProperties) == false);
}

BOOST_AUTO_TEST_CASE(IcePropertiesGetParentTest)
{
    Ice::PropertiesPtr iceProperties = Ice::createProperties();

    // finally add several properties using the Ice::Properties setter
    iceProperties->setProperty("DomainA.ObjectName.PropertyName", "fooA");
    iceProperties->setProperty("DomainB.ObjectName.PropertyName", "fooB");
    iceProperties->setProperty("DomainC.ObjectName.PropertyName", "fooC");


    iceProperties->setProperty("DomainB.ObjectName.inheritFrom", "DomainA.ObjectName");
    iceProperties->setProperty("DomainC.ObjectName.inheritFrom", "");

    armarx::IceProperties::InheritanceSolver inheritanceSolver;

    BOOST_CHECK(inheritanceSolver.getParent("DomainA.ObjectName", iceProperties).compare("") == 0);
    BOOST_CHECK(inheritanceSolver.getParent("DomainB.ObjectName", iceProperties).compare("DomainA.ObjectName") == 0);
    BOOST_CHECK(inheritanceSolver.getParent("DomainC.ObjectName", iceProperties).compare("") == 0);
}


BOOST_AUTO_TEST_CASE(IcePropertiesStripNamespaceTest)
{
    armarx::IceProperties::InheritanceSolver inheritanceSolver;

    BOOST_CHECK(inheritanceSolver.stripNamespace(
                    "DomainA.ObjectName.PropertyName",
                    "DomainA.ObjectName").compare("PropertyName") == 0);
}

BOOST_AUTO_TEST_CASE(IcePropertiesIsInHeritageLineTest)
{
    std::vector<string> heritageLine;

    heritageLine.push_back("DomainA.ObjectName");
    heritageLine.push_back("DomainB.ObjectName");

    armarx::IceProperties::InheritanceSolver inheritanceSolver;

    BOOST_CHECK(inheritanceSolver.isInHeritageLine(heritageLine, "DomainA.ObjectName") == true);
    BOOST_CHECK(inheritanceSolver.isInHeritageLine(heritageLine, "DomainB.ObjectName") == true);
    BOOST_CHECK(inheritanceSolver.isInHeritageLine(heritageLine, "DomainC.ObjectName") == false);
}

BOOST_AUTO_TEST_CASE(IcePropertiesInheritPassTest)
{
    Ice::PropertiesPtr iceProperties = Ice::createProperties();

    // finally add several properties using the Ice::Properties setter
    iceProperties->setProperty("DomainA.ObjectName.PropertyNameA", "fooA");
    iceProperties->setProperty("DomainB.ObjectName.PropertyNameB", "fooB");
    iceProperties->setProperty("DomainC.ObjectName.PropertyNameC", "fooC");

    armarx::IceProperties::InheritanceSolver inheritanceSolver;
    std::vector<std::string> hl;

    inheritanceSolver.inherit("DomainA.ObjectName", "DomainX.ObjectName", iceProperties, hl);
    inheritanceSolver.inherit("DomainB.ObjectName", "DomainA.ObjectName", iceProperties, hl);
    inheritanceSolver.inherit("DomainC.ObjectName", "DomainA.ObjectName", iceProperties, hl);
    inheritanceSolver.inherit("DomainD.ObjectName", "DomainC.ObjectName", iceProperties, hl);
    inheritanceSolver.inherit("DomainE.ObjectName", "DomainD.ObjectName", iceProperties, hl);

    BOOST_CHECK(inheritanceSolver.hasParent("DomainA.ObjectName", iceProperties) == false);
    BOOST_CHECK(inheritanceSolver.hasParent("DomainB.ObjectName", iceProperties) == false);
    BOOST_CHECK(inheritanceSolver.hasParent("DomainC.ObjectName", iceProperties) == false);
    BOOST_CHECK(inheritanceSolver.hasParent("DomainD.ObjectName", iceProperties) == false);
    BOOST_CHECK(inheritanceSolver.hasParent("DomainE.ObjectName", iceProperties) == false);
    BOOST_CHECK(inheritanceSolver.hasParent("DomainF.ObjectName", iceProperties) == false);

    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix("DomainA.ObjectName").size(), 1);
    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix("DomainB.ObjectName").size(), 2);
    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix("DomainC.ObjectName").size(), 2);
    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix("DomainD.ObjectName").size(), 2);
    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix("DomainE.ObjectName").size(), 2);
    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix("DomainF.ObjectName").size(), 0);
}

BOOST_AUTO_TEST_CASE(IcePropertiesInheritFailTest)
{
    Ice::PropertiesPtr iceProperties = Ice::createProperties();

    // finally add several properties using the Ice::Properties setter
    iceProperties->setProperty("DomainA.ObjectName.PropertyNameA", "fooA");
    iceProperties->setProperty("DomainB.ObjectName.PropertyNameB", "fooB");
    iceProperties->setProperty("DomainC.ObjectName.PropertyNameC", "fooC");

    iceProperties->setProperty("DomainB.ObjectName.inheritFrom", "DomainD.ObjectName");

    armarx::IceProperties::InheritanceSolver inheritanceSolver;
    std::vector<std::string> hl;
    hl.push_back("DomainB.ObjectName");

    BOOST_CHECK_THROW(inheritanceSolver.inherit("DomainA.ObjectName",
                      "DomainB.ObjectName",
                      iceProperties, hl),
                      armarx::exceptions::local::PropertyInheritanceCycleException);

    hl.clear();
    iceProperties->setProperty("DomainA.ObjectName.inheritFrom", "DomainC.ObjectName");
    iceProperties->setProperty("DomainB.ObjectName.inheritFrom", "DomainA.ObjectName");
    iceProperties->setProperty("DomainC.ObjectName.inheritFrom", "DomainB.ObjectName");

    BOOST_CHECK_THROW(inheritanceSolver.inherit("DomainA.ObjectName",
                      "DomainC.ObjectName",
                      iceProperties, hl),
                      armarx::exceptions::local::PropertyInheritanceCycleException);
}

BOOST_AUTO_TEST_CASE(IcePropertiesResolveNamespaceInheritanceTest)
{
    Ice::PropertiesPtr iceProperties = Ice::createProperties();

    iceProperties->setProperty("DomainA.ObjectName.PropertyNameA", "fooA");
    iceProperties->setProperty("DomainA.ObjectName.PropertyNameAA", "fooAA");
    iceProperties->setProperty("DomainB.ObjectName.PropertyNameB", "fooB");
    iceProperties->setProperty("DomainC.ObjectName.PropertyNameC", "fooC");
    iceProperties->setProperty("DomainD.ObjectName.PropertyNameC", "fooD");

    iceProperties->setProperty("DomainB.ObjectName.inheritFrom", "DomainA.ObjectName");
    iceProperties->setProperty("DomainC.ObjectName.inheritFrom", "DomainB.ObjectName");

    armarx::IceProperties::InheritanceSolver inheritanceSolver;

    inheritanceSolver.resolveInheritance(iceProperties);

    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix("DomainA.ObjectName").size(), 2);
    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix("DomainB.ObjectName").size(), 3);
    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix("DomainC.ObjectName").size(), 4);
    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix("DomainD.ObjectName").size(), 1);

    BOOST_CHECK_EQUAL(iceProperties->getPropertiesForPrefix(std::string()).size(), 10);
}

BOOST_AUTO_TEST_CASE(IcePropertiesInheritThenOverride)
{
    Ice::PropertiesPtr properties = armarx::IceProperties::create();

    properties->setProperty("ArmarX.CommonProperties.FrameRate", "30");

    properties->setProperty("VisionX.MyCapturer1.inheritFrom", "ArmarX.CommonProperties");
    properties->setProperty("VisionX.MyCapturer1.FrameRate", "60");

    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer1.FrameRate").compare("60"), 0);
}

BOOST_AUTO_TEST_CASE(IcePropertiesSetThenInherit)
{
    Ice::PropertiesPtr properties = armarx::IceProperties::create();

    properties->setProperty("ArmarX.CommonProperties.FrameRate", "30");

    properties->setProperty("VisionX.MyCapturer1.FrameRate", "60");
    properties->setProperty("VisionX.MyCapturer1.inheritFrom", "ArmarX.CommonProperties");

    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer1.FrameRate").compare("60"), 0);
}


BOOST_AUTO_TEST_CASE(IcePropertiesTest)
{
    Ice::PropertiesPtr properties = armarx::IceProperties::create();

    properties->setProperty("ArmarX.CommonProperties.FrameRate", "30");
    properties->setProperty("ArmarX.CommonProperties.ColorMode", "HSV");
    properties->setProperty("ArmarX.CommonProperties.ShutterSpeed", "0.003");
    properties->setProperty("ArmarX.CommonProperties.Aperture", "1.2");
    properties->setProperty("ArmarX.CommonProperties.WhiteBalance", "Auto");
    properties->setProperty("ArmarX.CommonProperties.Metering", "CenteredMean");
    properties->setProperty("ArmarX.CommonProperties.Resolution", "640x480");
    properties->setProperty("ArmarX.CommonProperties.CropFactor", "1.5");

    properties->setProperty("VisionX.MyCapturer1.inheritFrom", "ArmarX.CommonProperties");
    properties->setProperty("VisionX.MyCapturer1.ColorMode", "RGB");

    properties->setProperty("VisionX.MyCapturer2.inheritFrom", "ArmarX.CommonProperties");
    properties->setProperty("VisionX.MyCapturer2.ShutterSpeed", "0.001");

    properties->setProperty("VisionX.MyCapturer3.FrameRate", "60");
    properties->setProperty("VisionX.MyCapturer3.inheritFrom", "ArmarX.CommonProperties");

    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer1.FrameRate").compare("30"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer1.ColorMode").compare("RGB"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer1.ShutterSpeed").compare("0.003"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer1.Aperture").compare("1.2"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer1.WhiteBalance").compare("Auto"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer1.Metering").compare("CenteredMean"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer1.Resolution").compare("640x480"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer1.CropFactor").compare("1.5"), 0);

    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer2.FrameRate").compare("30"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer2.ColorMode").compare("HSV"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer2.ShutterSpeed").compare("0.001"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer2.Aperture").compare("1.2"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer2.WhiteBalance").compare("Auto"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer2.Metering").compare("CenteredMean"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer2.Resolution").compare("640x480"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer2.CropFactor").compare("1.5"), 0);

    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer3.FrameRate").compare("60"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer3.ColorMode").compare("HSV"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer3.ShutterSpeed").compare("0.003"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer3.Aperture").compare("1.2"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer3.WhiteBalance").compare("Auto"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer3.Metering").compare("CenteredMean"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer3.Resolution").compare("640x480"), 0);
    BOOST_CHECK_EQUAL(properties->getProperty("VisionX.MyCapturer3.CropFactor").compare("1.5"), 0);
}

