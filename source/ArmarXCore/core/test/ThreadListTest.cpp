/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::ThreadList
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/services/tasks/ThreadList.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/Component.h>

#include <iostream>

using namespace armarx;

class TestApp : public Application
{
public:
    void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override {}

};

class TestComponent : public Component
{
public:
    void onInitComponent() override {}
    void onConnectComponent() override {}
    std::string getDefaultName() const override
    {
        return "TestComponent";
    }
    void runTimerTask()
    {

    }

};

BOOST_AUTO_TEST_CASE(testThreadList)
{
    ApplicationPtr app =  Application::createInstance<TestApp>();

    PeriodicTask<TestComponent>::pointer_type periodicTask = new PeriodicTask<TestComponent>(new TestComponent(), &TestComponent::runTimerTask, 1, false, "TestPeriodicTask");

    periodicTask->start();
    BOOST_CHECK_EQUAL((unsigned int)1, ThreadList::getApplicationThreadList()->getPeriodicTaskNames().size());
    BOOST_CHECK_EQUAL("TestPeriodicTask", ThreadList::getApplicationThreadList()->getPeriodicTaskNames()[0]);

    periodicTask->stop();

    BOOST_CHECK_EQUAL((unsigned int)1, ThreadList::getApplicationThreadList()->getPeriodicTaskNames().size());// should be still on list but running should be false
    BOOST_CHECK_EQUAL(false, periodicTask->getStatistics().running);
    periodicTask = nullptr;
    BOOST_CHECK_EQUAL((unsigned int)0, ThreadList::getApplicationThreadList()->getPeriodicTaskNames().size());

}

