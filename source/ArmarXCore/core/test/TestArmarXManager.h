/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>

/**
* Helper class for using Ice in tests
*/
class TestArmarXManager: public virtual armarx::ArmarXManager
{
public:
    TestArmarXManager(const std::string& applicationName, Ice::CommunicatorPtr communicator, Ice::PropertiesPtr properties = Ice::createProperties()) :
        ArmarXManager(applicationName, communicator), properties(properties)
    {
    }

    template <class C, class Prx>
    Prx createComponentAndRun(const std::string& domain, const std::string& name, Ice::PropertiesPtr overwriteProperties = Ice::PropertiesPtr())
    {
        IceInternal::Handle<C> component = armarx::Component::create<C>((overwriteProperties) ? overwriteProperties : properties, name, domain);

        addObject(component);

        component->getObjectScheduler()->waitForObjectState(armarx::eManagedIceObjectStarted, 15000);

        return Prx::uncheckedCast(component->getProxy());
    }

protected:
    Ice::PropertiesPtr properties;
};

typedef IceUtil::Handle<TestArmarXManager> TestArmarXManagerPtr;

