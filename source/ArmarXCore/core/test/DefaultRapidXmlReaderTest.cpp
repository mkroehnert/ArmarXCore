/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::Test
 * @author     Stefan Reither
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::Core::rapidxml::DefaultRapidXmlReaderTest

#define ARMARX_BOOST_TEST

#include <cstdio>
#include <boost/test/included/unit_test.hpp>

#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>

#define FLOAT_COMPARE_ACCURACY 0.0000001f

using namespace armarx;

class DefaultRapidXmlReaderTestSetup
{
public:
    std::string path;
    RapidXmlWriter writer;

    /*
     * Structure of the generated xml-file used for testing
     *
     * <Root>
     *      <default>
     *          <value1>true</value1>
     *          <value_a1 attr1="1.0" attr2="true" attr3="1.r"/>
     *      </default>
     *      <joint1>
     *          <value1>true</value1>
     *          <value2>54321</value2>
     *          <value3>2.34</value3>
     *          <value_a1 attr1="2.5" attr2="false"/>
     *      </joint1>
     *      <joint2>
     *          <value2>54322</value2>
     *          <value_a1 attr1="1.2"/>
     *      </joint2>
     *      <bla1>
     *          <bla2>
     *              <joint3>
     *                  <value1>true</value1>
     *                  <value2>54320</value2>
     *                  <value3>4.67</value3>
     *                  <value_a1 attr1="2.9" attr2="true"/>
     *              </joint3>
     *          </bla2>
     *      </bla1>
     * </Root>
     */
    DefaultRapidXmlReaderTestSetup()
    {
        RapidXmlWriterNode root = writer.createRootNode("Root");
        path = "DefaultRapidXmlReaderTestFile.xml";

        RapidXmlWriterNode c = root.append_node("default");
        c.append_string_node("value1", "true");
        c = c.append_node("value_a1");
        c.append_attribute("attr1", "1.0");
        c.append_bool_attribute("attr2", "true", "false", true);
        c.append_attribute("attr3", "1.r");

        c = root.append_node("joint1");
        c.append_string_node("value1", "true");
        c.append_string_node("value2", "54321");
        c.append_string_node("value3", "2.34");
        c = c.append_node("value_a1");
        c.append_attribute("attr1", "2.5");
        c.append_bool_attribute("attr2", "true", "false", false);

        c = root.append_node("joint2");
        c.append_string_node("value2", "54322");
        c = c.append_node("value_a1");
        c.append_attribute("attr1", "1.2");

        // extra node for adding later
        c = root.append_node("bla1");
        c = c.append_node("bla2");
        c = c.append_node("joint3");
        c.append_string_node("value1", "true");
        c.append_string_node("value2", "54320");
        c.append_string_node("value3", "4.67");
        c = c.append_node("value_a1");
        c.append_attribute("attr1", "2.9");
        c.append_bool_attribute("attr2", "true", "false", true);

    }
    ~DefaultRapidXmlReaderTestSetup()
    {
        clear();
    }

    void createFile()
    {
        struct stat buffer;
        if (stat(path.c_str(), &buffer) == 0)
        {
            clear();
        }

        writer.saveToFile(path, false);
    }

    void clear()
    {
        std::remove(path.c_str());
    }

    DefaultRapidXmlReaderNode getDefaultRapidXmlReaderNode()
    {
        createFile();
        RapidXmlReaderPtr reader = RapidXmlReader::FromFile(path);
        RapidXmlReaderNode root = reader->getRoot("Root");

        std::vector<RapidXmlReaderNode> v;
        v.push_back(root.first_node("default"));
        v.push_back(root.first_node("joint1"));
        v.push_back(root.first_node("joint2"));

        DefaultRapidXmlReaderNode defaultNode(v);
        return defaultNode;
    }

    RapidXmlReaderNode getExtraNode()
    {
        createFile();
        RapidXmlReaderPtr reader = RapidXmlReader::FromFile(path);
        RapidXmlReaderNode root = reader->getRoot("Root");
        return root.first_node("bla1").first_node("bla2").first_node("joint3");
    }
};


BOOST_AUTO_TEST_CASE(OpenXmlAndCreateDefaultReaderNode)
{
    DefaultRapidXmlReaderTestSetup setup;
    setup.createFile();

    RapidXmlReaderPtr reader = RapidXmlReader::FromFile(setup.path);
    RapidXmlReaderNode root = reader->getRoot("Root");
    BOOST_CHECK(root.is_valid());
    root = reader->getRoot("root1");
    BOOST_CHECK(!root.is_valid());
    root = reader->getRoot();
    BOOST_CHECK(root.is_valid());

    std::vector<RapidXmlReaderNode> v;
    v.push_back(root.first_node("default"));
    v.push_back(root.first_node("joint1"));
    v.push_back(root.first_node("joint2"));

    DefaultRapidXmlReaderNode defaultNode(v);
    BOOST_CHECK(defaultNode.is_valid());

    std::vector<RapidXmlReaderNode> v1;
    v1.push_back(root.first_node("default"));
    v1.push_back(root.first_node("joint1"));
    v1.push_back(root.first_node("joint2_notThere"));

    BOOST_CHECK_THROW(DefaultRapidXmlReaderNode defaultNode1(v1), exceptions::local::RapidXmlReaderException);
}

BOOST_AUTO_TEST_CASE(GetValues)
{
    DefaultRapidXmlReaderTestSetup setup;
    DefaultRapidXmlReaderNode defaultNode = setup.getDefaultRapidXmlReaderNode();

    DefaultRapidXmlReaderNode value1 = defaultNode.first_node("value1");
    BOOST_CHECK_EQUAL(value1.getPaths().size(), 2);

    bool value1bool = value1.value_as_bool("true", "false");
    BOOST_CHECK_EQUAL(value1bool, true);

    std::string value1string = value1.value_as_string();
    BOOST_CHECK_EQUAL(value1string, "true");

    BOOST_CHECK_THROW(value1.value_as_bool("True", "False"), exceptions::local::RapidXmlReaderException);

    DefaultRapidXmlReaderNode value2 = defaultNode.first_node("value2");
    uint32_t value2int = value2.value_as_uint32();
    BOOST_CHECK_EQUAL(value2int, 54322);

    DefaultRapidXmlReaderNode value3 = defaultNode.first_node("value3");
    float value3float = value3.value_as_float();
    BOOST_CHECK(abs(value3float - 2.34) <= FLOAT_COMPARE_ACCURACY);
}

BOOST_AUTO_TEST_CASE(GetAttributes)
{
    DefaultRapidXmlReaderTestSetup setup;
    DefaultRapidXmlReaderNode defaultNode = setup.getDefaultRapidXmlReaderNode();

    DefaultRapidXmlReaderNode value_a1 = defaultNode.first_node("value_a1");
    bool hasAttr = value_a1.has_attribute("attr2");
    BOOST_CHECK(hasAttr);
    hasAttr = value_a1.has_attribute("attr4");
    BOOST_CHECK(!hasAttr);

    bool attrBool = value_a1.attribute_as_bool("attr2", "true", "false");
    BOOST_CHECK(attrBool == false);
    BOOST_CHECK_THROW(value_a1.attribute_as_bool("attr2", "True", "False"), exceptions::local::RapidXmlReaderException);
    attrBool = value_a1.attribute_as_optional_bool("attr4", "true", "false", true);
    BOOST_CHECK(attrBool == true);
    BOOST_CHECK_THROW(value_a1.attribute_as_optional_bool("attr1", "true", "false", true);, exceptions::local::RapidXmlReaderException);

    std::string attrStr = value_a1.attribute_as_string("attr3");
    BOOST_CHECK_EQUAL(attrStr, "1.r");
    float attrF = value_a1.attribute_as_float("attr3");
    BOOST_CHECK(abs(attrF - 1.0) <= FLOAT_COMPARE_ACCURACY);

    attrF = value_a1.attribute_as_float("attr1");
    BOOST_CHECK(abs(attrF - 1.2) <= FLOAT_COMPARE_ACCURACY);

    uint32_t attrI = value_a1.attribute_as_uint("attr1");
    BOOST_CHECK_EQUAL(attrI, uint32_t(1.2));
}

BOOST_AUTO_TEST_CASE(AddingAndRemovingNodes)
{
    DefaultRapidXmlReaderTestSetup setup;
    DefaultRapidXmlReaderNode defaultNode = setup.getDefaultRapidXmlReaderNode();

    DefaultRapidXmlReaderNode value2 = defaultNode.first_node("value2");
    BOOST_CHECK_EQUAL(defaultNode.getPaths().size(), 3);

    uint32_t value2int = value2.value_as_uint32();
    BOOST_CHECK_EQUAL(value2int, 54322);

    RapidXmlReaderNode extra = setup.getExtraNode();
    DefaultRapidXmlReaderNode extraNode = defaultNode.add_node_at(extra, 3);
    BOOST_CHECK_EQUAL(extraNode.getPaths().size(), 4);

    value2 = extraNode.first_node("value2");
    value2int = value2.value_as_uint32();
    BOOST_CHECK_EQUAL(value2int, 54320);

    extraNode = defaultNode.add_node_at(extra, 2);
    value2 = extraNode.first_node("value2");
    value2int = value2.value_as_uint32();
    BOOST_CHECK_EQUAL(value2int, 54322);

    DefaultRapidXmlReaderNode lessNode = extraNode.remove_node_at(3);
    BOOST_CHECK_EQUAL(lessNode.getPaths().size(), 3);

    value2 = lessNode.first_node("value2");
    value2int = value2.value_as_uint32();
    BOOST_CHECK_EQUAL(value2int, 54320);

    lessNode = lessNode.remove_node_at(3);
    BOOST_CHECK_EQUAL(lessNode.getPaths().size(), 2);

    value2 = lessNode.first_node("value2");
    value2int = value2.value_as_uint32();
    BOOST_CHECK_EQUAL(value2int, 54321);
}

