/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::Scheduler
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/util/StringHelpers.h>
#include <ArmarXCore/core/logging/LogSender.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

#include <iostream>

using namespace armarx;

class Obj1: public armarx::ManagedIceObject
{
public:


    // ManagedIceObject interface
    void onInitComponent() override
    {
        ARMARX_INFO << getName() << __FUNCTION__;
    }
    void onConnectComponent() override
    {
        ARMARX_INFO << getName() << __FUNCTION__;
    }
    std::string getDefaultName() const override
    {
        return "Obj1";
    }

    // ManagedIceObject interface
protected:
    void onDisconnectComponent() override
    {
        ARMARX_INFO << getName() << __FUNCTION__;

    }
    void onExitComponent() override
    {
        ARMARX_INFO << getName() << __FUNCTION__;

    }
};


class Obj2: public armarx::ManagedIceObject
{
public:

    // ManagedIceObject interface
    void onInitComponent() override
    {
        ARMARX_INFO << getName() << __FUNCTION__;

        usingProxy("Obj1");
    }
    void onConnectComponent() override
    {
        ARMARX_INFO << getName() << __FUNCTION__;

    }
    std::string getDefaultName() const override
    {
        return "Obj2";
    }

    // ManagedIceObject interface
protected:
    void onDisconnectComponent() override
    {
        ARMARX_INFO << getName() << __FUNCTION__;

    }
    void onExitComponent() override
    {
        ARMARX_INFO << getName() << __FUNCTION__;

    }
};


BOOST_AUTO_TEST_CASE(SingleThreadedSchedulerPerformanceTest)
{
    IceTestHelper iceTestHelper;
    iceTestHelper.startEnvironment();
    TestArmarXManagerPtr man = new TestArmarXManager("SchedulerTest", iceTestHelper.getCommunicator());
    man->increaseSchedulers(10);
    TIMING_START(PerfTest);
    for (int i = 10; i < 100; ++i)
    {
        IceUtil::Handle<Obj1> obj = new Obj1;
        man->addObject(obj, "Obj" + to_string(i), false, false);
    }
    IceUtil::Handle<Obj1> obj1 = new Obj1;
    IceUtil::Handle<Obj2> obj2 = new Obj2;
    TIMING_END(PerfTest);
    armarx::LogSender::SetGlobalMinimumLoggingLevel(armarx::eDEBUG);
    man->addObject(obj2, std::string {"Obj1"}, false, false);
    man->addObject(obj1, std::string {"Obj2"}, false, false);
    obj1->getObjectScheduler()->waitForObjectStateMinimum(armarx::eManagedIceObjectStarted);
    sleep(1);
    man->removeObjectBlocking(obj1);
    sleep(1);
    man->shutdown();
}




