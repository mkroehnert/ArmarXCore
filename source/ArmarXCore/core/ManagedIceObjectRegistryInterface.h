/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author    Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Ice/Object.h>

namespace armarx
{
    // forward declaration
    class ManagedIceObject;
    typedef IceInternal::Handle<ManagedIceObject> ManagedIceObjectPtr;

    /**
     * ManagedIceObjectRegistryInterface smart pointer type
     */
    class ManagedIceObjectRegistryInterface;
    typedef IceUtil::Handle<ManagedIceObjectRegistryInterface> ManagedIceObjectRegistryInterfacePtr;

    /**
     * @class ManagedIceObjectRegistryInterface
     * @brief The registery interface is implemented by ArmarXManagers.
     * @ingroup Core
     *
     * It is mainly used to provide the ArmarXDummyManager for properties parsing.
     */
    class ManagedIceObjectRegistryInterface
        : virtual public Ice::Object
    {
    public:
        /**
         * Add a ManagedIceObject to the registry. Takes care of the ManagedIceObject
         * lifcycle using and ArmarXObjectScheduler. addObject is
         * thread-safe and can be called from any method in order to dynamically
         * add ManagedIceObjects (as e.g. required for GUI).
         *
         * @param object      object to add
         * @param addWithOwnAdapter      If true this object will have it's own adapter
         * @param objectName      Name that the object should have. if not empty overrides the currently set name.
         */
        virtual void addObject(const ManagedIceObjectPtr& object, bool addWithOwnAdapter = true, const std::string& objectName = "",  bool useOwnScheduleThread = true) = 0;

        /**
         * Removes an object from the registry.
         *
         * This version waits until all calls to this component are finished and
         * the has been completely removed.
         *
         * @param object object to remove
         */
        virtual void removeObjectBlocking(const ManagedIceObjectPtr& object) = 0;

        /**
         * Removes an object from the manageregistry.
         *
         * This version waits until all calls to this component are finished and
         * the has been completely removed.
         *
         * @param objectName name of the object to remove
         */
        virtual void removeObjectBlocking(const std::string& objectName) = 0;

        /**
         * Removes an object from the registry.
         *
         * This version returns immediatly and does <b>not</b> wait for all call to this
         * to finish.
         *
         * @param object object to remove
         */
        virtual void removeObjectNonBlocking(const ManagedIceObjectPtr& object) = 0;

        /**
         * Removes an object from the registry.
         *
         * This version returns immediatly and does <b>not</b> wait for all call to this
         * to finish.
         *
         * @param objectName name of the object to remove
         */
        virtual void removeObjectNonBlocking(const std::string& objectName) = 0;

        /**
         * Retrieve pointers to all ManagedIceObject.
         *
         * @return vector of pointers
         */
        virtual std::vector<ManagedIceObjectPtr> getManagedObjects() = 0;
    };
}

