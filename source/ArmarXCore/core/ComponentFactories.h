/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore
* @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include "util/Registrar.h"
#include <functional>
#include "Component.h"

/**
 * \page ComponentFactoriesDoc Component factories
 * ArmarX Components can be registered with a name to the \ref armarx::ComponentFactory.
 * This factory can be used to create components given a name used for registration.
 *
 * Use the class \ref armarx::RegisterComponent to perform registration.
 *
 * Retrieve a Component factory by calling:
 * \code
 * ComponentFactory::get("nameUsedForRegistration")
 * \endcode
 * This offers you a function-object behaving like \ref armarx::Component::create.
 *
 * As a difference:
 * The first parameter can also be an Ice::PropertyDict (aka.: std::map<std::string, std::string>) instead of a Properties object.
 *
 * @see armarx::RegisterComponent
 */

/**
 * \defgroup ComponentFactories
 * \ingroup core-utility
 */

namespace armarx
{
    struct ComponentCreatorObject;
    /**
     * @brief holds all registered components
     * \ingroup ComponentFactories
     */
    using ComponentFactory = Registrar<ComponentCreatorObject>;

    /**
     * @brief The ComponentCreatorObject stores the create call for a Component.
     * It is used with the component factory.
     * Use the class \ref armarx::RegisterComponent to perform registration.
     * @see RegisterComponent
     * \ingroup ComponentFactories
     */
    struct ComponentCreatorObject
    {
        /**
         * @brief Calls armarx::Component::create<T> for the type passed to createComponentCreator
         * @see armarx::Component::create
         * @return The created component.
         */
        ComponentPtr operator()(Ice::PropertiesPtr properties = Ice::createProperties(), const std::string& configName = "", const std::string& configDomain = "ArmarX") const
        {
            return create(properties, configName, configDomain);
        }

        /**
         * @brief Creates ice properties form the given dictionary and calls armarx::Component::create<T> for the type passed to createComponentCreator
         * @param dict The properties given as Ice::PropertyDict
         * @see armarx::Component::create
         * @return The created component.
         */
        ComponentPtr operator()(Ice::PropertyDict dict, const std::string& configName = "", const std::string& configDomain = "ArmarX") const
        {
            auto prop = Ice::createProperties();
            for (const auto& elem : dict)
            {
                prop->setProperty(elem.first, elem.second);
            }
            return create(std::move(prop), configName, configDomain);
        }
    private:
        template<class ComponentT> friend struct RegisterComponent;

        /**
         * @brief The armarx::Component::create function for the represented type
         */
        std::function<ComponentPtr(Ice::PropertiesPtr, const std::string&, const std::string&)> create;
    };

    /**
     * @brief A class performing component registration on its construction.
     *
     * Register per calling
     * \code{.cpp}
     * RegisterComponent<ComponentT> registerVar{ComponentName};
     * \endcode
     *
     * \ingroup ComponentFactories
     */
    template<class ComponentT>
    struct RegisterComponent
    {
        RegisterComponent(std::string registrationName)
        {
            ComponentCreatorObject componentCreator;
            componentCreator.create = armarx::Component::create<ComponentT>;
            ComponentFactory::RegisterElement {registrationName, componentCreator};
        }
    };
}
