/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include "PropertyDefinitionInterface.h"
#include "PropertyDefinition.h"
#include "PropertyDefinitionFormatter.h"

#include <ArmarXCore/core/exceptions/Exception.h>



namespace armarx
{
    class PropertyDefinitionBase;

    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionContainer
     * @brief PropertyDefinitionContainer
     */
    class PropertyDefinitionContainer: public virtual IceUtil::Shared
    {
    public:

        typedef std::map<std::string, PropertyDefinitionBase*>
        DefinitionContainer;

        PropertyDefinitionContainer(const std::string& prefix);

        ~PropertyDefinitionContainer() override;

        Ice::PropertiesPtr getProperties();

        void setProperties(Ice::PropertiesPtr properties);

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& defineRequiredProperty(
            const std::string& name,
            const std::string& description = "",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& defineOptionalProperty(
            const std::string& name,
            PropertyType defaultValue,
            const std::string& description = "",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        PropertyDefinitionBase* getDefinitionBase(const std::string& name);

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& getDefintion(const std::string& name);
        bool hasDefinition(const std::string& name)
        {
            return (definitions.find(name) != definitions.end());
        }

        std::string toString(PropertyDefinitionFormatter& formatter);

        /**
         * Returns the detailed description of the property user
         *
         * @return detailed description text
         */
        std::string getDescription() const;

        /**
         * Sets the detailed description of the property user
         *
         * @param description detailed description text
         */
        void setDescription(const std::string& description);

        void setPrefix(std::string prefix);

        std::string getPrefix();

        bool isPropertySet(const std::string& name);

        std::string getValue(const std::string& name);

        Ice::PropertyDict getPropertyValues(const std::string& prefix = "") const;

    protected:
        /**
         * Property definitions container
         */
        DefinitionContainer definitions;

        /**
         * Prefix of the properties such as namespace, domain, component name,
         * etc.
         */
        std::string prefix;

        /**
         * Property User description
         */
        std::string description;

        /**
         * PropertyUser brief description
         */
        std::string briefDescription;

        Ice::PropertiesPtr properties;
    };


    /**
     * PropertyDefinitions smart pointer type
     */
    typedef IceUtil::Handle<PropertyDefinitionContainer> PropertyDefinitionsPtr;

    /* ====================================================================== */
    /* === Property Definition Container Implementation ===================== */
    /* ====================================================================== */

    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::defineRequiredProperty(const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness)
    {
        PropertyDefinition<PropertyType>* def =
            new PropertyDefinition<PropertyType>(name, description, constness);

        def->setTypeIdName(typeid(PropertyType).name());

        definitions[name] = static_cast<PropertyDefinitionBase*>(def);

        return *def;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::defineOptionalProperty(
        const std::string& name,
        PropertyType defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness)
    {
        PropertyDefinition<PropertyType>* def =
            new PropertyDefinition<PropertyType>(name,
                    defaultValue,
                    description,
                    constness);

        def->setTypeIdName(typeid(PropertyType).name());

        definitions[name] = static_cast<PropertyDefinitionBase*>(def);

        return *def;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::getDefintion(const std::string& name)
    {
        // check definition existance
        if (definitions.find(name) == definitions.end())
        {
            std::stringstream ss;
            ss << name + " property is not defined. Defined are ";
            for (typename DefinitionContainer::iterator it = definitions.begin(); it != definitions.end(); ++it)
            {
                ss << "\n" << it->first;
            }
            throw armarx::LocalException(ss.str());
        }

        PropertyDefinitionBase* def = definitions[name];

        // check definition type
        if (def->getTypeIdName().compare(typeid(PropertyType).name()) != 0)
        {
            throw armarx::LocalException(
                std::string("Calling getProperty<T>() for the property '")
                + name
                + "' with wrong property type [note: "
                + typeid(PropertyType).name()
                + " instead of "
                + def->getTypeIdName()
                + "]");
        }

        // retrieve and down cast the requested definition
        return *dynamic_cast< PropertyDefinition<PropertyType>* >(def);
    }


    template <>
    PropertyDefinition<bool>&
    PropertyDefinitionContainer::defineOptionalProperty(
        const std::string& name,
        bool defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness);
    template <>
    PropertyDefinition<bool>&
    PropertyDefinitionContainer::defineRequiredProperty(const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness);

    /**
     * Parses strings like "key1:value1,key2:value2" to a string-string map. Escaping of , and : is *not* supported. Keys and values are trimmed of whitespaces.
     */
    template <>
    PropertyDefinition<std::map<std::string, std::string> >&
    PropertyDefinitionContainer::defineOptionalProperty(
        const std::string& name,
        std::map<std::string, std::string> defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness);
    /**
     * Parses strings like "key1:value1,key2:value2" to a string-string map. Escaping of , and : is *not* supported. Keys and values are trimmed of whitespaces.
     */
    template <>
    PropertyDefinition<std::map<std::string, std::string> >&
    PropertyDefinitionContainer::defineRequiredProperty(const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness);

    /**
     * Parses strings like "value1,value2" to a string-vector. Commas in double-quoted strings are ignored. The values are trimmed of whitespaces.
     */
    template <>
    PropertyDefinition<Ice::StringSeq>&
    PropertyDefinitionContainer::defineOptionalProperty(
        const std::string& name,
        Ice::StringSeq defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness);
    /**
     * Parses strings like "value1,value2" to a string-vector. Commas in double-quoted strings are ignored. The values are trimmed of whitespaces.
     */
    template <>
    PropertyDefinition<Ice::StringSeq>&
    PropertyDefinitionContainer::defineRequiredProperty(const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness);


}

