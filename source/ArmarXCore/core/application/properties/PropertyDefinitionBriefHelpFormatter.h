/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "PropertyDefinitionFormatter.h"

#include <string>
#include <vector>
#include <list>

// Workaround for QTBUG-22829
#ifndef Q_MOC_RUN
#include <boost/program_options.hpp>
#endif

namespace armarx
{
    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionBriefHelpFormatter
     * @brief PropertyDefinitionBriefHelpFormatter
     */
    class PropertyDefinitionBriefHelpFormatter:
        public PropertyDefinitionFormatter
    {
    public:
        PropertyDefinitionBriefHelpFormatter(boost::program_options::options_description* optionsDescription);

        std::string formatDefinition(std::string name,
                                     std::string description,
                                     std::string min,
                                     std::string max,
                                     std::string defaultValue,
                                     std::string casesensitivity,
                                     std::string requirement,
                                     std::string reged,
                                     std::vector<std::string> values, std::string value) override;

        std::string formatName(std::string name) override
        {
            return std::string();
        }
        std::string formatDescription(std::string description) override
        {
            return std::string();
        }
        std::string formatBounds(std::string min, std::string max) override
        {
            return std::string();
        }
        std::string formatDefault(std::string default_) override
        {
            return std::string();
        }
        std::string formatCaseSensitivity(std::string caseSensitivity) override
        {
            return std::string();
        }
        std::string formatRequirement(std::string requirement) override
        {
            return std::string();
        }
        std::string formatRegex(std::string regex) override
        {
            return std::string();
        }
        std::string formatValue(std::string value) override
        {
            return std::string();
        }
        std::string formatValues(std::vector<std::string> values) override
        {
            return std::string();
        }
        std::string formatAttribute(std::string name, std::string details) override
        {
            return std::string();
        }

        std::string formatHeader(std::string headerText) override;

        std::string getFormat() override
        {
            return std::string();
        }

    protected:
        boost::program_options::options_description* optionsDescription;
    };
}

