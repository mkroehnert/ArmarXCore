/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "PropertyUser.h"
#include "PropertyDefinitionFormatter.h"

#include <string>
#include <list>

namespace armarx
{
    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionContainerFormatter
     * @brief PropertyDefinitionContainerFormatter
     */
    class PropertyDefinitionContainerFormatter
    {
    public:
        PropertyDefinitionContainerFormatter(
            PropertyDefinitionFormatter& defFormatter):
            defFormatter(defFormatter)
        {

        }
        virtual ~PropertyDefinitionContainerFormatter() {}

        void setProperties(Ice::PropertiesPtr properties)
        {
            this->properties = properties;
        }

        std::string formatPropertyDefinitionContainer(PropertyDefinitionsPtr container)
        {
            std::string output;
            container->setProperties(properties);
            output += defFormatter.formatHeader(container->getDescription());
            output += container->toString(defFormatter);
            return output;
        }

        virtual std::string formatPropertyUsers(
            const PropertyUserList& propertyUserList)
        {
            std::string output;

            for (PropertyUserList::const_iterator it = propertyUserList.begin();
                 it != propertyUserList.end();
                 ++it)
            {
                output += formatPropertyDefinitionContainer((*it)->getPropertyDefinitions());
            }

            return output;
        }



    protected:
        PropertyDefinitionFormatter& defFormatter;
        Ice::PropertiesPtr properties;
    };
}

