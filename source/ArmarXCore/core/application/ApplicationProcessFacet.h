/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/Process.h>

namespace armarx
{
    // forward declarations
    class ArmarXManager;
    typedef IceUtil::Handle<ArmarXManager> ArmarXManagerPtr;

    /**
     * \brief The ApplicationProcessFacet class implements the Ice::Process facet to allow graceful shutdown of ArmarX applications when they are run via IceGrid.
     *
     * Detailed instructions can be found here:
     * https://doc.zeroc.com/display/Ice34/The+Process+Facet
     * https://doc.zeroc.com/display/Ice34/IceGrid+and+the+Administrative+Facility#IceGridandtheAdministrativeFacility-DeactivatingaDeployedServer
     */
    class ApplicationProcessFacet :
        public Ice::Process
    {
    public:
        ApplicationProcessFacet(const ArmarXManagerPtr& armarXManager);

        /**
         * \brief shutdown calls ArmarXManager::shutdown()
         */
        void shutdown(const Ice::Current&) override;

        /**
         * \brief writeMessage writes the string \p msg to std::out (\p fd = 1) or std::cerr (\fd = 2)
         * \param msg
         * \param fd
         */
        void writeMessage(const std::string& msg, Ice::Int fd, const Ice::Current&) override;

    private:
        const ArmarXManagerPtr& armarXManager;
    };
}

