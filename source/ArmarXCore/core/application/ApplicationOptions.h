/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Jan Issac (jan dot issac at gmail dot com)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/Properties.h>             // for PropertiesPtr
#include <IceUtil/Handle.h>             // for Handle
#include <boost/program_options.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>  // for shared_ptr
#include <iostream>                     // for cout, ostream
#include <ostream>
#include <string>                       // for string

#include "../ArmarXDummyManager.h"      // for ArmarXDummyManagerPtr
#include "properties/PropertyUser.h"    // for PropertyUserList

namespace boost
{
    namespace program_options
    {
        class options_description;
    }  // namespace program_options
}  // namespace boost

namespace armarx
{
    // forward declarations
    class Application;

    /**
     * \typedef ApplicationPtr shared pointer for armarx::Application
     */
    typedef IceUtil::Handle<Application> ApplicationPtr;


    /**
     * This namespace contains all relevent methods to parse and print options and propertiesas used by the armarx::Application.
     */
    namespace ApplicationOptions
    {
        /**
          \brief Help format to display. Help format is set on commandline using the -f option.
          \ingroup Application
         */
        enum OptionsFormat
        {
            eHelpBrief,
            eOptionsDetailed,
            eDoxygen,
            eDoxygenComponentPages,
            eConfig,
            eXml
        };

        /**
          \brief Stucture containing the parsed options of the application.
          \ingroup Application
         */
        struct Options
        {
            bool showHelp;
            bool showVersion;
            bool error;
            std::string outfile;
            OptionsFormat format;
            boost::shared_ptr<boost::program_options::options_description> description;
        };

        /**
         * Merge command line options into properties.
         * Command line options override already set properties.
         *
         * \param properties initialized properties object (see Ice::createProperties())
         * \param argc number of command line arguments
         * \param argv command line arguments
         *
         * \return merged properties
         */
        Ice::PropertiesPtr mergeProperties(Ice::PropertiesPtr properties, int argc, char* argv[]);

        /**
         * Parse the help options.
         *
         * \param properties properties object from Application
         * \param argc number of command line arguments
         * \param argv command line arguments
         *
         * \return the parsed options structure
         */
        Options parseHelpOptions(Ice::PropertiesPtr properties, int argc, char* argv[]);

        /**
         * Prints help according to the format selection in options.
         *
         * \param application pointer to application in order to display application properties
         * \param dummyManager pointer to dummyManager in order to display properties of add ManagedIceObjects
         * \param options the parsed help options
         * \param out optional output stream (default: std::cout)
         */
        void showHelp(ApplicationPtr application, ArmarXDummyManagerPtr dummyManager, Options options, Ice::PropertiesPtr properties, std::ostream& out = std::cout);

        /**
         * Return the list of property users.
         * In this case all components are property users stored in the ComponentManager.
         *
         * \return PropertyUser List
         */
        PropertyUserList getPropertyUsers(ArmarXDummyManagerPtr dummyManager);
    }
}

