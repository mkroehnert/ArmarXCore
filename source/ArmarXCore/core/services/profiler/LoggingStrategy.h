#pragma once

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::services::profiler
 * @author     Manfred Kroehnert ( manfred dot kroehnert at dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/interface/statechart/StatechartIce.h>

#include <boost/shared_ptr.hpp>

#include <string>

#include <unistd.h>


namespace armarx
{
    struct ProfilerStatechartTransition;

    namespace Profiler
    {
        class LoggingStrategy;
        typedef boost::shared_ptr<LoggingStrategy> LoggingStrategyPtr;
        /**
         * @class LoggingStrategy
         * @ingroup Profiling
         * @brief A brief description
         *
         * Detailed Description
         */
        class LoggingStrategy
        {
        public:
            LoggingStrategy() :
                id("")
            {
            }

            virtual ~LoggingStrategy()
            {
            }

            void setId(const std::string& id)
            {
                this->id = id;
            }

            virtual void logEvent(pid_t processId, uint64_t timestamp, const std::string& executableName, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName)
            {
                // empty implementation
            }

            virtual void logStatechartTransition(const ProfilerStatechartTransition& transition)
            {
                // empty implementation
            }

            virtual void logStatechartInputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& inputParameterMap)
            {
                // empty implementation
            }

            virtual void logStatechartLocalParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& localParameterMap)
            {
                // empty implementation
            }

            virtual void logStatechartOutputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& outputParameterMap)
            {
                // empty implementation
            }



            virtual void logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage)
            {
                // empty implementation
            }

            virtual void logProcessMemoryUsage(pid_t processId, uint64_t timestamp, int memoryUsage)
            {
                // empty implementation
            }

        protected:
            std::string id;
        };
    }
}

