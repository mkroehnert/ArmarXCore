/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::services::profiler
 * @author     Manfred Kroehnert ( manfred dot kroehnert at dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FileLoggingStrategy.h"

#include <sstream>
#include <fstream>

namespace
{
    const std::string SEPARATOR = "|";
}

void armarx::Profiler::FileLoggingStrategy::logEvent(pid_t processId, uint64_t timestamp, const std::string& executableName, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName)
{
    std::ostringstream logstring;
    logstring << processId << SEPARATOR
              << timestamp << SEPARATOR
              << timestampUnit << SEPARATOR
              << eventName << SEPARATOR
              << parentName << SEPARATOR
              << functionName << SEPARATOR;

    mutex.lock();
    eventList.push_back(logstring.str());
    mutex.unlock();
}


void armarx::Profiler::FileLoggingStrategy::writeFile()
{
    std::ofstream outputFile((id + ".trace").c_str());
    writeHeader(outputFile);

    for (std::string logline : eventList)
    {
        outputFile << logline << std::endl;
    }

    outputFile.close();
}


void armarx::Profiler::FileLoggingStrategy::writeHeader(std::ostream& outputStream)
{
    outputStream
            << "ProcessID" << SEPARATOR
            << "Timestamp" << SEPARATOR
            << "TimestampUnit" << SEPARATOR
            << "EventType" << SEPARATOR
            << "Parent" << SEPARATOR
            << "Function" << SEPARATOR << std::endl;
}



armarx::Profiler::FileLoggingStrategy::FileLoggingStrategy()
{
}

armarx::Profiler::FileLoggingStrategy::~FileLoggingStrategy()
{
    writeFile();
}
