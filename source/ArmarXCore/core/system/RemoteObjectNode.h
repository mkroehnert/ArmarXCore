/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>
#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandleControlBlock.h>
#include <ArmarXCore/interface/core/RemoteObjectNode.h>  // for upCast, etc
#include <Ice/BuiltinSequences.h>       // for StringSeq
#include <Ice/Config.h>                 // for Long
#include <Ice/Current.h>                // for Current
#include <Ice/Handle.h>                 // for Handle
#include <Ice/Identity.h>               // for Identity, IdentitySeq
#include <Ice/ObjectF.h>                // for ObjectPtr, upCast
#include <Ice/PropertiesF.h>            // for upCast
#include <Ice/ProxyF.h>                 // for ObjectPrx
#include <assert.h>                     // for assert
#include <atomic>                       // for atomic, atomic_bool
#include <cstddef>                      // for size_t
#include <limits>                       // for numeric_limits
#include <mutex>                        // for mutex
#include <string>                       // for string
#include <unordered_map>                // for unordered_map
#include <vector>                       // for vector

#include "ArmarXCore/core/ManagedIceObject.h"  // for ManagedIceObjectPtr
#include "ArmarXCore/core/application/properties/Property.h"
#include "ArmarXCore/core/application/properties/PropertyDefinition.hpp"
#include "ArmarXCore/core/application/properties/PropertyUser.h"
#include "ArmarXCore/interface/core/util/distributed/RemoteHandle/ClientSideRemoteHandleControlBlock.h"

namespace armarx
{
    class RemoteObjectNode;

    /**
     * @brief An ice handle for a RemoteObjectNodeComponent
     */
    typedef IceInternal::Handle<RemoteObjectNode> RemoteObjectNodePtr;

    /**
    * @brief Properties for a RemoteObjectNodeComponent
    */
    class RemoteObjectNodePropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        /**
        * @brief ctor
        * @param prefix the used prefix for properties
        */
        RemoteObjectNodePropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<int>("CoreCount", 0, "The used count of cores (<=0 => autodetect)");
        }
    };

    /**
     * @defgroup Component-RemoteObjectNode RemoteObjectNode
     * @ingroup ArmarXCore-Components
     * @brief Implementation of the slice interface RemoteObjectNodeInterface.
     *
     */
    class RemoteObjectNode:
        virtual public RemoteObjectNodeInterface,
        virtual public armarx::Component
    {
    public:
        static unsigned getDefaultCoreCount();

        /**
        * @brief ctor
        */
        RemoteObjectNode() = default;

        /**
         * @brief dtor
         */
        ~RemoteObjectNode() override
        {
            assert(shuttingDown);
        }

        // inherited from Component
        /**
         * @brief Returns the object node's default name.
         * @return The object node's default name.
         */
        std::string getDefaultName() const override
        {
            return "RemoteObjectNode";
        }
        /**
         * @brief reads in properties
         */
        void onInitComponent() override
        {
            setCoreCount(getProperty<int>("CoreCount").getValue());
        }

        void onConnectComponent() override {}
        //virtual void onDisconnectComponent() override;
        /**
         * @brief Removes all remote objects
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return new RemoteObjectNodePropertyDefinitions(getConfigIdentifier());
        }

        //from RemoteObjectNodeInterface
        Ice::ObjectPrx createPersistentComponent(const std::string& componentFactoryName, const std::string& registrationName, const ComponentParameter& params, const Ice::Current& = Ice::Current()) override;
        ClientSideRemoteHandleControlBlockBasePtr createRemoteHandledComponent(const std::string& componentFactoryName, const std::string& registrationName, const ComponentParameter& params, const Ice::Current& = Ice::Current()) override;

        Ice::ObjectPrx registerPersistentObjectWithIdentity(const Ice::Identity& ident, const Ice::ObjectPtr& registree, const Ice::Current& = Ice::Current()) override;
        ClientSideRemoteHandleControlBlockBasePtr registerRemoteHandledObjectWithIdentity(const Ice::Identity& ident, const Ice::ObjectPtr& registree, const Ice::Current& = Ice::Current()) override;

        Ice::ObjectPrx registerPersistentObject(const std::string& registrationName, const Ice::ObjectPtr& registree, const Ice::Current& = Ice::Current()) override;
        ClientSideRemoteHandleControlBlockBasePtr registerRemoteHandledObject(const std::string& registrationName, const Ice::ObjectPtr& registree, const Ice::Current& = Ice::Current()) override;

        /**
         * @brief Returns the number of cores to use.
         * @return The number of cores to use.
         */
        Ice::Long getNumberOfCores(const Ice::Current& = Ice::Current()) const override
        {
            return coreCountUsed;
        }
        Ice::Long getNumberOfObjects(const Ice::Current& = Ice::Current()) const override;
        Ice::Long getNumberOfPersistentObjects(const Ice::Current& = Ice::Current()) const override;
        Ice::Long getNumberOfRemoteHandledObjects(const Ice::Current& = Ice::Current()) const override;
        Ice::StringSeq getKnownComponentFactories(const Ice::Current& = Ice::Current()) const override;

        virtual void setCoreCount(int newCount);
    protected:
        //setup the object to be available per proxy
        struct ManagedIceObjectPtrAndPrx
        {
            ManagedIceObjectPtr ptr;
            Ice::ObjectPrx      prx;

        };

        ManagedIceObjectPtrAndPrx setupManagedIceObject(ManagedIceObjectPtr mioPtr, std::string registrationName);
        ManagedIceObjectPtrAndPrx setupComponent(const std::string& componentFactoryName, const std::string& registrationName, const ComponentParameter& params);

        struct IceObjectIdentityAndPrx
        {
            Ice::Identity  ident;
            Ice::ObjectPrx prx;
        };

        IceObjectIdentityAndPrx setupIceObject(const Ice::ObjectPtr& ptr, Ice::Identity ident);

        //helper add data to the RON
        ClientSideRemoteHandleControlBlockBasePtr doRegisterRemoteHandledManagedIceObjectAtRON(ManagedIceObjectPtrAndPrx mio);
        ClientSideRemoteHandleControlBlockBasePtr doRegisterRemoteHandledIceObjectAtRON(IceObjectIdentityAndPrx io);
        Ice::ObjectPrx doRegisterPersistentManagedIceObjectAtRON(ManagedIceObjectPtrAndPrx mio);
        Ice::ObjectPrx doRegisterPersistentIceObjectAtRON(IceObjectIdentityAndPrx io);

        //cleanup
        void removeRemoteHandledObject(Ice::Long id);
        void shutdownAndCleanup();

        //data
        std::atomic<Ice::Long> nextRemoteHandledObjectId {std::numeric_limits<Ice::Long>::min()};
        std::atomic_bool shuttingDown {false};

        /**
         * @brief the map of running objects
         */
        std::unordered_map<Ice::Long, RemoteHandleControlBlockPtr> remoteHandledObjects;
        /**
         * @brief The persistent managed ice objects
         */
        std::vector<ManagedIceObjectPtr> persistentManagedIceObjects;
        /**
         * @brief The identities of persistent ice objects
         */
        Ice::IdentitySeq                 persistentIceObjectIdentities;

        /**
         * @brief mutex to protect the map
         */
        mutable std::mutex dataMutex;

        /**
         * @brief The nuber of cores returned, when querried for it.
         */
        std::size_t coreCountUsed;
    };
}
