/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#include <ArmarXCore/core/ArmarXManager.h>  // for ArmarXManager
#include <ArmarXCore/core/ComponentFactories.h>  // for ComponentFactory
#include <Ice/Object.h>                 // for Object
#include <Ice/ObjectAdapter.h>          // for ObjectAdapter
#include <Ice/PropertiesAdmin.h>        // for PropertyDict
#include <IceUtil/Handle.h>             // for HandleBase, Handle
#include <boost/smart_ptr/shared_ptr.hpp>  // for shared_ptr
#include <boost/thread/detail/thread.hpp>  // for thread
#include <algorithm>                    // for move, max
#include <ostream>                      // for operator<<, etc
#include <thread>                       // for thread
#include <utility>                      // for pair

#include "ArmarXCore/core/Component.h"  // for Component
#include "ArmarXCore/core/logging/LogSender.h"  // for LogSender
#include "ArmarXCore/core/logging/Logging.h"  // for ARMARX_DEBUG_S, etc
#include "ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandleControlBlock.h"
#include "ArmarXCore/interface/core/RemoteObjectNode.h"
#include "ArmarXCore/interface/core/UserException.h"
#include "ArmarXCore/interface/core/util/distributed/RemoteHandle/RemoteHandleControlBlock.h"
#include "RemoteObjectNode.h"

namespace armarx
{
    /**
     * @brief The processor thread count according to boost.
     */
    static const unsigned CORE_COUNT_BOOST = boost::thread::hardware_concurrency();
    /**
     * @brief The processor thread count according to std. (in libstd shipped with gcc 4.6 this is 0)
     */
    static const unsigned CORE_COUNT_STD = std::thread::hardware_concurrency();
    /**
     * @brief The minimal used processor thread count. (in case both boost and std return 0)
     */
    static const unsigned CORE_COUNT_MIN = 1;
    /**
     * @brief Used core count. (in case both boost and std return 0 we will still use 1)
     */
    static const unsigned CORE_COUNT_DEFAULT = std::max(std::max(CORE_COUNT_BOOST, CORE_COUNT_STD), CORE_COUNT_MIN);

    unsigned RemoteObjectNode::getDefaultCoreCount()
    {
        return CORE_COUNT_DEFAULT;
    }

    void RemoteObjectNode::setCoreCount(int newCount)
    {
        coreCountUsed = newCount < 1 ? getDefaultCoreCount() : newCount;
        ARMARX_DEBUG_S << "processor core count according to (std/boost/requested from user) : " << CORE_COUNT_STD << "/" << CORE_COUNT_BOOST << "/" << newCount;
        ARMARX_VERBOSE_S << "processor core count used: " << coreCountUsed;
    }

    void RemoteObjectNode::onExitComponent()
    {
        assert(!shuttingDown);
        shutdownAndCleanup();
    }

    Ice::Long RemoteObjectNode::getNumberOfObjects(const Ice::Current&) const
    {
        std::lock_guard<std::mutex> lock {dataMutex};
        return persistentIceObjectIdentities.size() + persistentManagedIceObjects.size() + remoteHandledObjects.size();
    }
    Ice::Long RemoteObjectNode::getNumberOfPersistentObjects(const Ice::Current&) const
    {
        std::lock_guard<std::mutex> lock {dataMutex};
        return persistentIceObjectIdentities.size() + persistentManagedIceObjects.size();
    }
    Ice::Long RemoteObjectNode::getNumberOfRemoteHandledObjects(const Ice::Current&) const
    {
        std::lock_guard<std::mutex> lock {dataMutex};
        return remoteHandledObjects.size();
    }
    Ice::StringSeq RemoteObjectNode::getKnownComponentFactories(const Ice::Current&) const
    {
        return ComponentFactory::getKeys();
    }

    Ice::ObjectPrx RemoteObjectNode::createPersistentComponent(const std::string& componentFactoryName, const std::string& registrationName, const ComponentParameter& params, const Ice::Current&)
    {
        if (shuttingDown)
        {
            throw ServerShuttingDown {};
        }
        return  doRegisterPersistentManagedIceObjectAtRON(setupComponent(componentFactoryName, registrationName, params));
    }
    ClientSideRemoteHandleControlBlockBasePtr RemoteObjectNode::createRemoteHandledComponent(const std::string& componentFactoryName, const std::string& registrationName, const ComponentParameter& params, const Ice::Current&)
    {
        if (shuttingDown)
        {
            throw ServerShuttingDown {};
        }
        return doRegisterRemoteHandledManagedIceObjectAtRON(setupComponent(componentFactoryName, registrationName, params));
    }

    Ice::ObjectPrx RemoteObjectNode::registerPersistentObjectWithIdentity(const Ice::Identity& ident, const Ice::ObjectPtr& registree, const Ice::Current&)
    {
        if (shuttingDown)
        {
            throw ServerShuttingDown {};
        }
        auto mioPtr = ManagedIceObjectPtr::dynamicCast(registree);

        if (mioPtr)
        {
            return doRegisterPersistentManagedIceObjectAtRON(setupManagedIceObject(std::move(mioPtr), ident.category + ident.name));
        }
        return doRegisterPersistentIceObjectAtRON(setupIceObject(registree, ident));
    }
    ClientSideRemoteHandleControlBlockBasePtr RemoteObjectNode::registerRemoteHandledObjectWithIdentity(const Ice::Identity& ident, const Ice::ObjectPtr& registree, const Ice::Current&)
    {
        if (shuttingDown)
        {
            throw ServerShuttingDown {};
        }
        auto mioPtr = ManagedIceObjectPtr::dynamicCast(registree);

        if (mioPtr)
        {
            return doRegisterRemoteHandledManagedIceObjectAtRON(setupManagedIceObject(std::move(mioPtr), ident.category + ident.name));
        }
        return doRegisterRemoteHandledIceObjectAtRON(setupIceObject(registree, ident));
    }

    Ice::ObjectPrx RemoteObjectNode::registerPersistentObject(const std::string& registrationName, const Ice::ObjectPtr& registree, const Ice::Current&)
    {
        if (shuttingDown)
        {
            throw ServerShuttingDown {};
        }
        auto mioPtr = ManagedIceObjectPtr::dynamicCast(registree);

        if (mioPtr)
        {
            return doRegisterPersistentManagedIceObjectAtRON(setupManagedIceObject(std::move(mioPtr), registrationName));
        }
        Ice::Identity ident;
        ident.name = registrationName;
        return doRegisterPersistentIceObjectAtRON(setupIceObject(registree, std::move(ident)));
    }
    ClientSideRemoteHandleControlBlockBasePtr RemoteObjectNode::registerRemoteHandledObject(const std::string& registrationName, const Ice::ObjectPtr& registree, const Ice::Current&)
    {
        if (shuttingDown)
        {
            throw ServerShuttingDown {};
        }
        auto mioPtr = ManagedIceObjectPtr::dynamicCast(registree);

        if (mioPtr)
        {
            return doRegisterRemoteHandledManagedIceObjectAtRON(setupManagedIceObject(std::move(mioPtr), registrationName));
        }
        Ice::Identity ident;
        ident.name = registrationName;
        return doRegisterRemoteHandledIceObjectAtRON(setupIceObject(registree, std::move(ident)));
    }

    RemoteObjectNode::ManagedIceObjectPtrAndPrx RemoteObjectNode::setupManagedIceObject(ManagedIceObjectPtr mioPtr, std::string registrationName)
    {
        getArmarXManager()->addObject(mioPtr, true, registrationName);
        //the self proxy could still be null => wait for it
        auto mioPrx = mioPtr->getProxy(-1);
        return {std::move(mioPtr), std::move(mioPrx)};
    }
    RemoteObjectNode::ManagedIceObjectPtrAndPrx RemoteObjectNode::setupComponent(const std::string& componentFactoryName, const std::string& registrationName, const ComponentParameter& params)
    {
        if (!ComponentFactory::has(componentFactoryName))
        {
            throw NoSuchComponentFactory {"There is no component factory for the name " + componentFactoryName};
        }
        return setupManagedIceObject(ComponentFactory::get(componentFactoryName)(params.prop, params.configName, params.configDomain), registrationName);
    }
    RemoteObjectNode::IceObjectIdentityAndPrx RemoteObjectNode::setupIceObject(const Ice::ObjectPtr& ptr, Ice::Identity ident)
    {
        auto ioPrx = getArmarXManager()->getAdapter()->add(ptr, ident);
        return {std::move(ident), std::move(ioPrx)};
    }

    ClientSideRemoteHandleControlBlockBasePtr RemoteObjectNode::doRegisterRemoteHandledManagedIceObjectAtRON(RemoteObjectNode::ManagedIceObjectPtrAndPrx mio)
    {
        auto mioPtr = std::move(mio.ptr);
        auto axManager = getArmarXManager();

        auto id = nextRemoteHandledObjectId++;
        RemoteObjectNodePtr ron = this;

        auto mioRH = RemoteHandleControlBlock::create(
                         axManager,
                         std::move(mio.prx),
                         [axManager, mioPtr, ron, id]
        {
            axManager->removeObjectNonBlocking(mioPtr);
            ron->removeRemoteHandledObject(id);
        }
                     );
        {
            std::lock_guard<std::mutex> lock(dataMutex);
            if (shuttingDown)
            {
                mioRH.directHandle->forceDeletion();
                throw ServerShuttingDown {};
            }
            remoteHandledObjects[id] = std::move(mioRH.directHandle);
        }
        return mioRH.clientSideRemoteHandleControlBlock;
    }
    ClientSideRemoteHandleControlBlockBasePtr RemoteObjectNode::doRegisterRemoteHandledIceObjectAtRON(RemoteObjectNode::IceObjectIdentityAndPrx io)
    {
        auto ioIdent = std::move(io.ident);
        auto objAdapter = getArmarXManager()->getAdapter();

        auto id = nextRemoteHandledObjectId++;
        RemoteObjectNodePtr ron = this;

        auto ioRH = RemoteHandleControlBlock::create(
                        getArmarXManager(),
                        std::move(io.prx),
                        [objAdapter, ioIdent, ron, id]
        {
            objAdapter->remove(ioIdent);
            ron->removeRemoteHandledObject(id);
        }
                    );
        {
            std::lock_guard<std::mutex> lock(dataMutex);
            if (shuttingDown)
            {
                ioRH.directHandle->forceDeletion();
                throw ServerShuttingDown {};
            }
            remoteHandledObjects[id] = std::move(ioRH.directHandle);
        }
        return ioRH.clientSideRemoteHandleControlBlock;
    }
    Ice::ObjectPrx                            RemoteObjectNode::doRegisterPersistentManagedIceObjectAtRON(RemoteObjectNode::ManagedIceObjectPtrAndPrx mio)
    {
        {
            std::lock_guard<std::mutex> lock(dataMutex);
            if (shuttingDown)
            {
                getArmarXManager()->removeObjectBlocking(mio.ptr);
                throw ServerShuttingDown {};
            }
            persistentManagedIceObjects.emplace_back(std::move(mio.ptr));
        }
        return mio.prx;
    }
    Ice::ObjectPrx                            RemoteObjectNode::doRegisterPersistentIceObjectAtRON(RemoteObjectNode::IceObjectIdentityAndPrx    io)
    {
        {
            std::lock_guard<std::mutex> lock(dataMutex);
            if (shuttingDown)
            {
                getArmarXManager()->getAdapter()->remove(io.ident);
                throw ServerShuttingDown {};
            }
            persistentIceObjectIdentities.emplace_back(std::move(io.ident));
        }
        return io.prx;
    }

    void RemoteObjectNode::removeRemoteHandledObject(Ice::Long id)
    {
        std::lock_guard<std::mutex> lock(dataMutex);
        auto it = remoteHandledObjects.find(id);
        if (it == remoteHandledObjects.end())
        {
            ARMARX_ERROR_S << "RON " << getName() << " has no object with id " << id;
            return;
        }
        //just to be safe
        it->second->forceDeletion();
        remoteHandledObjects.erase(it);
    }

    void RemoteObjectNode::shutdownAndCleanup()
    {
        shuttingDown = true;
        std::lock_guard<std::mutex> lock(dataMutex);
        for (auto& elem : remoteHandledObjects)
        {
            elem.second->forceDeletion();
        }

        auto axManager = getArmarXManager();
        for (auto& obj : persistentManagedIceObjects)
        {
            axManager->removeObjectBlocking(obj);
        }
        persistentManagedIceObjects.clear();

        auto adapter   = axManager->getAdapter();
        for (auto& ident : persistentIceObjectIdentities)
        {
            adapter->remove(ident);
        }
        persistentIceObjectIdentities.clear();
    }
}
