/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
* @package    ArmarXCore
* @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "RemoteHandleControlBlock.h"

namespace armarx
{
    const unsigned int SharedRemoteHandleState::DEFAULT_DELETION_DELAY
    {
        3000
    };

    void RemoteHandleControlBlock::decrementUseCount(const Ice::Current&)
    {
        std::lock_guard<std::mutex> guard {mtx};
        assert(useCount);
        if (! --useCount)
        {
            lastTimeUseCountReachedZero = clock::now();
        }
    }

    ClientSideRemoteHandleControlBlockBasePtr RemoteHandleControlBlock::getClientSideRemoteHandleControlBlock(const Ice::Current&)
    {
        assert(managedObjectProxy);
        assert(selfProxy);
        return new ClientSideRemoteHandleControlBlock {selfProxy, managedObjectProxy};
    }

    RemoteHandleControlBlock::RemoteHandleControlBlock(Ice::ObjectAdapterPtr objectAdapter, Ice::ObjectPrx managedObjectPrx, std::function<void ()> deleter):
        objectAdapter {std::move(objectAdapter)},
        deleter {std::move(deleter)},
        managedObjectProxy {std::move(managedObjectPrx)}
    {
        if (!objectAdapter)
        {
            throw std::invalid_argument {"RemoteHandleControlBlock() called with a null object adapter."};
        }
        if (!managedObjectPrx)
        {
            throw std::invalid_argument {"RemoteHandleControlBlock() called with a null proxy."};
        }

        Ice::Identity ident;
        ident.name = "RemoteHandleControlBlock::" + managedObjectProxy->ice_getIdentity().name + "::" + IceUtil::generateUUID();
        selfProxy = RemoteHandleControlBlockInterfacePrx::uncheckedCast(objectAdapter->add(this, std::move(ident)));
    }

    SharedRemoteHandleState::SharedRemoteHandleState(unsigned int deletionDelayMs):
        deletionDelay {deletionDelayMs},
        sweeper {new PeriodicTaskType{this, &armarx::SharedRemoteHandleState::sweep, static_cast<int>(deletionDelayMs)}}
    {
        sweeper->start();
    }

    SharedRemoteHandleState::~SharedRemoteHandleState()
    {
        std::lock_guard<std::mutex> stateGuard {stateMutex};
        sweeper->stop();

        //delete all rhs
        for (auto& rh : rhs)
        {
            rh->cleanup();
        }
        rhs.clear();
    }

    void SharedRemoteHandleState::add(RemoteHandleControlBlockPtr rh)
    {
        std::lock_guard<std::mutex> stateGuard {stateMutex};
        rhs.emplace(std::move(rh));
    }

    void SharedRemoteHandleState::sweep()
    {
        auto deletionTime = RemoteHandleControlBlock::clock::now() - deletionDelay;
        std::lock_guard<std::mutex> stateGuard {stateMutex};
        auto blockIt = rhs.begin();
        while (blockIt != rhs.end())
        {
            auto curBlockIt = blockIt++;
            auto& block = *curBlockIt;

            std::unique_lock<std::mutex> blockGuard {block->mtx};
            if (
                //count is 0 and it reached zero more than deletionDelay ago
                (!block->useCount && deletionTime > block->lastTimeUseCountReachedZero) ||
                block->forcedDeletion
            )
            {
                //clean up
                block->cleanup();
                //remove from set (blockIt stays valid)
                rhs.erase(std::move(curBlockIt));
            }
        }
    }
}
