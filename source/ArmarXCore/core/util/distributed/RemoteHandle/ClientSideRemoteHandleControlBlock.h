/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
* @package    ArmarXCore
* @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <boost/shared_ptr.hpp>

#include <ArmarXCore/interface/core/util/distributed/RemoteHandle/RemoteHandleControlBlock.h>
#include <ArmarXCore/interface/core/util/distributed/RemoteHandle/ClientSideRemoteHandleControlBlock.h>

namespace armarx
{
    class ClientSideRemoteHandleControlBlock;
    using ClientSideRemoteHandleControlBlockPtr = boost::shared_ptr<ClientSideRemoteHandleControlBlock>;

    /**
     * @brief The ClientSideRemoteHandleControlBlock is used at the client side for reference counting.
     * It is send from the server to the client.
     * Its dtor decrements the use count.
     * If you get this class put it into a RemoteHandle!
     * (for more info: \ref ArmarXCore-Tutorials-RemoteHandles "RemoteHandle tutorial")
     *
     * \ingroup Distributed
     */
    class ClientSideRemoteHandleControlBlock:
        public ClientSideRemoteHandleControlBlockBase
    {
    public:
        ClientSideRemoteHandleControlBlock(const RemoteHandleControlBlockInterfacePrx& controlBlock, const Ice::ObjectPrx& object);
        ClientSideRemoteHandleControlBlock(const ClientSideRemoteHandleControlBlock&) = delete;
        ClientSideRemoteHandleControlBlock(ClientSideRemoteHandleControlBlock&&) = default;

        ~ClientSideRemoteHandleControlBlock() override;

        ClientSideRemoteHandleControlBlock& operator =(ClientSideRemoteHandleControlBlock&& other) = default;
        template<class T>
        ClientSideRemoteHandleControlBlock& operator =(T&&) = delete;

        void ice_postUnmarshal() override;
        Ice::ObjectPrx getManagedObjectProxy(const Ice::Current& = Ice::Current()) override;
    private:
        template <class IceBaseClass, class DerivedClass> friend class GenericFactory;
        ClientSideRemoteHandleControlBlock() = default;
    };

    inline ClientSideRemoteHandleControlBlock::ClientSideRemoteHandleControlBlock(const RemoteHandleControlBlockInterfacePrx& controlBlock, const Ice::ObjectPrx& object):
        ClientSideRemoteHandleControlBlockBase(controlBlock, object)
    {
        assert(managedObjectProxy);
        assert(remoteHandleControlBlockProxy);
        if (!managedObjectProxy || !remoteHandleControlBlockProxy)
        {
            throw std::invalid_argument {"Either the control block or the object proxy is null!"};
        }
        remoteHandleControlBlockProxy->incrementUseCount();
    }

    inline ClientSideRemoteHandleControlBlock::~ClientSideRemoteHandleControlBlock()
    {
        try
        {
            if (remoteHandleControlBlockProxy)
            {
                remoteHandleControlBlockProxy->decrementUseCount();
            }
        }
        catch (...)
        {
            // never throw
        }
    }

    inline void ClientSideRemoteHandleControlBlock::ice_postUnmarshal()
    {
        remoteHandleControlBlockProxy->incrementUseCount();
    }

    inline Ice::ObjectPrx ClientSideRemoteHandleControlBlock::getManagedObjectProxy(const Ice::Current&)
    {
        return managedObjectProxy;
    }
}
