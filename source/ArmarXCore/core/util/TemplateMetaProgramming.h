/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <type_traits>
#include <tuple>
#include <cstddef>

namespace armarx
{
    namespace meta
    {
        /// @brief Removes rvalue ref from a type
        template<class T> struct RemoveRvalueReference
        {
            using type = T;
        };
        template<class T> struct RemoveRvalueReference < T&& >
        {
            using type = T;
        };
        template<class T> using  RemoveRvalueReferenceT = typename RemoveRvalueReference<T>::type;

        /// @brief Helper for sfinae (is added in c++17)
        template<class...>
        using void_t = void;

        /**
         * @brief Traits about templates taking only types as parameters.
         *
         * \ingroup core-utility
         */
        struct TypeTemplateTraits
        {
            /**
             * @brief Whether a type T is the instance of a given template Template. (this is the case for false)
             */
            template<template<class...> class Template, class T       > struct IsInstanceOf                                : std::false_type {};
            /**
             * @brief Whether a type T is the instance of a given template Template. (this is the case for true)
             */
            template<template<class...> class Template, class...Params> struct IsInstanceOf<Template, Template<Params...>> : std:: true_type {};


            template<class T>
            struct DisassembleTemplate : std::false_type {};
            template<template<class...> class Template, class...Params>
            struct DisassembleTemplate<Template<Params...>> : std:: true_type
            {
                template<template<class...> class NewTemplate>
                using ReplaceTemplate = NewTemplate<Params...>;

                template<class...NewParams>
                using ReplaceParameters = Template<NewParams...>;
            };
        };

        /**
         * @brief Get the type of the last element of a template parameter pack.
         */
        template<class T0, class...Ts>
        using last_type = typename std::decay < typename std::tuple_element < sizeof...(Ts), std::tuple<T0, Ts... >>::type >::type;

        /**
         * @brief Get the type of the first element of a template parameter pack.
         */
        template<class T0, class...Ts>
        using first_type = T0;
        /**
         * @brief Get the type of the second element of a template parameter pack.
         */
        template<class T0, class T1, class...Ts>
        using second_type = T1;
        /**
         * @brief Get the type of the third element of a template parameter pack.
         */
        template<class T0, class T1, class T2, class...Ts>
        using third_type = T2;


        template<std::size_t n, class...Ts>
        struct NthType {};
        template<std::size_t n, class T0, class...Ts>
        struct NthType<n, T0, Ts...>
        {
            using type = typename std::decay < typename std::tuple_element < n, std::tuple<T0, Ts... >>::type >::type;
        };

        /**
         * @brief Get the type of the nth element of a template parameter pack.
         */
        template<std::size_t n, class T0, class...Ts>
        using nth_type = typename std::decay < typename std::tuple_element < n, std::tuple<T0, Ts... >>::type >::type;

        /**
         * @brief Can be used to determine if there is an overload for std::to_string for a type T
         *
         * This is the default if there is no overload
         */
        template<class T, class = void>
        struct HasToString : std::false_type {};

        /**
         * @brief Can be used to determine if there is an overload for std::to_string for a type T
         *
         * This is the specialization if there is an overload.
         */
        template<class T>
struct HasToString < T, typename std::enable_if < std::is_same < decltype(std::to_string(std::declval<T>()), int {}), int >::value >::type > :
        std::true_type {};
        //can't use this since gcc is broken / sfinae is bad in c++11
        //struct HasToString < T, void_t<decltype(std::to_string(std::declval<T>()))> > :std::true_type {};


        /**
         * @brief Can be used to determine if T has an at method accepting a type of IdxT
         *
         * This is the default if there is no method
         */
        template<class T, class IdxT, class = void>
        struct HasAtMethod : std::false_type {};

        /**
         * @brief Can be used to determine if T has an at method accepting a type of IdxT
         *
         * This is the specialization if there is a method
         */
        template<class T, class IdxT>
struct HasAtMethod < T, IdxT, typename std::enable_if < std::is_same < decltype(std::declval<T>().at(std::declval<IdxT>()), int {}), int >::value >::type > :
        std::true_type {};
        //can't use this since gcc is broken / sfinae is bad in c++11
        //struct HasAtMethod< T, IdxT,void_t<decltype(std::declval<T>().at(std::declval<IdxT>()))>> : std::true_type {};

#define ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(CHECKNAME, FNCNAME, FNCSIG)   \
    template <class T>                                                      \
    class CHECKNAME                                                         \
    {                                                                       \
        struct one {};                                                      \
        struct two { one a, b; };                                           \
        template<class C, C> struct classAndFncPtr;                         \
        template<class C>                                                   \
        static one has(classAndFncPtr<FNCSIG, &C::FNCNAME>*);               \
        template<class T2>                                                  \
        static two has(...);                                                \
    public:                                                                 \
        enum e {value = sizeof(has<T>(0)) == sizeof(one)};                  \
    }

        template<std::size_t...>
        struct IndexSequence
        {
            using type = IndexSequence;
        };

        namespace detail
        {
            template<class S1, class S2>
            struct ConcatIndexSequences;
            template<std::size_t... I1, std::size_t... I2>
            struct ConcatIndexSequences<IndexSequence<I1...>, IndexSequence<I2...>>
                    : IndexSequence < I1..., (sizeof...(I1) + I2)... > {};

            template<std::size_t N> struct MakeIndexSequence;
            template<> struct MakeIndexSequence<0> : IndexSequence<> {};
            template<> struct MakeIndexSequence<1> : IndexSequence<0> {};
            template<std::size_t N> struct MakeIndexSequence
                : detail::ConcatIndexSequences <
                  typename MakeIndexSequence < N / 2 >::type,
                  typename MakeIndexSequence < N - N / 2 >::type
                  > {};

            template<std::size_t Lo, class>
            struct AddToIndexSequence;

            template<std::size_t Lo, std::size_t...Is>
            struct AddToIndexSequence<Lo, IndexSequence<Is...>>
            {
                using type = IndexSequence < Lo + Is ... >;
            };

            template<std::size_t Lo, std::size_t Hi>
            struct MakeIndexRange : AddToIndexSequence < Lo, typename MakeIndexSequence < Hi - Lo >::type >
            {};
        }

        template<std::size_t N>
        using MakeIndexSequence = typename detail::MakeIndexSequence<N>::type;

        template<std::size_t Lo, std::size_t Hi>
        using MakeIndexRange = typename detail::MakeIndexRange<Lo, Hi>::type;

        template<class...Ts>
        using MakeIndexSequenceFor = MakeIndexSequence<sizeof...(Ts)>;

        namespace detail
        {
            /**
             * @see armarx::meta::DecayAll
             */
            template<class T>
            struct DecayAll
            {
                using type = typename std::decay<T>::type;
            };
            /**
             * @see armarx::meta::DecayAll
             */
            template<class T>
            struct DecayAll<T*>
            {
                using type = typename DecayAll<T>::type;
            };
            /**
             * @see armarx::meta::DecayAll
             */
            template<class T, std::size_t N>
            struct DecayAll<T[N]>
            {
                using type = typename DecayAll<T>::type;
            };
        }

        /**
         * @brief Similar to std::decay but also decays ptr and array
         */
        template<class T>
        using DecayAll = detail::DecayAll<typename std::decay<T>::type>;

        template<class T>
        struct Wrapper
        {
            using type = T;
        };

        template<class Base, class...Ds>
        struct IsBaseOf : std::true_type {};
        template<class Base, class D0, class...Ds>
        struct IsBaseOf<Base, D0, Ds...> :
            std::integral_constant <
            bool,
            std::is_base_of<Base, D0> ::value&&
            IsBaseOf<Base, Ds...>::value
            > {};
    }
}
