/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once
#include "StringHelpers.h"

#include <boost/lexical_cast.hpp>

namespace armarx
{

    template<typename T>
    inline std::vector<T> Split(const std::string& source, const std::string& splitBy, bool trimElements = false, bool removeEmptyElements = false)
    {
        if (source.empty())
        {
            return std::vector<T>();
        }

        std::vector<std::string> components = Split(source, splitBy, trimElements, removeEmptyElements);
        //        boost::split(components, source, boost::is_any_of(splitBy));

        std::vector<T> result(components.size());
        std::transform(components.begin(), components.end(), result.begin(), boost::lexical_cast<T, std::string>);
        return result;
    }

}


