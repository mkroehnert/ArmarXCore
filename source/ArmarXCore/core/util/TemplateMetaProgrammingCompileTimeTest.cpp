/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( raphael dot grimm at student dot kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "TemplateMetaProgramming.h"

#include <vector>
#include <set>
#include <string>
#include <map>

namespace armarx
{
    namespace meta
    {
        struct Foo {};

        static_assert(std::is_same<int, first_type<int, long, char>>::value, "first_type broken");
        static_assert(std::is_same<char, last_type<int, long, char>>::value, "last_type broken");

        static_assert(std::is_same<void, nth_type<0, void, long, char>>::value, "nth_type broken");
        static_assert(std::is_same<long, nth_type<1, void, long, char>>::value, "nth_type broken");
        static_assert(std::is_same<char, nth_type<2, void, long, char>>::value, "nth_type broken");

        static_assert(std::is_same<void, NthType<0, void, long, char>::type>::value, "NthType broken");
        static_assert(std::is_same<long, NthType<1, void, long, char>::type>::value, "NthType broken");
        static_assert(std::is_same<char, NthType<2, void, long, char>::type>::value, "NthType broken");

        static_assert(std::is_same<void, void_t<>>::value, "void_t broken");
        static_assert(std::is_same<void, void_t<int>>::value, "void_t broken");
        static_assert(std::is_same<void, void_t<decltype(1)>>::value, "void_t broken");

        static_assert(TypeTemplateTraits::IsInstanceOf<std::vector, std::vector<int>>::value, "TypeTemplateTraits::IsInstanceOf broken");
        static_assert(!TypeTemplateTraits::IsInstanceOf<std::set, std::vector<int>>::value,    "TypeTemplateTraits::IsInstanceOf broken");

        static_assert(std::is_same<void, last_type<int, void>>::value, "last_type broken");

        static_assert(HasToString<int>::value, "HasToString broken");
        static_assert(!HasToString<Foo>::value, "HasToString broken");

        static_assert(HasAtMethod<std::vector<int>, int>::value, "HasAtMethod broken");
        static_assert(HasAtMethod<std::vector<int>, std::size_t>::value, "HasAtMethod broken");
        static_assert(HasAtMethod<std::map<std::string, int>, std::string>::value, "HasAtMethod broken");
        static_assert(!HasAtMethod<std::vector<int>, Foo>::value, "HasAtMethod broken");
        static_assert(!HasAtMethod<Foo, int>::value, "HasAtMethod broken");

        // names are describing the memberfunction foo
        // nonstatic / static (n/s)
        // return void / char (v/c)
        // accept void / char (v/c)
        struct nvv
        {
            void foo(void) {}
        };
        struct nvc
        {
            void foo(char) {}
        };
        struct ncv
        {
            char foo(void)
            {
                return 0;
            }
        };
        struct ncc
        {
            char foo(char)
            {
                return 0;
            }
        };
        struct svv
        {
            static void foo(void) {}
        };
        struct svc
        {
            static void foo(char) {}
        };
        struct scv
        {
            static char foo(void)
            {
                return 0;
            }
        };
        struct scc
        {
            static char foo(char)
            {
                return 0;
            }
        };

        ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(hasFoo, foo, void(T::*)(void));
        ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(hasStaticFoo, foo, void(*)(void));

        static_assert(hasFoo<nvv>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasFoo<nvc>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasFoo<ncv>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasFoo<ncc>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasFoo<svv>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasFoo<svc>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasFoo<scv>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasFoo<scc>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");

        static_assert(!hasStaticFoo<nvv>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasStaticFoo<nvc>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasStaticFoo<ncv>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasStaticFoo<ncc>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(hasStaticFoo<svv>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasStaticFoo<svc>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasStaticFoo<scv>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");
        static_assert(!hasStaticFoo<scc>::value, "ERROR ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK");

        template<class...> struct Template0;
        template<class...> struct Template1;

        static_assert(!TypeTemplateTraits::DisassembleTemplate<int>::value, "TypeTemplateTraits::DisassembleTemplate");
        static_assert(TypeTemplateTraits::DisassembleTemplate<Template0<int, void, char>>::value, "TypeTemplateTraits::DisassembleTemplate");
        static_assert(
            std::is_same
            <
            TypeTemplateTraits::DisassembleTemplate<Template0<int, void, char>>::ReplaceTemplate<Template1>,
            Template1<int, void, char>
            >::value, "TypeTemplateTraits::DisassembleTemplate::ReplaceTemplate");

        static_assert(
            std::is_same
            <
            TypeTemplateTraits::DisassembleTemplate<Template0<int, void, char>>::ReplaceParameters<void>,
            Template0<void>
            >::value, "TypeTemplateTraits::DisassembleTemplate::ReplaceParameters");


        static_assert(std::is_same<IndexSequence<0, 1, 2>, MakeIndexSequence<3>>::value, "MakeIndexSequence broken");
        static_assert(std::is_same<IndexSequence<0, 1, 2>, MakeIndexSequenceFor<int, int, int>>::value, "MakeIndexSequenceFor broken");
        static_assert(std::is_same<IndexSequence<2, 3, 4>, MakeIndexRange<2, 5>>::value, "MakeIndexRange broken");

        template <class T, class U = int>
        struct DecayAllTest : std::is_same<typename DecayAll<T>::type, U> {};

        //type
        static_assert(DecayAllTest<int>::value, "ERROR DecayAll");
        static_assert(DecayAllTest<int&>::value, "ERROR DecayAll");
        static_assert(DecayAllTest < int&& >::value, "ERROR DecayAll");

        static_assert(DecayAllTest<const int>::value, "ERROR DecayAll");
        static_assert(DecayAllTest<const int&>::value, "ERROR DecayAll");
        static_assert(DecayAllTest < const int&& >::value, "ERROR DecayAll");

        static_assert(DecayAllTest<int*>::value, "ERROR DecayAll");
        static_assert(DecayAllTest<int* const>::value, "ERROR DecayAll");

        static_assert(DecayAllTest<const int*>::value, "ERROR DecayAll");
        static_assert(DecayAllTest<const int* const>::value, "ERROR DecayAll");

        //arrays
        static_assert(DecayAllTest<int[2]>::value, "ERROR DecayAll");
        static_assert(DecayAllTest<std::add_lvalue_reference<int[2]>::type>::value, "ERROR DecayAll");
        static_assert(DecayAllTest<std::add_rvalue_reference<int[2]>::type>::value, "ERROR DecayAll");

        static_assert(DecayAllTest<const int[2]>::value, "ERROR DecayAll");
        static_assert(DecayAllTest<const std::add_lvalue_reference<int[2]>::type>::value, "ERROR DecayAll");
        static_assert(DecayAllTest<const std::add_rvalue_reference<int[2]>::type>::value, "ERROR DecayAll");

        static_assert(DecayAllTest<int(*)[2]>::value, "ERROR DecayAll");
        static_assert(DecayAllTest<int(* const)[2]>::value, "ERROR DecayAll");

        static_assert(DecayAllTest<const int(*)[2]>::value, "ERROR DecayAll");
        static_assert(DecayAllTest<const int(* const)[2]>::value, "ERROR DecayAll");

        static_assert(DecayAllTest<int(int), int(*)(int)>::value, "ERROR DecayAll");
    }
}
