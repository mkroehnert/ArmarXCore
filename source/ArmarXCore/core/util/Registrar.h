/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <unordered_map>
#include <vector>
#include <mutex>
#include <stdexcept>
#include <boost/lexical_cast.hpp>

namespace armarx
{
    /**
     * @brief Stores key object pairs.
     * The third template parameter offers the option to tag this registrar with a class.
     *
     * If polymorphic objects are required use a base class and unique pointers.
     * E.g.:
     * \code{.cpp}
     * class Base{};
     * class DerivedA{};
     * class DerivedB{};
     * Registrar<std::unique_ptr<Base>>::registerElement("A", DerivedA{});
     * Registrar<std::unique_ptr<Base>>::registerElement("B", DerivedB{});
     * \endcode
     *
     * If functions are stored use std::function (or function pointers).
     *
     * To register elements on file inclusion use this code in the header (note the missing static).
     * \code{.cpp}
     * Registrar<Type>::RegisterElement register("A", object);
     * \endcode
     * Example for strings
     * \code{.cpp}
     * Registrar<std::string>::RegisterElement register1("key 1", "value 1");
     * Registrar<std::string>::RegisterElement register2("key 2", "value 2");
     * \endcode
     *
     * \ingroup core-utility
     */
    template <class RegisteredType, class KeyType = std::string, class = void>
    class Registrar
    {
    public:
        using ContainerType = std::unordered_map<KeyType, RegisteredType>;

        /**
         * @brief Registers the object passed to the constructor.
         * This can be used in combination with global variables to register types when a header is included.
         */
        struct RegisterElement
        {
            RegisterElement(const KeyType& key, RegisteredType element)
            {
                registerElement(key, std::move(element));
            }
        };

        /**
         * @brief Returns the registered object for the given key.
         * @param key The object's key.
         * @return The registered object for the given key.
         */
        static const RegisteredType& get(const KeyType& key)
        {
            std::lock_guard<std::recursive_mutex> guard {registeredElementsMutex()};
            if (!has(key))
            {
                throw std::invalid_argument {"No factory for this key! " + boost::lexical_cast<std::string>(key)};
            }
            return registeredElements().at(key);
        }

        static bool has(const KeyType& key)
        {
            std::lock_guard<std::recursive_mutex> guard {registeredElementsMutex()};
            return registeredElements().end() != registeredElements().find(key);
        }

        /**
         * @brief Registers an element.
         * @param key The element's key.
         * @param element The element.
         */
        static void registerElement(const KeyType& key, RegisteredType element)
        {
            std::lock_guard<std::recursive_mutex> guard {registeredElementsMutex()};
            registeredElements()[key] = std::move(element);
        }

        /**
         * @brief Retrieves the list of all registered elements.
         * @return The list of all registered elements.
         */
        static std::vector<KeyType> getKeys()
        {
            std::lock_guard<std::recursive_mutex> guard {registeredElementsMutex()};
            std::vector<KeyType> result;
            result.reserve(registeredElements().size());
            for (const auto& elem : registeredElements())
            {
                result.emplace_back(elem.first);
            }
            return result;
        }
    private:
        /**
         * @brief Holds the map of registered elements.
         * @return The map of registered elements.
         */
        static ContainerType& registeredElements()
        {
            static ContainerType elementContainer;
            return elementContainer;
        }
        static std::recursive_mutex& registeredElementsMutex()
        {
            static std::recursive_mutex elementContainerMutex;
            return elementContainerMutex;
        }
    };
}
