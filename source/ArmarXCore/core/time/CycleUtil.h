/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "TimeUtil.h"

namespace armarx
{
    /**
     * @brief This util class helps with keeping a cycle time during a control cycle.
     * The main function is waitForCycleDuration(), which should be called in every cycle.
     * The function will wait (virtual or system time) until the cycle time is reached.
     * If the last cycle took already too long, it will return immediately.
     * The last cycle time is updated, when waitForCycleDuration() is called.
     */
    class CycleUtil
    {
    public:
        CycleUtil(const IceUtil::Time& cycleDuration, bool forceSystemTime = false);
        CycleUtil(const long& cycleDurationMs, bool forceSystemTime = false);
        /**
         * @brief resets startTime, lastCycleTime and cycleCount.
         */
        void reset();
        /**
         * @brief
         * This function will wait (virtual or system time) until the cycle time is reached.
         * If the last cycle took already too long, it will return immediately.
         * The last cycle time is updated, when this function is called.
         * @return returns the time the function waited
         */
        IceUtil::Time waitForCycleDuration();
        IceUtil::Time update();

        /**
         * @brief Time when object was constructed or reset was last called.
         */
        IceUtil::Time getStartTime() const;

        /**
         * @brief Time when the last cycle finished or start time.
         */
        IceUtil::Time getLastCycleTime() const;

        /**
         * @brief Count of how many cycles were executed, i.e. how often waitForCycleDuration() was called.
         */
        long getCycleCount() const;
        IceUtil::Time getAverageDuration() const;
        IceUtil::Time getMinimumDuration() const;
        IceUtil::Time getMaximumDuration() const;
        float getBusyWaitShare() const;
        void setBusyWaitShare(float value);

    protected:
        IceUtil::Time startTime;
        IceUtil::Time lastCycleTime;
        IceUtil::Time cycleDuration;
        IceUtil::Time cycleMinDuration;
        IceUtil::Time cycleMaxDuration;
        long cycleCount;
        bool forceSystemTime;
        float busyWaitShare;
    };
}

