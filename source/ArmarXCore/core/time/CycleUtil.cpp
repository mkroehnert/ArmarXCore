/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CycleUtil.h"

using namespace armarx;

CycleUtil::CycleUtil(IceUtil::Time const& cycleDuration, bool forceSystemTime) :
    cycleDuration(cycleDuration),
    forceSystemTime(forceSystemTime),
    busyWaitShare(0.0f)
{
    reset();
}

CycleUtil::CycleUtil(const long& cycleDurationMs, bool forceSystemTime) :
    cycleDuration(IceUtil::Time::milliSeconds(cycleDurationMs)),
    forceSystemTime(forceSystemTime)
{
    reset();
}

void CycleUtil::reset()
{
    cycleCount = 0;
    startTime = lastCycleTime = TimeUtil::GetTime(forceSystemTime);
    cycleMaxDuration = IceUtil::Time::microSeconds(0);
    cycleMinDuration = IceUtil::Time::now();
}

IceUtil::Time CycleUtil::waitForCycleDuration()
{
    auto now = TimeUtil::GetTime(forceSystemTime);
    IceUtil::Time waitTime = IceUtil::Time::microSeconds(std::max<long>(0, (lastCycleTime + cycleDuration - now).toMicroSeconds()));
    double waitTimeDouble = waitTime.toMicroSecondsDouble() - (busyWaitShare * cycleDuration.toMicroSecondsDouble());
    if (busyWaitShare < 1.0)
    {
        TimeUtil::Sleep(IceUtil::Time::microSecondsDouble(waitTimeDouble));
    }
    while ((IceUtil::Time::now() - lastCycleTime).toMilliSecondsDouble() < cycleDuration.toMilliSecondsDouble())
    {

    }
    update();
    return TimeUtil::GetTime(forceSystemTime) - now;
}

IceUtil::Time CycleUtil::update()
{
    auto now = TimeUtil::GetTime(forceSystemTime);
    auto cycleTime = now - lastCycleTime;
    cycleCount++;
    if (cycleTime > cycleMaxDuration)
    {
        cycleMaxDuration = cycleTime;
    }
    if (cycleTime < cycleMinDuration)
    {
        cycleMinDuration = cycleTime;
    }
    lastCycleTime = now;
    return cycleTime;
}

IceUtil::Time CycleUtil::getStartTime() const
{
    return startTime;
}

long CycleUtil::getCycleCount() const
{
    return cycleCount;
}

IceUtil::Time CycleUtil::getAverageDuration() const
{
    if (cycleCount > 0)
    {
        return IceUtil::Time::microSeconds((lastCycleTime - startTime).toMicroSeconds() / cycleCount);
    }
    else
    {
        return IceUtil::Time();
    }
}

IceUtil::Time CycleUtil::getMinimumDuration() const
{
    return cycleMinDuration;
}

IceUtil::Time CycleUtil::getMaximumDuration() const
{
    return cycleMaxDuration;
}

float CycleUtil::getBusyWaitShare() const
{
    return busyWaitShare;
}

void CycleUtil::setBusyWaitShare(float value)
{
    busyWaitShare = value;
}

IceUtil::Time CycleUtil::getLastCycleTime() const
{
    return lastCycleTime;
}
