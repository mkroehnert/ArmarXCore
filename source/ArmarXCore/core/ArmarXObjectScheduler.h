/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once
#include <ArmarXCore/core/logging/Logging.h>  // for Logging
#include <ArmarXCore/interface/core/ManagedIceObjectDefinitions.h>
#include <Ice/Handle.h>                 // for Handle
#include <Ice/ObjectAdapterF.h>         // for ObjectAdapterPtr
#include <Ice/ObjectF.h>                // for upCast
#include <IceUtil/Handle.h>             // for Handle
#include <IceUtil/Shared.h>             // for Shared
#include <boost/thread/pthread/condition_variable_fwd.hpp>
#include <string>                       // for string

#include "ArmarXCore/core/IceManager.h"  // for IceManager
#include "ArmarXCore/core/ManagedIceObject.h"  // for ManagedIceObject
#include "ArmarXCore/core/system/Synchronization.h"  // for Mutex

namespace armarx
{
    /**
     * forward declarations
     */
    class ArmarXManager;

    typedef IceUtil::Handle<ArmarXManager> ArmarXManagerPtr;
    typedef IceUtil::Handle<IceManager> IceManagerPtr;
    typedef IceInternal::Handle<ManagedIceObject> ManagedIceObjectPtr;

    /**
     * ArmarXObjectScheduler shared pointer for convenience
     */
    class ArmarXObjectScheduler;
    template <class T>
    class RunningTask;

    typedef IceUtil::Handle<ArmarXObjectScheduler> ArmarXObjectSchedulerPtr;

    /**
     * @class ArmarXObjectScheduler
     * @brief Takes care of the lifecycle management of ManagedIceObjects.
     * @ingroup DistributedProcessingSub
     *
     * The ArmarXObjectScheduler schedules the lifecycle of a ManagedIceObject in a single thread.
     * According to the current state, the ManagedIceObject framework hooks are called.
     * It provides all necessary functionality for a clean initialisation of ManagedIceObjects
     * as well as dependency-resolution, dependency-monitoring, and exception handling.
     * ArmarXObjectSchedulers are created by the ArmarXManager.

     * @image html Lifecycle-UML.svg Lifecyle of a ManagedIceObject managed by the ArmarXObjectScheduler
     */
    class ArmarXObjectScheduler :
        public IceUtil::Shared,
        virtual public Logging
    {
    public:
        /**
         * Constructs an ArmarXObjectScheduler
         *
         * @param armarXManager pointer to the armarXManager
         * @param iceManager pointer to the iceManager
         * @param object object to schedule
         */
        ArmarXObjectScheduler(const ArmarXManagerPtr& armarXManager, const IceManagerPtr& iceManager, const armarx::ManagedIceObjectPtr& object, Ice::ObjectAdapterPtr objectAdapterToAddTo, bool startSchedulingObject = true);
        ~ArmarXObjectScheduler() override;


        /**
         * waits until all depenencies are resolved.
         * @param timeoutMs If set to -1, it waits indefinitely. Otherwise throws
         * exception after waiting the specifed time (given in milliseconds).
         */
        void waitForDependencies(int timeoutMs = -1);
        void wakeupDependencyCheck();
        bool dependsOn(const std::string& objectName);
        void disconnected(bool reconnect);

        /**
         * Waits until scheduler has been terminated. After termination the lifecycle of the ManagedIceObject
         * has ceased and the thread has joined.
         */
        void waitForTermination();



        /**
         * @brief waitForObjectStart waits (thread sleeps) until the object
         * reached a specific state.
         * @param stateToWaitFor State for which the calling thread should wait
         * for.
         * @param timeoutMs Timeout in miliseconds until this function stops
         * waiting and returns false. Set to -1 for infinite waiting.
         * @return Whether the object reached the state before timeout or not.
         * @see waitForObjectStateMinimum()
         */
        bool waitForObjectState(ManagedIceObjectState stateToWaitFor, const long timeoutMs = -1) const;

        /**
         * @brief waitForObjectStart waits (thread sleeps) until the object
         * reached a specific state (or higher/later).
         * @param minimumStateToWaitFor Minimum State for which the calling thread should wait
         * for.
         * @param timeoutMs Timeout in miliseconds until this function stops
         * waiting and returns false. Set to -1 for infinite waiting.
         * @return Whether the object reached the minimum state before timeout
         * or not.
         * @see waitForObjectState()
         */
        bool waitForObjectStateMinimum(ManagedIceObjectState minimumStateToWaitFor, const long timeoutMs = -1) const;

        /**
         * Terminates the ManagedIceObject.
         */
        void terminate();

        /**
         * Check whether the Scheduler is terminated
         *
         * @return terminated state
         */
        bool isTerminated() const;

        bool isTerminationRequested() const;

        /**
         * Retrieve pointer to scheduled ManagedIceObject
         *
         * @return ManagedIceObjectPtr
         */
        const armarx::ManagedIceObjectPtr& getObject() const;
        ManagedIceObjectState getObjectState() const;


        bool checkDependenciesStatus() const;
        void startScheduling();

    protected:
        bool checkDependenciesResolvement();
    private:
        void setInteruptConditionVariable(boost::shared_ptr<boost::condition_variable> interruptCondition, boost::shared_ptr<bool> interruptConditionVariable);
        // main scheduling thread functions
        void scheduleObject();

        void waitForInterrupt();




        // scheduling thread
        IceUtil::Handle<RunningTask<ArmarXObjectScheduler> > scheduleObjectTask;



        // phases of the object lifecycle
        void initObject();
        void startObject();
        void disconnectObject();
        void exitObject();

        // private fields
        ArmarXManagerPtr armarXManager;
        IceManagerPtr iceManager;
        ManagedIceObjectPtr managedObject;

        // termination handling
        //        boost::mutex terminateMutex;
        //        boost::condition_variable terminateCondition;
        //        bool terminateConditionVariable;
        Mutex interruptMutex;
        boost::shared_ptr<boost::condition_variable> interruptCondition;
        boost::shared_ptr<bool> interruptConditionVariable;
        Mutex dependencyWaitMutex;
        boost::condition_variable dependencyWaitCondition;
        bool dependencyWaitConditionVariable;
        bool terminateRequested;
        bool objectedInitialized;
        bool tryReconnect;
        Ice::ObjectAdapterPtr objectAdapterToAddTo;

        friend class ArmarXMultipleObjectsScheduler;

    };
}

