/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Core
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "ArmarXCore/core/exceptions/Exception.h"

#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            /**
              \class UnmappedValueException
              \brief This exception is thrown if a value specified
                    for a property is not found in the property mapping \ref PropertyMapper.
              \ingroup Exceptions
             */
            class UnmappedValueException: public armarx::LocalException
            {
            public:
                std::string propertyName;
                std::string invalidPropertyValue;

                UnmappedValueException(std::string propertyName, std::string invalidPropertyValue) noexcept :
                    armarx::LocalException(" Invalid value '" + invalidPropertyValue + "'" + " for property <" + propertyName + ">"),
                    propertyName(propertyName), invalidPropertyValue(invalidPropertyValue)
                { }

                ~UnmappedValueException() noexcept override
                { }

                std::string name() const override
                {
                    return "armarx::exceptions::local::UnmappedValueException";
                }
            };
        }
    }
}

