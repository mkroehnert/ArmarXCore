/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Dennis Weigelt
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "Scenario.h"
#include "Application.h"
#include <boost/shared_ptr.hpp>
#include <string>
#include <vector>

namespace ScenarioManager
{
    namespace Data_Structure
    {
        class Scenario;

        /**
        * @class Package
        * @ingroup Data_Structure
        * @brief Class containing data about a package, its scenarios and its applications.
        * Provides methods to get and set the data contained in the package. It is only representative
        * and doesn't actually manage the package.
        */
        class Package
        {

        private:
            std::string name;
            std::string path;
            std::string scenarioPath;
            ScenarioVectorPtr scenarios;
            ApplicationVectorPtr applications;

        public:
            /**
            * Constructor that sets the name and the path of the package.
            * @param name name of the package
            * @param path path to the root of the package
            */
            Package(std::string name, std::string path, std::string scenarioPath);

            /**
            * Constructor that creates a light copy of the given Package, only name and path get copied.
            * @param other Package to copy
            */
            Package(const Package& other);

            /**
            * @return name of this package
            */
            std::string getName();

            /**
            * @return path of this package
            */
            std::string getPath();

            std::string getScenarioPath();
            bool isScenarioPathWritable();

            /**
            * @return scenarios of this package
            */
            ScenarioVectorPtr getScenarios();

            /**
            * @return applications of this package
            */
            ApplicationVectorPtr getApplications();

            /**
            * Adds a Scenario to this package.
            * @param scenario Scenario to be added
            */
            void addScenario(ScenarioPtr scenario);
            void removeScenario(ScenarioPtr scenario);

            /**
            * Adds an Application to this package
            * @param application Application to be added
            */
            void addApplication(ApplicationPtr application);

            /**
            * Returns a Scenario contained in this package that has the given name.
            * @param name name of the scenario
            * @return a Scenario with the given name. Returns a null pointer if no scenario with that name exists.
            */
            ScenarioPtr getScenarioByName(std::string name);

            /**
            * Returns an Application contained in this package that has the given name.
            * @param name name of the application
            * @return an Application with the given name. Returns a null pointer, if no Application with that name exists.
            */
            ApplicationPtr getApplicationByName(std::string name);
        };

        typedef boost::shared_ptr<Package> PackagePtr;
        typedef boost::shared_ptr<std::vector<ScenarioManager::Data_Structure::PackagePtr>> PackageVectorPtr;
    }
}
