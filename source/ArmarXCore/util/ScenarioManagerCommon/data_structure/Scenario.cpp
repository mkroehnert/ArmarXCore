/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Dennis Weigelt
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Scenario.h"
#include "../parser/XMLScenarioParser.h"
#include <ArmarXCore/core/application/properties/IceProperties.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <boost/filesystem.hpp>
#include <Ice/Properties.h>
#include <fstream>

using namespace ScenarioManager;
using namespace Data_Structure;
using namespace std;

Scenario::Scenario(string name, string creationTime, string lastChangedTime, PackagePtr package, std::string globalConfigName, std::string subfolder)
    : name(name), creationTime(creationTime),
      lastChangedTime(lastChangedTime),
      globalConfigName(globalConfigName),
      subfolder(subfolder),
      package(package),
      applications(new vector<ApplicationInstancePtr>()),
      globalConfig(new armarx::PropertyDefinitionContainer(name))
{
    Ice::PropertiesPtr cfgProperties = armarx::IceProperties::create();
    armarx::IceProperties* cfgInternal = static_cast<armarx::IceProperties*>(cfgProperties.get());
    cfgInternal->setInheritanceSolver(nullptr);

    globalConfig->setProperties(cfgProperties);
    globalConfig->setPrefix("");
    globalConfig->setDescription("Global Config from Scenario " + name);
}

string Scenario::getName()
{
    return this->name;
}

void Scenario::setName(string name)
{
    this->name = name;
}

string Scenario::getCreationTime()
{
    return this->creationTime;
}

string Scenario::getLastChangedTime()
{
    return this->lastChangedTime;
}

string Scenario::getGlobalConfigName()
{
    return globalConfigName;
}

string Scenario::getSubfolder()
{
    return subfolder;
}

string Scenario::getPath()
{
    std::string scenarioPath = package.lock()->getScenarioPath();

    if (subfolder.empty())
    {
        scenarioPath.append("/").append(name).append("/").append(name).append(".scx");
    }
    else
    {
        scenarioPath.append("/").append(subfolder).append("/").append(name).append("/").append(name).append(".scx");
    }

    return scenarioPath;
}

bool Scenario::isScenarioFileWriteable()
{
    std::ofstream ofs;
    ofs.open(getPath().c_str(), std::ofstream::out | std::ofstream::app);
    ARMARX_DEBUG << getPath() << " is writeable: " << ofs.is_open();

    return ofs.is_open();

    boost::filesystem::file_status s = boost::filesystem::status(boost::filesystem::path(getPath()));
    if ((s.permissions() & boost::filesystem::owner_write) != 0)
    {
        return true;
    }
    return false;
}

PackagePtr Scenario::getPackage()
{
    return package.lock();
}

string Scenario::getStatus()
{
    string status = ApplicationStatus::Unknown;
    for (auto app : *applications)
    {
        if (!app->getEnabled())
        {
            continue;
        }
        if (status == ApplicationStatus::Unknown && app->getStatus() == ApplicationStatus::Running)
        {
            status = ApplicationStatus::Running;
        }
        else if (status == ApplicationStatus::Unknown && app->getStatus() == ApplicationStatus::Stopped)
        {
            status = ApplicationStatus::Stopped;
        }
        else if (status == ApplicationStatus::Running && app->getStatus() == ApplicationStatus::Running)
        {
            status = ApplicationStatus::Running;
        }
        else if (status == ApplicationStatus::Stopped && app->getStatus() == ApplicationStatus::Stopped)
        {
            status = ApplicationStatus::Stopped;
        }
        else if (status == ApplicationStatus::Running && app->getStatus() == ApplicationStatus::Stopped)
        {
            status = ApplicationStatus::Mixed;
            break;
        }
        else if (status == ApplicationStatus::Stopped && app->getStatus() == ApplicationStatus::Running)
        {
            status = ApplicationStatus::Mixed;
            break;
        }
        else if (app->getStatus() == ApplicationStatus::Waiting)
        {
            status = ApplicationStatus::Waiting;
            break;
        }
        else if (app->getStatus() == ApplicationStatus::Unknown)
        {
            status = ApplicationStatus::Unknown;
            break;
        }
    }

    return status;
}

void Scenario::setLastChangedTime(string time)
{
    this->lastChangedTime = time;
}

void Scenario::setGlobalConfigName(string name)
{
    globalConfigName = name;
}

ApplicationInstanceVectorPtr Scenario::getApplications()
{
    return applications;
}

ApplicationInstancePtr Scenario::getApplicationByName(std::string name)
{
    for (auto app : *applications)
    {
        if (app->getName().compare(name) == 0)
        {
            return app;
        }
    }

    return ApplicationInstancePtr(nullptr);
}

void Scenario::addApplication(ApplicationInstancePtr application)
{
    applications->push_back(application);
}

void Scenario::removeApplication(ApplicationInstancePtr application)   //?
{
    for (vector<ApplicationInstancePtr>::iterator iter = applications->begin(); iter != applications->end(); ++iter)
    {
        if (iter->get() == application.get())
        {
            applications->erase(iter);
            return;
        }
    }
}

void Scenario::updateApplicationByName(string name)   //gehört diese Methode hier rein? evtl signal/slot mit Controller
{
    // TODO - implement Scenario::updateApplicationByName
    //throw "Not yet implemented";
}

void Scenario::updateApplication(ApplicationInstancePtr application)
{
    // TODO - implement Scenario::updateApplication
    //throw "Not yet implemented";
}

void Scenario::save()
{
    Parser::XMLScenarioParser parser;
    parser.saveScenario(shared_from_this());
}

void Scenario::reloadAppInstances()
{
    for (auto it : *applications)
    {
        it->load();
    }
}

void Scenario::reloadGlobalConf()
{
    globalConfig->getProperties()->load(getGlobalConfigPath());
}

armarx::PropertyDefinitionsPtr Scenario::getGlobalConfig()
{
    return globalConfig;
}

std::string Scenario::getGlobalConfigPath()
{
    boost::filesystem::path scenarioPath;
    if (subfolder.empty())
    {
        scenarioPath = boost::filesystem::path(package.lock()->getScenarioPath().append("/").append(name).append("/"));
    }
    else
    {
        scenarioPath = boost::filesystem::path(package.lock()->getScenarioPath().append("/").append(subfolder + "/").append(name).append("/"));
    }

    scenarioPath = scenarioPath / boost::filesystem::path(globalConfigName);
    return scenarioPath.string();
}

bool Scenario::isGlobalConfigWritable()
{
    std::ofstream ofs;
    ofs.open(getGlobalConfigPath().c_str(), std::ofstream::out | std::ofstream::app);
    ARMARX_DEBUG << getGlobalConfigPath() << " is writeable: " << ofs.is_open();

    return ofs.is_open();

}
