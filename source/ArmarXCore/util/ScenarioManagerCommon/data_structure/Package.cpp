/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Dennis Weigelt
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Package.h"
#include <boost/filesystem.hpp>
#ifndef _WIN32
#include <fcntl.h> /* Definition of AT_* constants */
#include <unistd.h>
#endif
#include <ArmarXCore/core/logging/Logging.h>
using namespace ScenarioManager;
using namespace Data_Structure;
using namespace std;

Package::Package(string name, string path, string scenarioPath)
    : name(name),
      path(path),
      scenarioPath(scenarioPath),
      scenarios(new vector<ScenarioPtr>()),
      applications(new vector<ApplicationPtr>())
{
}

Package::Package(const Package& other) : name(other.name), path(other.path)
{
}

string Package::getName()
{
    return this->name;
}

string Package::getPath()
{
    return this->path;
}

string Package::getScenarioPath()
{
    return this->scenarioPath;
}

bool Package::isScenarioPathWritable()
{
#ifndef _WIN32
    bool writable = faccessat(0, scenarioPath.c_str(), W_OK, AT_EACCESS) == 0;
    if (!writable)
    {
        boost::filesystem::path scenarioDir(scenarioPath.c_str());
        if (!boost::filesystem::is_directory(scenarioDir))
        {
            if (boost::filesystem::create_directory(scenarioDir))
            {
                ARMARX_DEBUG << "Created Scenario dir for Package " << name;
                writable = true;
            }
            else
            {
                ARMARX_DEBUG << "Failed to create Scenario dir for Package " << name << " please check if you have the certain permissions";
            }
        }
    }
    ARMARX_DEBUG << scenarioPath << " is writeable: " << writable;
    return writable;
#else
    return true;
#endif
}

ScenarioVectorPtr Package::getScenarios()
{
    return this->scenarios;
}

ApplicationVectorPtr Package::getApplications()
{
    return this->applications;
}

void Package::addScenario(ScenarioPtr scenario)   //auch hier schauen
{
    scenarios->push_back(scenario);
}

void Package::removeScenario(ScenarioPtr scenario)
{
    for (auto it = scenarios->begin(); it != scenarios->end(); ++it)
    {
        if ((*it)->getName().compare(scenario->getName()) == 0 && (*it)->getPackage()->getName().compare(scenario->getPackage()->getName()) == 0)
        {
            scenarios->erase(it);
            return;
        }
    }
}

void Package::addApplication(ApplicationPtr application)
{
    applications->push_back(application);
}

ScenarioPtr Package::getScenarioByName(string name)   //drüber schauen kp ...
{
    for (vector<ScenarioPtr>::iterator iter = scenarios->begin(); iter != scenarios->end(); ++iter)
    {
        if (iter->get()->getName().compare(name) == 0)
        {
            return *iter;
        }
    }
    return ScenarioPtr();
}

ApplicationPtr Package::getApplicationByName(string name)   //same as getScenarioByName(...)
{
    for (vector<ApplicationPtr>::iterator iter = applications->begin(); iter != applications->end(); ++iter)
    {
        if (iter->get()->getName().compare(name) == 0)
        {
            return *iter;
        }
    }
    return ApplicationPtr();
}
