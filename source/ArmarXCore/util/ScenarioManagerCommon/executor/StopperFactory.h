/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "ApplicationStopper.h"
#include <boost/shared_ptr.hpp>

namespace ScenarioManager
{
    namespace Exec
    {
        /**
        * @class StopperFactory
        * @ingroup exec
        * @brief Abstract base class for factory classes that create ApplicationStopper.
        * Should be implemented for each operating system.
        */
        class StopperFactory
        {

        public:
            /**
            * Returns a system-specific ApplicationStopper, which stops by using the pid.
            * @return system-specific by pid stopper
            */
            virtual ApplicationStopperPtr getPidStopper() = 0;

            /**
            * Returns a system-specific ApplicationStopper, which stops by using the name.
            * @return system-specific by name stopper
            */
            virtual ApplicationStopperPtr getByNameStopper() = 0;

            /**
            * Returns an IceStopper. Should be indipendent from the operating system.
            * @return system-indipendent IceStopper
            */
            ApplicationStopperPtr getIceStopper();

            /**
            * Returns the right StopperFactory for this operating system.
            * @return StopperFactory depending on the operating system
            */
            static boost::shared_ptr<StopperFactory> getFactory();
        };
        typedef boost::shared_ptr<ScenarioManager::Exec::StopperFactory> StopperFactoryPtr;
    }
}

