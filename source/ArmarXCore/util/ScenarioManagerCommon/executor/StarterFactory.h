/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ApplicationStarter.h"
#include <boost/shared_ptr.hpp>

namespace ScenarioManager
{
    namespace Exec
    {
        /**
        * @class StarterFactory
        * @ingroup exec
        * @brief Abstract base class for factory classes that create ApplicationStarter.
        * Should be implemented for each operating system.
        */
        class StarterFactory
        {

        public:
            /**
            * Returns a system-specific ApplicationStarter.
            * @return system-specific starter
            */
            virtual ApplicationStarterPtr getStarter() = 0;

            /**
            * Returns an IceStarter. Should be independent from the operating system.
            * @return system-independent IceStarter
            */
            virtual ApplicationStarterPtr getIceStarter();

            /**
            * Returns the right StarterFactory for this operatin system.
            * @return StarterFactory depending on the operating system
            */
            static boost::shared_ptr<StarterFactory> getFactory();
        };
        typedef boost::shared_ptr<ScenarioManager::Exec::StarterFactory> StarterFactoryPtr;
    }
}

