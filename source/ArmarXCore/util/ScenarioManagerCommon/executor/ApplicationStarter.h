/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "../data_structure/ApplicationInstance.h"
#include <boost/shared_ptr.hpp>

namespace ScenarioManager
{
    class PidManager;
    namespace Exec
    {
        /**
        * @class ApplicationInstance
        * @ingroup Exec
        * @brief Interface for classes that handle the starting of applications
        * Classes implementing this interface also need to implement a method to get the status of an application
        */
        class ApplicationStarter
        {
        public:
            /**
            * Starts an application.
            * @param application application to be started.
            */
            virtual void start(Data_Structure::ApplicationInstancePtr application, PidManager pidManager, const std::string& commandLineParameters = "", bool printOnly = false) = 0;

            /**
            * Returns the status of an application.
            * @param application application whose status is returned
            * @return status of the application
            */
            virtual std::string getStatus(Data_Structure::ApplicationInstancePtr application, PidManager pidManager) = 0;

            /**
            * Generates an XML file of the given application and saves it in the specified path.
            * If there already is an XML file at that location it only reloads the file if the executable is more recently changed
            * than the Xml file
            * @param application Application whose XML file is to be generate
            * @param path location to save the XML, must be a folder
            * @param reload forces to reload the cached XML
            * @param set if true the newly Cached Xml gets loaded into the Application (Warning: If you load an ApplicationInstance the cfg parameter have to be reloaded)
            */
            virtual void loadAndSetCachedProperties(Data_Structure::ApplicationPtr application, std::string path, bool reload, bool set) = 0;

            std::string commandLineParameters;
        };

        typedef boost::shared_ptr<ApplicationStarter> ApplicationStarterPtr;
    }
}

