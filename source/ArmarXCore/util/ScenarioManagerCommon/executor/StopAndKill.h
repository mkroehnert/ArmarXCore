/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../data_structure/ApplicationInstance.h"
#include "ApplicationStopper.h"
#include "StopStrategy.h"

namespace ScenarioManager
{
    namespace Exec
    {
        /**
        * @class StopAndKill
        * @ingroup exec
        * @brief First tries to stop, then kills an application, using the given ApplicationStopper.
        * This StopStrategy first tries to stop an application. If after a certain period of time the application
        * hasn't yet stopped, it tries to kill it.
        * This StopStrategy can be made system-specific by switching out its ApplicationStopper.
        */
        class StopAndKill : public StopStrategy
        {

        private:
            ApplicationStopperPtr stopper;
            int timer;

        public:
            /**
            * Constructor that sets the ApplicationStopper used by this StopStrategy.
            * @param stopper stopper used in this strategy
            * @param delay delay between start and stop
            */
            StopAndKill(ApplicationStopperPtr stopper, int delay);

            /**
            * Stops an application. First tries to stop it, by calling (ApplicationStopper::stop(Data_Structure::ApplicationInstancePtr)),
            * then waits for #timer seconds and then calls (ApplicationStopper::kill(Data_Structure::ApplicationInstancePtr)).
            * @param application application to be stopped
            */
            void stop(Data_Structure::ApplicationInstancePtr application) override;

            /**
            * Returns the stopper used by this StopStrategy.
            * @return stopper used by this strategy
            */
            ApplicationStopperPtr getStopper();
        };

    }
}

