/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Philipp Schmidt( ufedv at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include "TopicReaderInterface.h"
#include <ArmarXCore/core/system/Synchronization.h>
#include <boost/filesystem.hpp>
#include <sqlite3.h>

namespace armarx
{

    class DatabaseTopicReader : public TopicReaderInterface
    {
    public:
        DatabaseTopicReader(boost::filesystem::path path);
        ~DatabaseTopicReader();
    public:
        bool read(TopicUtil::TopicData& data) override;
        bool seekTo(IceUtil::Time timestamp) override;
        IceUtil::Time getReplayLength() override;
        std::vector<std::string> getReplayTopics() override;
        boost::filesystem::path getFilepath() const;
    private:
        Mutex mutex;
        bool seekTo(int ID);
        boost::filesystem::path filepath;
        sqlite3* db;
        sqlite3_stmt* stmt;
        bool database_open;
        bool end_of_database;
    };

    typedef boost::shared_ptr<DatabaseTopicReader> DatabaseTopicReaderPtr;
} // namespace armarx

