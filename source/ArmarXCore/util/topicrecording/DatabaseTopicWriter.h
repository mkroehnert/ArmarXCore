/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Philipp Schmidt( ufedv at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include "TopicWriterInterface.h"
#include <boost/filesystem.hpp>
#include <sqlite3.h>
#include <unordered_set>

namespace armarx
{
    class DatabaseTopicWriter : public TopicWriterInterface
    {
    public:
        DatabaseTopicWriter(const boost::filesystem::path& path);
        ~DatabaseTopicWriter() override;
    private:
        boost::filesystem::path filepath;
        sqlite3* db;
        sqlite3_stmt* stmt;
        bool database_open;
        std::unordered_set<std::string> topics;

    public:
        void write(const TopicUtil::TopicData& topicData) override;
        boost::filesystem::path getFilepath() const;
    };

    typedef boost::shared_ptr<DatabaseTopicWriter> DatabaseTopicWriterPtr;
}

