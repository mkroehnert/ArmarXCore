/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "FileTopicWriter.h"
#include <fstream>
#include <ArmarXCore/util/json/JSONObject.h>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/algorithm/string.hpp>
#include <sstream>
#include <string>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx
{

    FileTopicWriter::FileTopicWriter(const boost::filesystem::path& path) :
        filepath(path)
    {
        std::ofstream* o = new std::ofstream(path.string().c_str());
        if (!o->is_open())
        {
            ARMARX_ERROR_S << "Could not open '" << path.string() << "'!";
        }
        stream = o;
    }

    FileTopicWriter::FileTopicWriter(std::ostream* stream) :
        stream(stream)
    {

    }

    FileTopicWriter::~FileTopicWriter()
    {
        if (!filepath.empty())
        {
            delete stream;
        }
    }

    boost::filesystem::path FileTopicWriter::getFilepath() const
    {
        return filepath;
    }

    void FileTopicWriter::write(const TopicUtil::TopicData& topicData)
    {
        JSONObjectPtr json = new JSONObject();
        json->setString("topic", topicData.topicName);
        json->setString("op", topicData.operationName);
        json->setDouble("t", topicData.timestamp.toMicroSecondsDouble());
        json->setString("data", TopicUtil::Encode64(std::string(topicData.inParams.begin(), topicData.inParams.end())));

        (*stream) << json->asString() << std::endl;
    }

} // namespace armarx
