/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     ALexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/interface/serialization/JSONSerialization.h>

#include <jsoncpp/json/json.h>

namespace armarx
{
    class JSONObject;
    typedef IceInternal::Handle<JSONObject> JSONObjectPtr;

    /**
     * @brief The JSONObject class is used to represent and (de)serialize JSON objects.
     *
     * Accessing the object is not thread safe.
     * (De)Serializing multiple JSON objects should be done with a new instance for
     * each object.
     */
    class JSONObject :
        public armarx::AbstractObjectSerializer
    {
    public:
        JSONObject(armarx::ElementType nodeType = armarx::ElementTypes::eObject, const Ice::CommunicatorPtr ic = Ice::CommunicatorPtr());
        JSONObject(const Ice::CommunicatorPtr ic);
        JSONObject(const JSONObject& toCopy);
        ~JSONObject() override;

    public:  // ObjectSerializerBase methods implementation
        std::string toString(const ::Ice::Current& = ::Ice::Current()) const override;
        void fromString(const ::std::string& jsonString, const ::Ice::Current& = ::Ice::Current()) override;
        const Json::Value& getJsonValue() const;
    public:  //
        armarx::ElementType getElementType(const ::Ice::Current& = ::Ice::Current()) const override;
        void setElementType(armarx::ElementType nodeType, const ::Ice::Current& = ::Ice::Current()) override;

        unsigned int size() const override;
        bool hasElement(const ::std::string& key) const override;

        float getFloat(const ::std::string& key) const override;
        double getDouble(const ::std::string& key) const override;
        int getInt(const ::std::string& key) const override;
        bool getBool(const ::std::string& key) const override;
        std::string getString(const ::std::string& key) const override;
        armarx::AbstractObjectSerializerPtr getElement(unsigned int index) const override;
        armarx::AbstractObjectSerializerPtr getElement(const ::std::string& key) const override;
        std::vector<std::string> getElementNames() const override;

        template <typename T>
        void getArray(const std::string& key, std::vector<T>& result)
        {
            const Json::Value& arrValue = getValue(key);

            if (!arrValue.isArray())
            {
                throw JSONInvalidDataTypeException();
            }

            result.clear();
            result.resize(arrValue.size());

            for (size_t i = 0; i < arrValue.size(); ++i)
            {
                get(arrValue[int(i)], result[int(i)]);
            }
        }

        void getIntArray(const ::std::string& key, std::vector<int>& result) override
        {
            getArray(key, result);
        }
        void getFloatArray(const ::std::string& key, std::vector<float>& result) override
        {
            getArray(key, result);
        }
        void getDoubleArray(const ::std::string& key, std::vector<double>& result) override
        {
            getArray(key, result);
        }
        void getStringArray(const ::std::string& key, std::vector<std::string>& result) override
        {
            getArray(key, result);
        }
        void getVariantArray(const ::std::string& key, std::vector<armarx::VariantPtr>& result) override;
        void getVariantMap(const ::std::string& key, armarx::StringVariantBaseMap& result) override;

        template <typename T>
        void set(const ::std::string& key, T val)
        {
            jsonValue[key] = Json::Value(val);
        }

        template <typename T>
        void set(int index, T val)
        {
            jsonValue[index] = Json::Value(val);
        }

        template <typename T>
        void setArray(const ::std::string& key, const std::vector<T>& val)
        {
            Json::Value arr(Json::arrayValue);

            if (val.size() > 0)
            {
                arr.resize(val.size());

                for (size_t i = 0; i < val.size(); ++i)
                {
                    arr[int(i)] = Json::Value(val[int(i)]);
                }
            }

            jsonValue[key] = arr;
        }

        void setBool(const ::std::string& key, bool val) override
        {
            set(key, val);
        }
        void setInt(const ::std::string& key, int val) override
        {
            set(key, val);
        }
        void setFloat(const ::std::string& key, float val) override
        {
            set(key, val);
        }
        void setDouble(const ::std::string& key, double val) override
        {
            set(key, val);
        }
        void setString(const ::std::string& key, const std::string& val) override
        {
            set(key, val);
        }
        void setElement(const ::std::string& key, const armarx::AbstractObjectSerializerPtr& obj) override;

        void setIntArray(const ::std::string& key, const std::vector<int>& val) override
        {
            setArray(key, val);
        }
        void setFloatArray(const ::std::string& key, const std::vector<float>& val) override
        {
            setArray(key, val);
        }
        void setDoubleArray(const ::std::string& key, const std::vector<double>& val) override
        {
            setArray(key, val);
        }
        void setStringArray(const ::std::string& key, const std::vector<std::string>& val) override
        {
            setArray(key, val);
        }
        void setVariantArray(const ::std::string& key, const std::vector<armarx::VariantBasePtr>& val) override;
        void setVariantArray(const ::std::string& key, const std::vector<armarx::VariantPtr>& val) override;
        void setVariantMap(const ::std::string& key, const armarx::StringVariantBaseMap& val) override;

        void setBool(unsigned int index, bool val) override
        {
            set(index, val);
        }
        void setInt(unsigned int index, int val) override
        {
            set(index, val);
        }
        void setFloat(unsigned int index, float val) override
        {
            set(index, val);
        }
        void setDouble(unsigned int index, double val) override
        {
            set(index, val);
        }
        void setString(unsigned int index, const std::string& val) override
        {
            set(index, val);
        }
        void setElement(unsigned int index, const armarx::AbstractObjectSerializerPtr& obj) override;

        // add all elements of val to json as siblings
        void merge(const armarx::AbstractObjectSerializerPtr& val) override;

        void append(const armarx::AbstractObjectSerializerPtr& val) override;

        void reset() override;

        static StringVariantBaseMap ConvertToBasicVariantMap(const JSONObjectPtr& serializer, const VariantBasePtr& variant);

    public:
        armarx::AbstractObjectSerializerPtr createElement() const override;

        std::string asString(bool pretty = false) const;

    private:
        Json::Value jsonValue;

        const Json::Value& getValue(const std::string& key) const;

        void get(const Json::Value& val, bool& result) const;
        void get(const Json::Value& val, int& result) const;
        void get(const Json::Value& val, float& result) const;
        void get(const Json::Value& val, double& result) const;
        void get(const Json::Value& val, std::string& result) const;

    };

}

