/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

module armarx
{
    const string GLOBAL_TIMESERVER_NAME = "MasterTimeServer";
    const string TIME_TOPIC_NAME = "Time";
    interface TimeServerInterface
    {
        idempotent long getTime(); // not exactly idempotent, but save to be called more than once
        /*!
         * \brief stop stops the clock. The clock is not reset on restart.
         */
        void stop();
        /*!
         * \brief start starts the clock
         */
        void start();
        /*!
         * \brief setSpeed sets the scaling factor for the speed of passing of time
         * e.g. setSpeed(2) leads to 2 virtual seconds passing every 1 real second
         * \param newSpeed new scaling factor for speed
         */
        void setSpeed(float newSpeed);
        /*!
         * \brief step advances the time by one step with the leng of getStepTimeMS() milliseconds
         */
        void step();
        /*!
         * \return the current scaling factor for the speed of passing of time
         */
        float getSpeed();
        /*!
         * \return the length of one time step in milliseconds
         */
        int getStepTimeMS();
    };
    interface TimeServerListener
    {
        void reportTime(long timestamp);
    };
    interface TimeServerRelay extends TimeServerInterface, TimeServerListener
    {
    };
};

