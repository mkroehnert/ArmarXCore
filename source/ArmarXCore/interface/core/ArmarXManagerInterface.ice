/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/ManagedIceObjectDefinitions.ice>
#include <ArmarXCore/interface/core/ManagedIceObjectDependencyBase.ice>

#include <Ice/PropertiesAdmin.ice>

module armarx
{

    struct ObjectPropertyInfo{
        bool constant;
        string value;
    };
    dictionary<string, ObjectPropertyInfo> ObjectPropertyInfos;

    /**
     * The ArmarXManagerInterface provides means to query the state, connectivity
     * and properties of remote Ice objects.
     */
    interface ArmarXManagerInterface
    {
        ManagedIceObjectState getObjectState(string objectName);
        ManagedIceObjectConnectivity getObjectConnectivity(string objectName);
        /**
         * getObjectProperties returns a map of propertynames and values
         */
        StringStringDictionary getObjectProperties(string objectName);
        ObjectPropertyInfos getObjectPropertyInfos(string objectName);
        ObjectPropertyInfos getApplicationPropertyInfos();

        Ice::PropertiesAdmin* getPropertiesAdmin();

        /**
         * returns a list of object names managed by the current ArmarXManager.
         */
        Ice::StringSeq getManagedObjectNames();

        bool loadLibFromPath(string path);
        bool loadLibFromPackage(string package, string libname);
    };

};

