/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>

/*
 * This file generates following structs:
 * Vector<NUMBER><TYPE>
 * Matrix_<NUMBER_ROWS>_<NUMBER_COLS><Type>
 * with <NUMBER> in {1,2,3,4,5,6,X}
 * and <TYPE> in {bool,byte,s,i,l,f,d}
 * while the abbreviation represents {bool, byte, int, long, float, double}
 *
 * Each struct holds <NUMBER> elements of the given <TYPE>.
 * if <NUMBER>==X the struct is a sequence of <TYPE>.
 * for <NUMBER> in {1,2,3,4,5,6} the member elements are named {e0, e1, e2, e3, e4, e5}.
 *
 * for each struct a sequence is provided.
 *
 * for <TYPE> in {byte, int, long, float, double} and each <NUMBER> an additional
 * Range struct is provided (for Vector and Matrix).
 * the Range struct holds two members min and max of type Vector<NUMBER><TYPE>.
 * for each Range struct a sequence is provided.
 *
 */



#define MakeRangeAndSeqHelper(Pre, Suff, Sz)                                  \
    sequence<Pre##Sz##Suff       >   Pre##Sz##Suff##Seq                     ; \
    struct   Pre##Sz##Suff##Range  { Pre##Sz##Suff min; Pre##Sz##Suff max; }; \
    sequence<Pre##Sz##Suff##Range>   Pre##Sz##Suff##Range##Seq;

#define MakeRangeAndSeq(Pre,Suff)       \
    MakeRangeAndSeqHelper(Pre, Suff, 1) \
    MakeRangeAndSeqHelper(Pre, Suff, 2) \
    MakeRangeAndSeqHelper(Pre, Suff, 3) \
    MakeRangeAndSeqHelper(Pre, Suff, 4) \
    MakeRangeAndSeqHelper(Pre, Suff, 5) \
    MakeRangeAndSeqHelper(Pre, Suff, 6) \
    MakeRangeAndSeqHelper(Pre, Suff, X)

#define GENERATE(Type,Pre,Suff)                                                    \
    sequence<Type> Pre##X##Suff;                                                   \
    struct Pre##1##Suff { Type e0                                             ; }; \
    struct Pre##2##Suff { Type e0; Type e1                                    ; }; \
    struct Pre##3##Suff { Type e0; Type e1; Type e2                           ; }; \
    struct Pre##4##Suff { Type e0; Type e1; Type e2; Type e3                  ; }; \
    struct Pre##5##Suff { Type e0; Type e1; Type e2; Type e3; Type e4         ; }; \
    struct Pre##6##Suff { Type e0; Type e1; Type e2; Type e3; Type e4; Type e5; }; \
    MakeRangeAndSeq(Pre, Suff)

#define MakeMatrix(Suff)                        \
    GENERATE(Vector1##Suff, Matrix_, _1_##Suff) \
    GENERATE(Vector2##Suff, Matrix_, _2_##Suff) \
    GENERATE(Vector3##Suff, Matrix_, _3_##Suff) \
    GENERATE(Vector4##Suff, Matrix_, _4_##Suff) \
    GENERATE(Vector5##Suff, Matrix_, _5_##Suff) \
    GENERATE(Vector6##Suff, Matrix_, _6_##Suff) \
    GENERATE(VectorX##Suff, Matrix_, _X_##Suff)

#define MakeVectroAndMatrix(Type,Suff) \
    GENERATE(Type, Vector, Suff)       \
    MakeMatrix(Suff)

module armarx
{
    MakeVectroAndMatrix(byte  , byte)
    MakeVectroAndMatrix(short , s   )
    MakeVectroAndMatrix(int   , i   )
    MakeVectroAndMatrix(long  , l   )
    MakeVectroAndMatrix(float , f   )
    MakeVectroAndMatrix(double, d   )
};

#undef MakeRangeAndSeqHelper
#undef MakeRangeAndSeq
#undef GENERATE
#undef MakeMatrix
#undef MakeVectroAndMatrix


