/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RemoteReferenceCount
* @author     Raphael Grimm <raphael dot grimm at kit dot edu>
* @date       2017
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

module armarx
{
    interface RemoteReferenceCountControlBlockInterface
    {
        ["indempotent"] void heartbeat(string counterId);
        void addCounter(string counterId);
        void removeCounter(string counterId);
    };

    interface SimpleRemoteReferenceCountControlBlockInterface
    {
        void addCounter(string counterId);
        void removeCounter(string counterId);
    };

    /**
     * @brief Performs reference counting on a associated RemoteReferenceCountControlBlock.
     * This version uses a heartbeat.
     *
     * This causes a slight computation and network overhead.
     * In case of a network problem, the RemoteReferenceCountControlBlock:
     *    *   may reach 0 even if a RemoteReferenceCounter still exists (e.g. the heartbeat did not reach the RemoteReferenceCountControlBlock).
     *    *   will reach 0 if the last Counter was deleted.
     * If destructors were not called (e.g. because of a segfault) the RemoteReferenceCountControlBlock will reach 0.
     *
     * The id can be used by the programmer to associate a block with some data structure. It is not used internally.
     */
    class RemoteReferenceCounterBase
    {
        ["cpp:const"] RemoteReferenceCounterBase copy();
        ["cpp:const"] string getId();

        ["protected"] RemoteReferenceCountControlBlockInterface* block;
        ["protected"] string id;
        ["protected"] long heartBeatMs;
    };

    /**
     * @brief Performs reference counting on a associated SimpleRemoteReferenceCountControlBlock.
     * This version uses NO heartbeat.
     *
     * In case of a network problem, the SimpleRemoteReferenceCounter:
     *    *   may not reach 0 even if all Counters are deleted (e.g. the deletion message did not reach the SimpleRemoteReferenceCountControlBlock).
     *    *   will not reach 0 if a Counter still exists.
     * If destructors were not called (e.g. because of a segfault) the SimpleRemoteReferenceCountControlBlock will not reach 0.
     *
     * The id can be used by the programmer to associate a block with some data structure. It is not used internally.
     */
    class SimpleRemoteReferenceCounterBase
    {
        ["cpp:const"] SimpleRemoteReferenceCounterBase copy();
        ["cpp:const"] string getId();

        ["protected"] SimpleRemoteReferenceCountControlBlockInterface* block;
        ["protected"] string id;
    };
};

