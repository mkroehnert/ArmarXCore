armarx_component_set_name(StateParameterExample)

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")

if(Eigen3_FOUND)
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
endif()

set(COMPONENT_LIBS ArmarXCoreInterfaces 
    ArmarXCore 
    ArmarXCoreObservers 
    ArmarXCoreStatechart)
#${Simox_LIBRARIES})

set(SOURCES main.cpp
    StateParameterExample.h
    StateParameterExample.cpp)

armarx_add_component_executable("${SOURCES}")
