armarx_component_set_name(ScenarioCli)

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ArmarXCoreObservers ScenarioManagerCommon)

set(SOURCES main.cpp
            ScenarioCli.h ScenarioCli.cpp
            ScenarioCliOptions.h ScenarioCliOptions.cpp)

armarx_add_component_executable("${SOURCES}")
