armarx_component_set_name(ExternalApplicationManager)

set(COMPONENT_LIBS
    ArmarXCoreInterfaces
    ArmarXCore
    ExternalApplicationManager
)

set(EXE_SOURCE ExternalApplicationManagerApp.h main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
