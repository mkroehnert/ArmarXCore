/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::application::ArmarXTimeserver
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ArmarXTimeserver.h"


using namespace armarx;


void ArmarXTimeserver::onInitComponent()
{
    offeringTopic(TIME_TOPIC_NAME);
    clock = TimeKeeper();
    stepTimeMS = getProperty<int>("TimeStepMS").getValue();
}


void ArmarXTimeserver::onConnectComponent()
{
    clock.start();
    timeTopicPrx = armarx::ManagedIceObject::getTopic<TimeServerListenerPrx>(TIME_TOPIC_NAME);

    if (broadcastTimeTask)
    {
        broadcastTimeTask->stop();
    }

    broadcastTimeTask = new PeriodicTask<ArmarXTimeserver>(this, &ArmarXTimeserver::broadcastTime, 1, false, "SpammingTimeServer::Broadcast");
    broadcastTimeTask->start();
}


void ArmarXTimeserver::onDisconnectComponent()
{

}


void ArmarXTimeserver::onExitComponent()
{

}

Ice::Long ArmarXTimeserver::getTime(const ::Ice::Current&)
{
    return static_cast<Ice::Long>(clock.getTime().toMilliSeconds());
}

void ArmarXTimeserver::broadcastTime()
{
    timeTopicPrx->reportTime(getTime());
}

void ArmarXTimeserver::stop(const ::Ice::Current&)
{
    ARMARX_DEBUG << "TimeServer clock has stopped";
    clock.stop();
}

void ArmarXTimeserver::start(const ::Ice::Current&)
{
    ARMARX_DEBUG << "TimeServer clock started";
    clock.start();
}

void ArmarXTimeserver::step(const ::Ice::Current&)
{
    clock.step(IceUtil::Time::milliSeconds(stepTimeMS));
}

void ArmarXTimeserver::setSpeed(Ice::Float newSpeed, const ::Ice::Current&)
{
    ARMARX_DEBUG << "Setting TimeServer clock speed to " << newSpeed;
    clock.setSpeed(newSpeed);
}

Ice::Float ArmarXTimeserver::getSpeed(const Ice::Current&)
{
    return clock.getSpeed();
}

Ice::Int ArmarXTimeserver::getStepTimeMS(const ::Ice::Current&)
{
    return stepTimeMS;
}

ArmarXTimeserver::~ArmarXTimeserver()
{
    broadcastTimeTask->stop();
}

armarx::PropertyDefinitionsPtr ArmarXTimeserver::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new ArmarXTimeserverPropertyDefinitions(
            getConfigDomain() + "." + "ArmarXTimeserver")); // not using defaultName since it has to be GLOBAL_TIMESERVER_NAME (="MasterTimeServer") for all timeservers
}
