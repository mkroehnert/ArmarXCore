# Add your components below as shown in the following example:
#
# set(SCENARIO_COMPONENTS
#    ConditionHandler
#    Observer
#    PickAndPlaceComponent)

set(SCENARIO_COMPONENTS
    ExternalApplicationManager
    )

# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario("ExternalApplicationManagerTestScenario" "${SCENARIO_COMPONENTS}")

