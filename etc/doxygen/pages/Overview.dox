/**

\page ArmarXCore-Overview ArmarXCore Overview


The ArmarXCore contains the base functionality for all other packages.
\image html ArmarXCore-architecture.png "Overview of the main features of ArmarXCore and their interactions"

It comprises four main building blocks:

\li \subpage DistributedProcessing "Inter-object communication"
\li Sensor-Actor Units (located in dependent projects)
\li \subpage Observers
\li \subpage Statechart "Statecharts"

Inter-object communication provides convenient mechanisms for communication between objects in the system.
These objects can be any of the other buildings blocks.
The Sensor-Actor Units offer a generic interface for robot components like motor controllers or sensors on the lowest level of abstraction.
ArmarX includes a default set of sensor-actor units for common robot components,
e.g. joints, force/torque-sensors and haptic sensors.
Implementing additional sensor-actor units for unsupported components is encouraged and straightforward.
Observers monitor sensor values from sensor-actor units and send application specific events to configured Statecharts.
These Statecharts process events and send resulting control commands to the sensor-actor units controlling the robot.
Furthermore, ArmarXCore provides \subpage core-utility-doc "Utility features" such as threading (armarx::RunningTask), \ref Logging or convenient
\ref properties "Application Properties".

\section ArmarXCore-ApplicationsExplanation Applications in ArmarX
\ref armarx::Application "Applications" in ArmarX consist of two fundamentally different building blocks: \ref armarx::Component "Components" and \ref Statechart "Statecharts".
Each application can have 0-N components or statecharts. These building blocks are always linked into C++ libraries instead of directly into an executable.
The applications then link the libraries to instantiate the component or statechart.
Of course, applications can also be used without any components and execute user code only.
To create an application with a component refer to \ref Observers-Custom-Unit.

\section ArmarXCore-ComponentsExplanation Components in ArmarX
\ref armarx::Component "Components" (or their base class \ref armarx::ManagedIceObject "ManagedIceObject") are the main building blocks of
ArmarX.
They are parameterizable network objects, which can offer a service through Ice interfaces or just do calculations on their own.
They have a managed life cycle (thus the name <i>Managed</i>IceObject) by the ArmarxObjectScheduler.
In this life cycle the components go through different phases based on the availability of their dependencies.
Each component can speficy dependencies, which are other components they intend to use.
To be able to use these, the ArmarxObjectScheduler makes sure that these other components are available, before the component
enters the *connected*-phase.
If they are not available or disappear the component is disconnected and waits until all dependencies are available.
The difference between \ref armarx::ManagedIceObject "ManagedIceObject" and \ref armarx::Component "Components" are properties.
Components can specify \ref PropertiesDoc for parameterization of the component with a config file.

\section ArmarXCore-Statecharts-ShortExplanation Statecharts in ArmarX
\ref Statechart "Statecharts" are another fundamental building block of ArmarX.
They are loosely based on the Harel statecharts (1987) and are state-transition networks for specifying control- and data-flow of a program.
In the \ref ArmarXGui-GuiPlugins-StatechartEditorController StatechartEditor it is possible to create statecharts graphically
and thus develop easily complex state-based behaviour programs.
To learn how to create ArmarX Statecharts refer to the \ref ArmarXCore-Tutorials "Statechart tutorials".


\section ComponentVsStatechart Component vs Statechart
When to use a component and when to use a statechart depends on the circumstances of the usecase.
Components offer functionality on a request basis by \ref HowtoNewInterface "implementing an Ice interface" and being started when the system starts.
Statecharts, however, take control over the system once they have been called by calling components and combining their functionality (e.g. Visual servo uses ObjectRecognition and the KinematicUnit).
Further, they react on events triggered by certain system states (e.g. hand has reached a goal position). In other words, they consider that actions in the real world take time and are not instantly available.

\image html StatechartComponent-Flow.svg "Interactions between Components and Statecharts"
\subsection WhenToUseComponentOrStatechart When to use a component or a statechart
The rules of thumb for when to use a component or a statechart are:
- \b Component:
    - Offers an interface to an algorithm for calculation upon request
    - or should always be running

- \b Statechart:
    - Executing something on a robot
    - or event driven execution
    - or in control of the system when active

*/
