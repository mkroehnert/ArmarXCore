/**
\page ArmarXCore-Tutorials-new-robot-sim New Robot Tutorial 1: Working with a new robot

\tableofcontents

This tutorial will guide you through creating a new package and starting the simulation with a new robot model.
You will need:
\li a working installation of ArmarX
\li a model of your robot in the simox xml format

As an example, this tutorial will use the model of youBot. The end goal will be starting a simulation with the new robot.

\section ArmarXCore-Tutorials-new-robot-sim-new-package Creating a new package
Since you'll need a place to start your scenarios, a new package will need to be created.
You can create a new package in your current folder by executing the following command:
\verbatim
${ArmarX_DIR}/ArmarXCore/build/bin/armarx-package init YouBotTutorial --dir=.
\endverbatim

For more information about the ArmarX package tool, refer to \ref armarx-package.
This tutorial will refer to the new package as ${RobotPackage}, and to its path as ${RobotPackage_DIR}

\section ArmarXCore-Tutorials-new-robot-sim-setup-package Setting up the package
Begin by copying your robot definition file (```YouBot.xml```) and all necessary data, such as models, into ```${RobotPackage_DIR}/data/${RobotPackage}```.
Next, you'll need to adjust the ```${RobotPackage_DIR}/CMakeLists.txt``` in the package to use other ArmarX components.
Add these lines after the line starting with ```armarx_project(...)```
\verbatim
depends_on_armarx_package(MemoryX)
depends_on_armarx_package(RobotAPI)
depends_on_armarx_package(ArmarXSimulation)
\endverbatim
Afterwards, run cmake to ensure that your package finds all dependencies:
\verbatim
cd ${RobotPackage_DIR}/build
cmake ..
\endverbatim

\note If you want to depend on a specific ArmarX release, you have to define the version number for your own package to match the ArmarX release version number. See \ref versioning "ArmarX Package Versioning" for details on how to do this.

\section ArmarXCore-Tutorials-new-robot-sim-new-scenario Creating a new scenario
Now that the package is set up, a scenario is needed to execute the simulation. You can do that by using the scenario manager (ArmarX GUI -> Add Widger -> Meta -> ScenarioManager). Create a new scenario after you have registered the package that you created earlier.
\image html tutorials-new-robot-create-scenario.png

Then add the following applications from the application database to the newly created scenario (use drag and drop):
\li CommonStorage
\li PriorKnowledge
\li WorkingMemory
\li SimulatorApp
\li SimulatorViewerApp

The resulting scenario view should look similar to this screenshot:
\image html tutorials-new-robot-scenario-apps.png

Some of the applications can be directly used while others need to be configured. The following table shows all applications and the corresponding properties that need to be set.
<table>
        <tr><td>Application</td><td>Properties</td></tr>

<tr><td>CommonStorage</td><td><pre>
MemoryX.CommonStorage.MongoUser = ""
MemoryX.CommonStorage.MongoPassword = ""
</pre></td></tr>

<tr><td>PriorKnowledge</td><td><pre>
MemoryX.PriorKnowledge.ClassCollections = testdb.testCollection
</pre></td></tr>

<tr><td>SimulatorApp</td><td><pre>
ArmarX.Simulator.FixedTimeStepStepTimeMS = 1
ArmarX.Simulator.FixedTimeStepLoopNrSteps = 3
ArmarX.Simulator.FloorTexture = "SimulationX/images/floor.png"
ArmarX.Simulator.RobotFileName=YouBotTutorial/YouBot.xml
</pre></td></tr>

<tr><td>SimulatorViewerApp.cfg</td><td><pre>
ArmarX.SimulatorViewer_SimulationWindow.Camera.x = 0
ArmarX.SimulatorViewer_SimulationWindow.Camera.y = 4000
ArmarX.SimulatorViewer_SimulationWindow.Camera.z = 2500
ArmarX.SimulatorViewer_SimulationWindow.Camera.roll = 1.2
ArmarX.SimulatorViewer_SimulationWindow.Camera.yaw = 3.14
</pre></td></tr>
</table>
\note SimulatorApp specifies YouBot as the robot, make sure you replace the filename with your robot.

\note Make sure to set the MongoUser and MongoPassword to empty strings (""). This is not equivalent to not specifying these properties. You will get errors because of missing required parameters if you do not explicitly set these parameters.

\section ArmarXCore-Tutorials-new-robot-sim-start-scenario Starting the scenario
Before continuing, make sure you've started ice and the database with the following commands:
\verbatim
${ArmarX_DIR}/ArmarXCore/build/bin/armarx start
${ArmarX_DIR}/ArmarXCore/build/bin/armarx memory start
\endverbatim
Once done, you can start and stop the scenario using the buttons in the scenario manager.

Alternatively you can use the command line interface:
\code{.sh}
armarx scenario start YouBotSimulation -p YouBotTutorial
\endcode

Doing so should start the simulation, and show you the specified robot.
\image html Tutorials-new-robot-sim.png
Once you're done, stop the scenario using the GUI or:
\code{.sh}
armarx scenario stop YouBotSimulation -p YouBotTutorial
\endcode

*/
