option(ARMARX_USE_GOLD_LINKER "Use the faster Gold Linker" FALSE)
if(ARMARX_USE_GOLD_LINKER)
    set (LDFLAGS "${LDFLAGS} -Wl,-fuse-ld=gold ")
endif()

#we want Release and RelWithDebInfo to use the same optimization level
string(REGEX MATCH "([ \t]|^)-O[^ \t]+([ \t]|^)" RELEASE_O_LEVEL ${CMAKE_CXX_FLAGS_RELEASE})
string(REGEX MATCH "([ \t]|^)-O[^ \t]+([ \t]|^)" RELWITHDEBINFO_O_LEVEL ${CMAKE_CXX_FLAGS_RELWITHDEBINFO})
if(NOT RELEASE_O_LEVEL STREQUAL RELWITHDEBINFO_O_LEVEL)
    string(REPLACE "${RELWITHDEBINFO_O_LEVEL}" "${RELEASE_O_LEVEL}" CMAKE_CXX_FLAGS_RELWITHDEBINFO ${CMAKE_CXX_FLAGS_RELWITHDEBINFO})
    message(STATUS "Changing o level of RelWithDebInfo to match o level of Release (from '${RELWITHDEBINFO_O_LEVEL}' to '${RELEASE_O_LEVEL}')")
    message(STATUS "CMAKE_CXX_FLAGS_RELWITHDEBINFO = ${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
endif()

# Various compiler settings for common targets
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
CHECK_CXX_COMPILER_FLAG("-stdlib=libstdc++" COMPILER_SUPPORTS_libstdcpp)
#gcc silently accepts unknown -Wno- warnings if no other error occurs
#-> test for positive version
#https://stackoverflow.com/a/38786117
CHECK_CXX_COMPILER_FLAG("-Winconsistent-missing-override" COMPILER_SUPPORTS_inconsistent_missing_override)
CHECK_CXX_COMPILER_FLAG("-Wdeprecated-register" COMPILER_SUPPORTS_deprecated_register)
CHECK_CXX_COMPILER_FLAG("-Wsuggest-override" COMPILER_SUPPORTS_suggest_override)

include(CheckCCompilerFlag)
CHECK_C_COMPILER_FLAG("-std=c99" COMPILER_SUPPORTS_C99)

option(ARMARX_COMPILER_WARNINGS_AS_ERROR "Treat compilation warnings as errors if set to TRUE/ON." FALSE)

set(ARMARX_GENERAL_COMPILE_FLAGS "-Wall -Wextra -Wpedantic -Wno-long-long -Wno-unused-parameter")
## needed for compability between gcc and clang
if(COMPILER_SUPPORTS_libstdcpp)
    message(STATUS "Using libstdc++")
    set(ARMARX_GENERAL_COMPILE_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS} -stdlib=libstdc++")
    # needed to make clang work
    set(LDFLAGS "${LDFLAGS} -lstdc++ -lm ")
endif()

if(COMPILER_SUPPORTS_inconsistent_missing_override)
    set(ARMARX_GENERAL_COMPILE_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS} -Wno-inconsistent-missing-override ")
endif()

if(COMPILER_SUPPORTS_deprecated_register)
    set(ARMARX_GENERAL_COMPILE_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS} -Wno-deprecated-register ")
endif()

if(COMPILER_SUPPORTS_suggest_override)
    set(ARMARX_GENERAL_COMPILE_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS} -Wsuggest-override -Werror=suggest-override ")
endif()

#fix for old graphviz version (similar issue, see: https://github.com/KDAB/GammaRay/issues/70)
# /usr/include/graphviz/cdt.h:27:20: error: declaration of 'int memcmp(const void*, const void*, size_t)' has a different exception specifier
set(ARMARX_GENERAL_COMPILE_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS} -DHAVE_STRING_H")

if (ARMARX_COMPILER_WARNINGS_AS_ERROR)
    set(ARMARX_GENERAL_COMPILE_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS} -Werror")
endif()

add_definitions(-DBOOST_ENABLE_ASSERT_HANDLER -DARMARX_VERSION=${ARMARX_PACKAGE_LIBRARY_VERSION})

set(CMAKE_C_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS}")

set(CMAKE_CXX_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS}")

if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 ")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x ")
else()
    message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

if(COMPILER_SUPPORTS_C99)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99 ")
else()
    message(FATAL_ERROR "The compiler ${CMAKE_C_COMPILER} has no C99 support. Please use a different C compiler.")
endif()

if (NOT ARMARX_OS_WIN)
   # set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O2 ")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g ")
endif()

# enable multipthreaded compilation on visual studio
if (ARMARX_OS_WIN AND MSVC)
    add_definitions(/MP)
endif()


macro(executable_settings EXECUTABLE_NAME)
    # allows additional compiler-flags (CXX_FLAGS and C_FLAGS) as well as linker-flags (LDFLAGS)
    ARMARX_MESSAGE(STATUS "        Configuring executable ${EXECUTABLE_NAME}.")

    ARMARX_MESSAGE(STATUS "        Using additional compiler flags (CXX_FLAGS): ${CXX_FLAGS} ${C_FLAGS}")
    target_compile_options(${EXECUTABLE_NAME} PUBLIC ${CXX_FLAGS} ${C_FLAGS})

    ARMARX_MESSAGE(STATUS "        Using linker flags: ${LDFLAGS}")
    set_target_properties(${EXECUTABLE_NAME} PROPERTIES
        LINK_FLAGS "${LDFLAGS}"
        LIBRARY_OUTPUT_DIRECTORY "${ARMARX_LIB_DIR}"
        ARCHIVE_OUTPUT_DIRECTORY "${ARMARX_ARCHIVE_DIR}"
        RUNTIME_OUTPUT_DIRECTORY "${ARMARX_BIN_DIR}"
    )

    ARMARX_MESSAGE(STATUS "        RUNTIME_OUTPUT_DIRECTORY: " ${ARMARX_BIN_DIR})

    if (ARMARX_OS_LINUX)
        get_target_property(EXECUTABLE_FILE ${EXECUTABLE_NAME} LOCATION)
        if (${CMAKE_BUILD_TYPE} MATCHES "Release")
            MESSAGE(STATUS "        RELEASE_MODE: stripping binaries")
            add_custom_command(TARGET ${EXECUTABLE_NAME} POST_BUILD
                               COMMAND strip ARGS --strip-unneeded ${EXECUTABLE_FILE})
        endif()
    endif()
endmacro()


macro(library_base_settings_end LIB_NAME HEADERS)
    set_target_properties(${LIB_NAME} PROPERTIES
        VERSION   ${ARMARX_PACKAGE_LIBRARY_VERSION}
        SOVERSION ${ARMARX_PACKAGE_LIBRARY_SOVERSION})
    if(NOT LIB_ALLOW_UNDEFINED_SYMBOLS)
        if (ARMARX_OS_LINUX)
            set(LDFLAGS "${LDFLAGS} -Wl,-z,defs")
        elseif (ARMARX_OS_MAC)
            set(LDFLAGS "${LDFLAGS} -Wl,-undefined,error")
        endif()
    endif()


    ARMARX_MESSAGE(STATUS "        Using compiler flags (CXX_FLAGS): ${CXX_FLAGS} ${C_FLAGS}")
    target_compile_options(${LIB_NAME} PUBLIC ${CXX_FLAGS} ${C_FLAGS})

    ARMARX_MESSAGE(STATUS "        Using linker flags: ${LDFLAGS}")

    set_target_properties(${LIB_NAME} PROPERTIES
        LINK_FLAGS "${LDFLAGS}"
        LIBRARY_OUTPUT_DIRECTORY "${ARMARX_LIB_DIR}"
        ARCHIVE_OUTPUT_DIRECTORY "${ARMARX_ARCHIVE_DIR}"
        RUNTIME_OUTPUT_DIRECTORY "${ARMARX_BIN_DIR}"
    )

   ARMARX_MESSAGE(STATUS "        LIBRARY_OUTPUT_DIRECTORY: " ${ARMARX_LIB_DIR})
   ARMARX_MESSAGE(STATUS "        ARCHIVE_OUTPUT_DIRECTORY: " ${ARMARX_ARCHIVE_DIR})
   ARMARX_MESSAGE(STATUS "        RUNTIME_OUTPUT_DIRECTORY: " ${ARMARX_BIN_DIR})

    if (ARMARX_OS_LINUX)
        get_target_property(LIB_FILE ${LIB_NAME} LOCATION)
        if (${CMAKE_BUILD_TYPE} MATCHES "Release")
            MESSAGE(STATUS "        RELEASE_MODE: stripping binaries")
            add_custom_command(TARGET ${LIB_NAME} POST_BUILD
                               COMMAND strip ARGS --strip-unneeded ${LIB_FILE})
        endif()
    endif()

    set(SLICE_DEPENDS "")
    foreach(SLICE_DEPEND ${SLICE_DEPENDS_DIRTY})
        string(LENGTH ${SLICE_DEPEND} SLICE_DEPEND_LENGTH)
        math(EXPR SLICE_DEPEND_LENGTH "${SLICE_DEPEND_LENGTH}-1")
        string(SUBSTRING ${SLICE_DEPEND} 1 ${SLICE_DEPEND_LENGTH} SLICE_DEPEND)
        string(STRIP ${SLICE_DEPEND} SLICE_DEPEND)
        list(APPEND SLICE_DEPENDS ${SLICE_DEPEND})
    endforeach()

    string(REPLACE "${PROJECT_SOURCECODE_DIR}/" "" HEADER_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
    library_install("${LIB_NAME}" "${HEADERS}" "${HEADER_DIR}")
endmacro()

macro(library_settings LIB_NAME HEADERS)
    ARMARX_MESSAGE(STATUS "        Configuring shared library ${LIB_NAME} version ${ARMARX_PACKAGE_LIBRARY_VERSION}.")

    if(NOT ARMARX_OS_WIN)
        set(CXX_FLAGS ${CXX_FLAGS} -fPIC)
        add_definitions(-D_REENTRANT)
    else()
        add_definitions(-D_USE_MATH_DEFINES -D${ARMARX_PROJECT_NAME}_EXPORTS)
    endif()

    library_base_settings_end("${LIB_NAME}" "${HEADERS}")
endmacro()
