###
### FindIce.cmake -- find Ice paths and files.
###
#
# Set Ice_DIR from the cmake commandline to specify where to find Ice
# besides the standard locations.
#
# This module defines the following variables:
# Ice_FOUND : 1 if Ice is found, 0 otherwise
# Ice_INCLUDE_DIRS
# Ice_LIBRARIES
# Ice_Slice_DIR
#
# <LibName>_FOUND   : 1 if <LibName> is found, 0 otherwise
# <LibName>_LIBRARY : path to <LibName> library
#
# LibName is one of:
#  Ice Freeze Glacier2 IceBox IceGrid IcePatch2 IceSSL IceStorm IceStormService IceUtil IceXML Slice
#
# Ice_Java_FOUND : 1 if Ice for Java is found, 0 otherwise
# Ice_Java_DIR   : path where to find Ice for Java archive
#
### Based on a module written by Markus Rickert for the JAST project.
### Updated by Mary Ellen Foster.
### Update by Manfred Kroehnert:
###  * lowercase macro names
###  * change ICE_ to Ice_ to match CMake conventions
###  * add Ice_DIR to match CMake conventions
###  * add slice2html
###  * make safe for use with pathnames containing spaces
###  * use foreach() loops instead of repetition
###

# Doesn't seem to work properly with CMake 2.6
cmake_minimum_required(VERSION 2.8)

# Workaround for missing library links on DICE
set(CMAKE_FIND_LIBRARY_SUFFIXES .so.34 .34.dylib ${CMAKE_FIND_LIBRARY_SUFFIXES})

# Hack for finding the right files for various Windows compilers
# See http://doc.zeroc.com/display/Ice/Using+the+Windows+Binary+Distribution
if(CMAKE_GENERATOR MATCHES "Visual Studio 10 Win64")
    set (SUBDIR "/vc100/x64")
elseif(CMAKE_GENERATOR MATCHES "Visual Studio 10")
    set (SUBDIR "/vc100")
elseif(CMAKE_GENERATOR MATCHES "Visual 9 2008 Win64")
    set (SUBDIR "/x64")
elseif(Ice_SUBDIR)
    # (e.g., if you're using NMake makefiles and you still need 64-bitness)
    set (SUBDIR ${Ice_SUBDIR})
endif()

file(
    GLOB
    Ice_BIN_DIRS
    "${Ice_DIR}/bin${SUBDIR}"
    "${ICE_HOME}/bin${SUBDIR}"
    "$ENV{Ice_DIR}/bin${SUBDIR}"
    "$ENV{ICE_HOME}/bin${SUBDIR}"
    "$ENV{HOME}/bin${SUBDIR}"
    "/opt/Ice-*/bin${SUBDIR}"
    "/usr/local/bin${SUBDIR}"
    "/usr/bin${SUBDIR}"
    "$ENV{SystemDrive}/Ice-*/bin${SUBDIR}"
    "$ENV{ProgramFiles}/ZeroC/Ice-*/bin${SUBDIR}"
    "$ENV{ProgramFilesW6432}/ZeroC/Ice-*/bin${SUBDIR}"
)
file(
    GLOB
    Ice_INCLUDE_DIRECTORIES
    "${Ice_DIR}/include"
    "${ICE_HOME}/include"
    "$ENV{Ice_DIR}/include"
    "$ENV{ICE_HOME}/include"
    "$ENV{HOME}/include"
    "/opt/Ice-*/include"
    "/usr/local/include"
    "/usr/include"
    "$ENV{SystemDrive}/Ice-*/include"
    "$ENV{ProgramFiles}/ZeroC/Ice-*/include"
    "$ENV{ProgramFilesW6432}/ZeroC/Ice-*/include"
)
file(
    GLOB
    Ice_LIBRARY_DIRS
    "${Ice_DIR}/lib${SUBDIR}"
    "${ICE_HOME}/lib${SUBDIR}"
    "$ENV{Ice_DIR}/lib${SUBDIR}"
    "$ENV{ICE_HOME}/lib${SUBDIR}"
    "$ENV{HOME}/lib${SUBDIR}"
    "/opt/Ice-*/lib${SUBDIR}"
    "/usr/local/lib${SUBDIR}"
    "/usr/lib/x86_64-linux-gnu${SUBDIR}"
    "/usr/lib${SUBDIR}"
    "$ENV{SystemDrive}/Ice-*/lib${SUBDIR}"
    "$ENV{ProgramFiles}/ZeroC/Ice-*/lib${SUBDIR}"
    "$ENV{ProgramFilesW6432}/ZeroC/Ice-*/lib${SUBDIR}"
)

file(
    GLOB
    Ice_Slice_DIRS
    "${Ice_DIR}/slice"
    "${ICE_HOME}/slice"
    "${Ice_Slice_DIR}"
    "$ENV{Ice_DIR}/slice"
    "$ENV{Ice_Slice_DIR}"
    "$ENV{ICE_HOME}/slice"
    "$ENV{HOME}/share/slice"
    "/opt/Ice-*/slice"
    "/usr/share/slice"
    "/usr/share/Ice-*/slice"
    "/usr/local/share/slice"
    "/usr/local/share/Ice-*/slice"
    "$ENV{SystemDrive}/Ice-*/slice"
    "$ENV{ProgramFiles}/ZeroC/Ice-*/slice"
    "$ENV{ProgramFilesW6432}/ZeroC/Ice-*/slice"
)

find_path(
    Ice_INCLUDE_DIR
    Ice/Ice.h
    ${Ice_INCLUDE_DIRECTORIES}
)

find_path(
    Ice_Slice_DIR
    Ice/BuiltinSequences.ice
    ${Ice_Slice_DIRS}
)

macro(ICE_ADJUST_LIB_VARS basename)
    if(Ice_${basename}_LIBRARY_DEBUG AND NOT Ice_${basename}_LIBRARY_RELEASE)
        set(Ice_${basename}_LIBRARY ${Ice_${basename}_LIBRARY_DEBUG})
    endif()

    if(Ice_${basename}_LIBRARY_RELEASE AND NOT Ice_${basename}_LIBRARY_DEBUG)
        set(Ice_${basename}_LIBRARY ${Ice_${basename}_LIBRARY_RELEASE})
    endif()

    if(Ice_${basename}_LIBRARY_DEBUG AND Ice_${basename}_LIBRARY_RELEASE)
        set(Ice_${basename}_LIBRARY debug ${Ice_${basename}_LIBRARY_DEBUG} optimized ${Ice_${basename}_LIBRARY_RELEASE})
    endif()

    if(Ice_${basename}_LIBRARY)
        set(Ice_${basename}_FOUND TRUE)
    endif()

    list(APPEND Ice_LIBRARIES "${Ice_${Ice_Lib_Name}_LIBRARY}")
    list(APPEND Ice_LIBRARIES_DEBUG "${Ice_${Ice_Lib_Name}_LIBRARY_DEBUG}")

    mark_as_advanced(
        Ice_${Ice_Lib_Name}_FOUND
        Ice_${Ice_Lib_Name}_LIBRARY
        Ice_${Ice_Lib_Name}_LIBRARY_DEBUG
        Ice_${Ice_Lib_Name}_LIBRARY_RELEASE
    )
endmacro()

# libIce needs special care because libICE from X11 might be found
find_library(Ice_Ice_LIBRARY_DEBUG NAMES Iced PATHS ${Ice_LIBRARY_DIRS})
find_library(Ice_Ice_LIBRARY_RELEASE NAMES Ice PATHS ${Ice_LIBRARY_DIRS} NO_DEFAULT_PATH)
ICE_ADJUST_LIB_VARS(Ice)

set(Ice_CHECK_LIBRARIES
    Freeze Glacier2 IceBox
    IceGrid IcePatch2 IceSSL
    IceStorm IceStormService
    IceUtil IceXML Slice
)
foreach(Ice_Lib_Name ${Ice_CHECK_LIBRARIES})
    find_library(Ice_${Ice_Lib_Name}_LIBRARY_DEBUG NAMES "${Ice_Lib_Name}d" PATHS ${Ice_LIBRARY_DIRS})
    find_library(Ice_${Ice_Lib_Name}_LIBRARY_RELEASE NAMES "${Ice_Lib_Name}" PATHS ${Ice_LIBRARY_DIRS})
    ICE_ADJUST_LIB_VARS(${Ice_Lib_Name})
endforeach()

if(NOT (CMAKE_SYSTEM MATCHES Windows))
    set (PTHREAD_IF_NECESSARY pthread)
    list(APPEND Ice_LIBRARIES ${PTHREAD_IF_NECESSARY})
    list(APPEND Ice_LIBRARIES_DEBUG ${PTHREAD_IF_NECESSARY})
endif()

# detect Ice programs
# sets Ice_${PROGRAM_NAME}_FOUND if successful
set(Ice_CHECK_BINARIES slice2cpp slice2html slice2java)
foreach(Ice_Current_Binary ${Ice_CHECK_BINARIES})
    find_program(Ice_${Ice_Current_Binary} ${Ice_Current_Binary} ${Ice_BIN_DIRS})
    if(Ice_${Ice_Current_Binary})
        set(Ice_${Ice_Current_Binary}_FOUND TRUE)
        mark_as_advanced(Ice_${Ice_Current_Binary} Ice_${Ice_Current_Binary}_FOUND)
    endif()
endforeach()


set(Ice_INCLUDE_DIRS "${Ice_INCLUDE_DIR}")
# set Ice_DIR
get_filename_component(Ice_DIR "${Ice_INCLUDE_DIR}" PATH CACHE)

# Check Ice version
exec_program("${Ice_slice2cpp}" ARGS --version OUTPUT_VARIABLE Ice_VERSION)

string(REPLACE "." ";" VERSION_LIST ${Ice_VERSION})
list(GET VERSION_LIST 0 Ice_VERSION_MAJOR)
list(GET VERSION_LIST 1 Ice_VERSION_MINOR)
list(GET VERSION_LIST 2 Ice_VERSION_PATCH)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Ice
    REQUIRED_VARS Ice_LIBRARIES Ice_LIBRARIES_DEBUG Ice_INCLUDE_DIRS Ice_Slice_DIR Ice_slice2cpp
    VERSION_VAR Ice_VERSION)


set(Ice_FOUND ${ICE_FOUND})
mark_as_advanced(
    Ice_FOUND
    ICE_FOUND
    Ice_INCLUDE_DIR
    Ice_INCLUDE_DIRS
    Ice_Slice_DIR
    Ice_LIBRARIES
    Ice_LIBRARIES_DEBUG
)


macro(ICE_WRAP_CPP_2 outfiles)
    set(SLICE_INCLUDES "-I${Ice_Slice_DIR}")
    file(TO_CMAKE_PATH "${ARGN}" argn_cmake)
    foreach(i ${argn_cmake})
        if(IS_DIRECTORY ${i})
            # If the argument is a directory, it's the directory where the next set of files exist
            # -- also add it to the persistent include path
            set(base "${i}")
            set(SLICE_INCLUDES -I"${base}" ${SLICE_INCLUDES})
        else()
            # Otherwise, it's a file to process
            get_filename_component(name "${i}" NAME)
            get_filename_component(name_we "${i}" NAME_WE)
            get_filename_component(relative "${i}" PATH)
            set(infile "${base}/${relative}/${name}")
            set(outfile1 "${CMAKE_CURRENT_BINARY_DIR}/${relative}/${name_we}.cpp")
            set(outfile2 "${CMAKE_CURRENT_BINARY_DIR}/${relative}/${name_we}.h")
            file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/${relative}")
            add_custom_command(
                OUTPUT "${outfile1}" "${outfile2}"
                COMMAND "${Ice_Slice2Cpp}"
                ARGS ${SLICE_INCLUDES} "-I${base}/${relative}" --output-dir "${CMAKE_CURRENT_BINARY_DIR}/${relative}" "${infile}"
                MAIN_DEPENDENCY "${infile}"
            )
            set(${outfiles} "${${outfiles}}" "${outfile1}" "${outfile2}")
            include_directories("${CMAKE_CURRENT_BINARY_DIR}")
            include_directories("${CMAKE_CURRENT_BINARY_DIR}/${relative}")
        endif()
    endforeach()
endmacro(ICE_WRAP_CPP_2)

#
# Ice for Java
#
find_path(Ice_Java_DIR Ice.jar
    # installation selected by user
    "$ENV{Ice_Java_HOME}"
    "${Ice_DIR}/lib"
    "${Ice_HOME}/lib"
    "$ENV{Ice_DIR}/lib"
    "$ENV{Ice_HOME}/lib"
    # debian package installs Ice here
    /usr/share/java
)

if(Ice_Java_DIR)
    set(Ice_Java_FOUND TRUE)
else()
    set(Ice_Java_FOUND FALSE)
endif()
