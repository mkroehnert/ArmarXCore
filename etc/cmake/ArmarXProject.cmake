# Macros for ArmarX Framework Projects

cmake_minimum_required(VERSION 2.8)

option(ARMARX_USE_ARMARX_GLOBAL_CMAKE_BUILD_TYPE "Use the environment variable 'ARMARX_GLOBAL_CMAKE_BUILD_TYPE' to determine the build type" ON)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release") #default
    if(ARMARX_USE_ARMARX_GLOBAL_CMAKE_BUILD_TYPE AND DEFINED ENV{ARMARX_GLOBAL_CMAKE_BUILD_TYPE})
        set(CMAKE_BUILD_TYPE "$ENV{ARMARX_GLOBAL_CMAKE_BUILD_TYPE}")
    endif()

    set(CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE}" CACHE STRING
        "Choose the type of build, options are: Debug Release RelWithDebInfo MinSizeRel."
       FORCE)
endif(NOT CMAKE_BUILD_TYPE)

message(STATUS "CMAKE_BUILD_TYPE = ${CMAKE_BUILD_TYPE}")

# include required for ExternalProject_Add()
include(ExternalProject)
if( DEFINED ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK_DEFAULT )
    option(ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK "Throws an error if dependent ArmarX packages have differing version number (set to FALSE to disable)" ${ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK_DEFAULT})
else()
    option(ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK "Throws an error if dependent ArmarX packages have differing version number (set to FALSE to disable)" FALSE)
endif()
# Set variable here (written in depends_on_armarx_package() and setupDirs.cmake)
# Otherwise they will get overwritten by the other macros, thus leading to missing dependencies
set(ARMARX_PROJECT_DEPENDENCIES "ArmarXCore" CACHE INTERNAL "Project Dependencies on other ArmarX Packages" FORCE)
set(ARMARX_PROJECT_DEPENDENT_DATA_DIRS "" CACHE INTERNAL "Collection of all *_DATA_DIR entries of all dependent projects")
set(ARMARX_PROJECT_DISABLED_TARGETS "" CACHE INTERNAL "Disabled Targets in current ArmarX Package" FORCE)
set(ARMARX_PROJECT_DISABLED_DEPENDENT_TARGETS "${ArmarXCore_DISABLED_TARGETS}" CACHE INTERNAL "Disabled Targets from dependent ArmarX Packages" FORCE)
set(ARMARX_PROJECT_DEPENDENT_TAG_FILES "ArmarXCore.tag=${ArmarXCore_DOXYGEN_TAG_FILE}" CACHE INTERNAL "Dependent Doxygen Tag files for linking with documentation of other packages" FORCE)
set(ARMARX_MISSING_PROJECT_DEPENDENCIES "" CACHE INTERNAL "Missing Project Dependencies on other ArmarX Packages")
if(DEFINED ArmarXCore_SOURCE_DIR)
    if(NOT "${ARMARX_PROJECT_NAME}" EQUAL "ArmarXCore")
        set(ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES "ArmarXCore" CACHE INTERNAL "Project Source Dependencies on other ArmarX Packages")
    endif()
else()
    set(ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES "" CACHE INTERNAL "Project Source Dependencies on other ArmarX Packages")
endif()
set(ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS "" CACHE INTERNAL "Project Source Dependencies with paths on other ArmarX Packages")
# Use 'make dependencies' to build all external project dependencies
add_custom_target(dependencies
    COMMAND echo "Building external dependencies: ${ARMARX_PROJECT_DEPENDENCIES}"
    COMMENT ""
)

# This macro does the following things:
# * set ARMARX_PROJECT_NAME
# * call the CMake project() macro
# * add external dependency on ArmarXCore
# * set ${PROJECT_NAME}_CMAKE_DIR
# * include the file setup.cmake
# * added include directories are stored in PROJECT_INCLUDE_DIRECTORIES
# * add ${ARMARX_LIB_DIR} and ${ArmarXCore_LIBRARY_DIRS} to link paths
macro(armarx_project NAME)
    set(ARMARX_PROJECT_NAME "${NAME}")
    project("${NAME}" CXX)
    set(${NAME}_FOUND ON)
    # Add dependency on ArmarXCore but not for ArmarXCore itself
    if(NOT "${ARMARX_PROJECT_NAME}" STREQUAL "ArmarXCore")
        if(DEFINED ArmarXCore_SOURCE_DIR)
            add_external_dependency(ArmarXCore)
        endif()
        list(APPEND CMAKE_MODULE_PATH "${ArmarXCore_CMAKE_DIR}")
        #message(FATAL_ERROR "cmake_minimum_required(): ${ArmarXCore_CMAKE_DIR}")

        set(ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS "ArmarXCore:${ArmarXCore_PACKAGE_CONFIG_DIR}" CACHE INTERNAL "Project Source Dependencies with paths on other ArmarX Packages")
    endif()

    if(EXISTS "${PROJECT_SOURCE_DIR}/etc/cmake/")
        set(${PROJECT_NAME}_CMAKE_DIR "${PROJECT_SOURCE_DIR}/etc/cmake/")
        list(APPEND CMAKE_MODULE_PATH "${${PROJECT_NAME}_CMAKE_DIR}")

        set(PROJECT_USE_FILE "${${PROJECT_NAME}_CMAKE_DIR}/Use${PROJECT_NAME}.cmake")
    endif()

    # The variable ArmarXCore_CMAKE_DIR gets set in ProjectConfig.cmake.in
    # or in the toplevel CMakeLists.txt of ArmarXCore
    include(${ArmarXCore_CMAKE_DIR}/setup.cmake)

    # include the macros defined in the package specific Use${ARMARX_PROJECT_NAME}.cmake
    if(NOT "${ARMARX_PROJECT_NAME}" STREQUAL "ArmarXCore")
        if (EXISTS ${PROJECT_USE_FILE})
            include(${PROJECT_USE_FILE})
        endif()
    endif()

    if(NOT "${ARMARX_PROJECT_NAME}" STREQUAL "ArmarXCore")
        set(ARMARX_PROJECT_DEPENDENT_DATA_DIRS "${ARMARX_PROJECT_DEPENDENT_DATA_DIRS}" "${PROJECT_DATA_DIR}" "${ArmarXCore_DATA_DIR}" CACHE INTERNAL "Collection of all *_DATA_DIR entries of all dependent projects")
    else()
        set(ARMARX_PROJECT_DEPENDENT_DATA_DIRS "${PROJECT_DATA_DIR}" CACHE INTERNAL "Collection of all *_DATA_DIR entries of all dependent projects")
    endif()
    # This variable is needed in Installation.cmake for setting
    # PROJECT_INCLUDE_DIRS
    # the last entry is necessary for #include directives inside the generated slice files
    set(PROJECT_INCLUDE_DIRECTORIES
        ${PROJECT_SOURCECODE_DIR}
        ${PROJECT_BINARY_DIR}/source
        )

    string(REPLACE ";" "/" DEF_DEPENDENCIES "${ARMARX_PROJECT_DEPENDENCIES}")
    add_definitions(-DDEPENDENCIES=${DEF_DEPENDENCIES})

    foreach(dir ${ArmarXCore_INCLUDE_DIRS})
        string(REGEX MATCHALL ^.*/build/source$ matched "${dir}")
        if(matched)
            include_directories(SYSTEM ${dir})
        else()
            include_directories(${dir})
        endif()
    endforeach()

    foreach(dir  ${PROJECT_INCLUDE_DIRECTORIES})
        string(REGEX MATCHALL ^.*/build/source$ matched "${dir}")
        if(matched)
            include_directories(SYSTEM ${dir})
        else()
            include_directories(${dir})
        endif()
    endforeach()

    link_directories(${ARMARX_LIB_DIR}
                     ${ArmarXCore_LIBRARY_DIRS})

    # make path absolute
    get_filename_component(${PROJECT_NAME}_CMAKE_DIR "${${PROJECT_NAME}_CMAKE_DIR}" ABSOLUTE)

endmacro()

# This macro uses find_package() to search for ${DEPENDENCY} with the REQUIRED mode.
# Add a second argument to the macro parameters to search for optional packages with the QUIET mode.
#
# If the dependency was found it adds ${DEPENDENCY}_INCLUDE_DIRS
# via include_directories() and the ${DEPENDENCY} itself to ${ARMARX_PROJECT_DEPENDENCIES}.
# ${ARMARX_PROJECT_DEPENDENCIES} in turn is used by ArmarXScenario.cmake for generating scenarios
# and in Installation.cmake to write the dependecies into the necessary configuration files.
macro(depends_on_armarx_package DEPENDENCY)
    if (NOT ARMARX_PROJECT_NAME)
        # FATAL_ERROR if used before armarx_project() macro
        message(FATAL_ERROR "\n\nThe macro depends_on_armarx_package() must be called AFTER the armarx_project() macro.")
    endif()

    if("${DEPENDENCY}" STREQUAL "${ARMARX_PROJECT_NAME}")
        message(STATUS "skipping self dependency")
    else()
        if (NOT "${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_LIST_DIR}")
            message(FATAL_ERROR "\n\nThe macro depends_on_armarx_package() can only be called in the toplevel CMakeLists.txt file of the current ArmarX Package.")
        endif()

        if(${${DEPENDENCY}_FOUND})
            message(WARNING "There was already a call to find_package(${DEPENDENCY} ${ARMARX_PACKAGE_LIBRARY_VERSION}) in your project")
            message(WARNING "Please remove it in favor of the depends_on_armarx_package(${DEPENDENCY}) in your toplevel CMakeLists.txt file.")
        else()
            if (${ARGC} GREATER 1)
                # package is optional if a second argument is given
                find_package(${DEPENDENCY} QUIET
                    PATHS "$ENV{HOME}/armarx/${DEPENDENCY}/build"
                )
            else ()
                find_package(${DEPENDENCY} QUIET
                    PATHS "$ENV{HOME}/armarx/${DEPENDENCY}/build"
                )
                if( NOT ${${DEPENDENCY}_FOUND})
                    set(ARMARX_MISSING_PROJECT_DEPENDENCIES "${ARMARX_MISSING_PROJECT_DEPENDENCIES}" "${DEPENDENCY}" CACHE INTERNAL "Missing Project Dependencies on other ArmarX Packages")
                endif()
            endif()
        endif()

        string(FIND "${ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES}" "${DEPENDENCY}" pos)
        # ${DEPENDENCY}_BUILD_DIR is only set if source package and not installed
        # if ${${DEPENDENCY}_FOUND} is not available the project was not found at all, assume its a source package
        if(${pos} EQUAL -1 AND (${DEPENDENCY}_BUILD_DIR OR NOT ${${DEPENDENCY}_FOUND}))
            set(ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES "${ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES}" "${DEPENDENCY}" CACHE INTERNAL "Project Source Dependencies on other ArmarX Packages")
        endif()

        string(FIND "${ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS}" "${DEPENDENCY}" pos)
        if(${pos} EQUAL -1)
           set(ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS "${ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS}" "${DEPENDENCY}:${${DEPENDENCY}_PACKAGE_CONFIG_DIR}" CACHE INTERNAL "Project Dependencies paths on other ArmarX Packages")
        endif()


        # check if the version number of the found dependency matches the version of the current package
        # throw an error if the version numbers don't match
        if (ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK AND ${${DEPENDENCY}_FOUND} AND NOT "${${DEPENDENCY}_VERSION}" VERSION_EQUAL "${ARMARX_PACKAGE_LIBRARY_VERSION}")
            message(STATUS "")
            message(STATUS "")
            message(FATAL_ERROR "\n\nVersion mismatch\n ${ARMARX_PROJECT_NAME} version: ${ARMARX_PACKAGE_LIBRARY_VERSION}\n ${DEPENDENCY} version: ${${DEPENDENCY}_VERSION} instead of ${ARMARX_PACKAGE_LIBRARY_VERSION}). \n Please update the depencency ${DEPENDENCY} or adapt the version number of ${ARMARX_PROJECT_NAME}.\n For details see: https://i61wiki.itec.uka.de/doc/armarxdoc/index.html")
        endif()

        if(${${DEPENDENCY}_FOUND})
            set(ARMARX_PROJECT_DEPENDENCIES "${ARMARX_PROJECT_DEPENDENCIES}" "${DEPENDENCY}" CACHE INTERNAL "Project Dependencies on other ArmarX Packages")
            set(ARMARX_PROJECT_DEPENDENT_DATA_DIRS "${ARMARX_PROJECT_DEPENDENT_DATA_DIRS}" "${${DEPENDENCY}_DATA_DIR}" CACHE INTERNAL "Collection of all *_DATA_DIR entries of all dependent projects")
            set(ARMARX_PROJECT_DISABLED_DEPENDENT_TARGETS "${ARMARX_PROJECT_DISABLED_DEPENDENT_TARGETS}" "${${DEPENDENCY}_DISABLED_TARGETS}" CACHE INTERNAL "Disabled Targets from dependent ArmarX Packages")
            list(APPEND CMAKE_MODULE_PATH "${${DEPENDENCY}_CMAKE_DIR}")

            foreach(dir ${${DEPENDENCY}_INCLUDE_DIRS})
                string(REGEX MATCHALL ^.*/build/source$ matched "${dir}")
                if(matched)
                    include_directories(SYSTEM ${dir})
                else()
                    include_directories(${dir})
                endif()
            endforeach()

            message(STATUS "${DEPENDENCY}_USE_FILE: ${${DEPENDENCY}_USE_FILE}")
            if(NOT ${DEPENDENCY}_USE_FILE)
                message(FATAL_ERROR "${DEPENDENCY}_USE_FILE is not set!" )
            endif()
            include(${${DEPENDENCY}_USE_FILE})
            # this has to be a string (not a list) so it is understood by doxygen
            set(ARMARX_PROJECT_DEPENDENT_TAG_FILES "${ARMARX_PROJECT_DEPENDENT_TAG_FILES} ${DEPENDENCY}.tag=${${DEPENDENCY}_DOXYGEN_TAG_FILE}")
            if(DEFINED ${DEPENDENCY}_SOURCE_DIR)
                add_external_dependency(${DEPENDENCY})
            endif()
        endif()
    endif()
endmacro()


# This macro is called via depends_on_armarx_package() and allows the rebuilding
# of ArmarX package dependencies with 'make dependencies'
macro(add_external_dependency DEPENDENCY_NAME)
    set(DEPENDENCY_TARGET_NAME "ExternalDependency_${DEPENDENCY_NAME}")
    set(DEPENDENCY_TARGET_SOURCE_DIR "${${DEPENDENCY_NAME}_SOURCE_DIR}")
    set(DEPENDENCY_TARGET_BUILD_DIR "${${DEPENDENCY_NAME}_BUILD_DIR}")

    # add ${DEPENDENCY} as an external build target
    # build the dependencies of ${DEPENDENCY} first, then ${DEPENDENCY} itself
    ExternalProject_Add(${DEPENDENCY_TARGET_NAME}
        PREFIX ${DEPENDENCY_TARGET_SOURCE_DIR}
        SOURCE_DIR ${DEPENDENCY_TARGET_SOURCE_DIR}
        BINARY_DIR ${DEPENDENCY_TARGET_BUILD_DIR}
        TMP_DIR ${DEPENDENCY_TARGET_BUILD_DIR}/cmake-tmp
        STAMP_DIR ${DEPENDENCY_TARGET_BUILD_DIR}/cmake-stamp
        BUILD_IN_SOURCE 0
        BUILD_COMMAND ${CMAKE_BUILD_TOOL} dependencies all
        INSTALL_COMMAND echo "Not installing"
    )

    # This FORCED build step is required
    # without it the dependencies won't be rebuild by cmake
    ExternalProject_Add_Step(${DEPENDENCY_TARGET_NAME} forcebuild
        COMMAND ${CMAKE_COMMAND} -E echo "Force build of ${DEPENDENCY_TARGET_NAME}"
        DEPENDEES configure
        DEPENDERS build
        ALWAYS 1
    )

    # execute this dependency before the collective one
    add_dependencies(dependencies ${DEPENDENCY_TARGET_NAME})

    # exclude the target for external dependencies from the standard execution of 'make'
    set_target_properties(${DEPENDENCY_TARGET_NAME} PROPERTIES EXCLUDE_FROM_ALL TRUE)
endmacro()
