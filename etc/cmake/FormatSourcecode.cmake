# Format all source files according to the ArmarX CodingStyle

# this script should be used in CMake script node (cmake -Dvariable=... -P FormatSourcecode.cmake)
# variables used in here have to be passed as commandline parameters with -Dvariable=...

if(FORCE_ALL_FILES)
    # get all sourcecode files
    file(GLOB_RECURSE UNFORMATTED_FILES RELATIVE ${PROJECT_SOURCECODE_DIR} "*.c" "*.cc" "*.cpp" "*.h" "*.hh" "*.hpp")
else()
    # get changed files via git (--short required to get relative paths)
    execute_process(COMMAND "${GIT_EXECUTABLE}" status --porcelain --short
      WORKING_DIRECTORY "${PROJECT_SOURCECODE_DIR}"
      RESULT_VARIABLE STATUS
      OUTPUT_VARIABLE CHANGED_FILES
      OUTPUT_STRIP_TRAILING_WHITESPACE)

    # create list of files from output of git command where files are separated by newline
    string(REPLACE "\n" ";" CHANGED_FILES_LIST "${CHANGED_FILES}")

    # extract sourcecode files since running astyle on other file types is not healthy
    foreach(CHANGED_FILE ${CHANGED_FILES_LIST})
        # match: git status (modified, added, renamed, copied); at least one character; ending with .c .cc .cpp .h .hh .hpp;
        string(REGEX MATCH "^([MARC][M ]| M|\\?\\?) (.+\\.)(c|cc|cpp|h|hh|hpp)$" CHANGED_FILE "${CHANGED_FILE}")
        if(CHANGED_FILE)
            # remove git status prefix
            string(REGEX REPLACE "^([MARC][M ]| M|\\?\\?) " "" CHANGED_FILE "${CHANGED_FILE}")
            list(APPEND UNFORMATTED_FILES "${CHANGED_FILE}")
        endif()
    endforeach()
endif()

# only run astyle if the list of unformatted files containes at least one entry
list(LENGTH UNFORMATTED_FILES RUN_CODE_FORMAT)
if (RUN_CODE_FORMAT)
    execute_process(COMMAND ${AStyle_EXECUTABLE} --options=${AStyle_OPTIONS_FILE} ${UNFORMATTED_FILES}
        WORKING_DIRECTORY "${PROJECT_SOURCECODE_DIR}")
endif()
