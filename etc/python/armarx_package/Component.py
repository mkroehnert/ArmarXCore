##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from Template import Template
from ComponentDefinition import ComponentDefinition
from Exceptions import MissingFileException
from Exceptions import ComponentNotFoundException
from Exceptions import ComponentRemovedException
from Exceptions import UnmanagedComponentException
import Directory as Dir
import CMakeUtil
from os import path

class Component(ComponentDefinition):

    ##
    # Constructor
    #
    def __init__(self, componentDefinition, name, rootDirectory):
        self._type = componentDefinition._type
        self._directoryDefinitions = componentDefinition._directoryDefinitions
        self._templateDefinitions = componentDefinition._templateDefinitions
        self._dependencies = componentDefinition._dependencies

        self._name = name
        self._rootDirectory = rootDirectory
        self._directories = []
        self._templates = []

        self.templateReplacementStrategy = componentDefinition.templateReplacementStrategy

        for subdirectoryDef in self._directoryDefinitions:
            self._directories.append(Dir.Directory(subdirectoryDef, self._rootDirectory, True))

        for templateDefinition in self.getTemplateDefinitions():
            print templateDefinition.getName()
            self._templates.append(Template(templateDefinition))


    ##
    # Instantiates the component definition by replacing all variables in the
    # definition to specific values
    #
    def bind(self, variables):
        for variable, value in variables.items():
            if self._name is not None: #if only a lib is created
                self._name = self._name.replace(variable, value)

        for subdirectory in self._directories:
            subdirectory.bind(variables)

        for template in self._templates:
            template.compile(variables)


    ##
    # Creates the package component if doesn't exist
    #
    def write(self):
        # check for component existence
        if self.exists():
            pass
            #if not self.removed():
            #    raise ComponentJoinedException(self._name, self.getType())
            #else:
            #    raise ComponentExistsException(self._name,
            #                                   self.getType(),
            #                                   self._rootDirectory.getPath())

        for directory in self._directories:
            directory.makeAll(silent=True)
            CMakeUtil.addToCMake(directory)

        for template in self._templates:
            template.writeToDirectory(self.getRootDirectory(), self.templateReplacementStrategy)

        CMakeUtil.addDependencies(self)


    ##
    # Returns whether the component has been removed from CMake structure
    #
    def removed(self):
        for directory in self._directories:
            if not CMakeUtil.isInCMake(directory):
                if CMakeUtil.isCMakeManaged(directory):
                    return True

        return False


    ##
    # Removes the package component if exists
    #
    def remove(self, forceCompleteRemoval = False):
        # check for component existence
        if not self.exists():
            raise ComponentNotFoundException(self._name,  self.getType())

        if not CMakeUtil.isCMakeManaged(self._directories[0]):
            if not forceCompleteRemoval:
                raise UnmanagedComponentException(self._name, self.getType())

        if self.removed():
            raise ComponentRemovedException(self._name, self.getType())

        for directory in self._directories:
            CMakeUtil.removeFromCMake(directory)

        if forceCompleteRemoval:
            print '%s Deleting component files ...' % u'>'
            for directory in self._directories:
                directory.remove()

            for template in self._templates:
                template.remove(self.getRootDirectory())


    ##
    # Joins the package component if exists
    #
    def join(self):
        # check for component existence
        if not self.exists():
            raise ComponentNotFoundException(self._name,  self.getType())

#        if not self.removed():
#            raise ComponentJoinedException(self._name, self.getType())

        for directory in self._directories:
            if not CMakeUtil.isInCMake(directory):
                CMakeUtil.addToCMake(directory)


    ##
    # Checks the existence of this component.
    # The component exists if the directory exists and the content matches
    # the component description.
    #
    def exists(self):
        for directory in self._directories:
            if not directory.exists(True):
                return False

        if not self._directories:
            return False

        return True


    ##
    # Validates the component by checking the existence of all required
    # component parts.
    #
    def validate(self):
        superclassVerified = False

        if self.exists():
            for directory in self._directories:
                directory.validate(True)

            for template in self._templates:
                if not template.exists(self.getRootDirectory()):
                    print "Warning: Template file {0} is missing".format(path.join(self.getPath(), template.getName()))
                else:
                    template.validate(self.getRootDirectory())
        else:
            raise ComponentNotFoundException(self._name, self._type)


    ##
    # Returns the component directory list
    #
    def getDirectories(self):
        return self._directories


    ##
    # Returns the root directory object
    #
    def getRootDirectory(self):
        return self._rootDirectory


    ##
    # Returns the component name
    #
    def getName(self):
        return self._name
