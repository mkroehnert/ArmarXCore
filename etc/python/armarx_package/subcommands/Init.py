##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2013
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import path
from argparse import RawTextHelpFormatter
from armarx_package.SubCommand import SubCommand
from armarx_package.Structure import Structure
from armarx_package.TemplateReplacementStrategy import TemplateReplacementStrategy

from armarx_package.Manpage import Manpage
import argparse

def SpecialString(v):
    regex = "^([a-zA-Z])([/a-zA-Z_0-9]*)$"
    import re  # Unless you've already imported re previously
    try:
        return re.match(regex, v).group(0)
    except:
        raise argparse.ArgumentTypeError("String '%s' does not match required format - regex: %s"%(v,regex))

class InitSubCmd(SubCommand):

    ##
    # init sub-command constructor
    #
    def __init__(self, parserContainer):

        self.name = 'init'

        self.manpage = Manpage()

        self.manpage.setProgName('armarx-package')
        self.manpage.setName(self.name)
        self.manpage.setDescription('''
This sub-command creates an empty ArmarX package. The created package
will consist of a top-level package directory which in turn will contain
the minimum package configuration. From this point on you may add
package elements of different categories. For more details on package
elements check out the 'add' sub-command.
''')
        self.manpage.setBriefDescription('Create an empty ArmarX package')
        self.manpage.setExamples('''
.B 1.
Creating a package within workspace directory

To create a package, first  go to the location where you want to create
your package and execute e.g. the following:

    $ cd /path/to/workspace
    $ armarx-package init HelloWorldPackage
    $ %s HelloWorldPackage package created

.B 2.
Creating a package from an arbitrary location

    $ armarx-package init HelloWorldPackage --dir=/path/to/workspace
    $ %s HelloWorldPackage package created

''' % (u'>', u'>'))

        self.parser = parserContainer.add_parser(
                                 self.name,
                                 help="Create a new ArmarX package",
                                 formatter_class=RawTextHelpFormatter,
                                 description=self.manpage.getBriefDescription(),
                                 epilog='Checkout \'armarx-package help init\' for more details and examples!')

        self.parser.add_argument("packageName", type=SpecialString, help="ArmarX package Name")

        self.parser.set_defaults(func=self.execute)

        self.addGlobalArguments()

    ##
    #
    #
    def execute(self, args):
        structureDefinitionFilePath = path.join(self.getSkeletonPath(), "package-structure.xml")

        templateReplacementStrategy = TemplateReplacementStrategy.getStrategyForComponentType(args.packageName, "default")

        # create a new package structure object from the definition
        structure = Structure.CreateStructureFromDefinition(structureDefinitionFilePath, args.packageName, args.dir, False, templateReplacementStrategy)

        # write the structure
        try:
            structure.write()
            print self.outputPrefix, args.packageName, "package created."
        except Exception as exc:
            self.handleException(args, exc)

            print self.outputPrefix, "Cannot create package failed."
