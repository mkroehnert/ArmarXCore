##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import path
from datetime import datetime
from StructureDefinition import StructureDefinition
from Directory import Directory
from Exceptions import UndefinedComponentException
from Exceptions import InvalidPackageException
from Exceptions import InvalidComponentException
from Exceptions import MissingDirectoryException
from Exceptions import MissingFileException

import GitUtil

##
# Package structure
#
class Structure(StructureDefinition):

    ##
    # Constructor
    #
    # @param structureDefinitionFile  XML package structure definition file path
    # @param name                     Package name
    # @param location                 Package root directory
    # @param forceFullStructure       Flag indicating whether to force a full
    #                                 structure generation
    #
    def __init__(self, structureDefinitionFile, name, location, forceFullStructure = False, templateReplacementStrategy=None):

        super(Structure, self).__init__(structureDefinitionFile, templateReplacementStrategy=templateReplacementStrategy)

        self._name = name
        self._location = location
        self._forceFullStructure = forceFullStructure
        self._variables = {}

        # create directory tree instance
        self._packageDirectory = Directory(self.getPackageDirectoryDefinition(), None, self.forceFullStructure())

        packageName = path.join(location, self.getPackageDirectoryDefinition().getName())
        self._packageDirectory.setName(packageName)

        useremail = GitUtil.getUserEmail()
        useremail = useremail.replace("@", " at ")
        useremail = useremail.replace(".", " dot ")
        self.defineVariable("AUTHOR_EMAIL", useremail)
        self.defineVariable("AUTHOR_NAME", GitUtil.getUserName())
        self.defineVariable("YEAR", str(datetime.now().year))
        self.defineVariable("PACKAGE_NAME", name)
        self.defineVariable("PACKAGE_LOCATION", location)
        self.defineVariable("SKELETON_PATH", self.getStructureSkeletonPath())


    ##
    # Returns the package name
    #
    def getName(self):
        return self._name


    ##
    # Returns the root directory path of the package
    #
    def getLocation(self):
        return self._location


    ##
    # Determine whether to force a full structure build.
    #
    # Returns true if full build structure has been requested, otherwise
    # false
    #
    def forceFullStructure(self):
        return self._forceFullStructure


    ##
    # Sets the structure build mode
    #
    def setForceFullStructure(self, value):
        self._forceFullStructure = value


    ##
    # Returns the package template variable dictionary
    #
    def getVariables(self):
        return self._variables


    ##
    # Returns the package directory object
    #
    def getPackageDirectory(self):
        return self._packageDirectory


    ##
    # Defines a package template variable
    #
    # @key        Variable key name
    # @value      Variable value
    #
    def defineVariable(self, key, value):
        self._variables.update({"@" + key + "@": value})


    ##
    # Adds a component to the package
    #
    # @param componentType    Creates an instance of component
    # @param name             Component instance name
    #
    def createComponent(self, componentType, componentName, componentPath, libPath, libName):
        component = self.getPackageDirectory().createComponent(componentType, componentName)
        if component:
            if componentName is not None:
                self.defineVariable("COMPONENT_NAME", componentName)
            self.defineVariable("COMPONENT_PATH", componentPath)
            self.defineVariable("LIBRARY_PATH", libPath)
            self.defineVariable("LIBRARY_NAME",libName)
            component.bind(self.getVariables())

            return component
        else:
            raise UndefinedComponentException(componentType)


    ##
    # Returns a existing component from the package
    #
    def getComponent(self, componentType, name):
        component = self.createComponent(componentType, name)

        try:
            component.validate()
        except (MissingDirectoryException, MissingFileException) as exc:
            raise InvalidComponentException(name, componentType, exc.message)

        return component


    ##
    # Scans for components within the package
    #
    def getComponents(self):
        return self.getPackageDirectory().getComponents(self.getVariables())


    ##
    # Binds the directory definitions
    #
    def bind(self):
        self.getPackageDirectory().bind(self.getVariables())


    ##
    # Creates the package content
    #
    def write(self):
        # bind all structure definitions
        self.bind()

        try:
            self.getPackageDirectory().make()
        except:
            raise


    ##
    # Validates the package structure by checking the existence of the minimum
    # package structure
    #
    def validate(self):
        self.bind()

        try:
            self._packageDirectory.validate()
        except Exception, exc:
            raise InvalidPackageException(self._name,
                                          self._packageDirectory.getPath(),
                                          exc.message)


    ##
    # Returns the structure skeleton root path
    #
    def getStructureSkeletonPath(self):
        return path.realpath(path.dirname(self._structureDefinitionFile))


    #===========================================================================
    # Factory methods
    #===========================================================================

    ##
    # Creates a structure object from a definition
    #
    @staticmethod
    def CreateStructureFromDefinition(structureDefinitionFile, name, location, forceFullStructure, templateReplacementStrategy):
        location = path.realpath(location)

        # create the structure
        structure = Structure(structureDefinitionFile, name, location, forceFullStructure, templateReplacementStrategy=templateReplacementStrategy)

        return structure


    ##
    # Creates a structure object from an existing package
    #
    @staticmethod
    def CreateStructureFromPackage(structureDefinitionFile, directoryPath, templateReplacementStrategy):
        directoryPath = path.realpath(directoryPath)

        name = path.basename(directoryPath)
        location = path.dirname(directoryPath)

        # create the structure
        structure = Structure(structureDefinitionFile, name, location, templateReplacementStrategy=templateReplacementStrategy)

        return structure
