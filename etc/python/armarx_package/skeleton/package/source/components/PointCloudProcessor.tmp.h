/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::ArmarXObjects::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_@PACKAGE_NAME@_@COMPONENT_NAME@_H
#define _ARMARX_COMPONENT_@PACKAGE_NAME@_@COMPONENT_NAME@_H




#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

#include <pcl/point_types.h>



namespace armarx
{
        
    typedef pcl::PointXYZRGBA PointT;

    /**
     * @class @COMPONENT_NAME@PropertyDefinitions
     * @brief
     */
    class @COMPONENT_NAME@PropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        @COMPONENT_NAME@PropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ProviderName", "PointCloudProvider", "Name of the point cloud provider.");
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
        }
    };

    /**
     * @defgroup Component-@COMPONENT_NAME@ @COMPONENT_NAME@
     * @ingroup @PACKAGE_NAME@-Components
     * A description of the component @COMPONENT_NAME@.
     * 
     * @class @COMPONENT_NAME@
     * @ingroup Component-@COMPONENT_NAME@
     * @brief Brief description of class @COMPONENT_NAME@.
     * 
     * Detailed description of class @COMPONENT_NAME@.
     */

    class @COMPONENT_NAME@ :
        virtual public visionx::PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "@COMPONENT_NAME@";
        }

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        virtual void onInitPointCloudProcessor();

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        virtual void onConnectPointCloudProcessor();

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        virtual void onDisconnectPointCloudProcessor();

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        virtual void onExitPointCloudProcessor();

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();


    private:

        DebugDrawerInterfacePrx debugDrawer;
        DebugObserverInterfacePrx debugObserver;
    };
}

#endif
