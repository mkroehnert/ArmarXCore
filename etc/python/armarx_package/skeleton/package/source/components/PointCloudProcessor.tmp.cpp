/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::ArmarXObjects::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "@COMPONENT_NAME@.h"


#include <pcl/filters/filter.h>
#include <Eigen/Core>
#include <RobotAPI/libraries/core/Pose.h>

using namespace armarx;



armarx::PropertyDefinitionsPtr @COMPONENT_NAME@::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new @COMPONENT_NAME@PropertyDefinitions(
            getConfigIdentifier()));
}



void armarx::@COMPONENT_NAME@::onInitPointCloudProcessor()
{
    offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
    offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());

    usingPointCloudProvider(getProperty<std::string>("ProviderName").getValue());
}

void armarx::@COMPONENT_NAME@::onConnectPointCloudProcessor()
{

    debugObserver = getTopic<DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());
    debugDrawer = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopicName").getValue());

    enableResultPointClouds<PointT>();
    // add a name to provide more than one result point cloud
    // enableResultPointClouds<PointT>("ResultProviderName");
}


void armarx::@COMPONENT_NAME@::onDisconnectPointCloudProcessor()
{
}

void armarx::@COMPONENT_NAME@::onExitPointCloudProcessor()
{
}

void armarx::@COMPONENT_NAME@::process()
{
    pcl::PointCloud<PointT>::Ptr inputCloud(new pcl::PointCloud<PointT>());

    if (!waitForPointClouds())
    {
        ARMARX_INFO << "Timeout or error while waiting for point cloud data";
        return;
    }
    else
    {
        getPointClouds<PointT>(inputCloud);
    }
    StringVariantBaseMap debugValues;
    debugValues["debug_value"] = new Variant(inputCloud->header.timestamp);
    debugObserver->setDebugChannel(getName(), debugValues);



    provideResultPointClouds<PointT>(inputCloud);

    // provideResultPointClouds<PointT>(inputCloud, "ResultProviderName");

}
