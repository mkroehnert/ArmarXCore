##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
#

from xml.etree import ElementTree as ET

class TemplateDefinition(object):

    ##
    # Constructor
    #
    # @param templateFileNode    XML template file element
    #
    def __init__(self, templateFileNode):
        self._node = templateFileNode
        self._name = self._node.get("name")
        self._superclass = self._node.get("superclass")
        self._templateFile = self._node.text
        self._static = (self._node.get("static") == "yes")
        self._empty = (self._node.get("empty") == "yes")
        self._content = ""

    ##
    # Returns the template XML definition element
    #
    def getTemplateNode(self):
        return self._node


    ##
    # Returns the template destination name
    #
    def getName(self):
        return self._name


    ##
    # Returns the template content
    #
    def getContent(self):
        return self._content

    ##
    # Returns the template file path
    #
    def getTemplateFile(self):
        return self._templateFile

    ##
    # Returns true if the component has a specified superclass which is used
    # to identify its type
    #
    def hasSuperclass(self):
        return self._superclass is not None

    ##
    # Returns the super class name
    #
    def getSuperclass(self):
        return self._superclass
        
    ##
    # Returns whether this file is required by the structure.
    #
    def isRequired(self):
        return self._required

    ##
    # Returns true if the file is defined as static, which means will not be
    # modified once created.
    #
    def isStatic(self):
        return self._static

    ##
    # Creates a directory definition from a specified directory path
    #
    @staticmethod
    def Create(name, tempFile):
        tempNode = ET.Element("template")
        tempNode.set("name", name)
        tempNode.text = tempFile

        return TemplateDefinition(tempNode)
