try:
    import unittest2 as unittest
except ImportError:
    import unittest

from mock import patch, call  # MagicMock, mock_open

from armarx import armarx_version


class TestArmarXVersion(unittest.TestCase):

    def test_format_version(self):
        expected = 'v1.2.3'
        v = armarx_version.Version(1, 2, 3)
        actual = armarx_version.format_version(v)
        self.assertEquals(expected, actual)

    def test_is_newer_version(self):
        old_version = armarx_version.Version(1, 2, 3)
        new_version = armarx_version.Version(1, 3, 0)
        self.assertTrue(new_version > old_version)
        self.assertTrue(new_version == armarx_version.Version(1, 3, 0))
        self.assertFalse(new_version == old_version)
        self.assertFalse(old_version > new_version)

    def test_parse_version(self):
        expected = armarx_version.Version(1, 2, 3)
        actual = armarx_version.parse_version('v1.2.3')
        self.assertEquals(expected, actual)
        actual = armarx_version.parse_version('1.2.3')
        self.assertEquals(expected, actual)

    def test_parse_invalid_version(self):
        self.assertRaises(TypeError, armarx_version.parse_version, '4.2')
        self.assertRaises(TypeError, armarx_version.parse_version, '1.2.3.0')
        self.assertRaises(ValueError, armarx_version.parse_version, '1.2.3b')
        self.assertRaises(ValueError, armarx_version.parse_version, 'foo')

    def test_increase_version(self):
        version = armarx_version.Version(1, 2, 3)
        expected = armarx_version.Version(1, 3, 0)
        actual = armarx_version.increase_version(version, 'minor')
        self.assertEquals(expected, actual)
        actual = armarx_version.increase_version(version, 'major')
        expected = armarx_version.Version(2, 0, 0)
        self.assertEquals(expected, actual)

    """
    def test_read_armarx_version(self):
        m = mock_open()
        with patch('{}.open'.format(armarx_version.__name__), m, create=True):
            actual = armarx_version.read_armarx_version('/tmp/foo')
        m.assert_called_once_with('/tmp/foo/etc/cmake/ArmarXPackageVersion.cmake')
        expected = armarx_version.Version(0, 6, 3)
        self.assertEquals(expected, actual)

    def test_write_armarx_version(self):
        m = mock_open()
        version = armarx_version.Version(1, 2, 3)
        with patch('{}.open'.format(armarx_version.__name__), m, create=True):
            armarx_version.write_armarx_version('/tmp/foo', version)
        m.assert_called_once_with('/tmp/foo/etc/cmake/ArmarXPackageVersion.cmake', 'w')

    """

    @patch('os.listdir')
    def test_find_armarx_pkg_dir(self, list_dir_mock):
        self.assertRaises(ValueError, armarx_version.get_armarx_pkg_dir, 'INVALID_PACAKGE_NAME')

    @patch('armarx.armarx_version.set_version')
    @patch('armarx.armarx_version.read_armarx_version')
    @patch('armarx.armarx_version.get_armarx_pkg_dir')
    def test_bump_version(self, find_dir_mock, read_version_mock, set_version_mock):
        pkg_names = ['PackageA', 'PacakgeB']
        read_version_mock.return_value = armarx_version.Version(1, 2, 3)
        armarx_version.bump_version('major', pkg_names)
        calls = [call('PackageA'), call('PacakgeB')]
        find_dir_mock.assert_has_calls(calls)
        new_version = armarx_version.Version(2, 0, 0)
        set_version_mock.assert_called_once_with(new_version, pkg_names)

    def test_invalid_bump_version(self):
        pass
