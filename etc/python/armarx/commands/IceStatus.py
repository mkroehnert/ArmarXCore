##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

from Command import Command

class IceStatus(Command):

    commandName = "status"

    requiredArgumentCount = 0

    def __init__(self, profile):
        super(IceStatus, self).__init__(profile)


    def execute(self, args):
        serverPerNode = self.iceHelper().getServerPerNode()
        serverPerApp = self.iceHelper().getServerPerApp()
        nodeHostNames = self.iceHelper().getNodeHostnames()
        registryHostnames = self.iceHelper().getRegistries()

        print "Registries:"
        for registry in registryHostnames.keys():
            print "\t" + registry + ": " + registryHostnames[registry]

        print "\nIceGrid Nodes + running servers:"
        for node in serverPerNode.keys():
            print "\t" + node + " (" + nodeHostNames[node] + ")"
            for server in serverPerNode[node]:
                print "\t\t" + server
        print "\nIceGrid applications + associated servers:"
        for app in serverPerApp.keys():
            print "\t" + app
            for server in serverPerApp[app]:
                print "\t\t" + server


    @classmethod
    def getHelpString(cls):
        return "reads and prints information about all running applications and processes"