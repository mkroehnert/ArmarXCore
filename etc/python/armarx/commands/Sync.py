##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

import argparse
import os
import stat
import subprocess
import shlex
import shutil
import sys
import socket

from armarx import Util
from armarx import ArmarXBuilder

from Command import Command, CommandType

def getScriptPath():
    return os.path.dirname(os.path.realpath(sys.argv[0]))

class DefaultConfig:
    Port = "22"
    # IcePatchServerHost = "127.0.0.1"
    DeployHosts = "10.4.1.1,10.4.1.2"
    DeployHostUser = "armar-user"
    username = os.getenv('USER', 'default')
    RemoteSyncDir = os.getenv('ARMARX_REMOTE_SYNC_DIR', "armarx_" + username.strip())
    LocalSyncDir = os.path.join(os.environ.get("HOME"), "sync-armarx")
    ArmarXCoreDefaultDir = os.path.join(os.environ.get('HOME'), "armarx", "Core")
    # RelativeSshKeyPath = os.path.join("etc", "scripts", "armarx-sync", "armarx-receive")
    # RelativeKnownHostsFile = os.path.join("etc", "scripts", "armarx-sync", "known_hosts")



class Sync(Command):

    commandName = "sync"
    commandType = CommandType.Developer

    requiredArgumentCount = 1

    requiresIce = False

    parser = argparse.ArgumentParser(description='ArmarX sync script for synchronizing ArmarX packages with all its dependencies to remote hosts', fromfile_prefix_chars='@')

    def __init__(self, profile):
        super(Sync, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        config = DefaultConfig()
        defaultJobCount = "0"
        # defaultSshKeyPath = os.path.join(config.ArmarXCoreDefaultDir, config.RelativeSshKeyPath)
        # defaultKnownHostsFile = os.path.join(config.ArmarXCoreDefaultDir, config.RelativeKnownHostsFile)

        subParser.add_argument('package', nargs='+').completer = cls.getCompletionPackages
        subParser.add_argument('--port', '-p', help='server port', default=config.Port, dest="port")
        # subParser.add_argument('--host', '-ho', help='IcePatch: server host', default=config.IcePatchServerHost, dest="icepatchHost")
        subParser.add_argument('--deployHosts', '-d', help='remote machines to synchronize to', default=config.DeployHosts, dest="deployHosts")
        subParser.add_argument('--localSyncDir', '-l', help='files are installed in this directory and then synchronized', default=config.LocalSyncDir, dest="localSyncDir")
        subParser.add_argument('--no-delete', '-nd', help='do not delete the localSyncDir content before installing', action="store_true", default=False, dest="noDelete")
        subParser.add_argument('--no-dep', help='only install locally the given package without dependencies. Local install-directory will NOT be wiped before installing.', action="store_true", default=False, dest="noDependency")
        subParser.add_argument('--remoteSyncDir', '-r', help='directory on the remote host to synchronize to (can be set via environment variable ARMARX_REMOTE_SYNC_DIR)', default=config.RemoteSyncDir, dest="remoteSyncDir")
        subParser.add_argument('--username', '-u', help='user for connecting to remote machine', default=config.DeployHostUser, dest="username")
        # subParser.add_argument('--sshKey', '-k', help='IcePatch: path to SSH public key for synchronization', default=defaultSshKeyPath, dest="sshKey")
        # subParser.add_argument('--knownHosts', help='IcePatch: path to knownHosts file used for SSH connections', default=defaultKnownHostsFile, dest="knownHosts")
        subParser.add_argument('--compile', '-c', help='compile project before installing', action="store_true", dest="compile")
        subParser.add_argument('--ninja', '-n', help='Use ninja instead of make', action="store_true", dest="ninja")
        subParser.add_argument('-j', help='Number of cores to use for compilation. Number of cores of cpu is used of omitted.', default=defaultJobCount, dest="jobCount")


    def execute(self, args):
        config = DefaultConfig()
        config.ArmarXCoreDefaultDir = self.configuration.armarxCoreDirectory
        # defaultJobCount = "0"
        # defaultSshKeyPath = os.path.join(config.ArmarXCoreDefaultDir, config.RelativeSshKeyPath)
        # defaultKnownHostsFile = os.path.join(config.ArmarXCoreDefaultDir, config.RelativeKnownHostsFile)

        sync_configuration_file = os.path.join(self.profile.getProfileDirectory(), "armarx-sync.ini")
        if os.path.exists(sync_configuration_file):
            print "Using synchronisation configuration file:  " + sync_configuration_file
            args.insert(0, "@" + sync_configuration_file)


        self.addtoSubArgParser(self.parser)
        arguments = self.parser.parse_args(args)

        patchHostnames = arguments.deployHosts.split(",")

        # create CMake generator
        generator = None
        if arguments.ninja:
            generator = ArmarXBuilder.ArmarXNinjaGenerator(arguments.jobCount)
        else:
            generator = ArmarXBuilder.ArmarXMakeGenerator(arguments.jobCount)

        builder = ArmarXBuilder.ArmarXBuilder(generator, True, True, True)

        if not self.check_parameters(arguments, config, builder, patchHostnames):
            return

        if not arguments.noDelete and not arguments.noDependency:
            print "Deleting content of: " + arguments.localSyncDir
            shutil.rmtree(arguments.localSyncDir)

        self.install_packages_to(arguments.package, arguments.localSyncDir, builder, arguments.compile, arguments.noDependency)

        #syncAction = IcePatchSyncAction(patchHostnames, arguments.username, arguments.localSyncDir, arguments.remoteSyncDir, arguments.port)
        syncAction = RSyncSyncAction(patchHostnames, arguments.username, arguments.localSyncDir, arguments.remoteSyncDir, arguments.port)
        syncAction.sync()


    def check_parameters(self, arguments, config, builder, patchHostnames):
        if 0 >= len(arguments.remoteSyncDir):
            print "Please specify either of the two environment variables: USER, or ARMARX_REMOTE_SYNC_DIR"
            return False

        if 0 >= len(patchHostnames):
            print "Please specify at least one hostname to synchronize to"
            return False

        if not os.path.exists(arguments.localSyncDir):
            try:
                Util.mkdir_p(arguments.localSyncDir)
            except:
                print "Could not create local sync directory: " + arguments.localSyncDir
                return False


        # IcePatch: check for SSH key file
        # if not os.path.exists(arguments.sshKey):
        #     arguments.sshKey = os.path.join(config.ArmarXCoreDefaultDir, config.RelativeSshKeyPath)
        # if not os.path.exists(arguments.sshKey):
        #     arguments.sshKey = os.path.join(getScriptPath(), config.RelativeSshKeyPath)
        # if not os.path.exists(arguments.sshKey):
        #     print "Please specify a correct SSH key location"
        #     return False

        # IcePatch: check permissions of ssh key file to be 400
        # requiredSshKeyMode = stat.S_IRUSR
        # currentSshKeyStats = os.stat(arguments.sshKey)
        # if not stat.S_IMODE(currentSshKeyStats.st_mode) == requiredSshKeyMode:
        #     print "Changing SSH key file permissions to 400: " + arguments.sshKey
        #     try:
        #         os.chmod(arguments.sshKey, requiredSshKeyMode)
        #     except:
        #         print "Could not change access rights to 400: " + arguments.sshKey
        #         return False

        # IcePatch: check for known_hosts file
        # if not os.path.exists(arguments.knownHosts):
        #     arguments.knownHosts = os.path.join(config.ArmarXCoreDefaultDir, config.RelativeKnownHostsFile)
        # if not os.path.exists(arguments.knownHosts):
        #     arguments.knownHosts = os.path.join(getScriptPath(), config.RelativeKnownHostsFile)
        # if not os.path.exists(arguments.knownHosts):
        #     print "Please specify a correct known_hosts file location"
        #     return False

        return True


    def install_packages_to(self, packageList, localSyncDir, builder, compile, noDependency):
        # run CMake and make install on all packages to synchronize
        for inputPackageName in packageList:
            if noDependency:
                allPackages = [(inputPackageName, "")]
            else:
                allPackages = builder.getOrderedDependencyList(inputPackageName)
                builder.printDependencyList(allPackages)

            for packageName, package_path in allPackages:
                print(packageName)
                package = builder.getPackageForName(packageName)
                packageBuilder = package.getCMakeGenerator()

                if packageBuilder is None:
                    print "Package " + packageName + " does not contain Makefiles/Ninja files"
                    continue

                if compile:
                    package.build(packageBuilder, "")
                package.installComponents(localSyncDir)


    @classmethod
    def getHelpString(cls):
        return ""


class IcePatchSyncAction:
    def __init__(self, patchHostnames, username, localDirectory, remoteDirectory, port):
        self.patchHostnames = patchHostnames
        self.username = username
        self.localDirectory = localDirectory
        self.remoteDirectory = remoteDirectory
        self.icePatchHost = None
        self.port = port


    def sync(self):
        if not self.remoteDirectory.startswith("/"):
            self.remoteDirectory = "$HOME/" + self.remoteDirectory

        self.icepatchHost = self.get_icepatch_server_ip()

        if not self.run_icepatch_calc_bocking():
            return

        icepatchServerPID = self.start_icepatch_server_async()

        self.start_icepatch_clients_blocking()

        self.shutdown_icepatch_server(icepatchServerPID)


    def get_icepatch_server_ip(self):
        # try to retrieve the IP address used for communicating with the remote host
        try:
            remoteHostNames = filter(lambda host: host != socket.gethostname(), self.patchHostnames)
            remoteHost = ""
            if 0 < len(remoteHostNames):
                remoteHost = remoteHostNames[0]
            else:
                remoteHost = self.patchHostnames[0]

            print "Figuring out interface by connecting to: " + remoteHost
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect((remoteHost, 22))
            ip = s.getsockname()[0]
            s.close()
            print "IP: " + ip
        except:
            ip = raw_input("Enter your local ip address for communication with the remote host:")
        return ip


    def run_icepatch_calc_bocking(self):
        # run icepatch2calc
        print "\nCalculating checksums for files to synchronize" + self.localDirectory
        if not os.path.exists(self.localDirectory):
            print "Sync directory does not exist: " + self.localDirectory
            return False
        else:
            icepatchCalcCommand = "icepatch2calc " + self.localDirectory
            exitCode = os.system(icepatchCalcCommand)
            if 0 != exitCode:
                print "Failed to run: " + icepatchCalcCommand
                return False
        return True


    def start_icepatch_server_async(self):
        # run icepatch2server
        icepatchServerCommand = "icepatch2server --IcePatch2.Endpoints=\"tcp -h " + self.icepatchHost + " -p " + self.port + "\" --IcePatch2.Directory=" + self.localDirectory
        print "Executing: " + icepatchServerCommand
        icepatchServerPID = subprocess.Popen(shlex.split(icepatchServerCommand), stdout=sys.stdout, stderr=sys.stderr).pid
        print "IcePatch2Server PID: " + str(icepatchServerPID)
        return icepatchServerPID


    def start_icepatch_clients_blocking(self):
        # run the icepatch2clients on remote hosts
        patchclientPIDs = []
        for client in self.patchHostnames:
            # command is handled via armarx-receive.sh and requires 3 parameters
            icepatchClientCommand = "ssh -n " + self.username + "@" + client
            icepatchClientCommand += " mkdir -p " + self.remoteDirectory + " && "
            icepatchClientCommand += "icepatch2client -t --IcePatch2Client.Proxy=\\\"IcePatch2/server:tcp -h " + self.icepatchHost + " -p " + self.port + "\\\" " + self.remoteDirectory
            print"Starting IcePatch2Client: " + icepatchClientCommand
            icepatchClientProcess = subprocess.Popen(shlex.split(icepatchClientCommand), stdout=sys.stdout, stderr=sys.stderr)
            print "IcePatch2Client PIDs: " + str(icepatchClientProcess.pid)
            patchclientPIDs.append(icepatchClientProcess.pid)

        # wait for icepatch2clients to finish synchronizing
        for pid in patchclientPIDs:
            print "Waiting for IcePatch2 client to finish: " + str(pid)
            os.waitpid(pid, 0)


    def shutdown_icepatch_server(self, icepatchServerPID):
        # terminate the icepatch2server
        print "Shutting down IcePatch2Server: " + str(icepatchServerPID)
        os.kill(icepatchServerPID, 15)



class RSyncSyncAction:
    def __init__(self, patchHostnames, username, localDirectory, remoteDirectory, port):
        self.patchHostnames = patchHostnames
        self.username = username
        self.localDirectory = localDirectory
        self.remoteDirectory = remoteDirectory
        self.port = port


    def sync(self):
        # "/" must be appended, otherwise the directory instead of the directory contents gets synchronized
        if not self.localDirectory[-1] == "/":
            self.localDirectory += "/"
        if not self.remoteDirectory[-1] == "/":
            self.remoteDirectory += "/"

        rsyncPIDs = []
        for client in self.patchHostnames:
            remoteDestination = self.username + "@" + client + ":" + self.remoteDirectory
            rsyncCommand = ["rsync", "--recursive", "--links", "--executability", "--times", "-D", "--delete", "--compress", "--human-readable", "--progress", "--verbose", self.localDirectory, remoteDestination]
            print"Starting RSync: " + str(rsyncCommand)
            rsyncProcess = subprocess.Popen(rsyncCommand, stdout=sys.stdout, stderr=sys.stderr)
            print "RSync PIDs: " + str(rsyncProcess.pid)
            rsyncPIDs.append(rsyncProcess.pid)

        # wait for RSync to finish synchronizing
        for pid in rsyncPIDs:
            print "Waiting for RSync to finish: " + str(pid)
            os.waitpid(pid, 0)

        print ""
        print "Finished synchronizing"
        print "    from: " + self.localDirectory
        print "    to:   " + self.remoteDirectory
