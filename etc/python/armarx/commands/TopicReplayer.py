import argparse
import os

from armarx.ArmarXBuilder import ArmarXBuilder

from Command import Command

import ConfigParser


class Config(Command):

    commandName = 'replay'
    requiredArgumentCount = 1
    requiresIce = True

    parser = argparse.ArgumentParser(description='ArmarX Config')

    def __init__(self, profile):
        super(Config, self).__init__(profile)

        self.config = ConfigParser.ConfigParser()

    @classmethod
    def addtoSubArgParser(cls, sub_parser):
        sub_parser.add_argument('-l', '--loop', action='store_true', help='loop')
        sub_parser.add_argument('bag_file')

    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        self.builder = ArmarXBuilder(None, False, False, False, False)
        package_data = self.builder.getArmarXPackageData('ArmarXCore')
        binary_dir = self.builder.getArmarXPackageDataValue(package_data, 'BINARY_DIR')

        cmd = [os.path.join(binary_dir[0], 'TopicReplayerAppRun')]

        if args.loop:
            cmd.append('--ArmarX.TopicReplayer.Loop')
        cmd.append('--ArmarX.TopicReplayer.RecordFile={}'.format(args.bag_file))

        os.system(' '.join(cmd))

    @classmethod
    def getHelpString(cls):
        return 'replay bag files'
