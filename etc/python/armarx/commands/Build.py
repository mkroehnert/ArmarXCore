##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @author     Michael Bechtel (Michael dot Bechtel at kit dot edu)
# @author     Mirko Waechter (waechter at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

import argparse
from armarx import ArmarXBuilder


from Command import Command, CommandType


class Build(Command):

    commandName = "build"
    commandType = CommandType.Developer

    requiredArgumentCount = 0

    requiresIce = False

    parser = argparse.ArgumentParser(description='ArmarX build command for building ArmarX packages with all its dependencies easily')


    def __init__(self, profile):
        super(Build, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('--nodefaultpath', help='Do not use default paths for the packages and always ask the user for the path in case of unknown packages', action="store_true", dest="nodefaultpath")
        subParser.add_argument('package', nargs='*', default=['Armar3', 'Armar4']).completer = cls.getCompletionPackages
        subParser.add_argument('-j', help='Number of cores to use for compilation. Number of cores of cpu is used of omitted or 0.', default="0", dest="jobCount")
        subParser.add_argument('--no-deps', '-nd', help='Execute command only for the given package and NOT its dependencies', action="store_true", dest="no_deps")
        subParser.add_argument('--pull', '-p', help='Git pull all dependencies and target project before compilation', action="store_true", dest="pull")
        subParser.add_argument('--stashnpull', '-sp', help='Git stash&&pull&&pop all dependencies and target project before compilation', action="store_true", dest="stashnpull")
        subParser.add_argument('--cmake', '-c', help='Force cmake to run before compilation', action="store_true", dest="cmake")
        subParser.add_argument('--ninja', '-n', help='Use ninja instead of make', action="store_true", dest="ninja")
        subParser.add_argument('--clean', help='Clean build dir before compiling', action="store_true", dest="clean")



    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        if args.ninja:
            generator = ArmarXBuilder.ArmarXNinjaGenerator(args.jobCount)
        else:
            generator = ArmarXBuilder.ArmarXMakeGenerator(args.jobCount)

        builder = ArmarXBuilder.ArmarXBuilder(generator, args.cmake, args.pull, args.stashnpull, args.nodefaultpath, args.clean)

        packages = args.package
        for package in packages:
            if not builder.buildPackageWithDependencies(package, args.no_deps):
                return False
            else:
                self.configuration.addUsedPackageName(package)
        return True

    @classmethod
    def getHelpString(cls):
        return "build specified ArmarX package and all the packages it depends on."
