##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

from Command import Command, CommandType
import armarx.ArmarXBuilder as ArmarXBuilder
import argparse
class PackageLocate(Command):

    commandName = "locatePackage"
    commandType = CommandType.Developer
    requiredArgumentCount = 1

    requiresIce = False

    parser = argparse.ArgumentParser(description='Locate an ArmarX package and prints the available cmake variables')


    def __init__(self, profile):
        super(PackageLocate, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('package', help='ArmarX package to search for').completer = cls.getCompletionPackages
        subParser.add_argument('--json', help='Prints the packacke information in JSON format', action="store_true", dest="json")



    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        packagename = args.package

        generator = ArmarXBuilder.ArmarXMakeGenerator(0)
        builder = ArmarXBuilder.ArmarXBuilder(generator, True, True, True, False)
    
        package_data = builder.getArmarXPackageData(packagename)
        if not package_data:
            print("ArmarX package <" + packagename + "> not found")
            return
    
        base_directories = builder.getArmarXPackageDataValue(package_data, "PACKAGE_CONFIG_DIR")

        if not base_directories:
            print("ArmarX package <" + packagename + "> not found")
            return

        entries = ["PACKAGE_CONFIG_DIR", "PACKAGE_VERSION","PROJECT_BASE_DIR", "BUILD_DIR", "INCLUDE_DIRS", "LIBRARY_DIRS", "LIBRARIES", "CMAKE_DIR", "BINARY_DIR", "SCENARIOS_DIR", "DATA_DIR", "EXECUTABLE", "INTERFACE_DIRS",  "DEPENDENCIES", "SOURCE_PACKAGE_DEPENDENCIES", "PACKAGE_DEPENDENCY_PATHS"]


        if args.json:
            import json
            values = {}
            for entry in entries:
                packageResult = builder.getArmarXPackageDataValue(package_data, entry)
                if len(packageResult) > 0:
                    values[entry] = packageResult
                else:
                    values[entry] = []
            print json.dumps({packagename :values}, sort_keys=True, indent=4, separators=(',', ': '))
        else:

            print("\nArmarX Package <" + packagename + ">\n")
            for variable in entries:
                print("\t" + variable + " : " + str(builder.getArmarXPackageDataValue(package_data, variable)) + "\n")

        self.configuration.addUsedPackageName(packagename)


    @classmethod
    def getHelpString(cls):
        return "Locate an ArmarX package and print meta information to the shell (location, data directories, ...)"
