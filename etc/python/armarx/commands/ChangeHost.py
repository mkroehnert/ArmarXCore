##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Mirko Waechter (waechter at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
import os
import subprocess

__author__ = 'waechter'


from tempfile import mkstemp
from shutil import move
from os import fdopen, remove



from Command import Command, CommandType
import argparse
from armarx import ArmarXBuilder

class ExecCommand(Command):
    helpString = "Allows to change the host address and ip of the IceGrid"

    commandName = "changeHost"
    # commandType = CommandType.Developer
    requiresIce = False

    requiredArgumentCount = 0
    parser = argparse.ArgumentParser(description=helpString)
    def __init__(self, profile):
        super(ExecCommand, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('--host', help='Host address of the IceGrid Registry', dest="host", required=True)
        subParser.add_argument('--port', help='Port of IceGrid Registry', dest="port", required=True)
        subParser.add_argument('--if', help='Ip of own interface to use (Sets Ice.Default.Host property)', dest="if_ip")


    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        self.configuration.armarxUserConfigurationDirectory
        configPath = self.configuration.getIceGridConfigForProfile(self.configuration.profilename)
        print("Editing config: " + configPath)
        if not os.path.exists(configPath):
            raise ValueError("The path " + configPath + " does not exist")
            return 1
        if len(configPath) == 0:
            raise ValueError("Configpath is empty!?")
            return 1
        port = int(args.port)
        if port <= 1024:
            raise ValueError("Port must be bigger than 1024 - given value: " + str(port))
            return 1
        foundDefaultHost = False
        # Create temp file
        fh, abs_path = mkstemp()
        with fdopen(fh, 'w') as new_file:
            with open(configPath) as old_file:
                for line in old_file:
                    if "Ice.Default.Locator" in line:
                        new_file.write("Ice.Default.Locator=IceGrid/Locator:tcp -p {} -h {}\n".format(port, args.host))
                    elif "IceGrid.Registry.Client.Endpoints" in line:
                        new_file.write("IceGrid.Registry.Client.Endpoints=tcp -p {}\n".format(port))
                    elif args.if_ip is not None and "Ice.Default.Host" in line:
                        foundDefaultHost = True
                        new_file.write("Ice.Default.Host={}\n".format(args.if_ip))
                    else:
                        new_file.write(line)

        if args.if_ip is not None and not foundDefaultHost:
            new_file.write("Ice.Default.Host={}".format(args.if_ip))

        # Remove original file
        remove(configPath)
        # Move new file
        move(abs_path, configPath)



    @classmethod
    def getHelpString(cls):
        return cls.helpString
