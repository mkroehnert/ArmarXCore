##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License


__author__ = 'kroehner'


from time import sleep
import traceback
import IceStorm

try:
    import Ice
    import IceGrid
    IceAvailable = True
except:
    IceAvailable = False


class IceHelper(object):
    def __init__(self, iceGridRegistry, iceCommunicator, username, password):
        self.iceCommunicator = iceCommunicator
        self.iceGridRegistry = iceGridRegistry
        self.iceGridUsername = username
        self.iceGridPassword = password



    def getTopicManager(self):
        obj = self.iceCommunicator.stringToProxy('IceStorm/TopicManager')
        return IceStorm.TopicManagerPrx.checkedCast(obj)


    def getAdminSession(self):
        return self.iceGridRegistry.createAdminSession(self.iceGridUsername, self.iceGridPassword)


    def getApplicationList(self):
        applicationList = []
        try:
            adminSession = self.getAdminSession()
            admin = adminSession.getAdmin()
            applicationList = admin.getAllApplicationNames()
        except:
            pass
        return applicationList


    def removeApplications(self, applicationNames):
        try:
            adminSession = self.getAdminSession()
            admin = adminSession.getAdmin()
            asyncRemoveResult = []
            deployedApplications = admin.getAllApplicationNames()
            for applicationName in applicationNames:
                if applicationName in deployedApplications:
                    print "Removing ArmarX scenario: " + applicationName
                    asyncRemoveResult.append(admin.begin_removeApplication(applicationName))

            for asyncResult in asyncRemoveResult:
                admin.end_removeApplication(asyncResult)
        except:
            pass


    def getRegistries(self):
        registryHostnames = dict()
        try:
            adminSession = self.getAdminSession()
            admin = adminSession.getAdmin()
            for registryName in admin.getAllRegistryNames():
                registryHostnames[registryName] = admin.getRegistryInfo(registryName).hostname
        except:
            pass
        return registryHostnames

    def getNodeHostnames(self):
        nodeHostNames = dict()
        try:
            adminSession = self.getAdminSession()
            admin = adminSession.getAdmin()
            nodeNames = admin.getAllNodeNames()
            for node in nodeNames:
                nodeHostNames[node] = admin.getNodeHostname(node)
        except:
            pass
        return nodeHostNames


    def getServerPerNode(self):
        serverPerNode = dict()
        try:
            adminSession = self.getAdminSession()
            admin = adminSession.getAdmin()
            nodeNames = admin.getAllNodeNames()
            serverIds = admin.getAllServerIds()
            for node in nodeNames:
                serverPerNode[node] = []
            for serverId in serverIds:
                serverInfo = admin.getServerInfo(serverId)
                serverState = admin.getServerState(serverId)
                serverPid = admin.getServerPid(serverId)
                serverString = serverId + " (" + str(serverState) + ", PID: " + str(serverPid) + ")"
                serverPerNode[serverInfo.node].append(serverString)
        except:
            pass
        return serverPerNode


    def getServerPerApp(self):
        serverPerApp = dict()
        try:
            adminSession = self.getAdminSession()
            admin = adminSession.getAdmin()
            serverIds = admin.getAllServerIds()
            applicationList = admin.getAllApplicationNames()
            for app in applicationList:
                serverPerApp[app] = []
            for serverId in serverIds:
                serverInfo = admin.getServerInfo(serverId)
                serverState = admin.getServerState(serverId)
                serverPid = admin.getServerPid(serverId)
                serverString = serverId + " (" + str(serverState) + ", PID: " + str(serverPid) + ")"
                serverPerApp[serverInfo.application].append(serverString)
        except:
            pass
        return serverPerApp


    def sendSignalToApplications(self, applicationNames, signalString):
        try:
            adminSession = self.getAdminSession()
            admin = adminSession.getAdmin()
            serverIds = admin.getAllServerIds()
            for serverId in serverIds:
                serverInfo = admin.getServerInfo(serverId)
                if serverInfo.application in applicationNames:
                    admin.sendSignal(serverInfo.descriptor.id, signalString)
        except Exception as e:
            print e
            pass


    def iceGridShutdown(self):
        adminSession = self.getAdminSession()
        admin = adminSession.getAdmin()
        asyncShutdownResult = []
        nodesNames = admin.getAllNodeNames()
        for nodeName in nodesNames:
            print "stopping icegridnode: " + nodeName
            asyncShutdownResult.append(admin.begin_shutdownNode(nodeName))

        # test if node is still reachable
        for nodeName in nodesNames:
            try:
                i = 0
                while(admin.pingNode(nodeName) and i < 100):
                    i += 1
                    sleep(0.05)
            except:
                pass

    @classmethod
    def killIce(cls):
        try:
            import psutil
            iceProcessNames = ["icegridnode", "icebox", "icegridadmin"]
            for procName in iceProcessNames:
                for proc in psutil.process_iter():
                    if proc.name == procName:
                        proc.kill()
        except:
            print "killIce command not available  since psutil module is missing"


    def isIceGridNodeRunning(self, nodeName):
        """
        try to contact the icegridnode by name and return True if it is already up and running
        :param nodeName: name of the icegridnode to contact
        :return: True if node is running, false otherwise
        """
        if not self.iceCommunicator:
            return False
        if not self.iceGridRegistry:
            return False
        try:
            adminSession = self.getAdminSession()
            admin = adminSession.getAdmin()
            if not admin.pingNode(nodeName):
                return False
        except:
            print "\nException while contacting IceGridNode: " + nodeName
            traceback.print_exc()
            print ""
            return False
        return True


    def isIceGridRunning(self, configuration):
        """
        check if IceGrid is running by querying communicator and registry from the configuration object first
        try to contact the IceGrid registry node if both proxies are valid
        :param configuration:
        :return:
        """
        return  self.isIceGridNodeRunning(configuration.iceGridRegistryNodeName)


    def iceShutdown(self):
        """
        shutdown Ice connection by destroying the communicator
        :param iceCommunicator:
        :return:
        """
        if self.iceCommunicator:
            try:
                self.iceCommunicator.destroy()
            except:
                traceback.print_exc()


    @classmethod
    def Initialize(cls, iceGridConfigurationFile, iceGridRegistryName, iceGridUsername, iceGridPassword):
        """
        initialize an Ice connection by trying to create an Ice.Communicator and contacting the IceGrid registry
        if both fail set iceCommunicator and iceRegistry to None
        :param configuration:
        :return:
        """

        iceCommunicator = None
        iceGridRegistry = None
        iceHelper = None
        try:
            #print "Ice Config: " + iceGridConfigurationFile
            iceCommunicator = Ice.initialize(["--Ice.Config=" + iceGridConfigurationFile])
            iceRegistry = iceCommunicator.stringToProxy(iceGridRegistryName);
            iceGridRegistry = IceGrid.RegistryPrx.checkedCast(iceRegistry)
        except Ice.ConnectFailedException, e:
            print "Exception while initializing Ice:\n", e
            iceCommunicator = None
            iceGridRegistry = None

        iceHelper = cls(iceGridRegistry, iceCommunicator, iceGridUsername, iceGridPassword)

        return iceHelper

class IceHelperConfig:
    def __init__(self, iceGridConfigurationFile, iceGridRegistryName, iceGridUsername, iceGridPassword):
        self.iceGridConfigurationFile = iceGridConfigurationFile
        self.iceGridRegistryName = iceGridRegistryName
        self.iceGridUsername = iceGridUsername
        self.iceGridPassword = iceGridPassword

    def createIceHelper(self):
        return IceHelper.Initialize(self.iceGridConfigurationFile, self.iceGridRegistryName, self.iceGridUsername, self.iceGridPassword)
