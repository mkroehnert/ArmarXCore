##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
import ConfigParser

__author__ = 'kroehner'

import os

class Configuration:
    if os.getenv("ARMARX_USER_CONFIG_DIR"):
        armarxUserConfigurationDirectory = os.getenv("ARMARX_USER_CONFIG_DIR")
    else:
        armarxUserConfigurationDirectory = os.path.join(os.getenv("HOME"), ".armarx")
    # make sure $HOME/.armarx exists
    if not os.path.exists(armarxUserConfigurationDirectory):
        os.makedirs(armarxUserConfigurationDirectory, 0700)
    armarx_ini = os.path.join(armarxUserConfigurationDirectory, "armarx.ini")
    armarx_profiles_directory = os.path.join(armarxUserConfigurationDirectory, "profiles")
    armarxCoreDirectory = None
    armarxCoreConfigurationDirectory = None
    profilename = "default"
    iceGridUsername = "user"
    iceGridPassword = "password"
    iceGridRegistryNodeName = "NodeMain"
    iceGridRegistryName = "IceGrid/Registry"
    iceGridNodeDirectory = os.path.join(armarxUserConfigurationDirectory, "icegridnode_data")
    iceGridDefaultConfigurationFilename = "default.cfg"
    iceGridGeneratedDefaultConfigurationFilename = "default.generated.cfg"
    iceGridDefaultConfigurationFile = os.path.join(armarxUserConfigurationDirectory, iceGridDefaultConfigurationFilename) + "," + os.path.join(armarxUserConfigurationDirectory, iceGridGeneratedDefaultConfigurationFilename)
    iceGridSyncVariablesFile = "icegrid-sync-variables.icegrid.xml"


    @classmethod
    def getIceGridConfigForProfile(cls, profilename):
        return os.path.join(cls.armarx_profiles_directory, profilename, cls.iceGridDefaultConfigurationFilename)

    @classmethod
    def getIceGridGeneratedConfigForProfile(cls, profilename):
        return os.path.join(cls.armarx_profiles_directory, profilename, cls.iceGridGeneratedDefaultConfigurationFilename)

    @classmethod
    def setArmarXCoreDir(cls, coreDir, buildDir, includeDirs, configDir):
        cls.armarxCoreDirectory = coreDir
        cls.armarxCoreConfigurationDirectory = configDir
        cls.armarxCoreIncludeDirectories = includeDirs

    @classmethod
    def getUsedPackageNames(self):
        Config = ConfigParser.ConfigParser()

        Config.read(self.armarx_ini)

        if not Config.has_section('AutoCompletion'):
            return []

        return Config.get("AutoCompletion", "Packages").split(",")

    @classmethod
    def addUsedPackageName(self, packageName):
        Config = ConfigParser.ConfigParser()

        Config.read(self.armarx_ini)
        packageNames = []

        if not Config.has_section('AutoCompletion'):
            Config.add_section('AutoCompletion')

        if Config.has_option("AutoCompletion", "Packages"):
            packageNames = Config.get("AutoCompletion", "Packages").split(",")
            if not packageName in packageNames:
                packageNames.append(packageName)
        else:
            packageNames = [packageName]
        Config.set("AutoCompletion", "Packages", ",".join(packageNames))

        with open(self.armarx_ini,'wb') as cfgfile:
            Config.write(cfgfile)



