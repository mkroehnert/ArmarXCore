##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Mirko Waechter (mirko dot waechter at kit dot edu)
# @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
# @date       2014, 2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

try:
    import readline
    readline_available = True
except:
    readline_available = False

import glob, subprocess, re, os, sys, multiprocessing, errno
from os import listdir
from os.path import isfile, join, islink
import time
import select
import getpass

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    BOLD = '\033[1m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

class ArmarXPackage:
    def __init__(self, packageName, sourceDir, buildDir):
        self.packageName = packageName
        self.sourceDir = sourceDir
        self.buildDir = buildDir


    def runCMake(self, arguments):
        if len(self.sourceDir) > 0:
            self.sourceDir = os.path.abspath(self.sourceDir)
        if len(self.buildDir) > 0:
            self.buildDir = os.path.abspath(self.buildDir)

        cmd = "cd " + self.buildDir + " && cmake " + self.sourceDir
        cmd += arguments
        print cmd
        return os.system(cmd)

    def installComponents(self, installDirectory, cmake_components=["binaries", "libraries", "headers", "scenarios", "config", "data", "cmake", "templates"]):
        if len(self.sourceDir) > 0:
            self.sourceDir = os.path.abspath(self.sourceDir)
        if len(self.buildDir) > 0:
            self.buildDir = os.path.abspath(self.buildDir)

        # http://stackoverflow.com/questions/9190098/for-cmakes-install-command-what-can-the-component-argument-do
        # cmake -DCOMPONENT=comp_one -P {your_build_dir}/cmake_install.cmake
        for cmake_component in cmake_components:
            cmd = "cd " + self.buildDir + " && cmake -DCMAKE_INSTALL_PREFIX=" + installDirectory + " -DCOMPONENT=" + cmake_component + " -P cmake_install.cmake"
            print cmd
            os.system(cmd)

    # takes a subclass of ArmarXCMakeBuilder and additional arguments
    # and runs ArmarXCMakeBuilder.build(buildDir, arguments)
    def build(self, builder, arguments):
        return builder.build(self.buildDir, arguments)

    # detect build system of this package and return a matching ArmarXCMakeGenerator
    # returns None if no generator has been run
    def getCMakeGenerator(self):
        if os.path.exists(self.buildDir + "/build.ninja"):
            return ArmarXNinjaGenerator(0)
        elif os.path.exists(self.buildDir + "/Makefile"):
            return ArmarXMakeGenerator(0)
        else:
            return None

    def requiresCMakeRun(self):
        return not os.path.isfile(self.buildDir + "/CMakeCache.txt")

    def printString(self):
        print self.sourceDir
        print self.buildDir


class ArmarXCMakeGenerator:
    def __init__(self, jobCount):
        self.jobCount = jobCount
        self.buildTool = "cmake --build . --"
        self.cmakeBuilderString = ""

    def getJobCount(self):
        jobCount = int(self.jobCount);
        if isinstance(jobCount,int):
            if jobCount > 0:
                return jobCount
        return multiprocessing.cpu_count()

    def build(self, buildDir, arguments):
        if len(self.buildTool) <= 0:
            return -1
        if len(self.cmakeBuilderString) <= 0:
            return -1
        cores = self.getJobCount()
        # ionice reduces the priority of the build process
        cmd = "cd " + buildDir + " && nice ionice -c 2 -n 7 " + self.buildTool + " -j" + str(cores) + " " + arguments
        print cmd
        return cmd


class ArmarXNinjaGenerator(ArmarXCMakeGenerator):
    def __init__(self, jobCount):
        ArmarXCMakeGenerator.__init__(self,jobCount)
        self.jobCount = jobCount
        self.cmakeBuilderString = " -G Ninja "


class ArmarXMakeGenerator(ArmarXCMakeGenerator):
    def __init__(self, jobCount):
        ArmarXCMakeGenerator.__init__(self,jobCount)
        self.jobCount = jobCount
        self.cmakeBuilderString = " -G \"Unix Makefiles\" "



class ArmarXBuilder:
    def __init__(self, generator, executeCMake, gitPull, gitStashNPull, nodefaultpath = False, cleanBuildDir = False):
        self.generator = generator
        self.executeCMake = executeCMake
        self.gitPull = gitPull
        self.gitStashNPull = gitStashNPull
        self.dependencyNotFound = False
        self.nodefaultpath = nodefaultpath
        self.cleanBuildDir = cleanBuildDir
        self.corePath = None
        self.packageData = {}

    def getPackageForName(self, packageName, packagePath = ""):
        list = self.getArmarXPackageData(packageName, packagePath)

        if len(list) == 0:
            package = self.promptPackageForName(packageName)
            print packageName + " not found"
            self.dependencyNotFound = True
            package.printString()
            ret = self.runCMake(package)

            if ret != 0:
               raise "Cmake returned with error code " + `ret`
            list = self.getArmarXPackageData(packageName)

        if len(self.getArmarXPackageDataValue(list, "PROJECT_BASE_DIR")) == 0:
            self.runCMake(package)
        project_base_dir_list = self.getArmarXPackageDataValue(list, "PROJECT_BASE_DIR")
        if len(project_base_dir_list) == 0:
            raise NameError("Could not find package " + packageName + " correctly!")
        buildDir = self.getArmarXPackageDataValue(list, "BUILD_DIR")[0]
        baseDir = project_base_dir_list[0]
        return ArmarXPackage(packageName, baseDir, buildDir)

    def runCMake(self, armarxPackage):
        return armarxPackage.runCMake(self.generator.cmakeBuilderString)


    def promptPackageForName(self, packageName, useDefaultPath=True):
        def complete(text, state):
                str = (glob.glob(text+'*')+[None])[state]
                return str


        defaultText = './' + packageName
        if readline_available:
            readline.set_startup_hook(lambda: readline.insert_text(defaultText))
            readline.set_completer_delims(' \t\n;')
            readline.parse_and_bind("tab: complete")
            readline.set_completer(complete)
        # search for missing package in the current dir and its subfolders
        source = None
        if not self.nodefaultpath:
            def checkForProjectInDir(dir, packageName):
                '''This function checks whether the given one of the following directories
                contains a CMakeLists.txt for the given project: dir, dir/projectName, dir/.projectName'''
                pattern = re.compile(r'^[^#]*armarx_project[ \t]*\("?' + packageName + r'"?\).*')
                for p in [ dir + "/" + packageName, dir + "/." + packageName, dir ]:
                    cmakeFile = p + "/CMakeLists.txt"
                    if os.path.exists(cmakeFile):
                        with open(cmakeFile) as f:
                            for line in f:
                                if pattern.match(line) is not None:
                                    print("Found Package " + packageName + " in " + p)
                                    return p
                return None
            print
            source = checkForProjectInDir(".", packageName)
            if source is None:
                for subdir in os.listdir("."):
                    source = checkForProjectInDir(subdir, packageName)
                    if source is not None:
                        break
        if not source:
            source = raw_input("Please enter the path to " + packageName +":")
            while not os.path.exists(source + "/CMakeLists.txt") and not os.path.exists(source + "/" + packageName +"Config.cmake"):
                print "CMakeLists.txt does not exist in " + os.path.abspath(source) + "!"
                source = raw_input('source? ')

        if not self.nodefaultpath:
            build = source + "/build/"
        else:
            if readline_available:
                readline.set_startup_hook(lambda: readline.insert_text(source + "/build/"))

            build = raw_input("Please enter the path to the desired build directory of " + packageName +":")
        mkdir_p(build)

        source = os.path.abspath(source)

        return ArmarXPackage(packageName,source, build)


    def gitContainsChanges(self, gitDir):
        cmd = ["cd " + gitDir + " && git status --porcelain"]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        out, err = p.communicate()
        # print out
        out = out.strip("\n")
        list = out.split("\n")
        exp = re.compile("^ (?P<value>[MADRCU]{1}) ")

        for str in list:
            m = exp.search(str)
            # print str
            if m is not None:
                return True
        return False

    def getArmarXCmakeTempDir(self):

        tmpdir = "/tmp"
        username = getpass.getuser()
        if len(username) > 0:
            tmpdir += "/armarxcmake-"+ username
        if not os.path.exists(tmpdir):
            os.mkdir(tmpdir)
        return tmpdir

    def findPackage(self, packageName):
        cmd = ["cmake", "--find-package", "-DNAME=" +packageName, "-DLANGUAGE=CXX",  "-DCOMPILER_ID=GNU", "-DMODE=COMPILE"]


        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.getArmarXCmakeTempDir())

        out, err = p.communicate()
        if len(err) > 0:
            print err
        out = out.strip()
        if out.find(packageName + " not found.") != -1:
            print "Could not find " + packageName + " with CMake."
            return
        list = out.split("-I")
        newList = []
        list[:] = [x.strip() for x in list]
        for l in list:
            if len(l) > 0:
                newList.append(l)
        return newList

    def getArmarXPackageData(self, packageName, packagePath = ""):
        if packageName in self.packageData and packagePath in self.packageData[packageName] and len(self.packageData[packageName][packagePath]) > 0:
            return self.packageData[packageName][packagePath]
        coreSourcePath = self.getCoreSourcePath()

        scriptPath = coreSourcePath + "/ArmarXCore/core/system/cmake/FindPackageX.cmake"
        if not packagePath:
            cmd = ["cmake", "-DPACKAGE=" + packageName, "-P", scriptPath]
        else:
            cmd = ["cmake", "-DPACKAGE=" + packageName, "-DPACKAGEBUILDPATH=" + packagePath, "-P", scriptPath]

        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.getArmarXCmakeTempDir())
        out, err = p.communicate()

        out = out.strip()
        out = out.strip("\n")
        list = out.split("\n")
        if len(err) > 0:
            print out
            print err

        newList = []
        for l in list:
            if len(l) > 0:
                newList.append(l)
        if self.packageData.has_key(packageName):
            self.packageData[packageName][packagePath] = newList
        else:
            self.packageData[packageName] = {}
            self.packageData[packageName][packagePath] = newList
        return newList

    @classmethod
    def GetArmarXPackageVariable(cls, packageName, variableName, packagePath = None):
        if len(packageName) == 0:
            raise ValueError("PackageName must not be empty!")
        if len(variableName) == 0:
            raise ValueError("variableName must not be empty!")
        builder = ArmarXBuilder(None,None,None,None)
        data = builder.getArmarXPackageData(packageName, packagePath)
        if len(data) == 0:
            raise ValueError("Package " + packageName + " could not be found!")
        variableContent = builder.getArmarXPackageDataValue(data, variableName)
        if len(variableContent) == 0:
            raise ValueError("Variable '" + variableName + "' could not be found in Package " + packageName + "!")
        return variableContent

    @classmethod
    def GetArmarXPackageDataFile(cls, packageName, relativePath, packagePath = None):
        dataDir = cls.GetArmarXPackageVariable(packageName, "DATA_DIR", packagePath)[0]
        if not os.path.exists(dataDir):
            raise ValueError("Data directory of package '" + packageName + "' does not exist (path '" + dataDir + "')")
        filepath = os.path.join(dataDir, relativePath)
        if os.path.exists(filepath):
            return filepath
        return None

    def getCoreSourcePath(self):
        if self.corePath is not None:
            return self.corePath
        corePaths = self.findPackage("ArmarXCore")
        if corePaths is None:
            package = self.promptPackageForName("ArmarXCore")
            ret = self.runCMake(package)
            if ret != 0:
                print "ArmarXCore CMake failed"
                sys.exit(1)
            self.corePath = self.findPackage("ArmarXCore")[0]
        else:
            self.corePath = corePaths[0]
        return self.corePath


    def getArmarXPackageDataValue(self, packageData, key):
        for s in packageData:
            exp = re.compile("\\-\\- (?P<name>[a-zA-Z0-9_]+):(?P<value>.+)")
            m = exp.search(s)
            if m != None:
                if m.group('name') == key:
                    values = m.group('value').split(";")
                    if len(values) > 0:
                        return values
        return []
        #raise NameError("Could not find the value named " + key + " in package data - Available fields: " + `packageData`)


    def getDepGraph(self, packageName, graph, absPackagePath = ""):
        if packageName in graph:
            return graph
        packagePath = absPackagePath
        list = self.getArmarXPackageData(packageName)
        initiallyFound = True
        if len(list) == 0:
            self.dependencyNotFound = True
            initiallyFound = False
            print "Package " + packageName + " not found with CMake!"
            package = self.promptPackageForName(packageName)
            package.printString()
            self.runCMake(package)

            list = self.getArmarXPackageData(packageName)

        deps = self.getArmarXPackageDataValue(list, "SOURCE_PACKAGE_DEPENDENCIES")
        if len(packagePath) == 0:
            packagePathData = self.getArmarXPackageDataValue(list, "PACKAGE_CONFIG_DIR")
            if packagePathData and len(packagePathData) > 0:
                packagePath = packagePathData[0]

        if deps != None:
            depsPathsRaw = self.getArmarXPackageDataValue(list, "PACKAGE_DEPENDENCY_PATHS")
            depsPaths = {}
            if depsPathsRaw:
                for dep in depsPathsRaw:
                    packageNameAndPath = dep.split(":")
                    if packageNameAndPath[0] in deps:
                        if len(packageNameAndPath[1]) > 0:
                                    depsPaths[packageNameAndPath[0]] = packageNameAndPath[1]
                        else: # package has not been cmaked yet, path will be determined later
                            depsPaths[packageNameAndPath[0]] = ""
            else:
                for dep in deps:
                    depsPaths[dep] = ""
            # deps = set(depsPaths)
            graph[packageName] = (packagePath, depsPaths)
            for dep in deps:
                if dep in depsPaths:
                    path = depsPaths[dep]
                else:
                    path = ""
                graph = self.getDepGraph(dep, graph, path)
        else:
            graph[packageName] = (packagePath,{})
        if not initiallyFound:
            self.runCMake(package)
        return graph

    def getOrderedDependencyList(self, packageName):
        graph = {}
        graph = self.getDepGraph(packageName, graph)
        orderedList = []
        while len(graph) > 0 :
            nextPack = ""
            for name, value in graph.iteritems():
                if len(value[1]) == 0: # if len==0 -> this package has no dependencies which are not yet in the list
                    nextPack = name
                    nextPath = value[0]
                    break
            if nextPack == "":
                print "Graph: ", graph
                print "Could not find a package without a dependency - do you maybe have a cycle in your dependencies?"
                return


            list = self.getArmarXPackageData(nextPack)

            if len(list) == 0:
                package = self.promptPackageForName(nextPack)
                package.printString()
                ret = self.runCMake(package)

                if ret != 0:
                    print bcolors.FAIL + "Could not cmake package: " + nextPack + bcolors.ENDC

                self.getArmarXPackageData(nextPack)

            for name, value in graph.iteritems():
                if nextPack in value[1]:
                    #value.remove(nextPack)
                    del value[1][nextPack]
                    graph[name] = value
            del graph[nextPack]
            orderedList.append((nextPack, nextPath))


        return orderedList


    def printDependencyList(self, dependenciesList):
        buildOrderString = ""
        i = 1

        for packageName in dependenciesList:
            buildOrderString += packageName[0]
            if i < len(dependenciesList):
                buildOrderString += " -> "
            i += 1
        print bcolors.BOLD + "Dependency order: " + buildOrderString + bcolors.ENDC


    def buildPackageWithDependencies(self, packageName, skip_dependencies = False):
        if "/" in packageName:
            raise NameError("The package name must not contain a slash.")

        if not skip_dependencies:
            orderedList = self.getOrderedDependencyList(packageName)
            self.printDependencyList(orderedList)

            if self.dependencyNotFound: # atleast one dependency was not known yet -> maybe wrong dependency orderedList -> run cmake again
                self.dependencyNotFound = False
                orderedList = self.getOrderedDependencyList(packageName)
        else:
            list = self.getArmarXPackageData(packageName)
            packagePathData = self.getArmarXPackageDataValue(list, "PACKAGE_CONFIG_DIR")
            if packagePathData and len(packagePathData) >0:
                orderedList = [((packageName, packagePathData[0]))]


        for (packageName, packagePath)  in orderedList:
            packageData = self.getArmarXPackageData(packageName)
            baseDir = self.getArmarXPackageDataValue(packageData, "PROJECT_BASE_DIR")

            if len(packageData) == 0:
                raise NameError("The package " + packageName + " could not be located by cmake")

            if baseDir and len(baseDir) > 0:
                baseDir = baseDir[0]
            else:
                raise NameError("The package " + packageName + " does not have a value named PROJECT_BASE_DIR\nAvailable fields: " + `packageData`)
            if self.gitPull:
                cmd = "cd " + baseDir + " && git pull"
                print cmd
                ret = os.system(cmd)
                if ret != 0:
                    print bcolors.FAIL +  "ERROR: Pull of " + packageName + " failed!" + bcolors.ENDC
                    return False
            elif self.gitStashNPull:
                if self.gitContainsChanges(baseDir):
                    cmd = "cd " + baseDir + " && git stash && git pull && git stash pop"
                else:
                    cmd = "cd " + baseDir + " && git pull"
                print cmd
                ret = os.system(cmd)
                if ret != 0:
                    print bcolors.FAIL +  "ERROR: Pull of " + packageName + " failed!" + bcolors.ENDC
                    return False



        for (nextPack, nextPackPath) in orderedList:
            print bcolors.BOLD + "\nPackage that will be build now is: " + nextPack + bcolors.ENDC
            if nextPack == "":
                print "No package name set - aborting"
                return False

            package = self.getPackageForName(nextPack, nextPackPath)

            if self.executeCMake or package.requiresCMakeRun():
                ret = self.runCMake(package)
                if ret != 0:
                    print bcolors.FAIL + "ERROR: CMake of " + nextPack + " failed!" + bcolors.ENDC
                    return False

            ret = self.build(package, nextPack)
            if ret is not None:
                return ret
        return True

    def build(self, package, packageName, retrying = False):

        if self.cleanBuildDir and not retrying:
            targets = " clean "
            cmd = package.build(self.generator, targets)
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            out, err = p.communicate()
            if len(out) > 0:
               print out
            ret = p.returncode
            if ret != 0:
                print bcolors.FAIL + "ERROR: Cleaning of " + packageName + " failed!" + bcolors.ENDC
                return False

        cmd = package.build(self.generator, "")
        p = subprocess.Popen("script -qfec '" + cmd + "' /dev/null", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        expUndefRef = re.compile("undefined reference to `(?P<value>.+)'")
        expFileTruncated = re.compile(R"(?P<file>.+): file not recognized: File truncated")

        foundUndefinedReference = False
        blackList = ['typeinfo for ', 'VVT for ', 'vtable for ']

        def checkLineUndefRef(line, foundUndefinedReference):
            m = expUndefRef.search(line)
            # check if output contains undefined reference errors
            if m != None:
                value = m.group('value')
                for item in blackList:
                    value = value.replace(item, "")
                print bcolors.FAIL + "ERROR: undefined reference: " + value  + bcolors.ENDC
                ret = self.searchSymbolInPackageTree(packageName, value)

                if ret[0]:
                    foundUndefinedReference = True
                    if len(ret[1]) > 0:
                        print ret[1]
            return foundUndefinedReference
        def checkLineFileTruncated(line, alreadyFound):
            m = expFileTruncated.search(line)
            # check if output contains file truncated errors
            if m != None:
                alreadyFound = alreadyFound + [m.group('file')]
            return alreadyFound

        truncatedFiles = []
        while p.poll() is None:
            read, write, error = select.select([p.stdout, p.stderr], [], [p.stdout, p.stderr], 60)

            for r in read:
                line = r.readline()
                #print(line) #print line to console - otherwise user does not see the output
                sys.stdout.write(line)
                foundUndefinedReference = checkLineUndefRef(line, foundUndefinedReference)
                truncatedFiles = checkLineFileTruncated(line, truncatedFiles)

            for r in error:
                line = r.readline()
                #print(line) #print line to console - otherwise user does not see the output
                sys.stderr.write(line)
                foundUndefinedReference = checkLineUndefRef(line, foundUndefinedReference)
                truncatedFiles = checkLineFileTruncated(line, truncatedFiles)

        out, err = p.communicate()
        if len(out) > 0:
            foundUndefinedReference = checkLineUndefRef(out, foundUndefinedReference)
            truncatedFiles = checkLineFileTruncated(out, truncatedFiles)
        if len(err) > 0:
            foundUndefinedReference = checkLine(err, foundUndefinedReference)
            truncatedFiles = checkLineFileTruncated(err, truncatedFiles)
        if foundUndefinedReference:
            print bcolors.FAIL + bcolors.BOLD + "It seems that you have encountered an undefined reference to an ArmarX symbol - above you can find hints on which library to link in your CMakeLists.txt" + bcolors.ENDC
        if len(truncatedFiles) != 0:
            print bcolors.OKBLUE + "Removing truncated files!" + bcolors.ENDC
            for root, dirs, files in os.walk(package.buildDir):
                for f in files:
                    for relative in truncatedFiles:
                        absPath = root+"/"+f
                        if absPath.endswith(relative):
                            print bcolors.OKBLUE + "    " + absPath + bcolors.ENDC
                            os.remove(absPath)

        ret = p.returncode
        if ret != 0:
            if len(truncatedFiles) != 0 and not foundUndefinedReference and not retrying:
                print bcolors.OKBLUE + "Deleted some truncated files! try rebuilding." + bcolors.ENDC
                return self.build(package, packageName, retrying = True)
            print bcolors.FAIL + "ERROR: Build of " + packageName + " failed!" + bcolors.ENDC
            return False

    def searchSymbolInPackageTree(self, packageName, symbolName):
        dependencies = self.getOrderedDependencyList(packageName)
        print "Searching in the following projects (None path means the path is determined later): " + `dependencies`
        resultStr = ""
        resultValue = False
        for dep in dependencies:
            ret = self.searchSymbolInPackage(dep[0], symbolName)
            if ret[0]:
                resultStr += ret[1]
                resultValue = True
        return (resultValue, resultStr)

    def searchSymbolInPackage(self, packageName, symbolName):
        if len(symbolName) == 0:
            print "No symbol name given"
            return
        symbolName = symbolName.replace("*", "\\*")

        packageData = self.getArmarXPackageData(packageName)
        lib_dir = self.getArmarXPackageDataValue(packageData, "LIBRARY_DIRS")[0]
        found = False
        allresults = ""
        print "Searching now in package " + packageName
        startTime = time.time()
        printDelay = 0.1 # seconds
        if lib_dir is None:
            allresults = "Could not read LIBRARY_DIRS from cmake data!"
        else:
            packageLibFiles = [ join(lib_dir, f) for f in listdir(lib_dir)
                                if isfile(join(lib_dir,f)) and not islink(join(lib_dir,f)) ] # get all real library files (no symlinks)
            libIndex = 0
            length = 0
            for lib in packageLibFiles:
                if time.time() - startTime > printDelay:
                    outstr = `libIndex` + "/" + `len(packageLibFiles)` + ": Checking library " + lib
                    sys.stdout.write('\r' + ' ' * length)
                    length = len(outstr)
                    sys.stdout.write('\r' + outstr)
                    sys.stdout.flush() # important
                    startTime = time.time()

                cmd = ["nm -g --defined-only -C " + lib + " | grep \"" + symbolName +"\""]
                # print cmd
                p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                out, err = p.communicate()
                if len(err) > 0:
                    print err
                out = out.strip("\n")
                list = out.split("\n")
                i = 1
                results = ""
                for line in list:
                    if len(line) > 0:

                        symbolContent = line.split(" ", 2)
                        if len(symbolContent) >= 3:
                            # only symbols of Type T interest us
                            if symbolContent[1] == "T":
                                results += "#" + `i` + ": " + symbolContent[2].replace(symbolName, bcolors.BOLD + symbolName + bcolors.ENDC) + "\n"
                            i+=1
                if len(results) > 0:
                    allresults += "Found " + `len(list)` + " matching symbol(s) in package " + bcolors.BOLD + packageName + bcolors.ENDC  +" in file " + bcolors.BOLD + lib + bcolors.ENDC + "\n" + results + "\n"
                    found = True
                libIndex+= 1
        print
        return (found, allresults)


def getArmarXDefaultPackages(prefix, **kwargs):
    return ['Armar3', 'Armar4', 'ArmarXCore', 'ArmarXGui', 'RobotAPI', 'VisionX', 'MemoryX', 'ArmarXSimulation',
            'RobotSkillTemplates', 'RobotComponents', 'SpeechX']
